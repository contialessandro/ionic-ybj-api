FROM node:14.15.1-alpine3.11 AS base
EXPOSE 8080
ENV DEFAULT_TZ='Europe/London'
RUN apk add --no-cache \
    tini \
    curl \
    tzdata \
    && cp /usr/share/zoneinfo/${DEFAULT_TZ} /etc/localtime \
    && apk del tzdata \
    && rm -rf \
    /var/cache/apk/*
ENV NODE_ENV=production
WORKDIR /app
COPY package*.json ./
RUN npm config list \
  && npm ci --only=production \
  && npm cache clean --force
ENTRYPOINT ["/sbin/tini","--"]

FROM base AS dev
ENV NODE_ENV=development
RUN npm config list \
  && npm install
ENV PATH=/app/node_modules/.bin:$PATH
CMD ["nodemon"]

FROM dev AS build
COPY . .
RUN ls -la
RUN tsc

FROM base AS prod
COPY --from=build /app/www/ ./www
RUN chown -R node:node /app
RUN ls -la /app/www
USER node
HEALTHCHECK --interval=1m --timeout=10s --start-period=30s --retries=3 CMD curl -f http://localhost:8080/healthz || exit 1
CMD ["node", "./www/app.js"]
