import express from 'express';
import {ThreadsServices} from '../services/threads.services';

export class ThreadsMiddleware {
    private static instance: ThreadsMiddleware;

    static getInstance() {
        if (!ThreadsMiddleware.instance) {
            ThreadsMiddleware.instance = new ThreadsMiddleware();
        }
        return ThreadsMiddleware.instance;
    }

    validateRequiredCreateMediaBodyFields(req: express.Request, res: express.Response, next: express.NextFunction) {
        if (req.body && req.body.type && req.body.name && req.body.color && req.body.thread_meta_id) {
            next();
        } else {
            res.status(400).send({error: `Missing required fields name and type`});
        }
    }

    async validateSameMediaDoesntExist(req: express.Request, res: express.Response, next: express.NextFunction) {
        const mediaService = ThreadsServices.getInstance();
        const media = await mediaService.getThreadByName(req.body.name);
        if (media) {
            res.status(400).send({error: `media name already exists`});
        } else {
            next();
        }
    }

    async validateMediaExists(req: express.Request, res: express.Response, next: express.NextFunction) {
        const mediaService = ThreadsServices.getInstance();
        const media = await mediaService.readById(req.params.categoryId);
        if (media) {
            next();
        } else {
            res.status(404).send({error: `Media ${req.params.userId} not found`});
        }
    }

    async extractUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        req.body._id = req.params.userId;
        next();
    }
}
