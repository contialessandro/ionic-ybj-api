import {CRUD} from '../../common/interfaces/crud.interface';
import {GenericInMemoryDao} from '../daos/in.memory.dao';
import {ThreadsDao} from '../daos/threads.dao';

export class ThreadsServices implements CRUD {
    private static instance: ThreadsServices;
    dao: GenericInMemoryDao;

    constructor() {
        this.dao = GenericInMemoryDao.getInstance();
    }

    static getInstance(): ThreadsServices {
        if (!ThreadsServices.instance) {
            ThreadsServices.instance = new ThreadsServices();
        }
        return ThreadsServices.instance;
    }

    async create(resource: any) {
        return await ThreadsDao.getInstance().addThread(resource);
    }

    deleteById(resourceId: any) {
        return ThreadsDao.getInstance().removeThreadById(resourceId);
    }

    list(limit: number, page: number) {
        return ThreadsDao.getInstance().listThread(limit, page);
    }

    getHomePageThreads() {
        return ThreadsDao.getInstance().getHomePageThreads();
    }

    patchById(resource: any) {
        return ThreadsDao.getInstance().patchThread(resource);
    }

    readById(resourceId: any) {
        return ThreadsDao.getInstance().getThreadById(resourceId);
    }

    readBy_Id(resourceId: any) {
        return ThreadsDao.getInstance().getThreadBy_Id(resourceId);
    }
    readByMetaId(resourceMetaId: any) {
        return ThreadsDao.getInstance().getThreadByMetaId(resourceMetaId);
    }
    updateById(resource: any) {
        return ThreadsDao.getInstance().patchThread(resource);
    }

    async getThreadByName(email: string) {
        return ThreadsDao.getInstance().getThreadByName(email);
    }

    async getThreadBySlug(slug: string) {
        return ThreadsDao.getInstance().getThreadBySlug(slug);
    }

    async getThreadByTopic(topic: string) {
        return ThreadsDao.getInstance().getThreadBySlug(topic);
    }
}
