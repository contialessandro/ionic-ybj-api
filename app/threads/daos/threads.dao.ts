import {MongooseService} from '../../common/services/mongoose.service';
import * as shortUUID from "short-uuid";
import {ThreadModel} from "../../json-schemas/models.schemas";

export class ThreadsDao {
    mongooseService: MongooseService = MongooseService.getInstance();
    private static instance: ThreadsDao;

    Schema = this.mongooseService.getMongoose().Schema;

    Thread = ThreadModel;
    constructor() {
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance = new ThreadsDao();
        }
        return this.instance;
    }

    async addThread(mediaFields: any) {
        mediaFields._id = shortUUID.generate();
        const media = new this.Thread(mediaFields);
        await media.save();
        return media;
    }

    async getThreadByName(name: string) {
        return this.Thread.findOne({name: name});
    }
    async getThreadBySlug(slug: string) {
        return this.Thread.find({'slug': slug}).limit(1);
    }

    async removeThreadById(userId: string) {
        await this.Thread.deleteOne({_id: userId});
    }

    async getThreadById(userId: string) {
        return this.Thread.findOne({id: +userId});
    }
    async getThreadByMetaId(metaid: string) {
        return this.Thread.findOne({meta_id: +metaid});
    }
    async getThreadBy_Id(id: string) {
        return this.Thread.findOne({_id: id});
    }


    async listThread(limit: number = 25, page: number = 0) {
        return this.Thread.find()
            .limit(250)
            .skip(limit * page)
            .exec();
    }

    async getHomePageThreads() {
        const threads = this.Thread.find()
            .limit(9)
            .sort({'published_at': 'desc'})
            .exec();
        return threads;
    }

    async patchThread(threadFields: any) {
        const thread: any = await this.Thread.findById(threadFields._id);
        if (thread) {
            for (const i in threadFields) {
                thread[i] = threadFields[i];
            }
            await thread.save();
        }
        return thread;
    }

    async getThreadByTopic(topic: string) {

    }
}
