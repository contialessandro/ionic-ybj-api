import express from 'express';
import {ThreadsServices} from '../services/threads.services';
import {CategoryService} from "../../categories/services/category.services";
import {PostsServices} from "../../posts/services/posts.services";

export class ThreadsController {
    protected threadsService: ThreadsServices;

    constructor() {
        this.threadsService = ThreadsServices.getInstance();
    }

    async listThread(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        const treads = await threadsService.list(100, 0);
        res.status(200).send(treads);
    }

    async getHomePageThreads(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        const treads = await threadsService.getHomePageThreads();
        res.status(200).send(treads);
    }

    async getThreadById(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        const user = await threadsService.readById(req.params.id);
        res.status(200).send(user);
    }

    async getThreadByMetaId(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        const user = await threadsService.readByMetaId(req.params.id);
        res.status(200).send(user);
    }

    async createThread(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        const thread = await threadsService.create(req.body);
        res.status(201).send(thread);
    }

    async patch(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        await threadsService.patchById(req.body);
        res.status(204).send(``);
    }

    async put(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();

        await threadsService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeThread(req: express.Request, res: express.Response) {
        const threadsService = ThreadsServices.getInstance();
        await threadsService.deleteById(req.params.mediaId);
        res.status(204).send(``);
    }

    async getThreadBySlug(req: express.Request, res: express.Response) {

        const PostService = PostsServices.getInstance();
        const posts = await PostService.getPostBySlug(req.params.slug);

        res.status(200).send(posts);
    }

    async getPostByLocation(req: express.Request, res: express.Response) {

        const PostService = PostsServices.getInstance();
        const posts = await PostService.getPostByLocation(req.params.location);
        res.status(200).send(posts);
    }

    async getThreadsByCategory(req: express.Request, res: express.Response) {
        try {
            const categoryName: any = req.params.topic;
            const categoryService = CategoryService.getInstance();
            const category: any = await categoryService.getCategoryByName(categoryName);
            if (category) {
                const PostService = PostsServices.getInstance();
                const posts = await PostService.getPostsByCategory(categoryName);
                return res.status(200).send(posts);
            }
        } catch (e) {
            return res.status(500).send({
                e
            });

        }
    }


//        try {
//             const categoryName: any = req.params.topic;
//             const categoryService = CategoryService.getInstance();
//             const category: any = await categoryService.getCategoryByName(categoryName);
//             if (category) {
//                 const threadMetaService = ThreadsMetaServices.getInstance();
//                 const threadMetas = await threadMetaService.getThreadMetasByCategory(category._id) as any;
//
//                 const homepageResources: any = [];
//
//                 for (const meta of threadMetas) {
//                     const threadService = ThreadsServices.getInstance();
//                     const thread: any = await threadService.readBy_Id(meta.thread_id);
//                     if (thread) {
//                         console.log({a: threadMetas});
//                         const imagePath = 'https://ybjlaravel.s3.eu-west-2.amazonaws.com/' + meta.image_id?.id + '/' + meta.image_id?.file_name;
//
//                         const obj = {
//                             id: thread?.id,
//                             label: meta?.category_id?.name ?? '',
//                             link: meta?.category_id?.name ?? '',
//                             location: meta?.location_id?.name ?? '',
//                             title: thread.title ?? '',
//                             image: imagePath,
//                             image1: imagePath,
//                             image2: imagePath,
//                             createdAt: thread.published_at,
//                             intro: thread.intro,
//                             alt: meta.category_id?.type,
//                             color: meta.category_id?.color,
//                             body: thread.body,
//                             slug: thread.slug,
//                         };
//                         homepageResources.push(obj);
//                     }
//                 }
//                 return res.status(200).send(homepageResources);
//             }
//         } catch (e) {
//             return res.status(500).send({
//                 message: e.message, line: e.line
//             });
//
//         }


    // const threadsService = ThreadsServices.getInstance();
    // const threads: any = await threadsService.getThreadBySlug(req.params.slug);
    // let obj = {};
    // if (threads) {
    //     const thread = threads[0]
    //     const threadMetaService = ThreadsMetaServices.getInstance();
    //     const metas = await threadMetaService.getThreadMetasById(thread.meta_id) as any;
    //     let imagePath;
    //     const meta = metas[0];
    //     console.log({thread, meta});
    //     if (thread.meta_id) {
    //         console.log(thread.meta_id);
    //     }
    //     if (meta) {
    //         imagePath = 'https://ybjlaravel.s3.eu-west-2.amazonaws.com/' + meta.image_id?.id + '/' + meta.image_id?.file_name;
    //     }
    //     obj = {
    //         id: thread?.id,
    //         label: meta?.category_id?.name ?? '',
    //         link: meta?.category_id?.name ?? '',
    //         location: meta?.location_id?.name ?? '',
    //         title: thread.title ?? '',
    //         image: imagePath,
    //         image1: imagePath,
    //         image2: imagePath,
    //         createdAt: thread.published_at,
    //         intro: thread.intro,
    //         alt: meta.category_id?.type,
    //         color: meta.category_id?.color,
    //         body: thread.body,
    //         slug: thread.slug,
    //     };
    // }
    // res.status(200).send([obj]);
}
