// import {CommonPermissionMiddleware} from '../common/middlewares/common.permission.middleware';
// import {JwtMiddleware} from '../auth/middlewares/jwt.middleware';
// import {ThreadsMiddleware} from "./middlewares/threads.middleware";
import {Router} from 'express'
import {ThreadsController} from "./controllers/threads.controller";

const router: Router = Router();
const threadController = new ThreadsController();
// const mediaMiddleware = ThreadsMiddleware.getInstance();
// const jwtMiddleware = JwtMiddleware.getInstance();
// const commonPermissionMiddleware = new CommonPermissionMiddleware();
router.get(`/thread`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    threadController.listThread
]);
router.post(`/thread`, [
    threadController.createThread
]);
router.patch(`/thread`, [
    threadController.patch
]);
router.get(`/thread/topic/:topic`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    threadController.getThreadsByCategory
]);
router.get(`/thread/slug/:slug`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    threadController.getThreadBySlug
]);
router.get(`/thread/location/:location`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    threadController.getPostByLocation
]);
router.get(`/thread/homePage`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    threadController.getHomePageThreads
]);
router.get(`/thread/:id`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    threadController.getThreadById
]);
router.get(`/thread/:metaid`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    threadController.getThreadByMetaId
]);
export const ThreadRouter: Router = router;
