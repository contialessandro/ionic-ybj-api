// import {CommonPermissionMiddleware} from '../common/middlewares/common.permission.middleware';
// import {JwtMiddleware} from '../auth/middlewares/jwt.middleware';
// import {PostsMiddleware} from './middlewares/posts.middleware';
import express, {Router} from 'express'
import {PostsController} from './controllers/posts.controller';
import {VerifyJWT} from '../config/Auth';
import {config} from "../config/config";
import {spotify} from "../common/services/spotify.service";
import {PostsServices} from "./services/posts.services";
import {PollySynth} from "../aws/PollyToS3";
import {decodeEntity} from 'html-entities';


const router: Router = Router();
const postsController = new PostsController();
// const mediaMiddleware = PostsMiddleware.getInstance();
// const jwtMiddleware = JwtMiddleware.getInstance();
// const commonPermissionMiddleware = new CommonPermissionMiddleware();
router.get(`/posts`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    postsController.listPost
]);
router.patch(`posts`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    postsController.patchById
]);
router.patch(`/admin/posts/`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    postsController.patch
]);
router.get(`posts/:postId`, async (req: express.Request, res: express.Response) => {
    try {
        const post = await postsController.getThreadByUniqueId;

        return res.status(200).send({
            post
        });
    } catch (e) {
        return res.status(500).send({message: e});
    }
});
router.get(`/HomePagePosts`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    postsController.getHomePagePosts
]);
router.post(`/admin/posts/:postId`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    postsController.patch
]);
router.post(`/admin/posts`, [
    VerifyJWT,
    postsController.CreatePost
]);
router.get('/admin/next_publish_date', [
    postsController.getNextPublishDate
]);
router.post('/admin/saveAudio', (req: express.Request, res: express.Response) => {
    try {
        // PollySynth.asyncAudioGenerator(req.body);
        return res.status(200).send({
            message: 'done',
            path: `${config.s3BucketBaseUrl}/${req.body.post_id}/${req.body.filename}.mp3`
        });
    } catch (e) {
        return res.status(500).send({message: e});
    }
});
router.post('/generateLegacyAudioFiles', async (req: express.Request, res: express.Response) => {
    const postsService = PostsServices.getInstance();
    const payloads = req.body as [];
    try {
        for (const postId of payloads) {

            const post: any = await postsService.readBy_Id(postId);

            if (post) {
                let cleanUpText = post.body.toString().replace(/<\/?[^>]+(>|$)/gi, "");
                cleanUpText = cleanUpText.toString().replace('&nbsp;', "");
                cleanUpText = cleanUpText.replace("&nbsp;", "").trim();
                while (cleanUpText !== decodeEntity(cleanUpText)) {
                    cleanUpText = decodeEntity(cleanUpText);
                }
                const AudioRequestInterface = {
                    body: cleanUpText,
                    slug: post.slug,
                    id: postId
                }
                // console.log({cleanu: post.body, emtytyclean: cleanUpText});

                const task = await PollySynth.startAsyncAudioGenerator(AudioRequestInterface);
                if (task) {
                    post.audio_task_id = task.TaskId;
                    await postsService.createAudioTask({
                        task_id: task.TaskId,
                        slug: post.slug,
                        status: task.TaskStatus
                    });
                }
                await postsService.patchById(post)
                console.log(post.audio_task_id);
            }
        }
    } catch (e) {
        console.log({
            message: (e as any).message, line: (e as any).line
        });
        return res.status(500).send({
            message: (e as any).message, line: (e as any).line
        });
    }
    return res.status(200).send({postAudio: payloads});

});
router.post('/admin/getPodcastForPost', async (req: express.Request, res: express.Response) => {
    try {
        const podcastTitle = `podcast-feed/${req.body.title}.mp3`
        const episode = await spotify.FindEpisodeByTitle(podcastTitle).then(r => r);
        return res.status(200).send({
            episode
        });
    } catch (e) {
        return res.status(500).send({message: e});
    }
});
router.get('/admin/saveAudios', [
    postsController.saveAudioFileforLegacy

]);
export const PostRouter: Router = router;
