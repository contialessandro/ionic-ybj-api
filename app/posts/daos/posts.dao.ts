import {MongooseService} from '../../common/services/mongoose.service';
import * as shortUUID from 'short-uuid';
import {AudioTaskModel, PostModel} from '../../json-schemas/models.schemas';
import mongoose from "mongoose";

export class PostsDao {

    private static instance: PostsDao;
    mongooseService: MongooseService = MongooseService.getInstance();

    Schema = this.mongooseService.getMongoose().Schema;

    Post = PostModel;

    AudioTask = AudioTaskModel;
    constructor() {
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance=new PostsDao();
        }
        return this.instance;
    }

    async addPost(post: any) {
        post._id = shortUUID.generate().toString();
        console.log(post);
        const media=new this.Post(post);
        await media.save();
        return media;
    }


    async getThreadByName(name: string) {
        return this.Post.findOne({name: name});
    }

    async getThreadBySlug(slug: string) {
        return this.Post.find({'slug': slug}).limit(1);
    }

    async removeThreadById(userId: string) {
        await this.Post.deleteOne({_id: userId});
    }

    async getThreadById(userId: string) {
        return this.Post.findOne({id: +userId});
    }

    async getThreadsAfterId(startingId: string) {
        // @ts-ignore
        const obj = await this.getThreadById(startingId.toString());

        if (obj) {
            if (obj && obj._id && obj._id instanceof mongoose.Types.ObjectId) {
                obj._id = obj._id.toString();
            }
            return obj;
        }
    }

    async getThreadByMetaId(metaid: string) {
        return this.Post.findOne({meta_id: +metaid});
    }

    async getThreadBy_Id(id: string) {
        return this.Post.findOne({_id: id});
    }


    async listPost(limit: number=25, page: number=0) {
        return this.Post.find().exec();
    }

    async listHomepagePost() {
        return this.Post.find({'hide': false}).limit(25)
            .sort({'published_at': -1}).exec();
    }

    async getPostsByCategory(categoryName: string) {
        return this.Post.find({category: categoryName, 'hide': false}).sort({'published_at': -1}).exec();
    }

    async getPostBySlug(slug: string) {
        return this.Post.find({'slug': slug}).limit(1);
    }

    async getPostByLocation(location: string) {
        return this.Post.find({location: location, 'hide': false}).sort({'published_at': -1}).exec();
    }

    async getHomePageThreads() {
        return this.Post.find({'hide': false})
            .limit(9)
            .sort({'published_at': -1})
            .exec();
    }

    async patchThread(threadFields: any) {
        const updatedDocument = await this.Post.findByIdAndUpdate(
            threadFields._id,
            {$set: threadFields},
            {new: true, runValidators: true}
        );
        console.log({
            publish_at: threadFields.published_at
        })
        if (!updatedDocument) {
            console.log('Document not found');
            return null;
        }
        return updatedDocument;
        //
        //
        //
        // const thread: any=await this.Post.findById(threadFields._id);
        //
        // console.log(`will patch post with id: ${thread._id}(DB), ${threadFields._id}(REQ)`);
        // if (thread) {
        //     for (const i in threadFields) {
        //         thread[i]=threadFields[i];
        //     }
        //     console.log(thread.url);
        //     console.log(threadFields.url);
        //
        //     // let cleanUpText = threadFields.body.toString().replace(/<\/?[^>]+(>|$)/gi, "");
        //     // cleanUpText = cleanUpText.toString().replace('&nbsp;', "");
        //     // cleanUpText.replace("&nbsp;", "").trim();
        //     // thread.body = cleanUpText;
        //     await thread.save();
        // }
        // return thread;
    }

    async patchThreadBySlug(threadFields: any) {
        const thread: any = await this.Post.findOne({'slug': threadFields.slug});
        console.log(Object.keys(thread));
        console.log(`will patch post with id: ${thread._id}(DB), ${threadFields._id}(REQ)`);
        if (thread) {
            for (const i in threadFields) {
                thread[i]=threadFields[i];
            }
            // let cleanUpText = threadFields.body.toString().replace(/<\/?[^>]+(>|$)/gi, "");
            // cleanUpText = cleanUpText.toString().replace('&nbsp;', "");
            // cleanUpText.replace("&nbsp;", "").trim();
            // thread.body = cleanUpText;
            await thread.save();
        }
        return thread;
    }

    async getLatestPost() {
        return this.Post.findOne().sort({published_at: -1}).limit(1);
    }

    async getPostToBePublishedToday(date: Date) {
        return await PostModel.findOne({
                published_at: '2023-03-15T22:17:00.000+00:00',
                hide: true,
            }
        ).exec();
    }

    async getPostByAudioTaskId(task_id: string) {
        {
            return this.Post.findOne({audio_task_id: task_id});
        }
    }

    async patchAudioTask(threadFields: any) {
        const thread: any = await this.AudioTask.findById(threadFields._id);
        if (thread) {
            for (const i in threadFields) {
                thread[i] = threadFields[i];
            }
            await thread.save();
        }
        return thread;
    }

    async getAudioTaskByAudioTaskId(task_id: string) {
        {
            return this.AudioTask.findOne({task_id: task_id});
        }
    }

    async getAllAudioTasks() {
        return this.AudioTask.find().exec();
    }

    async createAudioTask(audioTask: any) {
        audioTask._id = shortUUID.generate();
        const t = new this.AudioTask(audioTask);
        await t.save();
        return t;
    }

    async deleteAudioTaskById(userId: string) {
        await this.AudioTask.deleteOne({task_id: userId});
    }
}
