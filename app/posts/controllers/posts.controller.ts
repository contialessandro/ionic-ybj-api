import express from 'express';
import {PostsServices} from '../services/posts.services';
import {CategoryService} from '../../categories/services/category.services';
import {ThreadsMetaServices} from '../../threadsmeta/services/threadsmeta.services';
import {PollySynth} from "../../aws/PollyToS3";
import fs from 'fs';
import {PublicImagesBucket} from '../../aws/Bucket';

import {config} from '../../config/config';
import AWS from "aws-sdk";
import axios, {AxiosResponse} from 'axios';

// const convertSvgToJpeg = require('convert-svg-to-jpeg'); // Use your actual library for SVG to JPEG conversion
const {DateTime} = require('luxon');

AWS.config.update({region: config.aws_profile.region});
const credentials = new AWS.SharedIniFileCredentials({
    profile: config.aws_profile.profile,
});
AWS.config.credentials = credentials;

export class PostsController {
    protected postsService: PostsServices;

    constructor() {
        this.postsService = PostsServices.getInstance();
    }

    async listPost(req: express.Request, res: express.Response) {
        const postsService=PostsServices.getInstance();
        const treads=await postsService.list(100, 0);
        res.status(200).send(treads);
    }

    async patchById(req: express.Request, res: express.Response) {
        const postsService = PostsServices.getInstance();
        const allPosts = await postsService.readBy_Id("64f6c2514caaf98d74635683");
        const arrayPosts = [allPosts];
        const postsToConvert = arrayPosts.filter((post: any) => post.awsUrl.includes('.svg'));
        if (postsToConvert.length > 0) {
            try {
                for (const post of postsToConvert) {
                    // @ts-ignore

                    const response:AxiosResponse<any> = await axios.get(post.awsUrl, {responseType: 'stream'});
                    // @ts-ignore

                    const svgFilePath = `/app/app/dataStorage/${post.title.toLowerCase()}.svg`; // Specify a local directory to store SVG files
                    const writer = fs.createWriteStream(svgFilePath);
                    response.data.pipe(writer);

                    await new Promise((resolve, reject) => {
                        writer.on('finish', resolve);
                        writer.on('error', reject);
                    });
                    // @ts-ignore
                    const jpgPath = `/app/app/dataStorage/${post.title.toLowerCase()}.jpg`;
                    // const jpegBuffer = await sharp(svgFilePath)
                    //     .jpeg()
                    //     .toFile(jpgPath);
                    // @ts-ignore
                    // const spittedString = post.awsUrl.split('/');
                    // const [prefix, filename] = spittedString.slice(-2);
                    // @ts-ignore
                    // console.log({spittedString,jpegBuffer,url:post.awsUrl});
                    //     // await PublicImagesBucket.UploadToS3(`${prefix}`,`/app/app/dataStorage/${post.title}.jpg`)
                    //     // Upload the JPEG to S3
                    //     const s3 = new AWS.S3({
                    //         signatureVersion: config.aws_profile.signature_version,
                    //         region: config.aws_profile.region,
                    //         // @ts-ignore
                    //
                    //         params: {Bucket:`ybjlaravel` , Key: `/app/app/dataStorage/${post.title.toLowerCase()}.jpg`}
                    //     });
                    //     // @ts-ignore
                    //
                    //     const fileStream = fs.createReadStream(`/app/app/dataStorage/${post.title.toLowerCase()}.jpg`);
                    //     return new Promise(async (resolve, reject) => {
                    //         await s3.upload({
                    //             Bucket: 'ybjlaravel',
                    //             // @ts-ignore
                    //             Key: `${prefix}/${post.title.toLowerCase()}`,
                    //             Body: fileStream,
                    //             ACL: 'bucket-owner-full-control',
                    //         }).promise()
                    //             .then(resolve, reject);
                    //     });

                    // @ts-ignore
                    await PublicImagesBucket.UploadToS3(`${post.id}/`, jpgPath);

                    // Update the awsUrl of the post with the new S3 URL
                }
            } catch (error) {
                console.error('Error processing posts:', error);
            }
        }
        res.status(200).send(postsToConvert);
    }
    async getHomePagePosts(req: express.Request, res: express.Response) {
        const postsService=PostsServices.getInstance();
        const treads = await postsService.listHomepagePost();
        res.status(200).send(treads);
    }

    async getHomePageThreads(req: express.Request, res: express.Response) {
        const postsService=PostsServices.getInstance();
        const treads=await postsService.getHomePageThreads();
        res.status(200).send(treads);
    }

    async getThreadById(req: express.Request, res: express.Response) {
        const postsService=PostsServices.getInstance();
        const user=await postsService.readById(req.params.id);
        res.status(200).send(user);
    }

    async getThreadByUniqueId(req: express.Request, res: express.Response) {
        try {
            const postsService = PostsServices.getInstance();
            const user = await postsService.readBy_Id(req.params.postId);
            if (!user) {
                // const postsService = PostsServices.getInstance();
                //         const user = await postsService.readBy_Id(req.params.postId);
                //         return res.status(404).send({message: 'resource not found'});
            }

            return res.status(200).send(user);

        } catch (e) {
            return res.status(500).send({
                message: (e as any).message, line: (e as any).line
            });
        }
    }

    async getThreadByMetaId(req: express.Request, res: express.Response) {
        const threadsService = PostsServices.getInstance();
        const post = await threadsService.readByMetaId(req.params.id);
        res.status(200).send(post);
    }

    async CreatePost(req: express.Request, res: express.Response) {
        console.log(req.body);
        const postsService = PostsServices.getInstance();

        const isAudioNeeded = +req.body.audio_needed;
        delete req.body.audio_needed;
        const thread = await postsService.create(req.body);
        console.log({body:req.body})
        console.log({hide: req.body.hide, isAudioNeeded: isAudioNeeded});
        //deal with new audio
        if (isAudioNeeded) {
            const task = await PollySynth.startAsyncAudioGenerator(req.body);
            if (task) {
                thread.audio_task_id = task.TaskId;
                await postsService.createAudioTask({
                    task_id: task.TaskId,
                    slug: thread.slug,
                    status: task.TaskStatus
                });
            }
            await postsService.patchById(thread)
        }
        // await podcast.createEpisode(thread.title, thread.intro, thread.audio_url, `${thread.slug}.mp3`,thread.slug);
        res.status(201).send(thread);
    }

    async patch(req: express.Request, res: express.Response) {
        try {
            const isAudioNeeded = +req.body.audio_needed;
            console.log({hide: req.body.hide, isAudioNeeded: isAudioNeeded, slug: req.body.slug});
            const postsService = PostsServices.getInstance();
            const thread: any = await postsService.patch(req.body);
            if (isAudioNeeded) {
                const task = await PollySynth.startAsyncAudioGenerator(req.body);
                if (task) {
                    thread.audio_task_id = task.TaskId;
                    await postsService.createAudioTask({
                        task_id: task.TaskId,
                        slug: thread.slug,
                        status: task.TaskStatus
                    });
                }
                await postsService.patchById(thread)
            }
            return res.status(200).send(thread.url);

        } catch (error) {
            console.error('Error processing posts:', error);
        }
    }

    async put(req: express.Request, res: express.Response) {
        const threadsService=PostsServices.getInstance();

        await threadsService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeThread(req: express.Request, res: express.Response) {
        const threadsService=PostsServices.getInstance();
        await threadsService.deleteById(req.params.mediaId);
        res.status(204).send(``);
    }

    async getThreadBySlug(req: express.Request, res: express.Response) {
        const threadsService=PostsServices.getInstance();
        const threads: any=await threadsService.getThreadBySlug(req.params.slug);
        let obj={};
        if (threads) {
            const thread=threads[0]
            const threadMetaService=ThreadsMetaServices.getInstance();
            const metas=await threadMetaService.getThreadMetasById(thread.meta_id) as any;
            let imagePath;
            const meta=metas[0];
            if (thread.meta_id) {
            }
            if (meta) {
                imagePath='https://ybjlaravel.s3.eu-west-2.amazonaws.com/' + meta.image_id?.id + '/' + meta.image_id?.file_name;
            }
            obj={
                id: thread?.id,
                label: meta?.category_id?.name ?? '',
                link: meta?.category_id?.name ?? '',
                location: meta?.location_id?.name ?? '',
                title: thread.title ?? '',
                image: imagePath,
                image1: imagePath,
                image2: imagePath,
                createdAt: thread.published_at,
                intro: thread.intro,
                alt: meta.category_id?.type,
                color: meta.category_id?.color,
                body: thread.body,
                slug: thread.slug,
            };
        }
        res.status(200).send([obj]);
    }

    async getThreadsByCategory(req: express.Request, res: express.Response) {
        try {
            const categoryName: any=req.params.topic;
            const categoryService=CategoryService.getInstance();
            const category: any=await categoryService.getCategoryByName(categoryName);
            if (category) {
                const threadMetaService=ThreadsMetaServices.getInstance();
                const threadMetas=await threadMetaService.getThreadMetasByCategory(category._id) as any;

                const homepageResources: any=[];

                for (const meta of threadMetas) {
                    const threadService=PostsServices.getInstance();
                    const thread: any=await threadService.readBy_Id(meta.thread_id);
                    if (thread) {
                        const imagePath='https://ybjlaravel.s3.eu-west-2.amazonaws.com/' + meta.image_id?.id + '/' + meta.image_id?.file_name;

                        const obj={
                            id: thread?.id,
                            label: meta?.category_id?.name ?? '',
                            link: meta?.category_id?.name ?? '',
                            location: meta?.location_id?.name ?? '',
                            title: thread.title ?? '',
                            image: imagePath,
                            image1: imagePath,
                            image2: imagePath,
                            createdAt: thread.published_at,
                            intro: thread.intro,
                            alt: meta.category_id?.type,
                            color: meta.category_id?.color,
                            body: thread.body,
                            slug: thread.slug,
                        };
                        homepageResources.push(obj);
                    }
                }
                return res.status(200).send(homepageResources);
            }
        } catch (e) {
            const err: any = e;
            return res.status(500).send({
                message: err.message, line: err.line
            });

        }
    }

    async getNextPublishDate(req: express.Request, res: express.Response) {
        const postsService = PostsServices.getInstance();
        const post = await postsService.getLatestPost();
        const LastPublishedPostDate = post?.published_at;
        const nextPublishDate = DateTime.fromJSDate(LastPublishedPostDate);
        res.status(200).send(nextPublishDate.plus({week: 1}));
    }

    async saveAudioFileforLegacy(req: express.Request, res: express.Response) {
        const postsService = PostsServices.getInstance();
        const posts = await postsService.list(500, 0);
        try {
            posts.forEach((post) => {
                const payload = {
                    body: post.body,
                    slug: post.title,
                    id: post.id
                }
                PollySynth.GenerateAudio(payload);
            })
            return res.status(200).send('as');
        } catch (e) {
            return res.status(500).send({
                message: (e as any).message, line: (e as any).line
            });
        }
    }
}
