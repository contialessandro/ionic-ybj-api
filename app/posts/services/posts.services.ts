import {CRUD} from '../../common/interfaces/crud.interface';
import {GenericInMemoryDao} from '../daos/in.memory.dao';
import {PostsDao} from '../daos/posts.dao';
import {CategoryService} from '../../categories/services/category.services';
import {LocationsServices} from '../../locations/services/locations.services';

export class PostsServices implements CRUD {
    private static instance: PostsServices;
    dao: GenericInMemoryDao;

    constructor() {
        this.dao=GenericInMemoryDao.getInstance();
    }

    static getInstance(): PostsServices {
        if (!PostsServices.instance) {
            PostsServices.instance = new PostsServices();
        }
        return PostsServices.instance;
    }

    async create(resource: any) {
        const category = await this.GetCategroyById(resource.category);
        resource.color = '#ffeb00de';
        resource.category = 'Featured';
        // console.log({category, resource})
        if (category) {
            resource.color = category.color;
            resource.category = category.name;
        }
        const location = await this.GetLocationById(resource.location);
        if (location) resource.location = location.name;
        return await PostsDao.getInstance().addPost(resource);
    }

    GetCategroyById = async (categoryId: any) => {
        const categoryService = CategoryService.getInstance();
        return await categoryService.readById(categoryId);
    }
    GetLocationById = async (locationId: any) => {
        const locationService = LocationsServices.getInstance();
        return await locationService.readById(locationId);
    }

    deleteById(resourceId: any) {
        return PostsDao.getInstance().removeThreadById(resourceId);
    }

    list(limit: number, page: number) {
        return PostsDao.getInstance().listPost(limit, page);
    }

    listHomepagePost() {
        return PostsDao.getInstance().listHomepagePost();
    }

    getPostsByCategory(categoryName: string) {
        return PostsDao.getInstance().getPostsByCategory(categoryName)
    }

    getPostBySlug(slugName: string) {
        return PostsDao.getInstance().getPostBySlug(slugName)
    }

    getPostByLocation(location: string) {
        return PostsDao.getInstance().getPostByLocation(location)
    }

    getHomePageThreads() {
        return PostsDao.getInstance().getHomePageThreads();
    }

    patchById(resource: any) {
        return PostsDao.getInstance().patchThread(resource);
    }

    patch(resource: any) {
        return PostsDao.getInstance().patchThread(resource);
    }

    readById(resourceId: any) {
        return PostsDao.getInstance().getThreadById(resourceId);
    }

    readBy_Id(resourceId: any) {
        return PostsDao.getInstance().getThreadBy_Id(resourceId);
    }

    readByMetaId(resourceMetaId: any) {
        return PostsDao.getInstance().getThreadByMetaId(resourceMetaId);
    }

    updateById(resource: any) {
        return PostsDao.getInstance().patchThread(resource);
    }

    async getThreadByName(email: string) {
        return PostsDao.getInstance().getThreadByName(email);
    }

    async getThreadBySlug(slug: string) {
        return PostsDao.getInstance().getThreadBySlug(slug);
    }

    async getThreadByTopic(topic: string) {
        return PostsDao.getInstance().getThreadBySlug(topic);
    }

    async getLatestPost() {
        return PostsDao.getInstance().getLatestPost()
    };

    async getPostTobePublishedToday(date: any) {
        return await PostsDao.getInstance().getPostToBePublishedToday(date);
    }

    getPostByAudioTaskId(taskId: any) {
        return PostsDao.getInstance().getPostByAudioTaskId(taskId);
    }

    patchAudioTask(task: any) {
        return PostsDao.getInstance().patchAudioTask(task);
    }

    getAudioTaskByAudioTaskId(taskId: any) {
        return PostsDao.getInstance().getAudioTaskByAudioTaskId(taskId);
    }

    getAllTasks(): any {
        return PostsDao.getInstance().getAllAudioTasks();
    }

    async createAudioTask(resource: any) {
        return await PostsDao.getInstance().createAudioTask(resource);
    }

    deleteAudioTaskById(resourceId: any) {
        return PostsDao.getInstance().deleteAudioTaskById(resourceId);
    }

    async getThreadsAfterId(id: string) {
        return await PostsDao.getInstance().getThreadsAfterId(id);
    }
}
