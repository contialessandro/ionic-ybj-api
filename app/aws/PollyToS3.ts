import {PublicImagesBucket} from './Bucket';
import {AudioRequestInterface} from "../common/interfaces/AudioRequest.interface";
import {IAudioTask} from "../json-schemas/models.schemas";
import AWS from "aws-sdk";
import {config} from "../config/config";
import {SynthesizeSpeechOutput} from "@aws-sdk/client-polly";

const Fs = require('fs');

interface PollyToS3Config {
    signatureVersion: string;
    region: string;
    outputBucketName: string;
}
export class PollyToS3 {
    private Polly: AWS.Polly;
    private S3: AWS.S3;
    private params: any;
    private paramsLongText: any;

    constructor(private pollyConfig: PollyToS3Config) {
        AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: config.aws_profile.profile});
        this.Polly = new AWS.Polly({
            signatureVersion: pollyConfig.signatureVersion,
            apiVersion: '2016-06-10',
            region: 'eu-west-2',
        });
        this.S3 = new AWS.S3({
            signatureVersion: pollyConfig.signatureVersion,
            region: pollyConfig.region,
        });
        this.params = {
            Text: '',
            OutputFormat: 'mp3',
            VoiceId: 'Kimberly'
        };
        this.paramsLongText = {
            OutputFormat: 'mp3',
            OutputS3BucketName: pollyConfig.outputBucketName,
            OutputS3KeyPrefix: 'podcast-feed/',
            Text: '',
            TextType: 'text',
            VoiceId: 'Joanna',
            Engine: 'neural'
        };
    }
    GenerateAudio(payload: AudioRequestInterface) {
        AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: config.aws_profile.profile});
        try {
            this.params.Text = payload.body;
            const basePathForFile = `app/dataStorage/${payload.id}`;
            const bucketAudioBasePath = `${payload.id}/audio/`;
            this.Polly.synthesizeSpeech(this.params, (err: any, data: SynthesizeSpeechOutput) => {
                if (err) {
                    return console.log(err)
                }
                if (data && data.AudioStream instanceof Buffer) {
                    if (!Fs.existsSync(basePathForFile)) {
                        Fs.mkdirSync(basePathForFile, {recursive: true});
                    }
                    Fs.writeFile(`${basePathForFile}/${payload.slug}.mp3`, data.AudioStream, function (err: any) {
                        if (err) {
                            return console.log(err)
                        }

                        console.log("The file was saved!");
                        PublicImagesBucket.UploadToS3(`${bucketAudioBasePath}`, `${basePathForFile}/${payload.slug}.mp3`)
                            .then(r => r);
                    });
                    return Promise.resolve(data);
                }
            });
        } catch (err) {
            return Promise.reject(err);
        }
    }

    async startAsyncAudioGenerator(payload: any) {
        this.paramsLongText = {
            OutputFormat: 'mp3',
            OutputS3BucketName: 'ybjlaravel-dev',
            OutputS3KeyPrefix: 'podcast-feed/',
            Text: '',
            TextType: 'text',
            VoiceId: 'Joanna',
            Engine: 'neural'
        };
        let cleanUpText = payload.body.toString().replace(/<\/?[^>]+(>|$)/gi, "");
        cleanUpText.replace("&nbsp;", "").trim();
        this.paramsLongText.Text = cleanUpText;
        this.paramsLongText.OutputS3KeyPrefix = `${this.paramsLongText.OutputS3KeyPrefix}${payload.slug.toString().toLowerCase()}`;
        // Start the synthesis task and get the task ID
        const startTaskResponse = await this.Polly.startSpeechSynthesisTask(this.paramsLongText).promise();
        return startTaskResponse!.SynthesisTask;
    }
    async asyncAudioTaskProcessor(audioTask: IAudioTask) {
        AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: config.aws_profile.profile});
        this.paramsLongText = {
            OutputFormat: 'mp3',
            OutputS3BucketName: 'ybjlaravel-dev',
            OutputS3KeyPrefix: 'podcast-feed/',
            Text: '',
            TextType: 'text',
            VoiceId: 'Joanna',
            Engine: 'neural'
        };
        let taskStatus: string;
        const taskId = audioTask.task_id;
        const slug = audioTask.slug;
        const taskPath = `podcast-feed/${slug.toString().toLowerCase()}.${taskId}.mp3`;
        do {
            const getTaskResponse = await this.Polly.getSpeechSynthesisTask({TaskId: taskId}).promise();
            taskStatus = getTaskResponse.SynthesisTask!.TaskStatus!;
            console.log(`Task ${taskId} status: ${taskStatus}`);
            if (taskStatus === 'failed') {
                console.error(`Task ${taskId} failed: ${getTaskResponse.SynthesisTask!.TaskStatusReason}`);
                break;
            }
            await new Promise(resolve => setTimeout(resolve, 5000)); // Wait 5 seconds before checking again
        } while (taskStatus !== 'completed');
// If the task completed successfully, get the URL of the synthesized audio file
        try {
            if (taskStatus === 'completed') {
                await this.renameAudioFile(taskPath, `podcast-feed/${slug.toString().toLowerCase()}.mp3`);
                const newUrl = this.S3.endpoint.href + this.pollyConfig.outputBucketName + '/podcast-feed/' + `${slug.toString().toLowerCase()}.mp3`;
                return {audio_url: newUrl, taskStatus};
            }
        } catch (e) {
            return Promise.reject(e);
        }
    }

    private async renameAudioFile(oldKey: string, newKey: string): Promise<void> {
        const BUCKET_NAME = this.pollyConfig.outputBucketName;

        try {
            await this.S3.copyObject({
                Bucket: BUCKET_NAME,
                CopySource: `${BUCKET_NAME}/${oldKey}`,
                Key: newKey,
                ACL: 'bucket-owner-full-control',
            }).promise();
            await this.S3.deleteObject({
                Bucket: BUCKET_NAME,
                Key: oldKey,
            }).promise();
        } catch (error) {
            console.error(`Error renaming audio file from ${oldKey} to ${newKey}:`, error);
        }
    }
}

const pollyConfig: PollyToS3Config = {
    signatureVersion: config.aws_profile.signature_version,
    region: config.aws_profile.region,
    outputBucketName: 'ybjlaravel-dev'
};

export const PollySynth = new PollyToS3(pollyConfig);
