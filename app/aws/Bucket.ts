import {config} from "../config/config";
import AWS from "aws-sdk";

AWS.config.update({region: config.aws_profile.region});
// const credentials = new AWS.SharedIniFileCredentials({
//   profile: config.aws_profile.profile,
// });
// AWS.config.credentials = credentials;
const signedUrlExpireSeconds = 60 * 5;
var fs = require('fs');
var path = require('path');

export class Bucket {
  imagesBucketName = this.SetBucketNameFromEnvironment(config.aws_profile.public_images_bucket);
  audioBucketName = this.SetBucketNameFromEnvironment(config.aws_profile.audio_recording_bucket);
  public GetImagesBucketName = () => {
    return this.imagesBucketName;
  }
  GetAudioBucketName = () => {
    return this.audioBucketName;
  }
  /**
   * static async GetPutSignedUrl
   */
  GetPutSignedUrl = async (fileFullPath: string, contentType: string) => {
    try {

      AWS.config.update({region: config.aws_profile.region});
      AWS.config.credentials = new AWS.SharedIniFileCredentials({
        profile: config.aws_profile.profile,
      });
      const s3 = new AWS.S3(
          {
            signatureVersion: config.aws_profile.signature_version,
            region: config.aws_profile.region,
            params: {
              Bucket: this.GetImagesBucketName()
            }
          });
      const params = {
        Key: fileFullPath,
        Expires: signedUrlExpireSeconds,
        ContentType: contentType
      };
      const url = await s3.getSignedUrlPromise("putObject", params);
      return Promise.resolve(url);
    } catch (err) {
      return Promise.reject(err);
    }
  }
  GetObject = async (filePath: string) => {
    let data;
    try {
      const s3 = new AWS.S3({
        signatureVersion: config.aws_profile.signature_version,
        region: config.aws_profile.region,
        params: {Bucket: this.imagesBucketName, Key: filePath}
      });
      data = await s3.getObject().promise();
      return Promise.resolve(data?.Body?.toString('utf8'));
    } catch (err) {
      return Promise.reject(err);
    }
  }

  SetBucketNameFromEnvironment(bucketName: string) {
    let prefix = '';
    // if (config.environment === 'development') {
    //   prefix = '-dev';
    // }
    return bucketName + prefix;
  }

  UploadToS3(keyPrefix: any, filePath: any) {
    // ex: /path/to/my-picture.png becomes my-picture.png
    const fileName = path.basename(filePath);
    const fileStream = fs.createReadStream(filePath);

    // If you want to save to "my-bucket/{prefix}/{filename}"
    //                    ex: "my-bucket/my-pictures-folder/my-picture.png"
    const keyName = path.join(keyPrefix, fileName);
    AWS.config.update({region: config.aws_profile.region});
    AWS.config.credentials = new AWS.SharedIniFileCredentials({
      profile: config.aws_profile.profile,
    });
    return new Promise((resolve, reject) => {
      fileStream.once('error', reject);
      const s3 = new AWS.S3({
        signatureVersion: config.aws_profile.signature_version,
        region: config.aws_profile.region,
        params: {Bucket: this.GetImagesBucketName(), Key: filePath}
      });

      s3.upload({
        Bucket: this.GetImagesBucketName(),
        Key: keyName,
        Body: fileStream,
      });
      console.log({
        Bucket: this.GetImagesBucketName(),
        Key: keyName, filePath, keyPrefix, fileName
      })
      s3.upload({
        Bucket: this.GetImagesBucketName(),
        Key: keyName,
        Body: fileStream
      })
          .promise()
          .then(resolve, reject);
    });
  }
}

export const PublicImagesBucket = new Bucket();
