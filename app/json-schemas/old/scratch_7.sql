INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (416, 'Sheffield Forgemasters', 'Sheffield engineering company welcomes 24 apprentices', '<p>Sheffield Forgemasters has welcomed 24 new apprentices for 2023, as it progresses development on one of the world&rsquo;s most advanced manufacturing facilities.</p>

<p>&nbsp;</p>

<p>The successful applicants join the Ministry of Defence (MoD) owned company during a hugely exciting period to participate in an award-winning apprenticeship scheme, where they will receive fully-funded training, working towards a professional qualification whilst getting paid.</p>

<p>&nbsp;</p>

<p>Sheffield Forgemasters&rsquo; apprentices will start at a variety of levels including two degree apprenticeships as the company drives forward with significant investment into new plant and equipment.</p>

<p>&nbsp;</p>

<p>Nicola Childs, interim HR Director at Sheffield Forgemasters, said: &ldquo;The calibre of candidates that came forward to join our apprenticeships scheme was very high this year and we were pleased to see much greater gender diversity across applications and successful candidates.</p>

<p>&nbsp;</p>

<p>&ldquo;The apprentices join our company at an exciting time, having secured significant investment following the MoD acquisition. As we transform our capability to meet the demands of our defence work, we are also expanding in areas such as civil nuclear and renewable energy.&rdquo;</p>

<p>&nbsp;</p>

<p>Sheffield Forgemasters is investing heavily over the next ten years to support its defence-critical assets, including a new 13,000 tonne Forge line and building, 17 major machine tool replacements within a new machining facility, which will be unmatched outside of the UK.</p>

<p>&nbsp;</p>

<p>Nicola added: &ldquo;Our apprenticeship programme is recognised as one of the best our region has to offer, and we are incredibly proud to be able to provide future generations with the opportunity to not only have a meaningful and varied career but also to develop important skills for life.&rdquo;</p>

<p>&nbsp;</p>

<p>Apprentices have secured roles in the following disciplines; Machinists, Electrical and Control Engineers, NDT Technicians, Methods Engineer (Degree), Design Engineer (Degree), Production Planning and Estimating.</p>

<p>&nbsp;</p>

<p>Emily Wynne, People Development Advisor - Early Careers at Sheffield Forgemasters, said: &ldquo;The strength of our apprenticeships programme is growing as we continue to protect and develop our skills-base for high-technology manufacturing.</p>

<p>&nbsp;</p>

<p>&ldquo;It&rsquo;s great to welcome our latest cohort of apprentices and as they settle in, we are already planning for our 2024 apprentice requirements.&rdquo;</p>

<p>&nbsp;</p>

<p>Sheffield Forgemasters specialises in the design and manufacture of high integrity forgings and castings offering end-to-end manufacture for steel production from a single site in the UK.</p>

<p>&nbsp;</p>

<p>Global markets served include Defence, Marine, Civil Nuclear, Steel Processing, Offshore, Renewables, Power Generation and Pressure Vessels.</p>', 431, '
2023-11-21 16:59:45
', '2023-12-11 04:19:40
', 0, 'https://www.sheffieldforgemasters.com/', 'sheffield-forgemasters', '2023-11-22 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (417, 'Camira', 'Textile recycling specialist converts Savile Row tailors’ waste to create luxury wool fabric for Wool Month', '<p>Textile recycling specialist converts Savile Row tailors&rsquo; waste to create luxury wool fabric for Wool Month</p>

<p>Yorkshire-based textile reprocessing firm, iinouiio, which was acquired by global textile manufacturer Camira in 2022, has recycled 200kg of material waste from Savile Row tailors, to innovate a new circular wool fabric, which will be unveiled during Wool Month in October 2023.</p>

<p>&nbsp;</p>

<p>The only textile recycling capability of its kind in the UK, iinouiio, collaborated with the Campaign for Wool, which was founded by His Majesty King Charles III in 2010, (when he was HRH The Prince of Wales) Eco-Luxe, and Scotland-based micro mill, Woven in the Bone to create a luxury herringbone cloth, which will be showcased at Holland &amp; Sherry, Savile Row.</p>

<p>&nbsp;</p>

<p>Commenting on the project, founder of iinouiio, Dr John Parkinson said: &ldquo;It has been an incredibly exciting project to work on and we&rsquo;re delighted it will come to fruition during Wool Month, which is a key event for us to&nbsp;demonstrate and promote responsible textile manufacturing, with the aim of breathing new life into discarded textiles.</p>

<p>&nbsp;</p>

<p>&ldquo;Wool recycling is not a new concept; around&nbsp;200 years ago mill owner Benjamin Law transformed discarded rags&nbsp;into new cloth and initiated textile&rsquo;s first circular economy, known as &lsquo;shoddy,&rsquo; manufacturing. My life&nbsp;has been consumed by recycling textiles and we are determined to preserve and improve upon this craft&rsquo;s impressive heritage and the legacy of my own family&rsquo;s &lsquo;shoddy&rsquo; business.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>To produce the Savile Row cloth, the tailors&rsquo; waste was recycled and blended using iinouiio&rsquo;s advanced textile reprocessing machinery and then spun into yarn at Camira&rsquo;s manufacturing site in Huddersfield, Yorkshire, before being woven at Woven in The Bone in Scotland.</p>

<p>&nbsp;</p>

<p>During the launch, Savile Row tailors will display garments, products and homeware items created with the new recycled wool cloth.</p>

<p>&nbsp;</p>

<p>Su Thomas, the founder at Eco-Luxe added: &ldquo;The tailoring houses on Savile Row have always strived towards sustainable methods in their craft, with recycling, repurposing and repairing as some of their core values. Eco-Luxe takes these efforts a step further by collaborating with Savile Row companies on producing bespoke garments, accessories and textiles from recycled wool cloth made entirely of wool off-cuts. It has been fantastic to work with iinouiio and Camira to innovate a truly closed loop textile which we&rsquo;re incredibly excited to see launch during Wool Month.&rdquo;</p>

<p>&nbsp;</p>

<p>A step forward for the fashion industry, the project is the start of zero-waste tailoring and highlights the tailor&rsquo;s recognition and importance of reducing textile waste that goes to landfill.</p>

<p>&nbsp;</p>

<p>Peter Ackroyd, COO at The Campaign for Wool commented: &ldquo;The Campaign for Wool is delighted to see the development of a scheme to ensure tailors on the Row spare no effort in ensuring almost zero waste occurs in the already ecologically efficient creation of bespoke clothing. To see further life given to what would be considered waste is particularly encouraging. Wool, unlike the vast majority of fibres, at the very end of its life, is perfectly biodegradable and if buried, actually enriches the soil&rdquo;.</p>', 432, '
2023-11-21 17:03:11
', '2023-12-11 04:19:18
', 0, 'https://www.iinouiio.com/', 'camira-1', '2023-11-23 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (418, 'AgriSound', 'York-based Agri-tech pioneers develop AI to save nationally rare ‘hopping’ bee from extinction', '<p>The Species Recovery Trust has teamed up with industry-leading insect biodiversity innovator, (York-based) AgriSound, in a cutting-edge AI-driven project that aims to detect and protect a nationally rare and declining species of mining bee that has a hopping flight pattern when collecting food. </p>

<p></p>

<p>The small, black, solitary Tormentil mining bee which
‘hops
’ as it distinctively flies between flowers while gathering pollen and nectar and is codependent on just a single type of flower - Tormentil. This quirky bee could be saved from ultimate extinction thanks to the development of a groundbreaking new algorithm using the latest AI machine learning technology. </p>

<p></p>

<p>The Species Recovery Trust
’s project to monitor and protect the rare Tormentil mining bee is focused on the York Heathlands and North Yorkshire Moors. This species of bee has been in drastic decline over recent years and has been lost from 50% of its former sites since 1970s.  </p>

<p></p>

<p>The Tormentil mining bees
’ demise is being hastened by the loss of habitats rich in Tormentil plants and sandy bare ground areas where it nests.  Loss of heath and moorland areas to agriculture, overgrazing and scrub encroachment has resulted in a decline of tall Tormentil plants and sandy areas, restricting the bee to a small number of sites, and therefore the Trust has found it very hard to reliably monitor.
  </p>

<p> <br>
The Trust turned to agritech start-up, AgriSound to come on board with the project to develop a bespoke and groundbreaking algorithm, to integrate with their unique pollinator-tracking technology, in order to improve and quantify efforts to monitor and save this bee.  </p>

<p></p>

<p>Using AgriSound
’s
‘Polly
’ and its new, specifically developed algorithm, the resulting data is allowing the health and activity of the Tormentil mining bee to be accurately monitored enabling the Species Recovery Trust to deploy the most appropriate interventions and care for this rare and endangered species of bee. </p>

<p></p>

<p>Using bioacoustics is a highly novel approach to protecting and conserving insects, but one AgriSound is expert at and is able to develop species specific algorithms for.  </p>

<p></p>

<p>Its highly successful Polly bioacoustic listening devices are increasingly being used in commercial agriculture and by food producers as well across spaces utilising biodiversity conservation. They are revolutionising commercial pollination, helping increase crop yield and providing the data crucial, not only to help protect biodiversity, but also to allow organisations to demonstrate positive impacts of actions for ESG reporting metrics. </p>

<p></p>

<p>To track a species such as the Tormentil mining bee, AgriSound
’s approach was to catch and record the bee, using the latest AI (machine learning) to learn from the data to develop a bioacoustic algorithm that can detect the bee. Because of the bee
’s flight being much more of an extremely short
‘hop
’ and its tendency mine into the ground, as befits its name, the team had to make sure the bee was captured safely without allowing it to chew through nets or boxes. The bee also makes a very short noise when it
‘hop flies
’, making them incredibly hard to record and accurately measure. </p>

<p></p>

<p>Once AgriSound
’s scientists had recorded the tiny snippets of
‘hopping flight
’ sounds, these were fed into to its AI machine learning programme, which learnt and now understands how it sounds and it was able to create the algorithm that will help the Species Recovery Trust track and gather the necessary data on the bees in the wild. </p>

<p></p>

<p>Access to this data is what will allow the Species Recovery Trust to understand which areas are best to protect and develop, in order to help protect populations and boost local biodiversity. </p>

<p></p>

<p>Currently, just under thirty Pollys will be distributed across the York Heathlands and North Yorkshire Moors in a bid to track these insects. </p>

<p></p>

<p>Casey Woodward, founder, and CEO of AgriSound, said:
“AgriSound is working, more than ever, on projects that help detect and protect rare and endangered species, and with AI advances we are increasingly developing new, bespoke algorithms to help detect and protect a growing list of species, such as the Tormentil mining bee, that are often in decline, and can benefit from intervention the most. </p>

<p></p>

<p>
“Originally, we developed our Polly insect monitoring device to detect bee activity around crops and other pieces of land to provide farmers and food producers with the knowledge to increase crop pollination. We
’re proud to be continually developing our AI technology to provide important information for biodiversity development, provide crucial data to help companies meet ESG regulations and now also develop unique algorithms for species protection too.
” </p>

<p></p>

<p>Vicky Wilkins from the Species Recovery Trust said about the project:
“The Tormentil mining bee, a
‘national conservation priority species
’ has been lost from over half of its habitat since 1970s. Using this new technology we hope will be a game changer in helping us improve identification of areas for habitat creation, as well as restoring existing sites.  This will mean we can work to better provide the right habitat for and ultimately save these charismatic little bees from extinction.
”</p>', 433, '
2023-11-21 17:06:24
', '2023-11-27 00:05:05
', 0, 'https://agrisound.io/', 'agrisound-03-2023', '2023-11-27 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (419, 'Ward Hadaway', 'Employment team at Leeds law firm grows by 50%', '<p>Leading Yorkshire law firm Ward Hadaway has announced a key appointment in its employment team as the department continues to grow.</p>

<p>Ward Hadaway welcomes Amy Hughes to the employment department, which has grown by 50% in the last two years. Amy advises on all aspects of employment law providing expertise on workplace policies and procedures, grievance and disciplinary issues, and dealing with contract queries, as well as defending employment tribunal claims and negotiating settlement agreements.</p>

<p>With Amy
’s appointment, the team in Leeds is now nine people strong as the department supports a greater number of regional and national businesses and individuals requiring employment and immigration legal advice. Turnover has increased by over a third (35%) in the last 12 months, with a further 15% increase expected this year.</p>

<p>Harmajinder Hayre, a partner and head of employment at Ward Hadaway, said:
“We
’re delighted to bring Amy on board to join our rapidly growing national team of over 30 employment and immigration lawyers. Her expertise complements the team
’s existing skillset and experience. It also enables us to continue our growth trajectory as one of the leading law firms in the region and beyond that provides specialist advice in employment and immigration law.
”</p>

<p>Ward Hadaway provides a diverse range of legal support for employment issues. Sectors including transport, social housing, education and IT have been the fastest areas of growth for the department with a particular demand for services around business immigration, employment tribunals, mediation, workplace investigations and strategic workforce planning.</p>

<p>Employment solicitor Amy Hughes said:
“Joining Ward Hadaway was a natural choice given the firm
’s reputation in employment work, and its focus on both innovation and integrity. What stood out to me about Ward Hadaway was not just the quality of work, but also the culture of inclusivity, mentorship, and ambition that
’s evident across the business. I
’m pleased to be working within a technical and experienced employment team that
’s focused on securing the best outcomes for clients.
”</p>

<p>For more information about the employment services Ward Hadaway offers, visit:</p>', 434, '
2023-11-21 17:07:48
', '2023-11-28 00:05:05
', 0, 'https://www.wardhadaway.com/services/employment-lawyers/', 'ward-hadaway-1-1', '2023-11-28 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (421, 'University of Sheffield AMRC', 'Sheffield’s AMRC taps into the power of Creo to bring lifesaving ‘Robo Doctor’ technology closer to market', '<p>New robotic technology that can remotely treat casualties of a terrorist attack or major&nbsp;incident has taken a step closer to entering full production after it impressed at a special Ministry of Defence (MoD) event.&nbsp; Researchers from the University of Sheffield Advanced Manufacturing Research Centre (AMRC), Sheffield Robotics and Department of Automatic Control and Systems Engineering, successfully put its &lsquo;MediTel&rsquo; uncrewed ground vehicle (UGV) through its paces at BattleLab, an initial testing ground for new technologies that can be deployed in hazardous locations.&nbsp; Design engineers took just nine months to come up with a fully working prototype, leveraging the power of PTC&rsquo;s Creo 3D Computer Aided Design (CAD) solution to take a standard UGV platform and two robotic arms to create,what many are calling,</p>

<p>the next generation &lsquo;RoboDoctor&rsquo;.</p>

<p>&nbsp;</p>

<p>One of three novel telexistence technologies funded through a two-phase &pound;2.3m innovation</p>

<p>competition run by the Defence Security Accelerator (DASA), it can be remotely operated</p>

<p>from over one mile away by using Virtual Reality (VR).&nbsp; Rigorous testing has seen it successfully perform a critical initial assessment of a dummy casualty, including taking temperature, blood pressure, heart rate checks and even administering pain relief through an auto-injector.&nbsp; With the positive feedback emanating from its appearance at BattleLab, it is hoped that additional funding can be found to take the prototype forward with the longer-term possibility of scaling up for manufacturing in the UK. David King, Head of Digital Design at</p>

<p>the AMRC, commented: &ldquo;Experts from the MoD were amazed that we have come up with a working prototype in just nine months and within the financial budget that was set. &ldquo;That is in no small part because of the long-standing relationship we have with digital transformation specialist PTC and its industry-leading design software.&rdquo; He continued: &ldquo;We went straight from sketches on paper to quickly designing the first model on Creo, utilising all its simulation and custom features to get there within a matter of weeks.&nbsp; This included how we mounted the robot on the platform, the mechanical engineering and how we integrated the power systems and arms. &ldquo;There wasn&rsquo;t lots of time to build practical models, so we had to complete numerous design iterations to get to where we wanted to be and with an operational &lsquo;MediTel&rsquo; that could be tested on High Bradfield in the Peak District.&rdquo;</p>

<p>
​</p>

<p>The MediTel robot can traverse rough terrain and can be used by operators located in a nearby vehicle or in a central hub.&nbsp; A bespoke VR simulator - utilising the HTC Vive Pro - places the user in the heart of the affected area with a 360-degree camera giving a bird&rsquo;s eye view of the patient and the situation.&nbsp; Importantly, engineers at the AMRC believe any medical person can be trained on how to use the technology within 15 minutes.&nbsp; Elliot Clarke, UKi Regional Director at PTC, added his support: &ldquo;Our software is often at the heart of new innovations, and we are really pleased that it has now helped to create a piece of technology that could save thousands of lives all over the world. &ldquo;Creo is the ideal platform if you are looking for agile design, with the real time iterations allowing different team members to input the changes.&nbsp; &ldquo;The AMRC team has used Creo on hundreds of different projects and their knowledge of its capabilities and functions were imperative in meeting the nine-month project timeframe. With the positive trial results and all the data in play, it wouldn&rsquo;t take much to move this forward into real-world production.&rdquo;</p>', 436, '
2023-11-21 17:12:53
', '2023-12-11 04:19:10
', 0, 'https://www.amrc.co.uk/', 'university-of-sheffield-amrc-1', '2023-11-29 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (422, 'BRITANNIA FIRE', 'Lessons from Valley Parade - how big business can prevent similar disasters', '<p>Two days before Bradford City were to tear down their wooden grandstand, history changed&nbsp;forever. Four minutes after a cigarette was dropped under the stand, a fire erupted that killed 56&nbsp;people and seriously injured 265 more. The 1984/85 football season came to a close with Bradford City closing out as champions of England&rsquo;s Third Division. The final match of the season was due to be a damp squib by all accounts, the title was wrapped up and the final game was a technicality - a chance to celebrate the Bantam&rsquo;s success that season with the fans and players in excellent spirits.&nbsp; Captain Peter Jackson was awarded with the first piece of silverware the club has won in 56 years before the game against Lincoln City. As expected the game was a quiet affair, with a conservative scoreline of 0-0 as the game approached half-time.</p>

<p>What occurred five minutes before the end of the first half though, no one expected. Tragedy struck, as fire ripped through the antiquated wooden grandstand killing 56 and injuring 265 others.&nbsp; Caused by a dropped lit cigarette falling through the stand onto a build-up of litter. In less than four minutes, the entire stand was engulfed in flames. With the fire brigade unable to control the fire due to the enormous size of the blaze. The antiquated wooden stand with bituminous roofing-felt created a perfect recipe for a flashpoint of fire.&nbsp; 38 years since the disaster at Valley Parade, we&rsquo;re reminded by the fire at Luton Airport how easily and quickly a fire can take hold of a public space and threaten thousands of people. In the years following, the Popplewell Inquiry uncovered heart wrenching truths that the fire could have been avoided. The Popplewell Inquiry found that the club had been warned about the fire risk that the rubbish accumulating under the stand had posed. The stand had already been condemned, and the demolition teams were due to start work two days later. One letter from the council said the&nbsp;problems &quot;should be rectified as soon as possible&quot;; a second ominously stated: &quot;A carelessly discarded cigarette could give rise to a fire risk.&quot;</p>

<p>&nbsp;</p>

<p>The outcome of the inquiry led to the introduction of new legislation to improve safety at the UK&
#39;s football grounds. Among the main outcomes of the inquiry were the banning of new wooden grandstands at all UK sports grounds, the immediate closure of other wooden stands deemed unsafe and the banning of smoking in other wooden stands. 38 years after the disaster, we know more about fire safety and prevention thanks to investigationsinto tragic events like this. Thanks for the investigation, sports stadia are now safer than ever before.</p>

<p>Having learnt from the disaster. Here are a few ways big businesses can benefit from the learnings after disasters like this, to ensure they remain fire safe; Fire Risk Assessments</p>

<p>An absolute &lsquo;must -have&lsquo; in modern businesses. A legal requirement in 2023, but sadly only a</p>

<p>relatively recent development. This due-diligence can highlight potential risk, and provide an easy fix for businesses to ensure their premises are safe from fire. A fire risk assessment</p>

<p>would have told managers at Valley Parade that locking fire doors is a huge problem, and not having fire extinguishers available likewise. Factors overlooked by day-to-day managers of the facility.</p>

<p>
​</p>

<p>Wireless Fire Alarms Few football clubs can build billion pound stadiums from the ground up like Tottenham Hotspurs.&nbsp; Many more in the football league have to commit renovations in stages, doing upgrades when the season finishes for two months in the summer. Valley Parade was in desperate need of renovation, and although the works were scheduled for when the season finished in two days, it didn&rsquo;t stop the disaster occurring. Wireless fire alarms need no hard wiring, and can be installed on existing infrastructure in 1/8th of the time wired systems would need. This solution are beneficial to all sports organisation that have to remain open for extended periods, and need to make fire safety precautions quickly and with minimal disruption.</p>

<p>Many businesses in the UK have to contend with opening hours, and they don&rsquo;t want to disturb or inconvenience customers by closing or committing renovations during business hours. A Wireless Fire Alarm System can be added to the building quickly, discretely and above all with no need for part or total closure during installation.&nbsp; While it won&rsquo;t stop a fire, it can alert managers in seconds of a fire, and exactly where it is. Giving authorities the vital time they need to evacuate the facility and fight the fire at its source.&nbsp; P50 Fire Extinguishers</p>

<p>Fighting the fire upon first detecting it is key to controlling the situation. There are several different types of fire from wood, to electrical and gas.&nbsp; The P50 fire extinguisher from Britannia replaces all four types of fire extinguisher. Meaning you can have just one type of extinguisher for all areas to fight all types of fire.</p>

<p>&nbsp;</p>

<p>The P50 fire extinguisher needs little to no training, so virtually anyone can pick it up and start using it. These seconds are crucial to keep a fire under control. At the Valley Parade fire, everyone from players, managers and fans themselves were doing their bit to save lives-</p>

<p>attempting to fight the fire is one way that people can help.&nbsp; The only solution to fire is a comprehensive training schedule for staff, clearly located and usable fire exits and modern early fire detection and alert systems. Sadly, Valley Parade did not have those. We remember the 56 victims of the fire 38 years on. Only by learning the lessons from Valley Parade can we prevent something like this happening again.</p>', 437, '
2023-11-21 17:20:59
', '2023-12-11 04:18:59
', 0, 'https://www.britannia-fire.co.uk/p50/', 'britannia-fire', '2023-11-30 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (423, 'Joshua Ellis', 'Joshua Ellis welcomes six-figure investment into new machinery.', '<p>Yorkshire-based textile manufacturer, Joshua Ellis, has taken a significant leap forward in its pursuit of growth, with a substantial investment of £130,000 into a cutting-edge Picanol jacquard loom.  </p>

<p></p>

<p>The state-of-the-art jacquard loom, installed last month, expands Joshua Ellis
’s design capabilities with an increased capacity of 76 threads per inch, which weaves independently, allowing Joshua Ellis to create intricate designs and fulfil bespoke retail &amp; wholesale orders.  </p>

<p></p>

<p>This substantial upgrade in machinery also significantly improves efficiency and speed, enhancing production capacity and positions the company to adequately cater to the surging demand for traditionally woven textile products both domestically and on the international stage. </p>

<p></p>

<p>The company is still adjusting after the pandemic
’s impact on the textile industry, but this strategic investment aligns with Joshua Ellis
\'s overarching plan for growth.  Oliver Platts, Managing Director at Joshua Ellis, said "We are thrilled to introduce this advanced machinery into our production process. It not only upholds our reputation for excellence but also positions us for growth in an increasingly competitive market. Over the past couple of years, we have witnessed a surge in demand for bespoke jacquard work, and this investment ensures that we can keep up with the expectations of our premium customer base in terms of design and speed of production.”  </p>

<p></p>

<p>In a nod to their rich history dating back to 1767, Joshua Ellis chose a symbolic tribute for the first weave on the new loom  an intricate image of their founder, Joshua Ellis himself.  </p>

<p></p>

<p>Penny Lovatt, Senior Designer at Joshua Ellis, said "Investing in this new loom is an extraordinary decision for the company. As a designer, the new loom opens up a world of creative possibilities, allowing me to experiment with designs that were previously unattainable on our older Dobby looms. It was particularly gratifying to breathe life into the image of Joshua Ellis, our founding visionary, as a fitting tribute." </p>

<p></p>

<p>Joshua Ellis, under its parent company SIL Group, continues to invest in the future of Yorkshire
\'s textile industry ensuring its 21st-century relevance by meeting the demands of global retail and wholesale clients. </p>', 438, '2023-11-21 17:24:45', '2023-12-04 00:05:05', 0, 'https://joshuaellis.com/', 'joshua-ellis', '2023-12-04 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (424, 'GRIPPLE', 'A WORLD OF OPPORTUNITY FOR GRIPPLE GRADUATES', '<p>Gripple, the Sheffield-headquartered, employee-owned manufacturer of wire joiners and tensioners and suspension solutions for agriculture, construction, solar and infrastructure, has appointed two new graduates, plus an undergraduate placement student to the business, demonstrating its commitment to investing in its future talent.</p>

<p></p>

<p>Joining the Gripple Graduate Scheme is Jacob Bradder, a mechanical engineering graduate of Sheffield Hallam University and Claudia Creswell, an economic and modern languages graduate from the University of Sheffield. In addition, Samuel Valentine, a product design engineering student from Loughborough University has joined Gripple on an undergraduate placement. Samuel is spending a year working in product design within the Ideas &amp; Innovation team at the Innovation Centre in Sheffield, gaining real life experience of product design and design engineering.</p>

<p></p>

<p>Jacob and Claudia were successful in applying for Gripple
’s scheme, which always receives a huge number of applications and is in its eleventh year.   They will spend 16 months rotating around the business, spending several weeks in a number of key departments, including engineering and advanced manufacturing, marketing, finance, digital transformation, IT and sales, where they will work on specific projects and get real hands-on experience of what it
’s like to work in each department. </p>

<p></p>

<p>To enhance their personal development, each graduate is allocated a
‘home
’ department for the duration of the programme and is encouraged to find a senior mentor in the business, whilst being supported by Gripple
’s People and Culture team.</p>

<p></p>

<p>Emma Hibbert, Recruitment Manager at Gripple, explains;
“What makes our grad scheme different is that our graduates can, and indeed are expected to make an impact from day one and they are involved in real-life projects that add value to the business.  They are able to experience many different areas of working at Gripple, which helps them build up their knowledge and reach the right decision as to the best role for them within the company at the end of the programme.
”</p>

<p></p>

<p>As part of the Gripple Graduate Scheme, the graduates are expected to organise a major company event to raise money for charity.  This gives them the experience of using skills in organisation, networking, sales and negotiation.</p>

<p></p>

<p>Recent graduates Kris Chua and Becky Malkin this summer raised more than
£6,000 with their GLIDEFest event this summer, which was split between St Luke
’s Hospice, BEAT (UK eating disorder charity) and the Gripple Foundation charity of the year, Create a Dream Foundation. The event was a huge success and was held at Gripple
’s GLIDE House in Attercliffe with live music, a barbecue, games, raffles and fundraising activities.</p>

<p></p>

<p>Gripple is committed to its people and is dedicated to its unique culture
‘the Gripple spirit
’ that sets it apart.  The company aims to recruit people who share its values  of fun, innovation, passion, entrepreneurship, teamwork, integrity - who are invested in the shared purpose to be a fun, rewarding place to work.  As an employee-owned business, Life at Gripple isn
’t just about getting the job done, it
’s about contributing to the present and working towards an ambitious future.</p>

<p></p>

<p>Gripple will shortly be launching its 2024 scheme, with pathways on offer including Technology, Commercial, People &amp; Culture and Operations, with the company looking to recruit graduates with a specific interest in business IT, business management and Human Resources. </p>

<p></p>

<p><strong>Anyone interested in applying for the 2024 Gripple Graduate Scheme should visit </strong><a href="https://www.gripple.com/gripple-careers/graduates-placements/">https://www.gripple.com/gripple-careers/graduates-placements/</a></p>', 439, '2023-11-21 17:26:37', '2023-12-05 00:05:05', 0, 'https://www.gripple.com/', 'gripple-12-2023', '2023-12-05 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (425, 'Ward Hadaway', 'Leeds law firm nurtures homegrown talent with new trainees', '<p>Leading Northern law firm Ward Hadaway has announced the recruitment of three new trainees into its Leeds office, with 13 trainees recruited nationally as it continues to grow.</p>

<p>&nbsp;</p>

<p>Ward Hadaway welcomes Aimee Winterbottom, Elizabeth Collings and Ross Henderson to the team. The new recruits take the number of trainees in the Leeds office to five as the firm strengthens its commitment to developing the next generation of lawyers.</p>

<p>&nbsp;</p>

<p>Caroline Jones, recruitment and emerging talent manager at the firm, said: &ldquo;We&rsquo;re delighted to welcome new trainees to our team in Leeds. At Ward Hadaway, we believe that nurturing talent and providing the right environment for growth is paramount. Our commitment to employee development is a priority, and we are excited to see our trainees thrive in our inclusive, supportive culture.&rdquo;</p>

<p>&nbsp;</p>

<p>By expanding its trainee programme, the firm continues to highlight its commitment to investing in emerging talent, shaping the legal landscape for years to come. Trainees embark on a two-year training contract, rotating through four distinct practice areas every six months, ensuring they acquire the necessary skills and expertise to practice as solicitors upon qualification.</p>

<p>&nbsp;</p>

<p>Emma Digby, executive partner in the Leeds office, said: &ldquo;It&rsquo;s great to welcome more talented people who are eager to learn and follow a successful career in law. Our commitment to employee development and ensuring everyone is given the opportunities to fulfil their potential is central to the firm&rsquo;s ethos. Having the right individuals with the necessary skills and capabilities is vital for us to achieve our vision and continue to grow, creating job opportunities for our future legal practitioners. By providing an environment where trainees can excel, Ward Hadaway is not only shaping its own future but also contributing to the development of the legal profession as a whole.&rdquo;</p>

<p>&nbsp;</p>

<p>Trainees are a key part of the firm&rsquo;s people strategy with approximately 30% of the organisation&rsquo;s current management team beginning their careers as Ward Hadaway trainees.</p>

<p>&nbsp;</p>

<p>Aimee Winterbottom, one of the Leeds-based trainees, said: &ldquo;I&rsquo;m a big advocate of the recruitment process I experienced at Ward Hadaway. They were genuinely interested in my aspirations and career goals. Since I joined, the commitment to helping trainees step up to the newly qualified role and reach other professional milestones has been highly evident, which makes me confident about my long-term future at the firm.&rdquo;</p>

<p>&nbsp;</p>

<p>For more information about Ward Hadaway and its trainee programme, visit:&nbsp;<a href="https://www.wardhadaway.com/join-us/trainees-apprentices/trainees/">https://www.wardhadaway.com/join-us/trainees-apprentices/trainees/</a></p>', 440, '2023-11-21 17:34:56', '2023-12-11 04:18:50', 0, 'https://www.wardhadaway.com/', 'ward-hadaway-2-1', '2023-12-06 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (426, 'HS2', 'Lessons must be learned from HS2 says compulsory purchase and planning expert', '<p>A leading figure in the compulsory purchase and planning process says lessons must be learned after hundreds of property owners have been left out of pocket after the northern leg of HS2 was cancelled.</p>

<p>Jon Stott is Group Managing Director of Ardent &ndash; a leading property and consents management practice with offices across the UK &ndash; and a former chair of the Compulsory</p>

<p>Purchase Association.</p>

<p>He said the decision damages the levelling up agenda and serious ramifications for property and landowners along the route of the proposed line.</p>

<p>Stott said: &ldquo;This will have a massive impact on the UK&rsquo;s reputation as a place to invest and no amount of spin can underplay the damage the decision does to the north/south levelling-up agenda. &ldquo;Aside from the macro factors, today&rsquo;s decision is also a travesty for so many individuals and businesses and it raises huge questions about how we protect landowners and property owners whose land is earmarked to be acquired for major projects in the future. &ldquo;The statutory blight provisions are very narrow and mean that only owner-occupiers of property in certain circumstances can force the Government to acquire their land/property once it has been identified as potentially being required for a major scheme. &ldquo;HS2 introduced various discretionary hardship schemes that did help some additional people but still left many landowners and developers without any ability to sell their property, leaving them in a horrible state of limbo whilst their land has also been subject to safeguarding directions &ldquo;In addition to being unable to sell, the safeguarding directions have effectively precluded landowners from obtaining their own planning consent for any form of development.</p>

<p>This has impacted huge swathes of land and thousands of landowners for over a decade.&nbsp; &ldquo;I am aware of hundreds of property owners who have incurred thousands of pounds of costs in engaging with HS2. Whilst some have been opposing the scheme, the majority have simply been seeking certainty, or design changes that would reduce the impact on their lives and businesses. These property owners have no route to being reimbursed and, in light of this new announcement, it means they have incurred the costs totally unnecessarily.&nbsp; &ldquo;One particular major landowner has incurred close to &pound;1 million in legal and other fees, trying to work with HS2 to maximise the benefits of the Birmingham to Manchester leg &ndash; it is a scandal that they will have no route to recovering that sum now the project has been scrapped.</p>

<p>
​</p>

<p>&ldquo;In addition to financial impacts, there&rsquo;s a human aspect to all of this too which should not be underestimated. Thousands of businesses and families have already been displaced, in</p>

<p>many cases causing significant mental and emotional anguish.&nbsp; &ldquo;It&rsquo;s hard to imagine how they are feeling knowing that it has all been completely unnecessary. The public benefits for their displacement will now never be realised and, instead, we will be left with huge visible scars on the landscape, particularly between Birmingham and Crewe, where massive construction activity has already been undertaken.&nbsp; &ldquo;Sadly, I&rsquo;m aware of multiple stories of people who have felt suicidal and who have suffered significant mental health issues as a consequence of the threat of being compulsorily purchased, and the uncertainty that has caused for them. Of course, it opens up bigger questions about the process on all schemes and lessons must be learned and acted upon.&rdquo;</p>', 441, '
2023-11-21 21:52:42
', '2023-12-11 00:05:05
', 0, 'https://www.hs2.org.uk/', 'hs2', '2023-12-11 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (427, 'Clegg Construction', 'Clegg Construction stages topping out ceremony at apartment scheme in Leeds', '<p>Contractor Clegg Construction has held a topping out ceremony at an 11-storey apartment complex being built for property developer Rise Homes in Leeds.</p>

<p></p>

<p>Spinners Yard is a 185-apartment, U-shaped, build-to-rent scheme in Regent Street in the Mabgate area of Leeds City Centre.</p>

<p></p>

<p>It is the second build-to-rent project Clegg Construction has undertaken in partnership with Rise Homes and follows the recent completion of The Ironworks - a
£28.7m, 11-storey apartment</p>

<p>development in Sheffield.</p>

<p></p>

<p>Featuring a mix of studio, one, two and three-bedroom apartments, Spinners Yard is due to be completed in 2025.</p>

<p></p>

<p>Clegg Construction operations director, Darren Chapman, said: "We are very pleased to have reached the key topping out stage for this development. This will allow us to continue adding the external walls and glazing up the building, followed by the internal fit-out.</p>

<p></p>

<p>"Spinners Yard is regenerating brownfield land and will breathe new life into the area, providing a desirable residential development with a range of properties to rent."</p>

<p></p>

<p>The ground floor of Spinners Yard will incorporate a plant room, enclosed parking for cars, motor bikes and cycles to one half with the other containing the entrance lobby and reception, the centre management office, a lounge, break out and work spaces, and a gym.  Two staircases/lift areas provide access to upper residential floors. The first floor will incorporate a residents
\' garden space with another spacious roof garden on the tenth floor.</p>

<p></p>

<p>Once completed, the development will feed into the Leeds PIPES District Heat Network.</p>

<p></p>

<p>Nigel Rawlings, CEO of Rise Homes, added: "We are delighted with Clegg\'s progress at Spinner\'s Yard and we are very much looking forward to the completion of this sustainable development linked to the Leeds City energy from waste plant offering high-quality, spacious apartments at both affordable and market rents featuring many other environmental benefits."</p>

<p></p>

<p>During construction work on the Spinners Yard site, Clegg Construction has been hosting groups of Leeds College of Building Trade and Management students, aged 16 to 19, to provide them with valuable insight into the sector. The students have also been visiting the site next door, where Clegg Construction is building The Fabric Works, a 402-bed student accommodation scheme.</p>

<p></p>

<p>In addition, Clegg Construction is sponsoring a local Army Reserve (formerly the Territorial Army) squadron in Leeds by providing T-shirts worn as part of the uniform and during physical training, and is currently collecting food, toiletries and other items for the Blessed of the Father Food Bank, part of Leeds Food Aid Network, at its Spinners Yard and The Fabric Works sites.</p>

<p></p>

<p>The company is very familiar with Leeds, having delivered the external recladding of the Premier Inn Leeds City Centre Arena hotel in 2021, the replacement of flammable cladding on the high-rise Waterside Apartments development during the same year, and the design and build of the Premier Inn Leeds Headingly Hotel (constructed on top of the Arndale shopping centre) in 2017. More recently, Clegg Construction started working on a multi-million-pound refurbishment of the Cosmopolitan Hotel in the city.</p>

<p></p>

<p>Clegg Construction is a Midlands, East Anglia, and Yorkshire-based construction firm specialising in the delivery of public and private sector projects.  </p>

<p></p>

<p>The company works with organisations of all sizes and specialties across a range of different sectors.</p>

<p><br>
For more information visit <a href="http://www.cleggconstruction.co.uk/">www.cleggconstruction.co.uk</a></p>', 442, '
2023-12-16 07:53:11
', '2023-12-16 07:53:11
', 0, 'https://cleggconstruction.co.uk/', 'clegg-construction-1', '2024-01-03 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (428, 'Banks Group', 'BANKS GROUP TAKES ON LARGER YORKSHIRE OFFICE AS PART OF COUNTY INVESTMENT AND JOB CREATION PLANS', '<p>Property, mining and energy firm the Banks Group has move into bigger Yorkshire premises as part of fulfilling its investment and job creation plans in the county.</p>

<p>Banks took on a new office at Harewood Yard, close to Harewood House, in 2020 as it looked to increase its project portfolio across the county,&nbsp;and has quickly increased its Yorkshire-based team from two people to nine.</p>

<p>The family-owned firm has now taken on a 1,400 sq ft office in the same location, and is expecting to create a number of additional new jobs in&nbsp;the county over the next 12 months, with space available in the new office to accommodate future new recruits.</p>

<p>The office, which is located in a converted Grade II-listed building, is also used by staff from Banks&rsquo; County Durham headquarters who are&nbsp;working on local projects.</p>

<p>The Banks Group has a long track record of successful Yorkshire&nbsp;developments across its property, renewable energy and mining&nbsp;businesses.</p>

<p>It has developed housing schemes in Sheffield, Harrogate and Leeds, and&nbsp;currently has 18 residential property schemes at different stages of&nbsp;development across the county, including projects in North Yorkshire and&nbsp;Wakefield that will see planning applications submitted to their&nbsp;respective local councils in the near future.</p>

<p>They include the first site in Yorkshire to be brought forward by new&nbsp;regional housebuilder Banks Homes, which was launched earlier this year&nbsp;by the Banks Group and which is now focusing on sites of all sizes&nbsp;across Yorkshire and North East England.</p>

<p>Banks Property also secured planning permission over the summer for a&nbsp;residential development of up to 390 new homes at Barton-upon-Humber.</p>

<p>Rob Ormrod, head of development - Yorkshire at Banks Property, says: &ldquo;We&nbsp;made increasing our presence and range of projects across Yorkshire a&nbsp;strategic priority for the business when we first moved to Harewood&nbsp;three years ago and we&rsquo;ve made a great deal of progress since then.</p>

<p>&ldquo;We&rsquo;ve now got access to the extra workstations and meeting rooms that&nbsp;we&rsquo;ll need as we continue to grow while retaining the excellent highway&nbsp;links and superb location that Harewood Yard has always offered.</p>

<p>&ldquo;We have a range of different property projects in the pipeline right&nbsp;across Yorkshire and expect to be continuing to further expand our&nbsp;county-based team in the next year as we work towards bringing these plans to fruition.&rdquo;</p>', 443, '
2023-12-16 08:01:22
', '2023-12-16 08:01:28
', 0, 'https://www.banksgroup.co.uk/', 'banks-group', '2024-01-03 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (429, 'Brighouse Town Deal', 'Milestone for Brighouse Town Deal as plans progress for new market', '<p>Plans for a new, revitalised market at the heart of Brighouse have moved forward as part of the £19.1 million investment in the town centre.</p>

<p></p>

<p>Following the three-week engagement process in the summer by Calderdale Council and the Brighouse Town Deal Board, the design of the distinctive new market building has been finalised, a planning application is being submitted, and the tender process to find a contractor is due to start soon.</p>

<p></p>

<p>As part of the UK Government-funded Town Deal, the
£3 million revitalised market on the existing canalside site will have around 20 fixed stalls, each with water, drainage and power. There will also be a flexible central space that can be used for events or additional pop-up stalls to encourage new business start-ups.</p>

<p></p>

<p>Alongside spaces to sit, rest and meet, with views over the canal, there will be new toilets for traders plus storage, to create a modern, attractive and vibrant market with the aspiration to open more days of the week.</p>

<p></p>

<p>Other features will include an improved pedestrian area at the front and level access to the market; secure, decorative gates featuring a Brighouse-themed design; new cycle parking; and new entrances and exits to provide better connections to the town.</p>

<p></p>

<p>Subject to planning approval, work on the new market is due to start in spring 2024 and be complete by spring 2025.</p>

<p></p>

<p>With the build getting closer, work has been ongoing to identify a site for a temporary market to ensure stallholders can continue to trade, shoppers can continue to buy, and the rest of the town centre can operate as usual.</p>

<p></p>

<p>A location has now been provisionally secured and the current market will temporarily move to Daisy Street car park for around 12 months while construction is underway. Just across Anchor Bridge from the current Ship Street location, this will provide continuity for the market, supporting its current tenants and providing space for temporary market stalls. More details about the move to its temporary location will be provided closer to the time.</p>

<p></p>

<p>The market revitalisation is just one of the Brighouse Deal projects, powered by Levelling Up, which aims to transform the town centre, making it a more attractive place to visit and spend time and money  benefitting residents, shoppers, visitors and businesses.</p>

<p></p>

<p>Other Brighouse Deal projects include developing public areas of the town centre so they are more welcoming for people walking and cycling into the town; encouraging more people into the town centre for longer, with more to do at all times of the day and night; providing a quality event and community space at the historic Thornton Square; improving links between the canal and the town centre; and providing access to education, apprenticeships and jobs by building on Brighouse
’s manufacturing heritage.</p>

<p></p>

<p>The Brighouse Deal is a joint project between the town
’s private and voluntary sector, community and residents
’ groups, Calderdale Council and Calderdale College.</p>

<p></p>

<p>Cllr Sarah Courtney, Calderdale Council
’s Cabinet Member for Towns, Tourism and Voluntary Sector, said:</p>

<p></p>

<p>
“It
’s great to see our vision for Brighouse coming to life, starting with the market milestones. Already a vibrant town, we want to build on its distinctive character. Together with the Town Deal Board, we are striving to give local people and visitors an even better experience, day and night, by reinvigorating the town centre.</p>

<p></p>

<p>
“The Town Deal projects aim to create a place that
’s great to spend time in, great to walk and cycle in, a place to be connected to green spaces and nature, and a place to work and build skills  all supporting people
’s health and wellbeing, Calderdale
’s action to tackle the climate emergency and our priority for thriving towns and places.
”</p>

<p></p>

<p>Cllr Silvia Dacre, Cabinet Member for Resources, added:</p>

<p></p>

<p>
“Markets are an important and valued part of high streets across Calderdale. Investing in Brighouse
’s open market to bring fantastic new facilities will boost the shopper, visitor and trader experience.</p>

<p></p>

<p>
“We hope this will also encourage new businesses and start-ups, providing a route for them to use pop-up stalls occasionally, through to fixed market units, to permanent commercial space in the town centre.
”</p>

<p></p>

<p>Cllr Howard Blagbrough, Chair of the Brighouse Town Board, said:</p>

<p></p>

<p>
“The open market is an important part of Brighouse, and we have been determined to ensure it has an even brighter future, which is why I am delighted we are now in a position to submit the planning application and move forward with the project.</p>

<p></p>

<p>
“Ensuring we have a temporary market site that allows our current traders to continue to trade while construction takes place is another vital part of the scheme, and we will be working with the stallholders and the Council
’s markets team to ensure this is suitable for them, that shoppers know where the temporary market is, and they are encouraged to visit, shop and spend.</p>

<p></p>

<p>
“After years of discussion and debate, starting work on these projects will be an important moment and I am looking forward to that happening, all being well, in just a few months
’ time.
”</p>

<p></p>

<p>For more information about the Brighouse Town Deal, visit <a href="http://www.brighousedeal.co.uk/">www.brighousedeal.co.uk</a> and <a href="http://www.calderdalenextchapter.co.uk/">www.calderdalenextchapter.co.uk</a>, which also includes details of other major projects across Calderdale.</p>

<p></p>

<p>The Town Deal programme aims to regenerate towns and deliver long-term economic and productivity growth. This is through investments in urban regeneration, digital and physical connectivity, skills, heritage and enterprise infrastructure. As of July 2021, the Department for Levelling Up, Housing and Communities has offered Town Deals to all 101 places that submitted proposals, committing over
£2.35bn of investment across c.700 projects nationwide.</p>

<p></p>

<p>Distinctiveness and enterprise are key themes of the Vision 2024 for Calderdale. The year 2024 marks Calderdale
’s 50th birthday and there
’s less than a year to go. Where do we want to be by 2024? Get involved with the debate on X with
#VisionCdale2024 and find out more at <a href="http://www.calderdale.gov.uk/vision">www.calderdale.gov.uk/vision</a></p>', 444, '
2023-12-16 08:07:22
', '2023-12-16 08:07:22
', 0, 'https://www.brighousedeal.co.uk/', 'brighouse-town-deal', '2024-01-04 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (430, 'COPTRZ', 'COPTRZ AND XER TECHNOLOGIES FLY HIGH WITH NEW PARTNERSHIP', '<p>The UK&rsquo;s leading provider of commercial drones, Coptrz has announced a close partnership with Xer Technologies &ndash; the Swiss leading manufacturer of customisable, heavy-duty Unmanned Aerial Systems (UAS).</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>The partnership will see Leeds-based Coptrz become the exclusive distributor of the Xer X8 UAS in the UK, with its marketing focused mainly on infrastructure inspection, surveillance and search and rescue applications.</p>

<p>&nbsp;</p>

<p>The Xer X8 has the ability to fly for 2.5 hours with a 3kg payload and can be used for beyond visible line of sight (BVLOS) operations. It can carry up to 7kg and handle adverse weather and wind.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>G&ouml;kmen&nbsp;&Ccedil;etin, international sales manager for Xer, said: &ldquo;Working alongside Coptrz allows us to get quick access to the UK market through an experienced and competent partner. With&nbsp;Coptrz&rsquo; experience and its reputation as&nbsp;a leading provider of commercial drones,&nbsp;they meet the high requirements we put on our collaboration partners. Our collaboration will open up the UK market to Xer X8 and its heavy-duty design, long flight time and long range BVLOS operations.&rdquo;</p>

<p>&nbsp;</p>

<p>Being involved with drones since 2011, Switzerland-based Xer works to create a safer, and more efficient and sustainable world through its heavy duty UAS. The company was an early pioneer in commercial drone flying, enabling its customers to generate actionable data insights.</p>

<p>&nbsp;</p>

<p>Paul Luen, CEO and founder of Coptrz, which has grown from 2016 start-up to a &pound;27million turnover business, added: &ldquo;As part of our&nbsp;360&ordm; drone solution offering, we work hard to build the right partnerships, with the right manufacturers at the right time. The quality of drones offered by Xer is second to none and offer the opportunity for growth across UK and European markets. We are thrilled to be working with the team to capitalise on this growth potential and to further strengthen our reputation as being the UK&rsquo;s home of the latest drone technology.&rdquo;</p>', 445, '
2023-12-16 08:44:37
', '2023-12-16 08:44:46
', 0, 'https://coptrz.com/', 'coptrz-2024', '2024-01-04 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (431, 'Progeny', 'Progeny announces acquisition of Carbon Financial Partners', '<p>Multi-disciplinary professional services firm,&nbsp;<a href="https://theprogenygroup.com/">Progeny</a>, has announced plans to acquire Chartered financial advice firm,&nbsp;<a href="https://carbonfinancial.co.uk/">Carbon Financial</a>&nbsp;Partners.</p>

<p>&nbsp;</p>

<p>The deal will add &pound;600m to Progeny&rsquo;s assets under management.</p>

<p>&nbsp;</p>

<p>Carbon Financial Partners are a firm of Chartered financial planners, with a team of 45 staff across offices in Edinburgh, Glasgow, Aberdeen and Perth.</p>

<p>&nbsp;</p>

<p>They offer a personal service to clients with complex financial needs, business leaders and those approaching important life transitions, with a focus on making a meaningful impact on their lives.</p>

<p>&nbsp;</p>

<p>Gordon Wilson, Managing Director, Carbon Financial Partners, said: &ldquo;Progeny share our investment philosophy and our values. It was critical for the team that we found the right fit for our clients and we have achieved that.</p>

<p>&nbsp;</p>

<p>&ldquo;Combining with Progeny will help us to take our service and advice offering to new levels.</p>

<p>&nbsp;</p>

<p>Progeny CEO, Neil Moles, said: &ldquo;We are highly purposeful in our approach to selecting the businesses we bring into Progeny.</p>

<p>&nbsp;</p>

<p>&ldquo;We apply a set of strict criteria, with a laser focus on high quality firms that add a great deal of value.</p>

<p>&nbsp;</p>

<p>&ldquo;Welcoming Carbon Financial Partners to Progeny will enable us to consolidate and strengthen our existing presence in Scotland and I look forward to what we can achieve together for our clients.&rdquo;</p>

<p>&nbsp;</p>

<p>A team from global law firm Squire Patton Boggs acted as legal adviser to Progeny during the deal.</p>

<p>&nbsp;</p>', 446, '
2023-12-16 08:47:09
', '2024-01-15 08:06:48
', 0, 'https://theprogenygroup.com/', 'progeny-1-1', '2024-01-08 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (432, 'Another Concept', 'New Digital Marketing Agency, Another Concept, Launches in Leeds', '<p>A new, Leeds-based digital marketing agency has been launched, seeking to redefine the typical agency model by providing quality services for clients, but with a more open, accessible and honest approach.</p>

<p>&nbsp;</p>

<p>Aptly named &lsquo;<a href="https://anotherconcept.co.uk/">Another Concept</a>&rsquo;, the agency is the brainchild of four digital marketing experts with more than 50 years of combined experience: Alex Gregory, Marcus Hearn, Tom Brook and Rich Hart. All proud, no-nonsense Yorkshiremen, the quartet first met back in 2015 when they worked at one of the largest digital marketing agencies in the UK. Although the four had different specialisms, they immediately developed a strong bond that they&rsquo;ve maintained ever since.</p>

<p>&nbsp;</p>

<p>Prior to starting Another Concept, the co-founders each held senior roles at digital agencies. Between them they&rsquo;ve also worked in both in-house positions and on a freelance basis for some of the country&rsquo;s biggest brands. But, despite this, the four all claim that they&rsquo;ve never lost sight of their shared belief that clients deserve more from their agency partners.</p>

<p>&nbsp;</p>

<p>Motivated by their collective experience and their strong desire to change the perception of agency relationships, the four have started Another Concept and now offer a variety of digital marketing services including organic search, content, digital PR and outreach. However, they promise to deliver these in an open and collaborative way that offers clients a direct route of communication to their expertise, as and when they need it. They will also be offering a &lsquo;fluid&rsquo; approach to strategy and retainers that puts performance first.</p>

<p>&nbsp;</p>

<p>Speaking about Another Concept&rsquo;s launch, co-founder, Tom Brook, who heads up the content marketing services, offered these comments:</p>

<p>&nbsp;</p>

<p>&ldquo;All four of us have worked in a number of situations where we felt things could and should have been done differently. Often that was simply because we just wanted to do the best we could for the client. Sadly though, too many digital marketing agencies are stuck in a set way of working and are too busy thinking about profitability rather than performance.</p>

<p>&nbsp;</p>

<p>&ldquo;There are some good agencies out there, but I think far too many aren&rsquo;t maximising their clients&rsquo; budgets or delivering the best possible ROI. I also think many agencies have far too many layers of account management. At Another Concept, we&rsquo;re giving people direct access to experts. It means we can get work done faster, work holistically and respond to client queries in a much more timely manner.</p>

<p>&nbsp;</p>

<p>&ldquo;Ultimately, with Another Concept we just went back to the drawing board. We highlighted what we felt was problematic with the current agency model and vowed to take a new, better approach that will ensure clients get quality services without the nonsense. We want to be an extension of our clients&rsquo; teams and more like a partnership, rather than a standalone third-party that floats about on the edge.&rdquo;</p>

<p>&nbsp;</p>

<p>Alongside this, co-founder, Rich Hart, who leads on the Digital PR service, spoke about how excited the team is to work together as both digital marketing experts and friends:</p>

<p>&nbsp;</p>

<p>&ldquo;I think it&rsquo;s a rarity to find an agency these days that has the shared core values that we have and we have our collective wealth of experience - and of course the fact we&rsquo;ve known each other so long - to thank for that.</p>

<p>&nbsp;</p>

<p>&ldquo;Personally, I couldn&rsquo;t think of a better group of people to have around and work with here. But also I&rsquo;m proud to have co-founded Another Concept, and I think I speak for all of us, when I say that we can&rsquo;t wait to properly get out there into the online markets and deliver some excellent results.&rdquo;</p>

<p>&nbsp;</p>

<p>Following the launch, Another Concept is looking to grow its client base within a variety of sectors, but has already been working with a number of firms within the sports niche. This includes providing a number of services for leading rugby news website, RugbyPass - from which the team has already achieved some excellent results - as well as content and digital PR work for several other sports organisations.</p>', 447, '
2023-12-16 08:48:27
', '2024-01-15 08:12:36
', 0, 'https://anotherconcept.co.uk/', 'another-concept-01-2024', '2024-01-08 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (433, 'HyperFinity', 'HyperFinity launches retail media product to supercharge first-party data', '<p>Leading decision intelligence experts,&nbsp;<a href="https://hyperfinity.ai/">HyperFinity</a>, have launched a brand new retail media product, designed to help retailers leverage their first-party data, maximise the impact of advertising for brands, and achieve a potential sales uplift of 20% for targeted retail media campaigns.&nbsp;</p>

<p>&nbsp;</p>

<p>The Leeds-based company has been supporting several leading retailers, assessing key data points and building targeted audiences. The product launch comes as retail media&rsquo;s global advertising revenue is forecast to exceed television revenue by 2028, according to<a href="https://www.groupm.com/mid-year-advertising-forecast-2023/">&nbsp;GroupM</a>.&nbsp;</p>

<p>&nbsp;</p>

<p>HyperFinity&rsquo;s platform links together customer spend data, product attributes, and web browsing data to deliver precise insight into consumer behavior and needs. These insights are used to precisely target consumers with highly relevant products on retailers&rsquo; websites, apps and in-store. HyperFinity has seen sales uplifts of 20% for targeted campaigns driven by the platform.&nbsp;</p>

<p>&nbsp;</p>

<p>Peter Denby, Chief Commercial Officer at HyperFinity, commented:&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;We&rsquo;ve been working on our retail media offering under the radar for a while now, and we&rsquo;re seeing fantastic results. Our platform enables retailers to improve their targeting and personalisation, then, assess campaigns effectively with closed-loop measurement. We can attribute retail media success by measuring ROI and return on ad spend.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;Retailers are embracing retail media with open arms. They see it as a triple win. Customers are served more relevant ads, brands reach their target audience more effectively, and retailers increase profitability. Ultimately, it&rsquo;s a space that&rsquo;s only set to keep growing, as retailers look for ways to cut wasted ad spend and improve customer loyalty, whilst also supercharging profits.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>HyperFinity has also launched an eBook, which uncovers how data can fuel the retail media revolution for retailers looking to diversify their income streams. The eBook can be downloaded&nbsp;<a href="https://hyperfinity.ai/how-is-data-fuelling-the-retail-media-revolution?utm_source=linkedin&amp;utm_medium=social+media&amp;utm_campaign=retail+media+ebook&amp;utm_content=brand+account">here</a>.&nbsp;</p>', 448, '
2023-12-16 08:49:44
', '2024-01-15 08:06:56
', 0, 'https://hyperfinity.ai/', 'hyperfinity-1', '2024-01-09 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (434, 'CORNISH BAKERY', 'CORNISH BAKERY READY TO OPEN IN HARROGATE', '<p>One of the UK&rsquo;s fastest growing independent bakery companies, Cornish Bakery, is set to open its doors in the North Yorkshire Victorian spa town of Harrogate on Monday 30th&nbsp;October.</p>

<p>&nbsp;</p>

<p>Located on James Street, which was the most fashionable street in Harrogate in the early 1900s, the new bakery will occupy a beautiful 19th century property built by George Dawson. Originally the home of a chemist and later part of the Ogden jewellery store, the new Cornish Bakery has retained some striking original features with gilded columns and stained-glass windows. The original cast iron canopy has been left in its natural black and the Cornish Bakery signage has been designed to match, remaining in keeping with the local conservation area.</p>

<p>&nbsp;</p>

<p>Given the company&rsquo;s Cornish heritage, they always use new bakery opportunities to showcase the products and artworks of the county&
#39;s talented makers. The new Harrogate bakery is adorned with lights from Cornwall-based Tom Raffield and Studio Haran and there are also inspirational pieces from South West-based artists Sophie Harding and William Luz.</p>

<p>&nbsp;</p>

<p>Cornish Bakery Founder and Owner Steve Grocutt said: &ldquo;We are delighted to be opening our Cornish Bakery in the beautiful spa town of Harrogate. We are on a constant journey to redefine what a bakery is does and what it can be, and we therefore build all our bakeries differently.&nbsp;We have taken on this historic Harrogate property, uncovering some interesting features within it that will be showcased in the beautiful interior we have created. We also believe a bakery should be part of the community so our newly employed team will be actively seeking opportunities&nbsp;to work with local organisations in the Harrogate area.&rdquo;</p>

<p>&nbsp;</p>

<p>A perfect breakfast, lunch or afternoon stop off for shoppers, visitors, residents and spa-users alike, the Harrogate Bakery Manager Stephen and his team will be ready to welcome customers from 8am - 6pm every day of the week after their opening next Monday.</p>', 449, '
2023-12-16 08:51:14
', '2024-01-15 08:07:04
', 0, 'https://thecornishbakery.com/', 'cornish-bakery', '2024-01-09 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (435, 'Design Odyssey', 'Bathroom products design from concept to manufacture', '<p>Nothing is stopping Paul Hernon as he continues to follow his dream of improving people&rsquo;s lives&nbsp;and wellbeing with his World&rsquo;s First Vertical Bathroom, Vertebrae&reg;</p>

<p>This year&rsquo;s Homebuilding and Renovation Show will take place in just a few weeks&rsquo; time, and this year the home extenders are set to meet quite the space-saving invention.
..</p>

<p>Manchester-born inventor, Paul Hernon has forever had an inquisitive nature and appreciated all facets of design and architecture, and it was this which led to his first job out of school, as a Trainee Architectural Technician.</p>

<p>Paul had to readjust to the construction recession of 1992, by continuing his studies and achieving a National Certificate in Building Studies, a Certificate in Construction, and a Diploma in AutoCAD in 1995.&nbsp; In his mid-twenties, Paul&rsquo;s curiosity for gadgets, everyday objects and how they were designed and made, led him to complete a National Certificate (BTEC) in 3D Design. This was followed by a four- year BA Hons in Industrial Design. A work placement year, at Bradford-based, Jacuzzi&reg; further confirmed his love was product design.</p>

<p>In the final year of the degree, he researched and explored space-saving objects and architecture and how they would feature in our ever-growing world, and it was one night in the design studio, that the initial sketch ideas for the Vertebrae &reg; were visualized.</p>

<p>&nbsp;</p>

<p>Whilst working at Jacuzzi full time now, Paul&rsquo;s interest in the architecture of the bathroom continued and as with his curious nature, he continued to question the numerous functions and applications commonly known in this multi-functional space. In 2008 Paul got a Personal Loan, and created an aluminium prototype which he took along to the KBB Exhibition at the NEC, and attracted interest. The prototype was also featured that year in The Sony &lsquo;House of the Future&rsquo; display at the Grand Designs Show in London. Upon viewing the product at the NEC, a Business Consultant related to Paul&rsquo;s vision for the Vertebrae&reg; and they worked together to further the this. Sadly, the Business Consultant did not recover from a short illness and passed away. Paul was devastated and had lost a friend as well as a business partner.</p>

<p>&nbsp;</p>

<p>Paul was now working in the Ventilation industry, taking products from his conception through to&nbsp;manufacturing and into the marketplace. However, his true desire to pursue the Vertebrae &reg;</p>

<p>would not relent. He was back on the road using his holidays to meet suppliers, manufacturers, funding and grant bodies, and building more prototypes. However, his plans were derailed yet again due to an illness which allowed him to evaluate what was important in life. He concluded that he must continue with his endeavours in bringing this innovative, space-saving product to market to fulfil his desire of improving people&rsquo;s lives through ingenious product design.</p>

<p>In 2020 he installed a pre-manufacture version of his all-in-one bathroom in a dedicated extension in his Harrogate home. It has been fully working for the last 3 years and is now ready for market. After numerous funding application failures, it would be the Royal Academy of Engineering who would recognise his determination, tenacity and skills for engineering, manufacturing and design, by awarding him a grant, to reach his goal of manufacturing the Vertebrae&reg;.</p>

<p>&nbsp;</p>

<p>The RAE has a difficult and challenging application process, but nonetheless, Paul was able to explain his product, its application and his aspirations for this innovative, vertical, space-saving bathroom solution. The application process was well worth it as it is supported by full day workshops by specialists in a particular field to help the cohorts succeed with their business idea.</p>

<p>
​</p>

<p>&lsquo;Paul Hernon has been awarded a competitive place on the Royal Academy of Engineering&rsquo;s Regional Talent Engines programme, which helps early-stage founders in Yorkshire and Humber, and beyond, to transform great ideas into new engineering and tech startups.&rsquo;</p>

<p>There have been many adjustments along the way and the business may well have had to readdress its board of directors, finances and product materials based on a number of extrinsic factors, but it remains, Paul believes in his product, his vision and will be unveiling the latest design which will be available to order at the Homebuilding and Renovation Show at Harrogate Convention Centre this November 3rd&ndash;5th 2023.</p>

<p>&nbsp;</p>

<p>Visit his website at&nbsp;<a href="http://www.designodyssey.co.uk/">www.designodyssey.co.uk</a>&nbsp;</p>', 450, '
2023-12-16 08:54:40
', '2024-01-15 08:07:13
', 0, 'https://designodyssey.co.uk/', 'design-odyssey', '2024-01-10 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (436, 'Incommunities', 'Affordable homes to be built on site of former flats at Valley Road, Shipley', '<p>Bradford-based housing provider Incommunities has begun clearing the way for the construction of 42 new affordable homes on the site of former flats at Valley Road.</p>

<p>&nbsp;</p>

<p>Following the site clearance, Casey Construction will begin building the new homes over the next few weeks, with construction expected to be complete in summer 2024.</p>

<p>&nbsp;</p>

<p>The new development, named &lsquo;Valley View&rsquo;, is situated next to Shipley Fire Station, and will transform land that has been empty for the last eight years.</p>

<p>&nbsp;</p>

<p>There will be 32 two-bedroom homes and 10 three-bedroom homes, which will be available through the Affordable Rent, Shared Ownership and Rent to Buy schemes.</p>

<p>&nbsp;</p>

<p>The development of the new affordable homes is being partly funded by the Government&rsquo;s housing and regeneration agency, Homes England, as part of their pledge to increase the number of affordable homes across the country.</p>

<p>&nbsp;</p>

<p><strong>Graeme Scott, Executive Director of Development &amp; Assets at Incommunities, said:</strong>&nbsp;&ldquo;We&rsquo;re really pleased to be able to share that we&rsquo;re starting construction of these new affordable homes at Valley Road.</p>

<p>&nbsp;</p>

<p>&ldquo;Providing high quality affordable homes remains one of our major commitments, so we&rsquo;re delighted that we can build these homes in such a prime location.</p>

<p>&nbsp;</p>

<p>&ldquo;It&rsquo;s such a major route into Bradford, within walking distance to both Shipley town centre and Frizinghall train station, I&
#39;m sure these homes will be popular with local people.</p>

<p>&nbsp;</p>

<p><strong>Cllr Alex Ross-Shaw,&nbsp;Bradford Council&rsquo;s Executive Member for Regeneration, Planning and Transport,&nbsp;said</strong>: &ldquo;The Council&rsquo;s Housing Development team is working hard with our partners to make sure more affordable homes are being built across the district.</p>

<p>&nbsp;</p>

<p>&ldquo;We are delighted that work has started on preparing the ground for 42 new affordable homes in Shipley. These homes will be in an excellent location and will be a real boost for the area.&rdquo;</p>

<p>&nbsp;</p>

<p>Valley View will be the latest new build development by Incommunities designed to improve the standard of affordable housing in the region.</p>

<p>&nbsp;</p>

<p>It follows the creation of similar Incommunities schemes in the Bradford district, the Millgrove development off Manchester Road, due for completion this year and Crossley Wood in Bingley, scheduled for completion in early 2024.</p>

<p>&nbsp;</p>

<p>Over the coming years, Incommunities is committed to&nbsp;<strong>delivering larger-scale mixed developments, encompassing Affordable Rent, Social Rent, Rent to Buy, and Shared Ownership.</strong></p>

<p>&nbsp;</p>

<p><strong>The organisation&
#39;s geographic focus will span West and part of North Yorkshire, with specific emphasis on Bradford, Kirklees, and Calderdale.</strong></p>', 451, '
2023-12-16 08:56:49
', '2024-01-15 08:07:20
', 0, 'https://www.incommunities.co.uk/', 'incommunities-2', '2024-01-10 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (437, 'Digital Enterprise', '£4.6M Funding to Boost Digital Enterprise for West Yorkshire Businesses', '<ul>
	<li>Digital funding to improve productivity, boost performance and build a sustainable West Yorkshire economy</li>
	<li>Digital Enterprise 3 business support programme includes Accelerator Grants worth up to &pound;10,000 for digital development&nbsp;</li>
	<li>Limited number of High Impact Grants up to &pound;35,000 available for digital transformation projects</li>
</ul>

<p>Digital Enterprise has secured &pound;4.6 million funding from the UK Government through the UK Shared Prosperity Fund (UKSPF), to encourage ambitious businesses in West Yorkshire to take a technological leap to improve productivity, performance and competitiveness and achieve sustainable growth by investing in digital.&nbsp;</p>

<p>&nbsp;</p>

<p>As the new round of funding is now live, the clock is already ticking for businesses to access the support they need for growth. This third Digital Enterprise (DE3) programme will end in March 2025, giving businesses just over 12 months to apply for funding.</p>

<p>&nbsp;</p>

<p>Digital Enterprise has two levels of grant funding available. Eligible businesses considering a digital investment between &pound;2,000 to &pound;25,000 can apply for a Digital Accelerator Grant for up to 50% of the project costs, with a ceiling value of &pound;10,000.</p>

<p>&nbsp;</p>

<p>New for DE3 is a limited number of High Impact Grants for substantial and transformative digital projects, eligible businesses can receive up to 50% contribution, (capped at a maximum value of &pound;35,000) towards new digital projects costing between &pound;25,000 - &pound;100,000.</p>

<p>&nbsp;</p>

<p>High Impact projects will take longer to complete, further reducing the time businesses have to apply for this next level of funding. Representatives from DE3 are encouraging businesses to come forward sooner rather than later, in order to take full advantage of the support available.</p>

<p>&nbsp;</p>

<p>The new programme includes a streamlined application process that allows hardware, software, technology, connectivity and internet upgrades to be combined within a single application. Businesses will also receive impartial advice and support from their dedicated Digital Advisor, who will be on hand throughout the application process to help them make the best use of digital solutions.</p>

<p>&nbsp;</p>

<p>Digital Enterprise Programme Manager, Suzanne Bradbury, said: &ldquo;We&rsquo;re here to support West Yorkshire based businesses looking to scale up. While we are not prescriptive about business sectors, this grant funding is for growth-focussed businesses, operating for more than three years that need investment in digital innovation to propel them to the next stage.</p>

<p>&nbsp;</p>

<p>&ldquo;We know businesses are facing a myriad of challenges and are increasingly having to do more with less in order to survive. Technology can play a key role in overcoming those challenges, and we are here to provide businesses in the region with the advice and financial support needed to help them&nbsp;progress and grow.&rdquo;</p>

<p>&nbsp;</p>

<p>Since its launch in 2016, Digital Enterprise has provided over &pound;12 million in funding and helped over 2,700 businesses through its grant funding and business support&nbsp;programme, including Leeds-based businesses White Label Loyalty and VTR North.</p>

<p>&nbsp;</p>

<p>Digital innovation and customer engagement specialist and ex-pro footballer, Achille Traore, is recognised as one of the top 100 Retail Technology Entrepreneurs in the UK. His business&nbsp;<a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXHk-2B5rIGWD5-2F4fPqan3zdZMkkqubtk6TsYk7SVKqblrlwKea_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3ZZdNFnH6wB0Gmplq-2BjrbJI1WEULntC0rfQRqRIiaITOz4hf9yYBk22N1sAbOmyEpZhHadRkrnBTfUNpfaKeLNxpY7ibdzsb43uX4k1YmBlGdluqGYIcnKmk2bX3Ur4rksKD0qTLzVAusGFwwk-2FtmOn4xoBdMH9orYz5zrn6uScsHIEFK5FoAAschrQYZdkFxiOMd0Eg2zfX3jhadJKc5p-2Fsqo-2FM8SqlSoiKwR43l-2BjvhmOiXopOaB2-2FW3RAJ-2F1ynQyOvfyINXsojBi49e4VrHbqb96nqkN2c5WAAseUOcNvN9Z1ReMt6wQsui6sf4P9totMNsnJYZ3NA6KTDIVAbfVpCA3TeSl3rZcLT3JCqwEw5NcYzkOCaXwfIM9dZw3aHc-3D">White Label Loyalty</a>&nbsp;received a Digital Enterprise grant of &pound;8,000 towards bespoke software development.&nbsp;The additional development allowed White Label Loyalty clients to fully customise their own loyalty microsites, seamlessly integrated to provide customised loyalty interfaces&nbsp;to their own individual customers.</p>

<p>&nbsp;</p>

<p>Achille shared his insights and practical tips on leveraging digital technology to help improve performance and productivity at Digital Enterprise&rsquo;s recent showcase event, he commented:&nbsp;&ldquo;The financial grant support from Digital Enterprise and the advice we received from our Digital Growth Advisor allowed us to accelerate and achieve significant growth in White Label Loyalty and deliver a great experience for our customers.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;I was delighted to talk about our experience and encourage other businesses to see how they can benefit from Digital Enterprise support.&nbsp;In our case, grant funding helped us to simplify the build process to make it more user-friendly, as well as speed up the implementation and on-boarding of new clients.&nbsp;Without a doubt, this has led to increased productivity at White Label Loyalty and created more jobs.&nbsp;I&rsquo;m very proud of the fact that we&rsquo;ve achieved this growth while staying headquartered in the city of Leeds.&rdquo;</p>

<p>&nbsp;</p>

<p>VTR North, is an award-winning, TV production company, which was able to achieve its growth goals with support from Digital Enterprise.&nbsp;Already successfully providing production and post-production services for Channel 4 and Sky Bet, VTR North wanted to expand into the world of audio podcasts.</p>

<p>&nbsp;</p>

<p>With a Digital Enterprise Award of almost &pound;10,000, the company completed a studio fit out fully equipped with the hardware and software required, such as vision mixers, cameras and microphones, to give them the ability to live webcast and produce high-quality podcasts.</p>

<p>&nbsp;</p>

<p>Spencer Bain, MD at VTR North, explained:&nbsp;&ldquo;The new podcast service has created a real buzz with existing clients and new opportunity for us.&nbsp;Being able to offer this podcasting service creates another revenue stream which is instrumental for the continued growth of the company, as well as hopefully creating a few more jobs at VTR North.&rdquo;</p>

<p>&nbsp;</p>

<p>Digital Enterprise can support businesses from a wide range of sectors and fund impactful digital projects before the programme ends in March 2025. Ambitious growth businesses can find out more about eligibility and apply for a Digital Accelerator Grant or High Impact Grant while there&rsquo;s still time by visiting:&nbsp;<a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXOxzOm66EOd48POYYRmR6OjE9313rnms-2F3yidekEhQ8nEFLC_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3ZZdNFnH6wB0Gmplq-2BjrbJI1WEULntC0rfQRqRIiaITOz4hf9yYBk22N1sAbOmyEpZhHadRkrnBTfUNpfaKeLNxpY7ibdzsb43uX4k1YmBlGdluqGYIcnKmk2bX3Ur4rksKD0qTLzVAusGFwwk-2FtmOn4xoBdMH9orYz5zrn6uScsHIEFK5FoAAschrQYZdkFxi2XIEQTgivBlHMpbnj8IDotlWG-2BEFW9vfV5oYCkQ8uveRoEv6euA5xIvVRq8IuysF2YygLvYkALxRXkBzCCVYGElgF2T-2FQ2OPxsk-2BmDBFxCL8uX60D4vMfx6Lnu06AHp-2BLhE3LCmswlgNg-2F6QYfZEjoLpbfXMIBSKUUFXhDF78mEi9cQa0YPKFAlTr9sPz2vQ-3D">https://www.digitalenterprise.co.uk/</a></p>

<p>&nbsp;</p>', 452, '
2023-12-16 08:58:46
', '2024-01-15 08:07:30
', 0, 'https://www.digitalenterprise.co.uk/', 'digital-enterprise-1', '2024-01-11 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (438, 'Simon on the Streets', '£25,000 raised as 75 people bed down on Leeds street for charity sleep-out', '<p>75 people braved Storm Babet to sleep out on the streets of Leeds for a local homelessness charity, helping to raise almost &pound;25,000.</p>

<p>&nbsp;</p>

<p>Sponsored by British online homeware retailer, DUSK.com, West Yorkshire homelessness charity Simon on the Streets held its annual sleep-out event, &lsquo;Simon&rsquo;s Big Sleep Out&rsquo; on Thursday 19th&nbsp;October at Leeds Civic Hall.</p>

<p>&nbsp;</p>

<p>The 2023 event, which saw participants endure ferocious winds and rain as Storm Babet hit the city, provided the chance for individuals and businesspeople to sleep outside for one night and learn about some of the harsh realities that rough sleepers experience on the streets. It was also a chance to learn more about the increasing amount of people becoming homeless and how the charity supports rough sleepers.</p>

<p>&nbsp;</p>

<p>Having set up their beds for the night, attendees enjoyed an interactive quiz from Leeds-based KwizzBit with catering provided by Sodexo and Rapid Relief Team.</p>

<p>&nbsp;</p>

<p>Founded in 1999, Simon on the Streets is an independent charity, working with local people who are affected by homelessness and rough sleeping in Leeds and Bradford.</p>

<p>&nbsp;</p>

<p>Natalie Moran, CEO of Simon on the Streets commented:</p>

<p>&nbsp;</p>

<p>&ldquo;The Big Sleep Out is not intended to be a way for people to say they know what it&rsquo;s like to be homeless. This is just one night. After which everyone can go home, get dry and warm and climb into their own beds. What we hope is that by taking part, people get an opportunity to really think about the practicalities of not having somewhere to call home. The horrendous weather, courtesy of Storm Babet, made it an incredibly emotional night for those who took part. The fact that this is some people&rsquo;s daily reality shook so many of the participants.</p>

<p>&nbsp;</p>

<p>&ldquo;We are so humbled by how many people chose to share this experience and who, so far, have raised an incredible &pound;x. We know that many people are still pushing their Just Giving pages so we are hoping we can reach our &pound;30,000 target.&rdquo;</p>

<p>&nbsp;</p>

<p>As well as sponsoring the event, 15 colleagues from DUSK attended the event.</p>

<p>&nbsp;</p>

<p>Sian Guest, Head of Brand Marketing at DUSK.com said:</p>

<p>&nbsp;</p>

<p>&ldquo;We are incredibly proud to have sponsored Simon&rsquo;s Big Sleep Out for the second year running and to have raised extra funds in addition to our sponsorship and bedding donations. The colleagues who participated felt humbled and honoured to be part of such an important fundraising event, helping raise awareness of the vital outreach work Simon on the Streets carries out in our local communities.&rdquo;</p>

<p>&nbsp;</p>

<p>The Big Sleep Out marked the end of a busy month for the homelessness charity as Simon on the Streets dedicated the month to its campaign &lsquo;A walk in their shoes&rsquo; with early October events including a poignant display on the David Oluwale Bridge as part of Leeds Light Night.</p>

<p>&nbsp;</p>

<p>Simon on the Streets fundraising initiatives and events continue with&nbsp;the charity&rsquo;s regular monthly corporate socials&nbsp;and a Christmas&nbsp;Elf Run in December for school children to get involved.</p>', 453, '
2023-12-16 09:02:15
', '2024-01-15 08:07:40
', 0, 'https://simononthestreets.co.uk/', 'simon-on-the-streets', '2024-01-11 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (439, 'Gripple', 'GLIDE AWARDS ATTRACTS INTERNATIONAL FINALISTS TO SHEFFIELD', '<p>Gripple,&nbsp;the&nbsp;Sheffield-based&nbsp;employee-owned manufacturer of wire joiners and tensioners and suspension solutions for agriculture, construction, solar and infrastructure, celebrated with colleagues from across the globe at its fifth annual GLIDE Awards held in Sheffield recently.</p>

<p>&nbsp;</p>

<p>The GLIDE Awards, which recognise excellence and dedication amongst Gripple employees, is a unique celebration of the people that best represent Gripple Spirit.&nbsp; Colleagues from all Gripple territories, including Europe, USA, Canada and India, joined the UK team at the Sheffield event which celebrated the achievements of the 14 finalists for each region&rsquo;s coveted GLIDE Award winner title.</p>

<p>&nbsp;</p>

<p>Since 1994, all Gripple employees have had the opportunity to own a piece of the business and in 2011, GLIDE (Growth Led Innovation Driven Employee) Company was born.&nbsp; The annual GLIDE Awards celebrate the core values of Gripple, with colleagues voting for other team members across&nbsp;seven categories, including Learning, Communication, Community, Teamwork, Innovation, Fun &amp; Laughter and Leadership.&nbsp;</p>

<p>&nbsp;</p>

<p>This year, the awards received an overwhelming response with over 2,000 nominations, including 700 votes received from the UK alone.</p>

<p>&nbsp;</p>

<p>The GLIDE Reps, who are elected solely by shareholder employees from each Gripple company, have many duties in representing shareholders and they are also responsible for reviewing the Award nominations.&nbsp; There are 42 GLIDE Reps around the globe who decide the winners on the strength of the nominations.&nbsp; This includes the highly prestigious GLIDE Award winner, with two people selected from each Gripple&nbsp;territory for the GLIDE Awards final, which took place at GLIDE House in Sheffield on Friday 27th&nbsp;October.&nbsp;</p>

<p>&nbsp;</p>

<p>Congratulations&nbsp;to UK category winners Isabel Laroche, Jake Hallatt, The Old West Gun Works Packing team and Gary Hallatt, Phil Healey, Will Banks, Dan Spooner, the Finance Team and Robbie Sanderson.&nbsp;&nbsp;All category winners were invited to the awards event where they were presented with trophies in recognition of their efforts, by Deb Oxley, former CEO of the Employee Ownership Association, who hosted the evening.</p>

<p>&nbsp;</p>

<p>Michael Hodgson, Chair of GLIDE, explains: &ldquo;We had a tremendous response from our employees across the business this year, with a record-breaking number of nominations.&nbsp; Now in their 5th&nbsp;year, the GLIDE awards are the highlight of our calendar, helping us to recognise our people and their outstanding achievements.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>He adds: &ldquo;I would like to congratulate everyone who was highly commended or won a GLIDE award.&nbsp; Reading through the nominations is a fantastic part of the process for our GLIDE Reps as it shows us just how colleagues are demonstrating our values in their day to day lives, including driving innovation, working as a team and most importantly, having fun &ndash; well done to all!&rdquo;</p>

<p>&nbsp;</p>

<p>The UK GLIDE Awards finalists were Judit Rusznyak, a quality engineer at Norfolk Bridge and Hawke Street in Sheffield and Bradley Allott.&nbsp; Bradley, a diecasting facilitator at Gripple&rsquo;s Hellaby site in Rotherham, was selected as the winner of the prestigious title.&nbsp; Video footage played at the awards event demonstrated how he was admired by colleagues for his tireless work ethic and unwavering commitment to the business &ndash; which earned him the title of being one of Gripple&rsquo;s best operators.&nbsp;&nbsp;Bradley received a prize of a trip to a Gripple location of his choice plus an extra day&rsquo;s annual leave.</p>

<p>&nbsp;</p>

<p>There were also GLIDE Award winners from each of the Gripple territories.&nbsp;&nbsp;Congratulations&nbsp;go to Talel Bouquila (Gripple Europe), Mukesh Kumar Yadav (Gripple India), Yanick Dussault (Gripple Canada), Mick Blacktin (Loadhog) and Nick Jones (GoTools).</p>

<p>&nbsp;</p>

<p>The GLIDE Awards is a group-wide awards scheme which includes all Gripple GLIDE member businesses including GoTools and Loadhog.&nbsp;</p>

<p>&nbsp;</p>

<p>The GLIDE Awards are&nbsp;a celebration of shared GLIDE values, incorporating the Gripple Spirit principles.<em>&nbsp;</em>Gripple Spirit involves more than just getting the job done, but also sees employees contributing to the present and working towards an ambitious future. Gripple is committed to its people and the company aims to recruit people who share its values &ndash; of fun, innovation, passion, entrepreneurship, teamwork, integrity - who are invested in the shared purpose to be a fun, rewarding place to work.</p>', 454, '
2024-01-07 07:55:23
', '2024-01-15 08:08:00
', 0, 'https://www.gripple.com/', 'gripple-3', '2024-01-15 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (440, 'Palmer Landscapes', 'First ‘drive thru’ concrete, mortar and screed service sets itself in Yorkshire', '<ul>
	<li><strong>Local landscaper introduces first click and collect service in Yorkshire&nbsp;</strong></li>
	<li><strong>Builders, landscapers and DIY enthusiasts can now purchase concrete, mortar and screed in smaller quantities, starting at&nbsp;</strong><strong>0.25m3&nbsp;&nbsp;</strong></li>
	<li><strong>Showcases industry&rsquo;s commitment to reducing wate to landfill&nbsp;</strong></li>
</ul>

<p>Family run Yorkshire business, Palmer Landscapes has launched Yorkshire&rsquo;s first drive-thru service for concrete, mortar and screed products. Builders, landscapers and DIY enthusiasts will now be able to pay as they go for small scale building and landscaping projects, all in a bid to reduce the industry&rsquo;s contribution to landfill and reduce waste from the production process.&nbsp;</p>

<p>&nbsp;</p>

<p>Based in Calverley and doing business for over 50 years, Palmer Landscapes took the leap and made a significant investment to instal the fully automated and self-service station, which launched in October. The investment is in response to an increasing demand from customers who were keen to reduce the amount of leftover building materials going to landfill and save on costs when pricing materials.&nbsp;</p>

<p>&nbsp;</p>

<p>Palmer Landscapes spent a year researching the service, called fibo Collect, to learn about its adoption across Europe. It can so far be found in North Wales, Wolverhampton and Scotland.&nbsp;</p>

<p>&nbsp;</p>

<p>Adam Palmer, Director at Palmer Landscapes says: &ldquo;We&rsquo;re excited to launch this new service in Yorkshire. When we first came across the concept and saw it in action, we knew that it would be perfect for SMEs and DIY projects; we&rsquo;re often asked if we know of businesses who can supply smaller quantities of building materials and there isn&rsquo;t.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;It&rsquo;s a great opportunity to showcase the commitment the industry has to improving how much goes to landfill. The service is highly innovative, it has a built-in washing system and waste disposal plant. The mixer self-cleans between batches which ensures higher recipe accuracy.&nbsp;This is 100% a service which encourages builders, landscapers and DIY enthusiasts to do things more efficiently, with environmental consideration.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>Purchasing concrete, mortar, and screed in the traditional way from a local ready mix supplier can be discouraging for smaller businesses or DIY enthusiasts. It often involves a minimum order of 1m3 and collecting small quantities can result in significant waiting times. This method is primarily designed for high volume usage.&nbsp;</p>

<p>&nbsp;</p>

<p>With this new eco-friendly click-and-collect service, customers can now select from a range of recipes using a user-friendly touch-screen terminal. The system allows customers to specify their required volume starting from just 0.25m3. The material is then carefully mixed, poured, and dispensed from a conveyor belt onto the back of a pickup truck or directly into a tub. This entire process is completed in less than 5 minutes, ensuring time efficiency.&nbsp;</p>

<p>&nbsp;</p>

<p>Palmer Landscapes is committed to creating eco-friendly green spaces, alongside Palmer Nurseries and Palmer Plants. Combined, the business has an annual turnover of &pound;5.5m and has ambitious plans to grow their estate in the coming years.&nbsp;</p>', 455, '
2024-01-07 07:58:39
', '2024-01-15 08:07:52
', 0, 'https://palmerlandscapes.co.uk/', 'palmer-landscapes', '2024-01-15 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (441, 'Reward', 'Reward secures additional £50m lending facility to bolster support for SMEs across the UK', '<ul>
	<li><strong>Reward Finance Group has secured a significant &pound;50m increase to its lending facility from Foresight Group</strong></li>
	<li><strong>This underpins continuous business growth and takes the total lending facility from Foresight Group to &pound;180m</strong></li>
	<li><strong>Reward aims to increase its loan book to more than &pound;350m in the next three years</strong></li>
</ul>

<p><a href="https://www.rewardfinancegroup.com/">Reward Finance Group</a>, which provides SMEs with&nbsp;tailored business finance loans and asset-based solutions&nbsp;of up to &pound;5m, has secured an additional &pound;50m to its credit line from the alternative investment manager,&nbsp;<a href="https://foresight.group/shareholders">Foresight Group</a>.&nbsp;The opportunity will enhance its lending capabilities, fostering further business growth.</p>

<p>&nbsp;</p>

<p>This increase reflects Reward&rsquo;s swift expansion - the lender currently supports over 500 SMEs from six regional offices and has recently surpassed a&nbsp;&pound;200m&nbsp;loan book milestone for the first time. Furthermore, several senior-level appointments have been made to enhance its regional UK presence.</p>

<p>&nbsp;</p>

<p>Nick Smith, group managing director for Reward Finance Group, said; &ldquo;This opportunity supports not only our business but also our clients in a challenging industry landscape. It emerges as a genuine&nbsp;positive for SMEs, who often find it difficult to secure funding from high street banks, especially while still recovering in a post-COVID climate, hit by rising inflation, interest rates and corporation tax. The support Foresight has provided over the last six years has been pivotal to our growth ambitions. This latest &pound;50 million increase, taking the total lending facility from Foresight to &pound;180 million, further solidifies our shared confidence and vision for the business and fuels our aspirations to expand even further &ndash; in the next three years, we aim to surpass &pound;350 million in our loan book.&rdquo;</p>

<p>&nbsp;</p>

<p>Amy Crofton, director at Foresight Group, commented; &ldquo;We&rsquo;re pleased to extend Reward&rsquo;s credit line by an additional &pound;50 million. The business has not only achieved 12 years of continuous growth but has also proven to be indispensable to numerous UK based SMEs.</p>

<p>&nbsp;</p>

<p>&ldquo;Reward&rsquo;s responsible, common-sense approach to lending has been a cornerstone of its success to date and is integral to ensuring firms secure the working capital needed for growth and job creation. We&rsquo;re delighted to support Reward in offering even greater speed and flexibility in providing funding solutions to firms, especially in an increasingly challenging business environment.&rdquo;</p>

<p>&nbsp;</p>

<p>David Harrop, group finance director at Reward, added; &ldquo;The &pound;50 million increase is not only a major development for the business, allowing us to&nbsp;further enhance our lending capabilities, but also a significant boost for SMEs needing to borrow amidst prevailing economic and political uncertainty. We&rsquo;ve invested significant effort into regional expansion over the last two years, and the latest investment from Foresight is a recognition of our progress. In this period alone, we&rsquo;ve transitioned from being a highly-recognised lender in Yorkshire and the North West, to financing the growth ambitions of SMEs across nearly all corners of the UK.&rdquo;</p>

<p>&nbsp;</p>

<p>Since its inception in 2011,&nbsp;Reward&nbsp;has supported over 2,000 businesses across the UK by providing over &pound;1 billion of the working capital necessary to help SMEs elevate revenue, create jobs, innovate and navigate through difficult trading periods.</p>

<p>&nbsp;</p>

<p>Foresight&rsquo;s Private Credit strategy provides secured wholesale loan facilities to alternative lenders; designing bespoke facilities to enabling innovative finance businesses to scale and better serve&nbsp;its&nbsp;customers.</p>', 456, '
2024-01-07 08:05:11
', '2024-01-07 08:05:37
', 0, 'https://www.rewardfinancegroup.com/', 'reward-01-24', '2024-01-16 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (442, 'NORTH STAR', 'NORTH STAR SCIENCE EVENT WITH PROFESSOR BRIAN COX RETURNS TO INSPIRE SOUTH YORKSHIRE PUPILS', '<p>Four hundred and ninety students from 48 schools across South Yorkshire are set to take part in the fourth annual North Star Science School, which is designed to inspire the next generation about the wide range of careers in STEAM (Science, Technology, Engineering, Arts &amp; Maths) subjects.</p>

<p>&nbsp;</p>

<p>Organised by The Work-wise Foundation, a not-for-profit employer-led charity, the event will take place at the Gulliver&rsquo;s Valley Theme Park in Rotherham on 8th&nbsp;November in partnership with Well North Enterprises.</p>

<p>&nbsp;</p>

<p>Professor Brian Cox CBE, will open the event virtually and give an inspiring talk about the origins of the universe.</p>

<p>&nbsp;</p>

<p>John Barber, CEO of The Work-wise Foundation, said: &ldquo;Bookings for this event sold out within a week, which is testament to just how keen local schools are to get involved.&nbsp; The North Star Science School gives young people the chance to get hands-on and work with local businesses through a range of STEAM-related interactive workshops and to learn from some influential speakers from the industry.&rdquo;</p>

<p>&nbsp;</p>

<p>John adds: &ldquo;After the phenomenal feedback we received from students and teachers last year, we are really excited about the impact that this interactive event has and how it helps to widen young people&rsquo;s understanding and awareness of career options and hopefully inspires them to become our scientists and engineers of the future.&rdquo;</p>

<p>&nbsp;</p>

<p>The Work-wise Foundation has responded to feedback from teachers and students and has&nbsp;increased the number of workshops &ndash; with 16 different workshops* for students to choose from. These include uncovering the science of chocolate - exploring why chocolate cools differently on various foods using thermal imaging technology.&nbsp; Other workshops include making sand moulds by pouring low-temperature alloys, building and controlling a hydraulic crane and learning the science and art of brickmaking.</p>

<p>&nbsp;</p>

<p>STEAM-inspired workshops will be delivered during the North Star Science event by organisations including AESSEAL, AMRC Training Centre, Barratt Developments, CBE+, Esh Group, Forged Solutions, Ibstock, AMETEK Land, UK Atomic Energy Authority and United Cast Bar. Additional workshops, kindly sponsored by Doncaster Council, Rotherham Metropolitan Borough Council, and Sheffield City Council, will also be taking place.</p>

<p>&nbsp;</p>

<p>Some of the workshops are co-created by employers working with local school students to design and deliver the sessions. Mrs. Donna Barker, Subject Leader Technology, New
ﬁeld School said: &ldquo;Collaborating with an engineering company like CBE+ provides our students with a better understanding of the world of work and the challenges of working as a team, meeting deadlines and setting achievable objectives. Our students thrive in the working environment and bene
ﬁt hugely from the shared experiences and knowledge our colleagues bring to the project.&rdquo;</p>

<p>&nbsp;</p>

<p>The event is part of the Science Summer School national initiative co-founded in 2012 by Professor Brian Cox and Lord Andrew Mawson OBE, with the aim of making the UK the best place in the world to do science and engineering. It is presented in partnership with Well North Enterprises, a social enterprise business led by Lord Mawson.</p>

<p>&nbsp;</p>

<p>Professor Brian Cox, Britain&rsquo;s leading physicist and science communicator and Co-Founder of Science Summer School, says:&nbsp;&ldquo;An interest in science at a young age can develop into an exciting and rewarding career.&nbsp; With all the challenges our climate faces, we need scientists and engineers more than ever to make a brighter future for everyone.</p>

<p>&nbsp;</p>

<p>&ldquo;I am looking forward to speaking to South&nbsp;Yorkshire pupils at the North Star Science event and to bring them up to date with some exciting developments.&rdquo;</p>

<p>&nbsp;</p>

<p>Professor Cox will be joined by North Star speakers Ruth Amos, former Young Engineer for Britain, entrepreneur, and STEM Ambassador, along with Alex Gardner, event host and Managing Director of Smile Business Support. Other speakers include Melanie Oldham OBE, Founder of Bob&rsquo;s Business and Director of Yorkshire Cyber Security Cluster, and Professor Simon Goodwin, Professor of Astrophysics at the University of Sheffield.</p>

<p>&nbsp;</p>

<p>In addition, a special presentation by Lord Andrew Mawson with a focus on maritime careers will be presented from the Cunard liner, named Queen Elizabeth, as she cruises through the Mediterranean en route to Australia, using the Starlink satellite system created by SpaceX founder Elon Musk.</p>

<p>&nbsp;</p>

<p>Lord Andrew Mawson said: &ldquo;North Star Science School continues to act as a tremendous catalyst in South Yorkshire and beyond, helping to bring business, education, health, public sector and community together in a spirit of &lsquo;Learning by Doing&rsquo; to inspire our scientists and engineers of the future.&rdquo;</p>

<p>&nbsp;</p>

<p>Marie Cooper, Managing Director of sponsor company CBE+ explains: &ldquo;North Star is such an innovative and exciting event in the region which CBE+ are honoured to sponsor. As part of the year-round programme, we&rsquo;ve been working with Newfield School on a co-created workshop for the second year running, and it has been a brilliant opportunity for our team to work with their students to build a solid and long-lasting relationship. The work that The Work-wise Foundation do to bring employers and educators together as part of this event and the wider programme is incredible and a real testament to the region&rdquo;</p>

<p>&nbsp;</p>

<p>Following the day&rsquo;s activities, there will be an evening event where businesses will come together to celebrate the achievements of the North Star programme and hear about the exciting developments planned for Skills Street providing even more opportunities for the region&
#39;s young people.</p>

<p>&nbsp;</p>

<p>North Star 2023 is made possible through support and funding provided by local businesses and authorities.&nbsp;&nbsp;This year&rsquo;s sponsors include&nbsp;AESSeal, AMETEK Land, AMRC Training Centre, Barratt Developments Plc, CBE+, Doncaster Council, Esh Group, Forged Solutions Group, Gulliver&rsquo;s, Ibstock Plc, Rotherham Metropolitan Borough Council, Sheffield City Council, UK Atomic Energy Authority and United Cast Bar in collaboration with Cast Metals Federation<strong>.</strong></p>', 457, '
2024-01-07 08:12:40
', '2024-01-07 08:12:46
', 0, 'https://www.northstarscienceschool.co.uk/', 'north-star', '2024-01-16 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (443, 'Craggs Energy', 'Craggs Energy secures a double victory at the Calderdale Business Awards', '<p>The employee-owned Cragg Vale-based business, Craggs Energy is delighted to have won two awards at the most recent Calderdale Business Awards.<br>
 </p>

<p>The company, known for its excellence in fuel distribution, has been honoured with the titles of Employer of the Year and Business of the Year, solidifying its position as a standout contributor to the local business landscape.</p>

<p></p>

<p>Matthew Crockett, Managing Director at Craggs Energy comments:</p>

<p></p>

<p>"We are honoured to have received both the award we were nominated for and the prestigious Business of the Year award. This recognition stands as a testament to the dedication and hard work of our entire team, of which I couldn\'t be prouder.</p>

<p></p>

<p>At Craggs Energy, we believe that our employees are our most valuable assets, and we are committed to empowering our workforce. This accolade reflects our ongoing dedication to fostering a supportive and thriving workplace. We place high value on our employees, customers, and the community we serve.</p>

<p></p>

<p>Not only this but winning the Business of the Year award highlights our commitment to excellence and innovation within our industry. Our sustained growth, relentless pursuit of quality, and dedication to customer satisfaction drive us to continue providing reliable and renewable fuel solutions for our Calderdale community for years to come.
”</p>

<p></p>

<p>Emily Yates, Transport and Operations Planner said:</p>

<p></p>

<p>"We had a fantastic evening at the awards ceremony and we were blown away to be recognised for two awards. Over the past twelve months, we have seen lots of improvements within the business including the introduction of a dog-friendly office, revamped breakout areas, internal awards and recognition and more regular staff incentives including meals and team nights out.</p>

<p></p>

<p>I feel privileged to be working with such an amazing team and to share in the growing success of our one hundred per cent employee-owned business. The continued incentives and benefits that are introduced keep the team motivated and feeling valued. I
’m excited to see what the future holds for us.
’’</p>

<p></p>

<p>Craggs Energy
\'s impressive double victory at the Calderdale Business Awards 2023 is a testament to their enduring commitment to their colleagues, community, and industry. As they continue to grow and evolve, their presence in the energy sector remains influential and their dedication to excellence is unwavering.</p>

<p></p>

<p>The Employer of the Year and Business of the Year titles are not just accolades but a testament to the values that define Craggs Energy as a pillar of the Calderdale business community.</p>', 458, '
2024-01-07 08:14:11
', '2024-01-07 08:14:11
', 0, 'https://craggsenergy.co.uk/', 'craggs-energy', '2024-01-17 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (444, 'Hawley Group', 'Hawley Group Holdings Limited embarks on further expansion at Parkview House, Brighouse', '<p>Hawley Group Holdings Limited has undertaken its latest expansion at Parkview House, on Woodvale Office Park in Brighouse. The company has signed a new lease for the ground floor office suite, strengthening its presence in this historic and beautifully developed former 1800s silk mill.</p>

<p>&nbsp;</p>

<p>The self-contained office space spans an impressive 2,040 sq ft and features private restrooms and&nbsp;kitchen facilities. This expansion comes on the heels of Hawley Group&rsquo;s move into the 4,500 sq ft second&nbsp;floor earlier this year. With this new addition, the firm now occupies approximately 6,500 sq ft of this&nbsp;meticulously restored heritage property.</p>

<p>&nbsp;</p>

<p>Hawley Group, founded in 2010, has come a long way since its humble beginnings in a small Northowram&nbsp;office. Today, the company boasts a dedicated team of 26 staff members who specialise in building&nbsp;services and energy efficiency. The organisation provides tailor-made solutions and expert advice to its&nbsp;B2B client base.</p>

<p>&nbsp;</p>

<p>Situated at Parkview House, Hawley Group&rsquo;s neighbours include Stark Building Materials UK Limited, Venatu Consulting Limited, and UKCaravans4hirecom. This prime location is conveniently close to Brighouse town centre and Junction 25 of the M62 Motorway.</p>

<p>&nbsp;</p>

<p>The building, once an historic silk mill, has been ardently restored to offer refurbished office spaces with&nbsp;scenic views of Wellholme Park. The surroundings include tennis courts, running, cycling, and walking&nbsp;trails, a variety of shops, local gyms, and a swimming pool. Additionally, ample on-site parking is&nbsp;provided for the convenience of occupants.</p>

<p>&nbsp;</p>

<p>Julia Ford, marketing manager at Towngate PLC, noted: &ldquo;We are pleased to have been able to assist the&nbsp;Hawley Group with their expansion plans at the Woodvale Office Park and look forward to our continued&nbsp;working relationship. As Towngate&rsquo;s office sits close by, we are able to witness the firm&rsquo;s success firsthand from the neighbouring property.&rdquo;</p>

<p>&nbsp;</p>

<p>Andrew Hawley, founder and CEO of Hawley Group Holdings Limited, commented: &ldquo;We are excited about&nbsp;this latest expansion and the opportunities it brings. Our commitment to providing outstanding services&nbsp;to our clients remains unwavering, and we are grateful for the support of Towngate PLC in this journey.&rdquo;</p>', 459, '
2024-01-07 08:15:41
', '2024-01-07 08:15:48
', 0, 'https://hawley.group/', 'hawley-group', '2024-01-17 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (445, 'FASTSIGNS Leeds', 'HEAD DESIGNER AT LEEDS SIGNAGE BUSINESS REFLECTS ON TWO DECADES OF DESIGN AND COMMUNITY PROJECTS', '<p>A Leeds designer is preparing to celebrate his 20th anniversary at a local signage business, marking the occasion by reflecting on his early career and providing tips for the new workforce, discussing his favourite community projects and how the creative industry has changed in two decades.</p>

<p></p>

<p>Vinny Symes is a 45-year old<strong> </strong>designer, with a passion for all things creative.</p>

<p></p>

<p>Based just a stone
’s throw from the edge of the Peak District and North Yorkshire Moors, the head designer of signage specialists, <a href="https://www.fastsigns.co.uk/leeds/">FASTSIGNS Leeds</a>, has been involved in the creative industry since 1996.</p>

<p></p>

<p>Vinny
’s enrollment at Dewsbury College of Art &amp; Design for a year
’s foundation course was just the start of an illustrious career, and a love for design.</p>

<p></p>

<p>Vinny:
“I always knew I loved design and had a passion for spotting things that others perhaps didn
’t. But that course really ignited the possibilities of design, and its effect on audiences  of all ages and backgrounds. Design can really cut-through and speak to people on a personal level, and I leaned in on its ability to help tell stories.
”</p>

<p></p>

<p>From Dewsbury, he went on to The University of Lincolnshire &amp; Humberside to study museum and exhibition design, where he immersed himself in 3D model making and creating visuals. Three years well spent, according to Vinny.<br>
<br>
No career though, is linear, as he explains:</p>

<p></p>

<p>
“I came out of those three years to a job at St James
’ Hospital in Leeds, where I was a porter. While it might seem like a real career change, it was an opportunity for me to gain some real-world experience. I also put myself out there and got some work experience at the nearby Thackray Museum of Medicine, just next door, which I did during my free time in-between shifts.
”<br>
<br>
Looking back, Vinny believes that this
‘side
’ step enabled him to grow and challenge himself. He added:</p>

<p></p>

<p>
“Design is all about taking chances - pushing your designs to the limit. So, in my mind, why should that not also apply to our careers and how we showcase our passion and aspirations? I worked incredibly hard during that period of my life. Alongside a full-time job, I was working to develop my talent in the design space, and I
’ve really benefited from it in the long-run. At both organisations, I was fortunate to work with great colleagues, but one illustrator in particular, at Thackray, had a huge impact on me and the way I still design today.
”</p>

<p></p>

<p>Design, as a discipline, has evolved a lot over the last two decades, according to Vinny.</p>

<p></p>

<p>
“Much of what we did back then was done by eye, or by hand. Technology today plays a fundamental role in our day-to-day operations  it helps to refine our work. Today, you need to have many tools under your belt, especially to keep up with the technology that now assists us.</p>

<p></p>

<p>
“Design is everywhere. Thanks to the rise of social media, digital signage, print and more conventional media channels like TV, there
’s so many ways we can experience design. The challenges though, are to cut-through that crowded market and capture attention of customers who, today, demand immediacy.
”</p>

<p></p>

<p>Commenting on the growth of AI, and the battle to retain creative jobs, he adds:</p>

<p></p>

<p>
“A designer really has to know their craft, and know how that translates to different audiences, on varying platforms. With that though, I
’d say the discipline is even more challenging today. However, it
’s even more rewarding when you get it right as there
’s so much competition out there.</p>

<p></p>

<p>
“Like everything else, AI is playing its part  particularly in terms of image manipulation  things that may have taken half an hour to do 20 years ago can now be done in just seconds. While it
’s a brilliant asset to have, I encourage my designers to hone their skills and learn the basics, as the technology only builds on what we already do.
”</p>

<p></p>

<p>FASTSIGNS Leeds works within a number of markets, from retail, finance and education to hospitality and healthcare. Vinny has played a part in thousands of designs that have gone out to customers over the last two decades.
”</p>

<p></p>

<p>Talking about a few of his favourites, Vinny, said:</p>

<p></p>

<p>
“Tour De France has to be at the top of my list. To celebrate the arrival of the Tour to Leeds back in 2014, we created, produced and installed bike tracks across the floor at Leeds Railway Station. It looked incredibly effective and was a great way to bring the tour to life here at home. It also demonstrated the role that floor signage can play  a solution we
’d hear much more about during Covid, of course.</p>

<p></p>

<p>
“We
’ve produced vehicle graphics for all sorts over the years  from helicopters right through to microlights in Australia. We also recently created and installed the graphics for a boat that was to be rowed, single-handedly, across the Atlantic.
”</p>

<p></p>

<p>For more information on FASTSIGNS Leeds, visit <a href="https://www.fastsigns.co.uk/leeds/">https://www.fastsigns.co.uk/leeds/</a>.</p>', 460, '2024-01-07 08:18:03', '2024-01-07 08:18:03', 0, 'https://www.fastsigns.co.uk/leeds/', 'fastsigns-leeds', '2024-01-18 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (446, 'Reward Finance Group', 'Reward joins forces with The Vault to champion mental health for 14 million grassroots football enthusiasts', '<p>&nbsp;</p>

<p><a href="https://www.rewardfinancegroup.com/">Reward Finance Group</a>&nbsp;announces a pioneering partnership with the Football Mental Health Alliance (FMHA)&nbsp;and England&rsquo;s County Football Associations (CFAs)&nbsp;to bolster mental health support for all involved in grassroots football.&nbsp;</p>

<p>&nbsp;</p>

<p>At the heart of this partnership is &lsquo;<a href="https://vault.thefmha.com/">The Vault</a>&rsquo;, FMHA&rsquo;s free mental health platform, developed collaboratively by psychologists, coaches, parents, carers, licensed FIFA Agents and ex-pros. The goal is to address mental health challenges in grassroots football in a way that is relatable and easy to understand for everyone, regardless of their level of involvement.</p>

<p>&nbsp;</p>

<p>Reward&rsquo;s financial support will facilitate the expansion of The Vault, which offers expert-led, football-themed content such as webinars, podcasts,&nbsp;a mental health playbook&nbsp;and articles that delve into the intersection of mental health and football. Additionally, it provides mental health first aid training and signposting to specific mental health support, all tailored for football clubs to promote open conversations and create a more positive and supportive culture within grassroots football.</p>

<p>&nbsp;</p>

<p>Danny Matharu, founder of the FMHA and The Vault, said; &ldquo;We are elated to partner with Reward.&nbsp;They have a culture which recognises the importance of health and wellbeing, demonstrated by the recent launch of their internal&nbsp;<a href="https://www.rewardfinancegroup.com/reward-finance-group-launches-wellness-team-to-support-its-staff-nationwide/">Wellness Team</a>, dedicated to supporting colleagues. This approach&nbsp;resonates with our values and we look forward to working together to use the medium of football to promote mental health.</p>

<p>&nbsp;</p>

<p>&ldquo;Statistics show that one in four people experience mental health difficulties each year and one in six working-age adults have mental health symptoms at any given time. Mental health conditions often develop early, with 75% of cases emerging by age 18, and men aged 40-49 have the highest suicide rates in the UK.</p>

<p>&nbsp;</p>

<p>&ldquo;The Vault is dedicated to creating an inclusive and empowered environment where players, communities, coaches and volunteers can thrive both on and off the field, providing free support for all ages through anonymous sign-up, ensuring confidentiality and comfort. Mental health is a top priority for CFAs across England, and with support from Reward, we can continue to empower the English grassroots game by offering consistent, football-themed mental health tools.&rdquo;</p>

<p>&nbsp;</p>

<p>Nick Smith, group managing director of Reward Finance Group said; &ldquo;Our dedicated focus is promoting positive mental health and wellbeing within our team. Hence, partnering with Danny and the FMHA became a new way to further that aim. The evident lack of mental health support in the football community means this digital suite is a truly innovative development.</p>

<p>&nbsp;</p>

<p>&ldquo;Our partnership with the FMHA follows the launch our 2023-2024 corporate charity partner,&nbsp;<a href="https://www.rewardfinancegroup.com/charity-work/andysmanclub/">Andy&rsquo;s Man Club</a>, which is a men&
#39;s suicide prevention charity offering free-to-attend peer-to-peer support groups across the United Kingdom and online. At Reward, we strive to ignite a meaningful shift in mental health perceptions and&nbsp;to&nbsp;ensure&nbsp;robust support is accessible to those in need.&rdquo;</p>', 461, '
2024-01-07 08:19:34
', '2024-01-23 16:10:50
', 0, 'https://www.rewardfinancegroup.com/', 'reward-finance-group-1-1', '2024-01-18 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (447, 'Gripple', 'GRIPPLE SECURES ACCREDITATION THAT DEMONSTRATES COMMITMENT TO FAIR PAY', '<p>Gripple, the Sheffield-headquartered, employee-owned manufacturer of wire joiners and tensioners and suspension solutions for agriculture, construction, solar and infrastructure, is celebrating being accredited to the Living Wage Foundation, demonstrating its commitment to fair pay for employees.  <strong>(Living Wage Week is 6th to 12th November 2023)</strong></p>

<p></p>

<p>To become accredited, Gripple had to demonstrate that all its employees, plus contractors, received the Real Living Wage of
£10.90 for over 18s, which is a calculation of the cost of living, based on a basket of household goods and services. This figure is above both the Government
’s minimum wage (
£10.18 for over 21s), and the National Living Wage (
£10.42 for over 23 years). At the same time, Gripple
’s sister company Loadhog, which specialises in returnable packaging solutions, has also become a Living Wage Employer.</p>

<p></p>

<p>The Real Living Wage rate, that Gripple and Loadhog offers, is higher as it is independently calculated based on what people need to meet the costs of living, not just the government minimum. When the Living Wage rates are increased, accredited employers like Gripple and Loadhog are required to increase employee wages in line with this within six months.</p>

<p></p>

<p>Kevin St Clair, Managing Director, Gripple UK &amp; Ireland, said:
“We are delighted to be accredited to the Living Wage Foundation, which clearly demonstrates our commitment to our people as an employee-owned business. With recent cost of living increases, we wanted to reaffirm that our wages are in line with rising costs, for the benefit of our people now and in the future.
”</p>

<p></p>

<p>Kevin adds:
“We also offer a pension scheme which far exceeds that offered by most businesses, another great reason for people to work at Gripple!
”</p>

<p></p>

<p>Katherine Chapman, Director of Living Wage Foundation:
“We welcome the news that Gripple and Loadhog have committed to being Living Wage employers.  They join 13,000 other businesses across the UK who believe their staff deserve a wage which meets everyday needs. Businesses which pay the Real Living Wage usually find that they have a more loyal, more motivated workforce, which results in greater productivity, efficiency and retention, which is a win-win for businesses and their people.
” </p>

<p></p>

<p>The Living Wage Foundation is at the heart of the independent movement of businesses and people that campaign for the idea that a hard day
’s work deserves a fair day
’s pay. The organisation celebrates and recognises the leadership of responsible employers who choose to go further and provide a decent standard of living by paying the real Living Wage, adopting Living Hours and Living Pensions as well as wider good employment practices.</p>

<p></p>

<p>Over 400,000 employees have received a pay rise as a result of the Living Wage campaign and accredited employers include Nationwide, Google, LUSH, Everton FC and Chelsea FC, plus FTSE 100 companies and SMEs.</p>', 462, '
2024-01-07 08:21:57
', '2024-01-22 00:05:08
', 0, 'https://www.gripple.com/', 'gripple-2024', '2024-01-22 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (448, 'Ardent', 'A leading surveying and consents management practice has strengthened its presence in Leeds by opening a new office.', '<p>Ardent, which has expertise in the transport, renewables, utilities and regeneration sectors,</p>

<p>has expanded into a new location in the city following a series of new hires &ndash; and plans for</p>

<p>further growth.</p>

<p>&nbsp;</p>

<p>The company, which also has offices in Birmingham, London, Warrington and Dublin, is</p>

<p>aiming to reach a 250 headcount by 2026 and its teams in Yorkshire is key to that growth. It</p>

<p>has also expanded its office in Glasgow at the same time.</p>

<p>After six years in Leeds, Ardent is moving its 16-strong team into an office at 36 Park Row in</p>

<p>the city centre.</p>

<p>&nbsp;</p>

<p>The new office allows for further expansion in the region and is the base for the company&rsquo;s</p>

<p>new Planning &amp; Engagement directorate, as well as delivering across all of its key service and</p>

<p>sector areas.</p>

<p>&nbsp;</p>

<p>Carl Weaver, Chief Operating Officer at Ardent &ndash; which surpassed the &pound;10 million turnover</p>

<p>mark in its most recent set of accounts and has undergone a major rebrand, said the move</p>

<p>showed the company&rsquo;s commitment to all areas of the UK.</p>

<p>&nbsp;</p>

<p>He said: &ldquo;We&rsquo;ve been established for longer in Leeds &ndash; since 2017, when we first began work</p>

<p>on HS2 Phase 2B. And, while that was the project that gave us a foothold in the city, we have</p>

<p>grown significantly since then and people from all service areas and directorates within the</p>

<p>business are now based here.</p>

<p>&nbsp;</p>

<p>&ldquo;Again, we are showing to Leeds and to Yorkshire that we are committed to investing and</p>

<p>growing here and the two new offices form part of our wider ambitions to double the size of</p>

<p>the business over the next three years.&rdquo;</p>', 463, '
2024-01-07 08:26:40
', '2024-01-22 00:05:13
', 0, 'https://www.ardent-management.com/', 'ardent-01-2024', '2024-01-22 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (449, 'Incommunities', 'From school leavers to a former dental nurse, six Bradford residents choose new career paths', '<p>Housing provider Incommunities has welcomed a new group of apprentices for 2023.</p>

<p>&nbsp;</p>

<p>The successful six candidates were chosen from 360 applicants who were all vying for the opportunity to learn while they earn.</p>

<p>&nbsp;</p>

<p>The new recruits are all from Bradford. They join the 16 current Incommunities apprentices who are at various stages of their 2 to 4-year apprenticeships, spending the week split between learning &lsquo;on-the-job&
#39; skills with time at either Leeds College of Building, Bradford College, and most recently, Shipley College.</p>

<p>&nbsp;</p>

<p>Emma Thompson, from Wibsey, who is the first ever Horticultural apprentice at Incommunities, said: &ldquo;I&rsquo;m not your typical apprentice! I&rsquo;m not a school leaver, I&
#39;m 33 and have been a dental nurse for the last 7-years, but my real passion has always been gardening. So, when I decided to make the leap and look for a new job, I kept my options open and looked into less traditional routes.</p>

<p>&nbsp;</p>

<p>&ldquo;When I saw the ad for the horticultural apprentice, I thought it was too good to be true. I never expected to get the job!</p>

<p>&nbsp;</p>

<p>&ldquo;I&rsquo;m now loving what I&
#39;m doing, gaining knowledge and skills &ndash; even learning about soil pH levels and, at the minute I&nbsp;have to&nbsp;learn the Latin names for 20 UK plants &ndash; every day is different.</p>

<p>&nbsp;</p>

<p>&ldquo;Overall, the thing I&
#39;m enjoying the most is seeing the difference that our work makes to people living in the community.&rdquo;</p>

<p>&nbsp;</p>

<p>With a workforce of more than 900, Incommunities is one of Bradford&rsquo;s biggest employers. They provide more than 22,000 social housing homes across the district, and have their own building services team, horticultural specialists, and teams of estate cleaners.</p>

<p>&nbsp;</p>

<p>In addition to their trade NVQs, this year&rsquo;s six apprentices will be doing some extra training in customer services skills, as part of an NVQ at Shipley College.</p>

<p>&nbsp;</p>

<p>Sara Sheard, Executive Director of Business Operations at Incommunities, said: &ldquo;The volume and standard of applications for this year&rsquo;s apprentices was outstanding. Apprenticeships provide such valuable opportunities for everyone involved. We are delighted that this year we&rsquo;ve been able to expand our apprentice scheme to include horticultural and cleaning.</p>

<p>&nbsp;</p>

<p>&ldquo;The new recruits all demonstrated their desire and commitment, to not only develop their practical skills, but also to build on their interpersonal skills which will be hugely beneficial when they&rsquo;re communicating with our customers.&rdquo;</p>

<p>&nbsp;</p>

<p>Harry Singleton from Idle, who is one of four new apprentice property maintenance operatives starting this year, said: &ldquo;I wanted to do an apprenticeship because of the combination of developing practical skills and learning at the same time, plus getting paid.</p>

<p>&nbsp;</p>

<p>&ldquo;I was on another apprentice scheme before starting at Incommunities, but I left because it didn&
#39;t offer me enough variety of work. Since starting here, I have already been able to try plastering, joinery, painting and decorating &ndash; all with the support of my manager. And, the great thing is, I know there is so much more to come.&rdquo;</p>

<p>&nbsp;</p>

<p>Apprenticeship programmes usually start in August/ September. Anyone interested in apprenticeship opportunities should contact their local college.&nbsp;</p>', 464, '
2024-01-07 08:35:48
', '2024-01-23 16:11:59
', 0, 'https://www.incommunities.co.uk/', 'incommunities-1', '2024-01-23 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (450, 'LCM Environmental', 'A third acquisition expands the LCM Environmental Group', '<p><strong>Good Things Come in Threes &ndash;&nbsp;</strong><strong>Repair Protection &amp; Maintenance Limited and Sprayglass International Ltd become the third acquisition of LCM Environmental Services Limited in</strong><strong>&nbsp;the past year.</strong></p>

<p>&nbsp;</p>

<p>Completing a third acquisition in the past 12 months, international fuel and tank storage risk-mitigation specialists LCM Environmental Services Limited have successfully brought two North Yorkshire-based businesses,&nbsp;Repair Protection &amp; Maintenance Limited&nbsp;and Sprayglass International Ltd, into their group of companies.</p>

<p>&nbsp;</p>

<p>RPM are a highly regarded supplier of tank lining, bund and advanced coating service specialist services, whose offering compliments LCM&rsquo;s current environmental service range.&nbsp;This strategic collaboration represents an important milestone in LCM&
#39;s growth and diversification, enabling the company to further enhance its offerings and better serve the needs of businesses across the UK and Europe.</p>

<p>&nbsp;</p>

<p>Sprayglass specialises in the manufacture of a range of chemical-resistant Glass Flake and GRP Laminating Linings suitable for application to both metallic and concrete substrates, providing reliable protection to the industry since the 1970s.</p>

<p>&nbsp;</p>

<p>Throughout 2023,&nbsp;<a href="https://click.agilitypr.delivery/ls/click?upn=4nhDsAULJt2edAw2wx4g-2FCxtYZia4H7ncJ3LkPj5JW5cCqjMP4RuJKM3kKoEax7G2Yk2_o6dqhrN2Gk8ocrNSMZ6-2FgXuFK1azi5mRFl3Pe5Vlmn3EE1-2Fq5v-2BxKsBdHoSs4-2Be8fDalqgNAgHLMVdqgLoBkstSy41xosMIYQPdRiSjfztsIA1C2Eya5rK4oD-2FB66XsiLB-2BOIzOM0O1BtGMDXz5cFypmIkGJc4NFgLuterAWW6BOvdDJ2hvlv6f5bsV4OGCh2-2BoXhPg5J38j44O9SmB4eKXnIsmEfI6H0iTq8Jefj-2Bat-2Fcn3J7uQ2FK9uEJgse6ZqowYkavDO4PxcWVEF0w-2FtbjsAcVqK6t-2BCPXos8vB7TNrBzeWn5kKESG2QMVlCIBphq1Fr8CPuPkHTK3zGHY-2Bhm5SXJY5kgZfW6xkWAx3kuc-2BUB-2FLkOPxWqVRnGs4Ys5sXYYBA7dRyLAa1akEL4kMdhyv6usB54le01IokBDu8g4xVlGduYE52YcKMAnzxFx8ZOqR5DAHSou1SPjftj1iKTpiHRUjSVKWYVNAPN-2B7pnQ-3D">LCM Environmental</a>&nbsp;have exhibited exponential growth, and have been recognised by winning the &ldquo;Transformation Award&rdquo; at The Lancashire Red Rose Awards &ndash; as well as by claiming their rightful place on The Sunday Times&
#39; 100 Fastest Growing Companies for the year. Amongst their wealth of success in the past 12 months, LCM are in the process of completing several major European projects in Ireland, Germany, France, and Belgium.</p>

<p>&nbsp;</p>

<p>The acquisition of both RPM Ltd and Sprayglass International signifies LCM&
#39;s commitment to expanding their footprint and providing a more comprehensive suite of environmental services. This partnership brings together three industry leaders with a shared vision for supporting sustainable and environmentally responsible solutions.</p>

<p>&nbsp;</p>

<p>RPM&rsquo;s speciality focus is on protective coating, lining and repair/maintenance services. Their reach is international, with several completed projects across Europe, and plans to expand beyond. This acquisition enables both RPM and Sprayglass to benefit from LCM&rsquo;s own extensive experience in fuel management, as well as further environmental risk mitigation and scheduled maintenance programs.</p>

<p>&nbsp;</p>

<p>With over 80 years of experience combined, the synergy between the two companies will drive innovation and deliver more comprehensive solutions.</p>

<p>&nbsp;</p>

<p>Legal support was provided by Nicola Cooper and Olivia Storey from Clarion in Leeds, Taxation advice was provided by Ryan Wilkinson from BHP Chartered Accountants and Barclays Bank provided financial support.</p>

<p>&nbsp;</p>

<p><strong>Richard Wallace</strong>, CEO of LCM Environmental Group comments: &lsquo;<em>This acquisition represents a significant step in our mission to offer comprehensive, sustainable, and innovative environmental services. We are excited to welcome RPM Ltd into the LCM family of businesses and look forward to creating a brighter future together.</em>&rsquo;</p>

<p>&nbsp;</p>

<p><strong>Ian Simon</strong>, General Manager of RPM Ltd and Sprayglass International Ltd, shares: &lsquo;<em>We are thrilled to join forces with LCM Environmental Services Ltd. Our combined expertise will provide our clients with even more environmentally responsible solutions while contributing to a safer, more sustainable world</em>.&rsquo;</p>', 465, '
2024-01-07 08:38:44
', '2024-01-23 00:05:05
', 0, 'https://lcmenvironmental.com/', 'lcm-environmental-1', '2024-01-23 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (451, 'Yorkshire Business Journal 2024', 'Yorkshire Business Journal wins important award', '<p>The Yorkshire Business Journal is an online business news platform that aims to give a voice to Yorkshire based businesses.&nbsp; The team at the Yorkshire Business Journal is extremely happy to have won its first ever award since establishing the platform in 2020: SME News has awarded the Best Online Local Business News Platform 2023 &ndash; Yorkshire to the Yorkshire Business Journal.</p>

<p>We have exciting plans for the future, although it is easier to reach out to businesses through an online business news platform we will soon organise friendly in-person get-together events that will also host talks from some of the most interesting companies in Yorkshire.</p>

<p>&nbsp;</p>

<p>Ad maiora!</p>

<p>The team of the Yorkshire Business Journal&nbsp;</p>', 466, '
2024-01-10 10:44:13
', '2024-01-10 10:44:53
', 0, 'yorkshirebusinessjournal.co.uk', 'yorkshire-business-journal-2024
', '2024-01-10 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (452, 'Another Concept', 'Web Interview', '<p><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/yLrD7b-Txis?si=FjvkL2G0tlzjtOYM" title="YouTube video player" width="560"></iframe></p>

<p>Another Concept is a Leeds-based digital marketing agency with a difference. We&rsquo;re redefining the traditional agency model and creating lasting partnerships with ambitious brands who want to exceed their digital goals.</p>', 479, '
2024-01-23 16:11:48
', '2024-01-24 00:05:04
', 0, 'https://anotherconcept.co.uk/', 'another-concept', '2024-01-24 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (453, 'Palmer Landscapes', 'Creating beautiful eco-friendly green spaces for over 50 years.', '<p>&nbsp;</p>

<p>&nbsp;</p>', 480, '
2024-01-23 16:19:46
', '2024-01-25 06:51:17
', 0, 'https://palmerlandscapes.co.uk/', 'palmer-landscapes-1', '2024-01-25 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (454, 'Drake & Macefield', 'Historic butchers meat demand', '<p>Craven’s longest serving independent family butcher, Drake &amp; Macefield reflects on this year’s significant milestone - 125th years in business spanning three centuries, operating three branches and delivering direct to customers.</p>

<p></p>

<p></p>

<p></p>

<p>Gaining numerous local and national awards along the way, including a 2023 Great Taste Award, the family butchers has grown considerably since it was established on Otley Street in Skipton in 1898 by Robert Drake who was soon joined by his young nephew Jack Macefield, initially after school and on Saturdays before finishing his education.</p>

<p></p>

<p>Jack
’ son, Ian Macefield, recalls his father telling how he was often late for school as he considered their customers to be his number one priority. He explained how his Uncle Bob taught him how to judge the best quality cattle and sheep so that young Jack soon utilised those skills at the local farms and auctions. Jack maintained the provision of best quality and customer service throughout his working life and Ian believes he would be justly proud that Drake &amp; Macefield continue this philosophy today.</p>

<p></p>

<p>Jack died in in the early 80s, and nearly a century after the butchers shop was founded, in 1985 Ian Thompson who had been trained by Jack took over the business and was joined by partner, Richard Teal in 1997. Following Ian
’s retirement in 2016, Richard Teal
’s sons Andrew and Steven joined the business restoring the original
‘family business
’ ethos. In turn, they are joined by brothers Steven and Adam Proctor who manage the Skipton and Settle shops respectively.</p>

<p></p>

<p>Drake &amp; Macefield has weathered many challenges over the years including two world wars, post war rationing.
‘foot and mouth
’ which affected supply, along with recessions and vigorous competition from supermarkets which forced many butchers out of business. Far from closing, the firm has grown from strength to strength and now boasts branches in Settle, opened in 2000, and in Crosshills in 2018, along with a processing unit and a bakery producing their award-winning pies.</p>

<p>Drake &amp; Macefield now employs 28 staff and as well as serving domestic customers, commercial contracts mean its pies are favoured by Leeds Utd supporters and many local hotels, pubs, restaurants and village stores.</p>

<p></p>

<p>COVID lockdown brought about a seismic shift in the way Drake &amp; Macefield serves its customer base. Responding quickly to demand from its loyal customers, the team had to find a way to fulfil 250 deliveries a day. The firm soon had a fleet of five vans dashing round the dales and Aire Valley, ensuring their quality meat products kept people fed during those worrying times. An online shop had to be hastily constructed on the website to ease the administration of the new click-and-collect and home delivery service.</p>

<p></p>

<p>Although COVID restrictions are largely forgotten, the legacy of the online delivery service is still thriving. Director Steven Teal explains
“Customer demand is paramount alongside the quality of produce we offer, so if the customer enjoys the convenience of farm fresh meat delivered to their own doorstep, then that
’s what we
’ll continue to provide. Offering our own pies, sausages, cured and smoked meats, alongside delicious ready-to-cook meals, we are much more than a traditional small town butchers!
”</p>', 482, '
2024-01-25 06:50:55
', '2024-01-29 00:05:05
', 0, 'https://www.drakeandmacefield.co.uk/', 'drake-macefield', '2024-01-29 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (455, 'Spike', 'Leeds-based agency, Spike fully charged by GEO Green Power client win', '<p>Following a competitive pitching process, marketing agency&nbsp;<a href="https://u7061146.ct.sendgrid.net/ls/click?upn=4tNED-2FM8iDZJQyQ53jATUQfiB2dYJm2B8y7tjD0Wmx0-3DctZG_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQWdUbaz1aPjFvWg12WAhoAbU0dQ4j76cxY1HieV4-2FYF43zTGXTpD3PMU8cDQWk-2F8efmIOV61RBi6v-2Bnjc5yxPKHK0cdrCVlKWAqQ5iNygx8chYnRNM9qNZcLLRE49Bg1fwaq9qbaxUzAH7MyQ2lhOjLIt2NkKQzeTleh0zQgD20RCIuitwOddzwzNf9OJj0QuyfJFOS1dQZVPVlUzpeblmtgyckx7nQGJlmPHRn-2FnBqSyXxkyZ0BQ46r4eMVRVptTT0w0wXS6TlEqXW6LIsAn86x5eWARLUT-2Bi7uapFe81hek1WCofIAhUE3u37fShdHtc">Spike</a>&nbsp;has been appointed by nationwide renewable energy installer,&nbsp;<a href="https://u7061146.ct.sendgrid.net/ls/click?upn=4tNED-2FM8iDZJQyQ53jATUTIgxlv70Q7axk5MyPYYXPysAbykwdvkHCH59VtJMIgpyrXY_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQWdUbaz1aPjFvWg12WAhoAbU0dQ4j76cxY1HieV4-2FYF43zTGXTpD3PMU8cDQWk-2F8efmIOV61RBi6v-2Bnjc5yxPKHK0cdrCVlKWAqQ5iNygx8chYnRNM9qNZcLLRE49Bg1fwaq9qbaxUzAH7MyQ2lhOjLIt2NkKQzeTleh0zQgD20RCXw6Dxibxl2pR7bi1uNpNQxoREOi28kv199B9XS-2B-2BfXG-2FwucCLIm3-2F23c-2FbUCXlN7vGcn4p7-2FGYjAVqNMcmUeqruKGuZeazBBnvEBaKN4fourhiZ7b2z3mxT4Z-2FKKGdgRVHM-2BYd6XKiWEZyDROt0J8">Geo Green Power</a>, to provide search marketing and digital PR services.</p>

<p>&nbsp;</p>

<p>Spike will be working with Geo Green Power to help accelerate growth across UK businesses and homeowners looking to reduce their carbon emissions and save money by investing in renewable energy systems such as solar PV, battery storage and heat pumps. Spike will support Geo Green Power in achieving their goal to become the UK&rsquo;s leading renewable energy installer.</p>

<p>&nbsp;</p>

<p>Established in 2010, Geo Green Power has achieved consistent growth and now ranks among the UK&rsquo;s leading renewable energy installers. Committed to providing power for a green future, Geo Green Power install around 1.5MW of solar PV every month on sites across the UK. Committed to their own sustainability the company plant a tree for every quote they produce, heat and power their offices with solar energy and a ground source heat pump and are committed to transitioning to a fleet of plug-in hybrid and electric vehicles.</p>

<p>&nbsp;</p>

<p>Operating nationwide from its Wysall head office, the company offers a number of services for both commercial and residential customers. Their services include solar installations, renewable heating, battery storage systems, and electric vehicle charging. An impressive client list includes Mercedes Benz, Kingspan, JCB and Panasonic.&nbsp;</p>

<p>&nbsp;</p>

<p>Geo Green Power has also won its fair share of awards, recently being named a finalist for &lsquo;Specialist Contractor of the Year 2023&rsquo; by Construction News. Their submission highlighted a significant year during which they tripled their installation team and increased their turnover to &pound;12million.&nbsp;</p>

<p>&nbsp;</p>

<p>Kat Auckland, Communications &amp; Projects Director at Geo Green Power, commented that &ldquo;Geo Green Power has seen significant growth in the last two years, accelerated by increasing awareness of the climate crisis, with businesses and individuals wanting to reduce their emissions and save costs against steep rises in their energy costs caused by global events.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;We want to maintain and build on this growth to become the UK leader and are very excited about our new partnership with Spike, which will increase our online and digital presence and reach a wider audience.&rdquo;</p>

<p>&nbsp;</p>

<p>Rob Powell, Director at Spike added, &ldquo;As we continue to focus our efforts on attaining a B-Corp status, we are thrilled to announce that Spike will be working in partnership with Geo Green Power who are at the very forefront of sustainable power technology and true innovators in renewable energy&rdquo;</p>

<p>&nbsp;</p>

<p>The new client win reflects the agency&
#39;s continued expansion, marked by the acquisition of new clients across specialist PPC and SEO departments.</p>

<p>&nbsp;</p>

<p>Spike, an experienced digital agency based in Leeds, prides itself on cultivating lasting client relationships and implementing cutting-edge strategies to generate results. Their track record in Paid media includes award-winning campaigns, such as their work for Mighty Drinks, which boosted brand awareness and online revenue by 280% in the first year.</p>', 483, '
2024-01-27 06:16:03
', '2024-01-29 00:05:07
', 0, 'https://spike.digital/', 'spike-1-1', '2024-01-29 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (456, 'Levi Solicitors', 'Levi Solicitors win at the British Wills and Probate Awards 2023', '<p>Levi Solicitors won the Private Client Team of the Year - Boutique award at the sixth annual British Wills and Probate Awards in October.</p>

<p>&nbsp;</p>

<p>The awards were hosted in Manchester by broadcaster Jennie Bond, recognising and celebrating excellence in the wills and probate sector.</p>

<p>&nbsp;</p>

<p>The awards are increasingly recognised as a badge of honour for practitioners; recognising the individuals and organisations who deliver technological and operational innovation, excellence in the technicalities of the law, and exceptional client service.</p>

<p>&nbsp;</p>

<p>Commenting on their triumph, Levi&rsquo;s Senior Partner, Ian Land, said:</p>

<p>&nbsp;</p>

<p><em>&ldquo;We are so proud of our team&rsquo;s success in the British Wills and Probate Awards. This recognition in a national awards ceremony is testament to the team&rsquo;s fantastic work on behalf of our clients. It is a great way to round off a busy year of achievements and growth, particularly in our Wills and Probate team, and we are absolutely delighted to have won this award.</em><em>&rdquo;</em></p>', 485, '
2024-01-27 06:19:59
', '2024-01-30 00:05:04
', 0, 'https://levisolicitors.co.uk/', 'levi-solicitors-5', '2024-01-30 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (457, 'Gripple', 'Research shows employee-owned businesses are 8-12 per cent more productive', '<p>New research carried out across the UK* has revealed that employee-owned businesses (EOBs) are outperforming the UK&rsquo;s national productivity trend by being between 8-12% more productive than non-EOBs, based on Gross Value Added (GVA) per employee.</p>

<p>&nbsp;</p>

<p>South Yorkshire headquartered Gripple was part of this research project, which compared nine per cent of EOBs in the UK and contrasted them with a comparable control group of non-EOBs. This included interviewing the company&rsquo;s management team and many other businesses to better understand the decisions that are driving economic, social and environmental impacts.</p>

<p>&nbsp;</p>

<p><strong>Ed Stubbs, Gripple&rsquo;s Global Managing Director</strong><strong>,</strong>&nbsp;said: &ldquo;It is really satisfying to see the results of this recent survey.&nbsp; As an employee-owned business, we know the difference that having employees engaged in all aspects has on productivity.&nbsp;It means we all are committed to the quality of our products and services - something that is not easy for our competitors to match. We&rsquo;re all working towards the same targets, and we all benefit when we meet them, so the focus on maintaining high standards is consistent across the business.&rdquo;</p>

<p>&nbsp;</p>

<p>The study revealed that:</p>

<p>&nbsp;</p>

<ul>
	<li>EOBs return twice as much in bonuses and dividends to employees.</li>
	<li>EOBs were five times less likely to make people redundant in the last three years.</li>
	<li>EOBs tend to pay higher minimum annual wage by roughly &pound;2,900 and are over twice as likely to hold accreditation for fair pay.</li>
	<li>EOBs provide more supported access to private healthcare, mental health resources and flexible working.</li>
	<li>EOBs invest on average 12% per annum (&pound;38,000) more in on-the-job training and skills.</li>
	<li>83% of EOBs reported increased employee motivation since adopted an EO model and 73% reported increased job satisfaction.&nbsp;</li>
</ul>

<p>As well as being more productive, the research project found that EOBs were more profitable and were more likely to re-invest that money into the economy by creating more jobs and investing in improving their products and services.</p>

<p>&nbsp;</p>

<p>Gripple, which has been an employee-owned business since 2004, concurs with these latest findings, as the company&rsquo;s EO culture gives all its people direct responsibility and encourages them to be actively involved.&nbsp; This, in turn, empowers them to drive&nbsp;improvements, and ultimately, contribute to the business&
#39; success.</p>

<p>&nbsp;</p>

<p>Ed Stubbs adds: &ldquo;We are proud to be working alongside the Employee Ownership Association (EOA) to encourage and support other UK businesses that are transitioning to an Employee-Owned model &ndash; the research really does back up our experience!&rdquo;</p>

<p>&nbsp;</p>

<p>Commenting on this latest research,&nbsp;<strong>James de le Vingne, Chief Executive of the EOA, said:</strong>&nbsp;&ldquo;This is a new baseline in our understanding of the scale and impact of employee and worker ownership in the UK, contrasting EOBs with non-EOBs for the first time across economic, social and environmental outcomes. The findings are remarkable.&rdquo;</p>

<p>&nbsp;</p>

<p>&ldquo;They clearly show that this small but growing section of the UK economy is punching above its weight across multiple dimensions to impact on individuals, businesses, communities and the wider economy. We urge policy makers to work with us to super-charge this responsible productivity our country so sorely needs.&rdquo;</p>

<p>&nbsp;</p>

<p>More information is available to businesses interested in employee ownership through the Employee Ownership (EO) Knowledge Programme.&nbsp; This was launched by the EOA in 2022 and is led by Ownership at Work, working in partnership with independent research firms, including WPI Economics and DJS research.&nbsp;&nbsp;&nbsp; The Knowledge Programme aims to increase understanding of the scale and impact of employee and worker ownership in the UK, contrasting EOBs with non-EOBs for the first time across economic, social and environmental outcomes.&nbsp; You can download the EO Knowledge Programme&nbsp;<a href="https://employeeownership.co.uk/kp/">here</a></p>

<p>&nbsp;</p>

<p>Gripple currently employs 559 people across seven manufacturing facilities in Sheffield and has over 900 employees globally.&nbsp;&nbsp;All Gripple employees have had the opportunity to own a piece of the business since 2004 and in 2011, GLIDE (Growth Led Innovation Driven Employee) Company was born.&nbsp; GLIDE&nbsp;is an employee-owned private company&nbsp;which represents all the 900 shareholder members working in its partner companies.</p>', 486, '
2024-01-27 06:27:14
', '2024-01-30 00:05:06
', 0, 'https://www.gripple.com/', 'gripple-1', '2024-01-30 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (458, 'Basecamp Skills', 'Basecamp Skills hires first Operations Director, Ex-Northcoders Managing Director Claydon', '<p>Manchester (23rd November 2023), The fully-funded training academy, Basecamp Skills, has appointed Lisa Claydon as its first Operations Director this week. The education platform, founded by the former COO of Social Chain, Dominic McGregor, along with fellow entrepreneurs David Newns and Charlie Yates, hired former Managing Director of Northcoders to accelerate their next stage of growth, while promoting Amy Wild to Chief Executive Officer (CEO) of the company.</p>

<p>Launched in 2022, Basecamp Skills has since won over
£2M in funding from the Department for Education, as well as a string of impressive business awards. These include The British Business Awards under
‘Inclusion Initiative of The Year
’, as well as The Start-Up Awards
‘Education and Training Startup of the Year
’ and
‘Overall North West Business of the Year
’.</p>

<p>Basecamp Skills delivers industry-led digital marketing training to help individuals over the age of 19 looking to upskill, retrain or kick-start a career in digital marketing. They promise every single learner access to a fully-funded training course, delivered by industry marketing professionals and secure a job interview should they wish to pursue a career in the sector. To date, it has already successfully enrolled over 350 learners across 7 cohorts, with 58% being from gender minorities and 48% from an ethnic minority background.</p>

<p>Claydon brings a wealth of training provider experience from leading operations, managing staff and overseeing quality and compliance in her previous roles. With plans to build upon Basecamp Skill
’s strong foundation, Claydon joins the team with a focus on upholding the quality of its offerings and fine-tuning its experiential model.</p>

<p>Lisa Claydon, Operations Director at Basecamp Skills said:
“I am thrilled to be joining Basecamp Skills and leading operations for such an impactful training provider. Now more than ever, we need innovative approaches to empower learners and equip them with skills to succeed. Basecamp
\'s experiential learning model represents exactly the type of outside-the-box thinking required to engage learners meaningfully. I look forward to collaborating with the talented Basecamp team as we work to expand our reach and provide transformational experiences for more learners across the UK.”</p>

<p>Amy Wild, recently appointed CEO at Basecamp Skills said
“With a proven track record in leading successful government-funded training programmes, Lisa will play a crucial role in scaling our offerings during this period of rapid growth. Her extensive experience will be invaluable as we expand into new markets, enhance our offerings, and structure teams for greater impact. Lisa
\'s leadership and operational expertise will improve the learner experience and outcomes, marking an important milestone for Basecamp Skills. With Lisa on board, we are confident in our ability to change lives through access to in-demand skills at an unprecedented scale.”</p>

<p>Dominic McGregor, Fearless Adventures and Basecamp Skills co-founder said: "Amy\'s tenure as CEO has been truly transformational, marking a significant chapter in our company\'s growth. Now, with the strategic addition of a leader of Lisa\'s caliber to support in running the business, we are poised for an even more remarkable trajectory. I have the utmost confidence that, together, Amy and Lisa will revolutionise the education industry."</p>', 487, '
2024-01-27 06:32:54
', '2024-01-31 00:05:04
', 0, 'https://www.wearebasecamp.co.uk/', 'basecamp-skills', '2024-01-31 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (459, 'AgriSound', 'Harrogate environmental organisation enlists York based ag-tech trailblazers to demonstrate positive impacts of rewilding ', '<p>Launching at a time when prominent Royals have just been widely reported expressing opinions on what they see as negative impacts of rewilding, a groundbreaking environmental partnership using trailblazing AI algorithms to demonstrate the positive impacts of rewilding farmland is newly underway in North Yorkshire. With the aim of developing programmes for nurturing and boosting pollinator activity in the area, national rewilding project &lsquo;Make it Wild&rsquo; has teamed up with pioneering Ag-tech start-up, AgriSound in an initiative already providing demonstrable evidence of the significant benefits to pollinators and local biodiversity in Nidderdale.&nbsp;</p>

<p>&nbsp;</p>

<p>York-based ag-tech innovator, AgriSound has provided four bespoke &lsquo;Polly&rsquo; bioacoustic listening devices to monitor and produce robust data on the biodiversity and wildlife in areas that Make it Wild manages. These are allowing Make it Wild to gather irrefutable, data-driven evidence to show its rewilding in Harrogate&rsquo;s Bank Woods is leading to more pollinators and a significantly greater biodiversity than conventionally farmed land.&nbsp;</p>

<p>&nbsp;</p>

<p>AgriSound&rsquo;s Polly monitoring devices use cutting-edge AI tracking algorithms to recognise the sounds of pollinators and provide actionable data to improve land management strategies. Over just the past four months, the data collected has shown marked differences between pollinators levels across the site, and how the habitat influences the movement of pollinators. This data, gathered in real time, is providing Make it Wild with the capability to track which areas have the highest number of pollinators, and demonstrate where there is a noticeable increase throughout Bank Woods, adjusted for seasonal variations.Pollinator activity has increased by 38% since rewilding started at the site.&nbsp;</p>

<p>&nbsp;</p>

<p>Increasingly widely employed in commercial, regenerative agriculture, by food producers, helping revolutionise commercial pollination and increasing crop yield, this is the first time AgriSound&rsquo;s Polly bioacoustic listening devices have been used in rewilding.
  They are also now being used to produce crucial ESG data to businesses looking to minimise their impact on the environment, and recently worked to provide essential data to support biodiversity conservation.&nbsp;</p>

<p>&nbsp;</p>

<p>Make it Wild works with household names such as Footasylum and De Vere, through tree planting, or the sponsorship of other habitat creation projects to help reduce their carbon footprint and showcase their dedication to sustainability. Businesses such as Tesco and Marks &amp; Spencer&
#39;s work with AgriSound for similar reasons, with the Polly insect listening device feeding data into their sustainability efforts, strengthening their ESG reporting.&nbsp;</p>

<p>&nbsp;</p>

<p>Groups such as the Yorkshire Rewilding Network and Rewilding Britain are pillars of the movement, with the Rewilding Innovation Fund providing financing opportunities enabling large-scale restoration of ecosystems.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>The Government has put down plans for large-scale nature recovery projects that will make funding available to farmers looking to provided habitats for wildlife by rewilding. The total amount available for such schemes is reportedly anticipated to reach between &pound;700m and &pound;800m a year by 2028. If these plans are successful, it&
#39;s thought that rewilded farms could support more local employment and support the local economy more than conventional farms (1).&nbsp;</p>

<p>&nbsp;</p>

<p>Make it Wild&rsquo;s ambitious Harrogate project has involved removing the sheep, planting numerous trees, and restoring diverse habitats, including semi-improved pasture, gorse, upland scrub, small streams, and ancient woodlands.&nbsp;</p>

<p>&nbsp;</p>

<p>Founders, Christopher and Helen Neave bought their first piece of land to rewild as a family project in 2010 in response to the UK&rsquo;s ever-deepening ecological crisis, that has seen wildlife numbers plummet 70% since 1970. The impact on biodiversity of their rewilding and woodland creation was so great, that they decided to set up Make it Wild so they could have even more impact.
 &nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Helen Neave said about the collaboration: &quot;We know that pollinators are in deep trouble, and they are a crucial part of our ecosystem. In order to support our rewilding efforts and increase pollinator populations, we&rsquo;re very much anticipating the use of data from these Polly devices.&nbsp;</p>

<p>&nbsp;</p>

<p>&quot;We are the only rewilding project to be using AgriSound&rsquo;s monitoring devices, which can provide a crucial source of data on biodiversity. Rewilding is often mis-understood, and seen to threaten conventional farming, but we believe that the two philosophies of land management can work together.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;We are in the midst of the sixth mass extinction, and eager to demonstrate how we can make a difference in our impact on nature to reduce destruction and loss of habitat. The number one focus of our rewilding project is to increase biodiversity, and gathering data to show the positive results will be fundamental to our future success. We hope our partnership with AgriSound will underscore the importance of gathering demonstrable data for such initiatives and lay the ground for others to follow suit.&quot;&nbsp;</p>

<p>&nbsp;</p>

<p>Casey Woodward, CEO of AgriSound, stated about the project: &quot;We&rsquo;re constantly developing our AI bioacoustic monitoring technology to support an increasing variety of bespoke executions and we are excited to see how our data-driven approach to pollinator monitoring can be used in rewilding projects to protect and boost biodiversity.
 &nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;We&rsquo;re being asked to develop algorithms for increasingly inventive uses of the Polly in order to protect pollinators alongside our regenerative farming projects, and this rewilding project sets an exciting precedent for how our bioacoustic monitoring AI technology can be used for conservation.&rdquo;</p>', 488, '
2024-01-27 06:35:31
', '2024-01-31 00:05:06
', 0, 'https://agrisound.io/', 'agrisound', '2024-01-31 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (460, 'Ardent', 'Plans to speed up electricity connections welcome - but more needs to be done', '<p>A leading surveying and consents management practice with offices across the UK has welcomed a new Government plan to speed up connections to the electricity network  but says some of the detail could end up hindering the process. </p>

<p>The team at Ardent, which has expertise in the transport, renewables, utilities and regeneration sectors and has offices in Leeds, Glasgow Birmingham, London, Warrington and Dublin, has been reacting to the Connections Action Plan. </p>

<p>The plan aims to cut down the delays for new transmission connections to help speed up the drive towards net zero and was published alongside the Autumn Statement. </p>

<p>It is an area in which Ardent has particular expertise and its team has been looking at the detail within the report to see if it will achieve its aims. </p>

<p>And, while it has been broadly welcomed, Nick Dexter, Head of Utilities at Ardent, said other parts of the process would need to be reviewed to cut out delays in connections. </p>

<p>He said:
“We welcome the thinking and the sentiment behind the report. The drive to net zero is being held back by delays of up to five years and it is, therefore, vitally important that the Government recognises this and starts to address it. </p>

<p>
“One of the first aims is to improve the quality of projects applying for transmission connections in order to deter speculative applications and allow for those that really will make a difference. It
’s fair to say that there is a huge amount on the register that you could argue is clogging up the system. </p>

<p>
“However, we wouldn
’t want this to deter developers who have what appears to be a viable scheme but they don
’t necessarily believe it will meet new arbitrary requirements.  </p>

<p>
“If, for example, developers need to have land rights to be able to qualify for a connection it could weaken their position with a landowner when in negotiation as they won
’t want to lose out on achieving their connection so could, effectively, be held to ransom. </p>

<p>
“Seeing what the requirements actually are is going to be key. </p>

<p>
“Similarly, part of the plan is to reduce projects from the list that are not progressing to free up space and time to move on with those which are ready to go but, again, the detail here is going to be vital. </p>

<p>
“It is sensible to move away from the first come, first served model and try to move forward with projects that are feasible and ready more quickly but understanding what this means will be important as it will be quite a subjective view. It
’s important that this plan increases confidence in transmission connection applications rather than having the reverse effect. </p>

<p>
“It
’s also crucial that the plan balances out local and regional needs as well as looking at the national picture over what is more strategically important. </p>

<p>
“Ultimately, this plan focuses only on connections and unless there is a general review of major infrastructure delivery then delays will just be pushed to another part of the process either around land rights or consents.  </p>

<p>
“So, while we welcome the report and its aims, we still think there is more to be done to achieve the ultimate outcome of generating and transmitting more electricity to support great energy security and speed up the drive to net zero.
” </p>', 489, '
2024-01-27 06:40:16
', '2024-02-01 00:05:06
', 0, 'https://www.ardent-management.com/', 'ardent-1', '2024-02-01 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (461, 'Swype®', 'A Yorkshire Success Story, Defying Market Trends with 40% Growth!', '<p>Overcoming a crowded market sector and fierce competition, top design and marketing agency, Swype&reg; has experienced exceptional growth over the last 18 months. During this period turnover has increased by over 40%, their customer base has grown and diversified, the team has expanded, and they have relocated to a prestigious Head Office in Leeds whilst still retaining their Sheffield presence.</p>

<p>&nbsp;</p>

<p>It certainly has been a breathless period for the business who have also found time to pick up awards for the Clutch Global 1000 B2B Leaders, DNA Paris Design Awards 2023, the Global Web Excellence Awards 2023, Yorkshire Prestige&rsquo;s Branding Agency of the Year 2023/24, inclusion in the Clutch Top 100 SEO Agencies in the UK and the Clutch Top 100 Creative Agencies in UK.</p>

<p>&nbsp;</p>

<p>Founder and Head of Digital, Wesley Holmes says &ldquo;It&rsquo;s been a challenging period for us but ultimately generating growth, enhancing our reputation and delivering an unbeatable service is what Swype&reg; stands for.&rdquo; Certainly, the expansive range of services offered and the way they are delivered is enticing companies to come and talk to Wesley and his team.</p>

<p>&nbsp;</p>

<p>So, what exactly does Swype&reg; bring to the business community, how is it different to other, often larger competitors and why is it working so well?</p>

<p>&nbsp;</p>

<p>Their approach is to offer a multi-disciplinary team, a collective of creative thinkers and digital marketing experts that specialise in Branding, Websites and Digital Marketing. The key is to offer a personalised and collaborative approach where the design and marketing teams work</p>

<p>seamlessly towards their client&rsquo;s objectives. This unified framework is something that is frequently lost with their competitors resulting in an often-disjointed approach and unsatisfactory outcome. Wesley actively manages all client relationships thus ensuring a personalised approach throughout. Collaborative and transparent relationships mean that customers are encouraged to participate at all stages of a project should they wish to do so and be involved in shaping the strategy and project outcomes.</p>

<p>&nbsp;</p>

<p>Swype&reg; has a very results-based approach, focusing upon demonstrating how, where, and why their input is benefitting the customer&rsquo;s business. What the customer is getting for their money and demonstrating this is a priority for this team. Take the following examples:</p>

<p>&nbsp;</p>

<p>&middot; in a single client 360 Marketing Campaign, the team generated over &pound;1m in revenue representing 19x return on investment (ROI).</p>

<p>&nbsp;</p>

<p>&middot; the average Google Ads Campaign return on advertising spend (ROAS) across all clients both lead generation and eCommerce is an impressive 400%.</p>

<p>&nbsp;</p>

<p>Nor do the team stand still &ndash; a pre-requisite in the Swype&reg; business model is ongoing proactive work with customers ensuring that the agreed strategy is always agile and fluid, changing with customers&rsquo; aspirations and/or external market conditions.</p>

<p>&nbsp;</p>

<p>The business portfolio is diverse across sectors and those services delivered. Private and public sector clients ranging from Commerce to Logistics, Universities, Lifestyle and Leisure to Management Training, Retail and Personal Care. The expansion in the client base has been particularly strong reflecting the growing reputation and the subsequent plaudits that Swype&reg; have been experiencing. &ldquo;Just through sheer hard work, focusing upon giving the customer what they want in what I see as an innovative and refreshing approach has clearly made an impression in the marketplace and fuelled our growth&rdquo; is how Wesley explains this success.</p>

<p>&nbsp;</p>

<p>Linked to this approach is the fact that the business has niched down into what they do best which can be summarised as Branding, Websites, Search Engine Optimisation and Paid Marketing. They focus upon these disciplines and do not pretend to offer a one-stop-shop for everything where quality might be compromised. This is what they do, and this is what they do well!</p>

<p>&nbsp;</p>

<p>So, who is this young entrepreneur and digital marketing trailblazer? At just 25, Sheffield-born Wesley Ashton Holmes started creating websites at the age of 16 and gained a BA(Hons) at Sheffield Hallam University Graphic Design in 2021, then adding to his qualifications by graduating from the Digital Marketing Institute in 2022 and being awarded the Certified Digital Marketing Professional, CDMP. Now his business, Swype&reg; has blossomed both creatively and commercially. The creation of a team of talented thinkers, marketeers and designers has been key in taking the business to the next level according to Wesley, a team that offers a diverse skillset, innovative knowledge and application and a customer-centric approach giving measured outcomes and demonstrable value.</p>

<p>&nbsp;</p>

<p>The future certainly looks bright for Swype&reg; with a healthy new business pipeline, attracting enquiries from a wide range of sectors. A recent initiative that has been successfully launched</p>

<p>is the offer of a &pound;1500 grant to a start-up business every quarter. This allows new businesses to save costs in the most important part of building a new digital business, their brand, website, and route to market. To apply, contact the team on their website or email directly.</p>

<p>&nbsp;</p>

<p>An office in London is planned to follow the recent Leeds expansion and a further, ambitious plan is to enter the US with an office in California to support the growing base of overseas clients. Linked with this is that one of their most recent clients, (a leading UK teeth whitening brand), having experienced huge UK growth in partnership with Swype&reg;, are looking to launch their marketing strategy into the US in 2024.</p>

<p>&nbsp;</p>

<p>These certainly are very exciting times for this Yorkshire-based independent agency.</p>', 490, '
2024-01-27 07:20:22
', '2024-02-12 08:52:04
', 0, 'https://www.swype.co.uk/', 'swype', '2024-02-05 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (462, 'Spike - February 2024', 'Leeds-based agency, Spike fully charged by GEO Green Power client win', '<p>Following a competitive pitching process, digital marketing agency&nbsp;<a href="https://u7061146.ct.sendgrid.net/ls/click?upn=4tNED-2FM8iDZJQyQ53jATUQfiB2dYJm2B8y7tjD0Wmx0-3DAnUy_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQWdUbaz1aPjFvWg12WAhoAbU0dQ4j76cxY1HieV4-2FYF43zTGXTpD3PMU8cDQWk-2F8efmIOV61RBi6v-2Bnjc5yxPKHL75VkhttporeiE4g58Sdi7h3xjW7oYcrtJex9-2FYPy33OAanOpvfQ84HgvYecWh9yg5StgvWumYxyUNm8tcr8BpzxydTyRJww1pcxW1qXOMX-2FoC6lpUYsivaL8CtmzJgJX0xKnXKxhZLJA8cRud9KvR7lr54jFP9-2FN1JYA4bMvi1LLv6Q2FiJsub5-2FAq8QeiHGH3USVCPlDPZEQHeAl70C7cJpttgu7w9qy4Gn7Uxwnc">Spike</a>&nbsp;has been appointed by nationwide renewable energy installer,&nbsp;<a href="https://u7061146.ct.sendgrid.net/ls/click?upn=4tNED-2FM8iDZJQyQ53jATUTIgxlv70Q7axk5MyPYYXPysAbykwdvkHCH59VtJMIgpWw86_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQWdUbaz1aPjFvWg12WAhoAbU0dQ4j76cxY1HieV4-2FYF43zTGXTpD3PMU8cDQWk-2F8efmIOV61RBi6v-2Bnjc5yxPKHL75VkhttporeiE4g58Sdi7h3xjW7oYcrtJex9-2FYPy33OAanOpvfQ84HgvYecWh9yg5StgvWumYxyUNm8tcr8BqSYxlcIcVj-2Bf7kUATRht9yRZEMriR9vaQ8Mes3nKnzLeWLcCImslbVSK2KnwN10ms5AdO-2F0edRRQZUlKq8hYm1VbL89DVpHz03CYplqYqnKIXRkZZ-2BkivYcnI93hvJDcaCsfnUI9wZMfJqC6G7P2wc">Geo Green Power</a>, to provide search marketing and digital PR services.</p>

<p>&nbsp;</p>

<p>Spike will be working with Geo Green Power to help accelerate growth across UK businesses and homeowners looking to reduce their carbon emissions and save money by investing in renewable energy systems such as solar PV, battery storage and heat pumps. Spike will support Geo Green Power in achieving their goal to become the UK&rsquo;s leading renewable energy installer.</p>

<p>&nbsp;</p>

<p>Established in 2010, Geo Green Power has achieved consistent growth and now ranks among the UK&rsquo;s leading renewable energy installers. Committed to providing power for a green future, Geo Green Power install around 1.5MW of solar PV every month on sites across the UK. Committed to their own sustainability the company plant a tree for every quote they produce, heat and power their offices with solar energy and a ground source heat pump and are committed to transitioning to a fleet of plug-in hybrid and electric vehicles&nbsp;</p>

<p>&nbsp;</p>

<p>Operating nationwide from its Wysall head office, the company offers a number of services for both commercial and residential customers. Their services include solar installations, renewable heating, battery storage systems, and electric vehicle charging. An impressive client list includes Mercedes Benz, Kingspan, JCB and Panasonic.&nbsp;</p>

<p>&nbsp;</p>

<p>Geo Green Power has also won its fair share of awards, recently being named a finalist for &lsquo;Specialist Contractor of the Year 2023&rsquo; by Construction News. Their submission highlighted a significant year during which they tripled their installation team and increased their turnover to &pound;12million.&nbsp;</p>

<p>&nbsp;</p>

<p>Kat Auckland, Communications &amp; Projects Director at Geo Green Power, commented that &ldquo;Geo Green Power has seen significant growth in the last two years, accelerated by increasing awareness of the climate crisis, with businesses and individuals wanting to reduce their emissions and save costs against steep rises in their energy costs caused by global events.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;We want to maintain and build on this growth to become the UK leader and are very excited about our new partnership with Spike, which will increase our online and digital presence and reach a wider audience.&rdquo;</p>

<p>&nbsp;</p>

<p>Rob Powell, Director at Spike added, &ldquo;As we continue to focus our efforts on attaining a B-Corp status, we are thrilled to announce that Spike will be working in partnership with Geo Green Power who are at the very forefront of sustainable power technology and true innovators in renewable energy&rdquo;</p>

<p>&nbsp;</p>

<p>The new client win reflects the agency&
#39;s continued expansion, marked by the acquisition of new clients across specialist PPC and SEO departments.</p>

<p>&nbsp;</p>

<p>Spike, an experienced digital agency based in Leeds, prides itself on cultivating lasting client relationships and implementing cutting-edge strategies to generate results. Their track record in Paid media includes award-winning campaigns, such as their work for Mighty Drinks, which boosted brand awareness and online revenue by 280% in the first year.</p>', 493, '
2024-02-12 08:51:57
', '2024-02-13 19:42:07
', 0, 'https://spike.digital/', 'spike-february-2024', '2024-02-12 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (463, 'Bobble Digital', 'Bobble Digital sponsors Leeds Knights Ice Hockey Team', '<p>Leeds-based digital marketing agency Bobble Digital is sponsoring local ice hockey team Leeds Knights for the 2023-24 season.</p>

<p>&nbsp;</p>

<p>The 107th&nbsp;season runs from October 2023 to April 2024, and the Knights will play each weekend for 82 weeks, with games held at Planet Ice in Leeds and other UK ice hockey stadiums as part of the Planet Ice NIHL National Division.</p>

<p>&nbsp;</p>

<p>The Knights have started strong this season, leading their division and aiming to retain last season&rsquo;s title after a streak of consecutive wins over the past few weeks.</p>

<p>&nbsp;</p>

<p>Leeds Knights MD Warwick Andrews said, &ldquo;Manni and the team at Bobble are a pleasure to work with, and the team massively appreciates their support. We work with several local, regional and national businesses who support us through match, individual player and team sponsorships, which help us raise vital funds to run the team and offers businesses the chance to advertise to an evergrowing sporting audience. We look forward to welcoming them to Planet Ice throughout the season to watch some action-packed hockey&rdquo;.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Bobble Digital sponsors the team alongside several local and national businesses, including Thermos, Morley Glass &amp; Glazing, Ward Hadaway and MJ Sports. In addition to being a team sponsor, Bobble will also be sponsoring specific matches throughout the season.</p>

<p>&nbsp;</p>

<p>Manpreet Singh, CEO of Bobble Digital, explains, &ldquo;We&rsquo;re always looking for ways to support the local community in Leeds, and ice hockey has a growing fan base in West Yorkshire and the UK. Going to watch the live games is a brilliant way to socialise with the team, and it was really special being able to present the Man of the Match award at a recent game.&rdquo;</p>

<p>&nbsp;</p>

<p>Aside from sponsoring the Leeds Knights, Manpreet and the Bobble Digital team also proudly sponsor the Rainforest Trust and the Business Catalyst Club.</p>

<p>&nbsp;</p>

<p>Established in 2017,<a href="https://www.bobbledigital.com/">&nbsp;Bobble Digital</a>&nbsp;is headquartered in Leeds, UK and works with clients nationally and globally to drive digital growth. The award-winning, media-focused digital marketing agency specialises in marketing strategy, PPC, SEO, social media, video and analytics.</p>', 494, '
2024-02-13 19:42:00
', '2024-02-16 11:29:08
', 0, 'https://www.bobbledigital.com/', 'bobble-digital-2', '2024-02-14 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (464, 'GC Employment', 'GC Employment helps 3,309 people into work in South Yorkshire', '<p><strong>GC Employment helps 3,309 people into work in South Yorkshire</strong></p>

<p>&nbsp;</p>

<ul>
	<li>3,309 people started new jobs with the support of GC Employment in South Yorkshire between July 2022 and July 2023</li>
	<li>5,763 people in total have received support in that time</li>
	<li>GC Employment works with businesses and supports individuals throughout the North of England to gain meaningful employment</li>
</ul>

<p>Thousands of&nbsp;people in South Yorkshire have started new jobs with the help of GC Employment.</p>

<p>&nbsp;</p>

<p>The programmes they run are aimed at making a lasting difference by supporting individuals to gain meaningful employment, improving their quality of life for both their families and for their communities.</p>

<p>&nbsp;</p>

<p>Between July 2022 and July 2023, GC Employment has helped&nbsp;<strong>3,309 people</strong>&nbsp;in South Yorkshire to start new jobs, engaging with a total&nbsp;<strong>5,763</strong>&nbsp;people across the region through its Restart programme.</p>

<p>&nbsp;</p>

<p>Stephen is just one of the people who has benefitted from GC Employment&rsquo;s services in South Yorkshire, having taken part in the Winning Ways course, run with Sheffield Wednesday FC Community Programme as part of its Personal Wellbeing Service.</p>

<p>&nbsp;</p>

<p>The Personal Wellbeing Service offers an 8-week course to help people on probation develop the necessary skills and knowledge to lead healthier and more fulfilled lives &ndash; cutting their chances of re-offending in the process.</p>

<p>&nbsp;</p>

<p>The GC team delivers weekly educational classes focused on enhancing participants&rsquo; emotional wellbeing. Sheffield Wednesday FC Community Programme hosts parallel physical activity sessions, which incorporate circuit training, gym workouts, and football.</p>

<p>&nbsp;</p>

<p>Stephen had previously been a chef but hadn&rsquo;t cooked for a long time, but after taking part in employability programmes, he was taken on by Sheffield Wednesday on a casual basis to work as a chef on match days.</p>

<p>&nbsp;</p>

<p>&ldquo;The course was eight weeks and I enjoyed every minute of it,&rdquo; said Stephen. &ldquo;It&rsquo;s led onto better things and I ended up with a 20-hour contract working as a chef.&rdquo; He now has the goal of setting up his own business.</p>

<p>&nbsp;</p>

<p><strong>Michelle Leeson, Managing Director of GC Employment, said:</strong>&nbsp;&ldquo;We&rsquo;re delighted to see stories like Stephen&rsquo;s, which demonstrate that behind the overall statistics are individual tales of resilience and drive.</p>

<p>&nbsp;</p>

<p>&ldquo;At a time when so many people are struggling, the work that GC Employment and its partners does is so important to give them the chance to gain the support, the training and the opportunities they need to find and keep jobs so they can support themselves and their families.&rdquo;</p>

<p>&nbsp;</p>

<p>The Growth Company delivers employment services across the north of England, with a 2022/23 turnover of &pound;50m, with recent expansions of its employment and justice services as well as new specialist markets including finance, benefit and debt, peer mentoring, refugee support and specialist health programmes.</p>

<p>&nbsp;</p>

<p>For more information visit:&nbsp;<a href="https://www.gcemployment.uk/">https://www.gcemployment.uk/</a></p>', 495, '2024-02-16 11:29:03', '2024-02-19 00:05:04', 0, 'https://www.gcemployment.uk/', 'gc-employment', '2024-02-19 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (465, 'Hark', 'Asda\'s IPL partners with Hark on Energy Monitoring and Asset Performance Project', '<p>IPL (International Procurement &amp; Logistics) had a vision to improve efficiency in their facilities and are achieving this using Hark&rsquo;s industrial IoT and energy analytics software. The two areas of focus for the project were energy monitoring and asset performance.&nbsp;</p>

<p>&nbsp;</p>

<p>IPL (International Procurement &amp; Logistics), a part of the ASDA family, source a variety of different products from around the world, specifically for Asda customers, ranging from fresh fruit and vegetables to house plants and flowers; and award-winning wines.</p>

<p>&nbsp;</p>

<p><a href="https://harksys.com/">Hark</a>, recently acquired by NASDAQ listed company, SolarEdge, is an award-winning Energy Management and Industrial IoT company based in Leeds, UK.</p>

<p>&nbsp;</p>

<p>Founded in 2016, Hark&rsquo;s technology enables enterprises to improve efficiency, reduce waste and maximise yield by connecting to, providing visibility of, and automatically controlling assets such as energy meters, building management systems, energy storage systems and industrial systems, regardless of manufacturer.</p>

<p>&nbsp;</p>

<p>Hark are now working with IPL, one of the largest importers of produce in the UK, to gain connectivity and visibility of industrial assets within multiple packaging sites in the UK. The initial goal was to retrieve energy and asset performance data to feed into the company&rsquo;s OEE (overall equipment effectiveness) metrics.</p>

<p>&nbsp;</p>

<p>IPL&rsquo;s facilities contain production lines, refrigeration plants, and multiple sub-metered areas throughout. Working with Hark&rsquo;s IoT software, IPL has a central location to visualise and analyse their energy and process data.</p>

<p>&nbsp;</p>

<p>In order to make substantial performance gains and energy savings, IPL were aware that they would need cloud connectivity to a real-time asset and&nbsp;<a href="https://harksys.com/solutions/energy-monitoring/">energy management system.</a>&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Hark connected IPL&rsquo;s industrial assets and meters to the cloud, to feed process, power and energy data directly into the Hark Platform using the existing secure network in the facility.</p>

<p>&nbsp;</p>

<p>The Hark Platform gives users the ability to see real-time energy data, plant performance data and real-time process line data that is set up in custom dashboards. This data allows users to start identifying where potential energy savings can be made and highlight inefficiencies across the site.</p>

<p>&nbsp;</p>

<p>Commenting on the partnership, Patrick Waterton, Manager of Engineering &amp; Hygiene at IPL said:</p>

<p>&nbsp;</p>

<p><em>&ldquo;Having a deeper understanding of what&rsquo;s happening behind the scenes at our production sites has been a real game-changer for how IPL makes operational and capital decisions. The Hark Platform gives me and my team the insight we need to keep our assets running at peak efficiency and to identify potential improvement initiatives, which ultimately helps to reduce operational and energy costs.&rdquo; &ndash;&nbsp;</em>Patrick Waterton - Manager of Engineering &amp; Hygiene, IPL</p>', 496, '
2024-02-16 11:30:41
', '2024-02-19 00:05:06
', 0, 'https://harksys.com/solutions/energy-monitoring/', 'hark', '2024-02-19 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (466, 'Lhyfe', 'Global green hydrogen pioneer Lhyfe establishes Sheffield base in UK expansion', '<p>Global green hydrogen pioneer Lhyfe has expanded its UK operation to South Yorkshire with the aim of accelerating the roll-out of the environmentally friendly gas across the region.<br />
The France-based multinational has opened a Sheffield office to identify opportunities to deploy production facilities to support businesses and organisations in the drive to net zero.<br />
Lhyfe chose Sheffield for expansion due to its &ldquo;exceptional supply chain potential and strong industry connections, as well as ongoing world-leading hydrogen research.&rdquo;</p>

<p>The company, which already has a base in Newcastle, is actively exploring various production sites and partnerships nationwide.<br />
The new Sheffield office in the Wizu Workspace in the city centre will be headed up by renewable energy expert Stuart Sinclair.<br />
Companies and sectors looking to decarbonise their operations are encouraged to contact Lhyfe to discuss opportunities to collaborate.<br />
Lhyfe is also keen to establish links across academia with high-profile hydrogen fuel switching research projects underway at the University of Sheffield.<br />
The gas can support the deep decarbonisation of the UK economy, particularly in hard-to-abate sectors such as heavy industry, including steelmaking, and transport.<br />
The UK Government has doubled its low-carbon hydrogen production target from 5GW to 10GW by 2030, with at least half of this coming from green hydrogen.<br />
&nbsp;<br />
Lhyfe (EURONEXT: LHYFE) is one of the world&rsquo;s pioneers in producing renewable green hydrogen through water electrolysis, powered by renewable energy &mdash; primarily wind and solar power.<br />
Water is fed into the electrolyser, which is split into hydrogen and oxygen meaning the only by-product is oxygen.<br />
The hydrogen is then compressed and transported locally from the production unit to various consumers across the region, providing an alternative to fossil fuels and building a local eco-system.<br />
Its inaugural plant at Pays de La Loire has been operating since the second half of 2021, and an additional six sites across Europe are currently under construction.<br />
Lhyfe aims its production capacity to reach up to 22 tons of green hydrogen per day by end 2024 and up to 80 tons per day by end 2026.<br />
Since the launch of its UK operation last year, the British team has grown to six staff members, with a recruitment drive underway to bolster plant deployment.<br />
The company is currently hiring for a bids and funding manager, and a project development manager.</p>

<p>Stuart Sinclair, Offshore Deployment UK and Ireland at Lhyfe, said: &ldquo;At Lhyfe, we are already demonstrating that green hydrogen is now a reality and a key driver of the clean energy transition. Sheffield is at the epicentre of the development of the UK&rsquo;s hydrogen economy and we look forward to playing our part in its growth. The city boasts exceptional supply chain potential and strong industry connections, as well as ongoing world-leading hydrogen research and development. We&rsquo;re excited to offer our expertise to potential partners and fast-track the shift toward a sustainable future.&rdquo;</p>

<p>Colin Brown, UK and Ireland Country Manager of Lhyfe, said: &ldquo;Renewable green hydrogen production offers significant decarbonisation options for a range of organisations, and there is extraordinary potential in Sheffield. This new office is in line with our ambitious UK deployment plans and we felt it was the right time to establish a base here. I firmly believe creating regional green hydrogen eco-systems should allow everyone to benefit, in a way that is open to all while ensuring regional economies of scale.&rdquo;</p>', 497, '
2024-02-16 11:33:02
', '2024-02-20 00:05:04
', 0, 'https://www.lhyfe.com/', 'lhyfe', '2024-02-20 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (467, 'Reward', '£10.5m funding deal from Reward accelerates next stage of growth for Yorkshire home buying business', '<p>A Yorkshire-based property company, which specialises in buying homes for cash in just seven days, is eyeing further expansion having gained full shareholding ownership of the business after securing a &pound;10.5m funding solution.</p>

<p>&nbsp;</p>

<p><a href="https://www.thepropertybuyingcompany.co.uk/">The Property Buying Company</a>&nbsp;based in Wetherby, is primarily utilising the facility provided by&nbsp;<a href="https://www.rewardfinancegroup.com/">Reward Finance Group</a>&nbsp;to buy out a London investment and lending company, which has owned a 50% shareholding in the business since 2017.&nbsp;</p>

<p>&nbsp;</p>

<p>The complexity and speed of the buy-out meant that it needed a lender that could offer the pace and flexibility of funding. The deal is the highest single lend provided by Reward, which specialises in providing tailored business finance and asset based solutions to SMEs across England, Wales and Scotland.</p>

<p>&nbsp;</p>

<p>The Property Buying Company was founded in 2012 by Karl McArdle and Jonny Christie and has rapidly grown into the UK&rsquo;s largest house cash buyer. It guarantees a swift property transaction in any location nationwide to vendors facing a range of lifestyle circumstances, such as having recently inherited a property or needing a quick sale due to a chain break.</p>

<p>&nbsp;</p>

<p>The company is now looking to accelerate the next stage of its growth plans, with the funding allowing it to further bolster its systems, infrastructure and recruit, whilst being in a position to complete a significantly increased volume of property purchases in the year ahead.&nbsp;</p>

<p>&nbsp;</p>

<p>Karl McArdle, co-founder for The Property Buying Company, said: &ldquo;We&rsquo;re hugely grateful to the contribution made by investors who have helped us rapidly expand the business over the last 11 years. It&rsquo;s been an amazing journey. However, we&rsquo;d always hoped to regain full shareholding to gain greater control over the direction of the business. Now feels like the right time to make that move as we enter a major phase of growth.</p>

<p>&nbsp;</p>

<p>&ldquo;We turned to Reward as we needed a lender which understood our business and could provide a fast and flexible funding solution of this size and complexity which also involved taking 49 properties as security in a short space of time.&nbsp; The additional working capital will provide the catalyst for expansion and gear us up for what promises to be an exciting 12 months ahead.&rdquo;</p>

<p>&nbsp;</p>

<p>Dave Jones, Reward&rsquo;s founding director, added: &ldquo;I&rsquo;ve recently had the pleasure of spending a significant amount of time with both Karl and Jonny, to get under the skin of their business and structure a deal which is bespoke to their needs. They&rsquo;ve built up a fantastic business and so we&rsquo;re really pleased to have been able to provide an agile finance solution that has enabled them to regain full ownership and fuel future expansion.</p>

<p>&nbsp;</p>

<p>&ldquo;A single deal of this lending size, being in excess of &pound;10m, is also a first for Reward and a real landmark for the business. We have now completed over 2,000 deals. However, it seems very fitting that this milestone has been achieved by supporting Karl and Jonny, as Reward also first started its own journey in Leeds at a very similar time to them.&rdquo;</p>', 498, '
2024-02-16 11:36:48
', '2024-02-20 00:05:06
', 0, 'https://www.rewardfinancegroup.com/', 'reward-1-1', '2024-02-20 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (468, 'Caddick Group', 'Major milestone for Leeds’ SOYO neighbourhood with dual approval for final phase', '<p>Caddick Group’s SOYO development at the heart of Leeds’ cultural quarter has secured planning approval for the final phase of the new neighbourhood from Leeds City Council’s City Plans Panel. Following a positive period of public consultation, Blocks A and D have won dual approval from the local authority, marking a major step forward for this transformative project.</p>

<p></p>

<p>Designed with sustainability front of mind, SOYO blocks A and D will bring much-needed high-quality student accommodation to the city centre. Both blocks will be comprised of a mix of studio and cluster flats, with the overall design complementing the rest of the new neighbourhood. Block A will bring 360 student units to the neighbourhood, adding to the 291 units due to be delivered at Block D.</p>

<p></p>

<p>SOYO began construction in 2019, bringing in a new mixed-use development to the Quarry Hill area, creating a new neighbourhood at the heart of Leeds
’ cultural quarter. The first two buildings, Moda New York Square, opened in 2022, comprising 551 studios and apartments. The latest buildings, Madison East and Mercer West for Hestia, are due to open to residents in the New Year.</p>

<p></p>

<p><strong> </strong><strong>Alistair Smith, Associate Director at Caddick Developments said: </strong>
“We are pleased to receive approval on both Blocks A and D from Leeds City Council. With this, we can now progress the final phase of SOYO and complete this exemplar development that has already brought significant jobs and investment to the city.</p>

<p></p>

<p>
“The consultation phase was very positive, and we received great responses to our proposals for both blocks which helped inform the final presentation of the plans to Leeds councillors. Through our ongoing collaboration with the local community and surrounding cultural institutions, we are already seeing the positive change that SOYO is bringing to this area of Leeds, cementing its status as an exemplar regeneration story for the city.
”</p>

<p></p>

<p>Once completed, residents at Blocks A and D will benefit from a range of amenities such as a 24hr gym, keyfob secure entry, study and collaboration spaces, and roof-top gardens. The final phase of the development will also see the ambitious landscaping plans completed, creating one of the largest landscaped spaces in Leeds.</p>

<p></p>

<p>The blocks will meet a high standard of sustainability. Once consented, it
’s intended that the development will be connected to the Leeds district heating network, and will help meet the council
’s objective of making the city carbon neutral by 2030. The buildings will be designed to consume low amounts of energy when low on use, utilising low carbon technologies, LED lighting and low water consumption in the operation phase.</p>

<p></p>

<p>The project team includes ID Planning, Box Clever, DLG Architects, Fuse Architects, Re-Form Landscape Architecture, Roscoe, Tate Consulting, Richard Boothroyd and Associates, Fore Consulting, Jensen Hughes, Jameson Acoustics, Stroma Building Control, Ridge and Partners and Carbon-Climate-Certified.</p>

<p></p>

<p>For more information on Caddick Group and its companies, visit: <a href="http://www.caddick.co.uk/">www.caddick.co.uk</a>.</p>', 499, '
2024-02-16 11:42:53
', '2024-02-21 00:05:04
', 0, 'https://caddick.co.uk/', 'caddick-group-1', '2024-02-21 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (469, 'Pick Everard', 'PICK EVERARD EXPANDS NORTHERN PRESENCE WITH LEEDS CITY RELOCATION', '<p>ONE of the UK&rsquo;s leading multi-disciplinary property, construction and infrastructure consultancies, Pick Everard, has further cemented its presence in Leeds with a strategic move to a new office space in the heart of the city centre.</p>

<p>The new office space located within Clockwise in Greek Street will cater for Pick Everard&rsquo;s growing team which to date has been instrumental in supporting a variety of projects across the Yorkshire region, as well as nationally.</p>

<p>Having been firmly established in Leeds for more than four years, the move from Wellington Street to the new office solidifies Pick Everard&rsquo;s deep-seated commitment to the north of England. It also symbolises the firm&rsquo;s dedication to providing a holistic, national approach and fostering closer connections across all regions of the UK. David Harris, partner at Pick Everard, said: &ldquo;Our new city office stands out as a great match for both our growing team and clients. Its prime location serves as a catalyst for extending our integrated and cohesive approach, creating a collaborative environment where our specialists can share knowledge and expertise &ndash; and crucially develop the very best outcomes for clients.&rdquo;</p>

<p>The move provides expanded office space in the city centre for Pick Everard&rsquo;s team to continue working closely with its range of public and private sector clients. These include local authorities in the region, such as Leeds City Council and City of Doncaster Council, along with national entities such as National Grid and Premier Farnell.</p>

<p>The relocation also falls in line with Pick Everard&rsquo;s overarching strategy to provide more agile and collaborative working environments for its continually expanding 650-strong workforce providing professional consultancy services across the UK.</p>

<p>David said: &ldquo;Collaborating with Clockwise on this move has ensured a completely smooth transition. The central location, together with a community-led approach to workspace provision offers a vibrant and positive environment, enhancing the wider user experience for our staff and clients alike.</p>

<p>&ldquo;We&rsquo;re excited about the opportunities our new home will bring, which will enable us to continue to deliver better together for our teams, clients and the communities we serve.&rdquo;</p>

<p>Through strategic partnerships in the construction sector, Pick Everard aims to drive positive change and contribute to the sustainable growth of the regions in which it operates. For more information on Pick Everard and the services it provides, visit: www.pickeverard.co.uk/</p>', 500, '
2024-02-16 11:47:04
', '2024-02-21 00:05:06
', 0, 'https://www.pickeverard.co.uk/', 'pick-everard', '2024-02-21 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (470, 'Torsion Group', 'Yorkshire’s largest Build-to-Rent Residential Developments', '<p><strong>Torsion Group forms a Joint Venture to deliver one of Yorkshire’s largest Build-to-Rent Residential Developments with Khalbros Group and Oliver Cookson’s Family Office.</strong></p>

<p>Leeds-based Real Estate company Torsion Group is celebrating the successful sale of 50% of the freehold and shares in The Phoenix residential scheme.</p>

<p>The Phoenix consists of two tower blocks in the centre of Leeds. Tower Block A is a 156 Build-to-Rent apartment development. Tower Block B is a private build-to-sell residential development.</p>

<p>The combined GDV of the development is in excess of
£90m, making this one of Yorkshire
’s biggest residential developments, and it expects to bring high-end, affordable living into a prominent Leeds market.</p>

<p>The Joint Venture structure consists of 50% of the freehold being sold to <strong>Oliver Cookson
’s Family Office</strong>, with the remaining 50% being retained by Torsion Group and Joint Venture partner Khalbros Group. </p>

<p>Tower Block B, which represents the Build-to-Sell element of the scheme, has been pre-sold to private owners, and it will be ready for occupation in the first quarter of 2024.</p>

<p>Upon completion in 2024, The Phoenix will be the fourth tallest residential building on the Leeds city skyline. Block A reaches an impressive 21 storeys at 223ft high, and Block B stands at 180ft with 17 storeys.</p>

<p>The transaction team consisted of Shakespeare Martineau, DWF, Shoosmiths, Addleshaw Goddard on Legals, Cowgils and Saffery on Tax, Maslow Capital on Debt Financing with additional professional services provided by Abacus Cost Management, Gardiner &amp; Theobald and Dalbergia Group.</p>

<p><strong>Dan Spencer</strong>, Group CEO of Torsion Group Limited, said:
“The Phoenix represents our continued growth in the Living sector and the Company
’s first venture into the Build-to-Rent and Build-to-Sell market. This sale is a truly significant milestone in the evolution of Torsion Group.
”</p>

<p>
“When set against the backdrop of a challenging economic, financial, and housing market, it is clear to see just what a remarkable achievement it is for everyone involved. This deal shows that prime assets in great locations like The Phoenix are proving to be resilient and remain attractive to investors. I am incredibly proud of the Torsion team and their determination to develop and construct this landmark Leeds development.
”</p>

<p></p>

<p>
“The Build-to-Rent assets complement our delivery model and operational business and deliver excellent returns for ourselves and investor partners. Having a fully vertically integrated delivery model including developer, contractor, facilities management, and operator allows us to ensure we deliver best-in-class sustainable buildings.
”.</p>

<p></p>

<p>
“We have a strong secured pipeline across all our living sectors, including Purpose Built Student Accommodation, Residential, Later Living and Care Homes. We will continue to grow in the Build-to-Rent sector and have recently secured planning on our subsequent three schemes in Leeds, Manchester, and Sheffield.
”</p>

<p></p>

<p><strong>Oliver Cookson</strong>, said:
“My roots are in the North - it
’s where I
’ve launched my latest nutrition venture, <a href="https://www.vavaverve.com/">Verve</a>, and it
’s where I grew up and bought my first house. So I feel passionate about this investment as it allows me to drive local economic growth even more, while providing high quality new housing for the area
’s growing population of professionals. Leeds is a great place to invest for the long term. It
’s a leading business hub with key growth industries bringing new opportunities, and a huge student population with the talent to make it even better.
”</p>

<p></p>

<p>To find out more about The Phoenix, visit <a href="https://www.torsiongroup.co.uk/the-phoenix-leeds/">https://www.torsiongroup.co.uk/the-phoenix-leeds/</a></p>

<p>For more information on Torsion Group Ltd, go to <a href="https://www.torsiongroup.co.uk/">https://www.torsiongroup.co.uk/</a></p>', 501, '2024-02-16 11:52:24', '2024-02-22 00:05:04', 0, 'https://www.torsiongroup.co.uk/', 'torsion-group', '2024-02-22 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (471, 'Ward Hadaway - February 2024', 'A hat-trick of healthcare wins for Ward Hadaway', '<p>One of Yorkshire&rsquo;s leading healthcare law firms has secured its place on three prestigious National Healthcare Service (NHS) frameworks for a further four years.</p>

<p>&nbsp;</p>

<p>In what was a highly competitive process, Ward Hadaway&rsquo;s wealth of experience in healthcare law has earned the firm a coveted position on the HealthTrust Europe, NHS Commercial Solutions, and NHS Shared Business Services legal services frameworks, strengthening its position as a leading legal partner of choice for NHS organisations across England.</p>

<p>&nbsp;</p>

<p>The legal services covered by the framework span a wide range of services, including employment matters, procurement, inquests, Private Finance Initiatives (PFI), regulatory compliance, estate management, commercial, and insolvency legal matters.</p>

<p>&nbsp;</p>

<p>Across Yorkshire, Ward Hadaway works with several NHS trusts including Hull Teaching Hospitals NHS Trust, Harrogate and District NHS Foundation Trust and Rotherham NHS Foundation Trust.</p>

<p>&nbsp;</p>

<p>Melanie Pears, a partner, healthcare lead and Head of Public Sector at Ward Hadaway, said: &ldquo;We are incredibly pleased to have been selected for all three of these important NHS frameworks. It is a triple endorsement of the breadth and depth of our highly skilled team&
#39;s expertise and experience. It further highlights our capabilities to navigate the complexities of the healthcare legal landscape effectively and deliver innovative and high-quality legal services to our NHS clients.&rdquo;</p>

<p>&nbsp;</p>

<p>Ward Hadaway has a dedicated NHS team that consists of over 100 legal healthcare specialists. The team acts for NHS organisations nationwide from the Scottish Borders to Cornwall, including acute, mental health and community trusts, emergency services and Integrated Care Boards (ICBs) and Commissioning Support Units (CSUs).</p>

<p>&nbsp;</p>

<p>A spokesperson at HealthTrust Europe said: &ldquo;HealthTrust Europe is committed to helping healthcare organisations, including the NHS, achieve sustainable cost reductions while improving quality and patient outcomes. Part of our approach to achieving this includes choosing partners who deliver the highest standards in their service. Ward Hadaway not only emerged as the highest scoring firm overall, but also excelled in each category including service specialisms, contract management, and social value. This achievement highlights the firm&rsquo;s dedication to providing exemplary legal services to the NHS and the wider healthcare sector.</p>

<p>&nbsp;</p>

<p>&quot;We are confident that, with the team&rsquo;s vast experience and specialist knowledge, Ward Hadaway will bring significant value to our efforts of driving improvement and efficiency in healthcare provision across the country.&rdquo;</p>

<p>&nbsp;</p>

<p>The announcement follows a recent national award win by the Ward Hadaway healthcare team at the British Legal Awards. It was named Strategic Legal Operations Team of the Year for the PFI Monitoring service it provides to the NHS.</p>

<p>&nbsp;</p>

<p>For more information about Ward Hadaway&rsquo;s healthcare services, please visit:&nbsp;<a href="https://www.wardhadaway.com/what-we-do/healthcare/">https://www.wardhadaway.com/what-we-do/healthcare/</a></p>', 502, '2024-02-16 11:56:21', '2024-02-22 00:05:06', 0, 'https://www.wardhadaway.com/what-we-do/healthcare/', 'ward-hadaway-february-2024', '2024-02-22 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (472, 'Woodbank Office Solutions', 'Woodbank Office Solutions Ltd Awarded Managed Print Contract for Williams Tanker Services Ltd.', '<p>Woodbank Office Solutions Ltd, a leading provider of Managed Print Services in the Northwest, has been awarded the managed print contract by Leeds based Williams Tanker Services Ltd, part of the international TIP Group and one of the UK&#39;s largest tanker service companies.</p>

<p>Woodbank were selected after presenting a comprehensive managed print solution which achieves significant benefits for Williams Tanker Services by optimising their print infrastructure and eliminates the need for maintaining and replacing many of the printers they owned outright. The contract will also see Woodbank&rsquo;s solution for Williams Tanker Services improve its environmental impact, increase their GDPR compliance, and reduce their historically high Managed Print running costs.</p>

<p>&quot;We are delighted to welcome Williams Tanker Services as a new customer,&quot; said Janet Bowden, Managing Director of Woodbank Office Solutions Ltd. &quot;Our goal is always to provide our clients with tailored print management solutions that enhance productivity, efficiency, and cost-effectiveness. We are proud to have partnered with Williams Tanker Services in optimising their print infrastructure and look forward to continuing support them and their business in the future.&rdquo;</p>

<p>Peter Hughes, Operations Director at TIP Group and General Manager at Williams Tanker Services commented on the new contract awarded to Woodbank Office Solutions, who have been specialists in the Managed Print Services industry for over 30 years. &ldquo;I&rsquo;m super pleased with the solution that was presented by Woodbank that&rsquo;s now fully integrated into our business. The amount of time and resource this has released from our staff is significant &ndash; my only wish is we&rsquo;d done it sooner.&rdquo;</p>

<p>Woodbank Office Solutions may be over 30 years in the business but they are still showing the way in Managed Print Services and Document Management in 2023.</p>', 503, '
2024-02-16 11:58:49
', '2024-02-26 00:05:04
', 0, 'https://www.woodbankoffice.co.uk/', 'woodbank-office-solutions', '2024-02-26 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (473, 'Caddick Group', 'Caddick Group and Leeds Playhouse celebrate a decade of partnership', '<p>Caddick Group and Leeds Playhouse recently hosted city stakeholders and special guests, welcoming them to an exclusive pre-show reception ahead of the press night for James Brining&rsquo;s Oliver!. The event heralded a decade of collaboration between the two institutions, stemming from a shared vision to anchor<em>&nbsp;</em>culture at the heart of the regeneration of the Quarry Hill area.</p>

<p>&nbsp;</p>

<p>Local Leeds artist, Emma Hardaker was also present, discussing her role within the artistic and cultural reimaginging of the Quarry Hill district and offering attendees the opportunity to screen print their own tote bags to take away.</p>

<p>&nbsp;</p>

<p>From an initial sponsorship of Leeds Playhouse&rsquo;s 2014 festive production, White Christmas, Caddick Group and Moda have remained capital partners throughout the Playhouse&rsquo;s two year extension and refurbishment and the construction of the new SOYO Leeds neighborhood from 2019 onwards, the final phase of which has recently received planning permission from Leeds City Council.</p>

<p>&nbsp;</p>

<p>Myles Hartley, Managing Director at Caddick Developments, said: &quot;Over the last ten years, our work together has evolved into a true collaboration that goes beyond traditional sponsorship. Being capital partners with Leeds&nbsp;Playhouse has allowed our relationship to blossom, creating a shared vision for Leeds&rsquo; ongoing cultural evolution.&rdquo;</p>

<p>&nbsp;</p>

<p>James Brining, Chief Executive and Artistic Director at Leeds Playhouse, added: &quot;Our partnership with Caddick Group has not only facilitated the production of some of the fantastic theatre that we are known for, but their ongoing commitment and investment has also played a key role in allowing us to expand our operations, improve our spaces and welcome even more people to enjoy all that we have to offer.</p>

<p>&nbsp;</p>

<p>&ldquo;As the SOYO development works towards completion, we look forward to continuing to work together, ensuring the continued improvement of this area for all who participate within it.&rdquo;</p>', 504, '
2024-02-16 12:04:47
', '2024-02-26 00:05:06
', 0, 'https://caddick.co.uk/', 'caddick-group', '2024-02-26 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (474, 'ProSpeed', 'ProSpeed invests £2m in revolutionary production expansion, setting new standards in automotive manufacturing', '<p>ProSpeed, the world&#39;s leading manufacturer of premium roof racks and off-road accessories, has announced a &pound;2m investment in cutting-edge machinery, infrastructure, and processes aimed at redefining the future of roof rack production. &nbsp;</p>

<p>&nbsp;</p>

<p>This strategic investment represents a monumental leap forward in production efficiency and product quality, reinforcing ProSpeed&
#39;s commitment to excellence.&nbsp;</p>

<p>&nbsp;</p>

<p>Based in York, ProSpeed&
#39;s newly acquired machinery is fully operational and boasts the capacity to produce an astonishing 7,000 products annually, representing 6,900% increase in productivity.&nbsp;</p>

<p>&nbsp;</p>

<p>This means that ProSpeed can now produce a modular roof rack component every three minutes, setting a new industry standard for speed and precision.&nbsp;</p>

<p>&nbsp;</p>

<p>This substantial investment underlines ProSpeed&
#39;s dedication to meeting the surging global demand for its products and diversifying its range to cater to virtually any vehicle. &nbsp;</p>

<p>&nbsp;</p>

<p>The ProSpeed DNA will be present across the entire spectrum of roof racks it manufactures further elevating the already exceptional quality and performance standards initially set for Land Rover, Toyota, VW and a growing list of other vehicles.&nbsp;</p>

<p>&nbsp;</p>

<p>Managing Director Olly Marshall said: &quot;At ProSpeed, we&
#39;ve always strived for perfection in our products. &nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;This &pound;2m investment is not just about increasing our production capacity; it represents a significant upgrade in the quality and diversity of our products, and it empowers us to deliver roof racks that are stronger, lighter, and compatible with almost every vehicle on the planet.&quot;&nbsp;</p>

<p>&nbsp;</p>

<p>In addition to revolutionising production capabilities, ProSpeed has also made remarkable strides in sustainable practices. &nbsp;</p>

<p>&nbsp;</p>

<p>The company has pioneered innovative shipping processes that virtually eliminate packaging wastage, enabling them to reach more countries than ever before, despite rising international postage costs.&nbsp;</p>

<p>&nbsp;</p>

<p>Products are now shipped in boxes that are over 90% smaller than before, answering logistical challenges and delivering clear environmental benefits.&nbsp;</p>

<p>&nbsp;</p>

<p>ProSpeed&
#39;s roof racks are renowned for their impeccable craftsmanship, constructed from aerospace-grade aluminium and precision-machined for absolute perfection. &nbsp;</p>

<p>&nbsp;</p>

<p>The precision-machined joint system guarantees unparalleled durability and longevity. &nbsp;</p>

<p>&nbsp;</p>

<p>With multiple mounting options and a low-profile, lightweight, and exceptionally robust design, ProSpeed&
#39;s roof racks adapt to drivers&
#39; needs and come with a lifetime guarantee.&nbsp;</p>

<p>&nbsp;</p>

<p>ProSpeed is also in the process of securing multiple patents, a testament to their unwavering commitment to innovation and product excellence. &nbsp;</p>

<p>&nbsp;</p>

<p>These patents will further solidify ProSpeed&
#39;s position as an industry leader and innovator.&nbsp;</p>

<p>&nbsp;</p>

<p>The new machinery and processes have been operational for just over a month and have since been gradually scaling up to maximum output. &nbsp;</p>

<p>&nbsp;</p>

<p>With their enhanced production capabilities, unmatched quality, and global reach, ProSpeed is poised to continue setting the standard for premium roof racks and off-road accessories worldwide.&nbsp;</p>', 505, '
2024-02-16 12:09:11
', '2024-03-02 11:15:28
', 0, 'https://prospeed.co.uk/', 'prospeed', '2024-02-27 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (475, 'The Marketing Optimist', 'Leeds marketing agency and North Yorkshire Council partnership adds up', '<p>Leeds-based marketing firm The Marketing Optimist has partnered with City of York Council and North Yorkshire Council on a marketing project to promote MultiplyNYorks &ndash; a new, free adult numeracy project for York and North Yorkshire residents.</p>

<p>&nbsp;</p>

<p>The full-service digital marketing agency is supporting the council with marketing strategy, social media marketing, SEO, website design and copy on the programme.</p>

<p>&nbsp;</p>

<p>Multiply was created to help people over 19 years of age who don&rsquo;t have a maths GCSE at grade C (or equivalent). It&rsquo;s a national programme offering local community courses and activities that help people manage their money better, improve their confidence, and open up better employment opportunities.</p>

<p>&nbsp;</p>

<p>Employers and businesses can also access these adult numeracy courses to upskill their workforce or request bespoke training sessions to meet employee needs.</p>

<p>&nbsp;</p>

<p>The MultiplyNYorks project will run until 2025 and is led by a dedicated central team created as a partnership between the Adult Learning Service of North Yorkshire Council, City of York Council&rsquo;s York Learning, and a consortium led by Better Connect.</p>

<p>&nbsp;</p>

<p>The overall project has been given &pound;2.6 million from the UK Shared Prosperity Fund to improve the numeracy skills of working-age people in the UK.</p>

<p>&nbsp;</p>

<p>Lois Calvert, Multiply Development Manager for North Yorkshire Council and City of York Council, said, &ldquo;We are delighted to be working with The Marketing Optimist on this programme. The collaborative work has allowed us to reach individuals who would not usually engage in learning activities. The dedicated website has allowed us to showcase the fantastic work that is happening across York and North Yorkshire.&rdquo;</p>

<p>&nbsp;</p>

<p>Working on the client&rsquo;s brief,<a href="https://www.marketingoptimist.co.uk/">&nbsp;The Marketing Optimist</a>&nbsp;has delivered a new website for&nbsp;<a href="https://multiplynyorks.com/">MultiplyNYorks</a>&nbsp;&ndash; focusing on accessibility, design and usability. The team will also work on an in-depth SEO strategy and ongoing optimisation, writing compelling web copy and delivering a social media strategy to raise awareness of the Multiply programme.</p>

<p>&nbsp;</p>

<p>Richard Michie, CEO of The Marketing Optimist, explains, &ldquo;This project is one of our biggest to date, and we are thrilled to be chosen as the marketing partner for such an important initiative. This project requires a thorough, collaborative approach to deliver an extensive multi-channel marketing strategy, as there are over 20 delivery partners across York and North Yorkshire.&rdquo;</p>

<p>&nbsp;</p>

<p>The Marketing Optimist is a full-service digital marketing consultancy based in Leeds and founded in 2016. The agency works with a range of local, national and international clients.&nbsp;</p>

<p>&nbsp;</p>

<p>With a team of experts specialising in digital marketing, social media, SEO, copywriting,&nbsp; website design, video marketing, marketing strategy and PR, the business offers a strategic and agile approach to help clients achieve meaningful results from their marketing activities.</p>', 506, '
2024-02-16 12:10:58
', '2024-02-27 00:05:06
', 0, 'https://www.marketingoptimist.co.uk/', 'the-marketing-optimist', '2024-02-27 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (476, 'Ward Hadaway - March 2024', 'Turnover continues to rise at Leeds law firm Ward Hadaway', '<p>A leading Northern law firm today announced its 2022-23 financial results, revealing another consistently strong performance.</p>

<p>&nbsp;</p>

<p>Ward Hadaway, which has offices in Leeds, Manchester and Newcastle, announced revenue for the financial year of over &pound;45m. It marks its eighth consecutive year of growth, during which time it has grown by more than a third.</p>

<p>&nbsp;</p>

<p>Following significant investments in the Leeds and Manchester regions in recent years and expansion of its legal services, the firm experienced significant growth across its Yorkshire and North West teams. This progress aligns with the firm&
#39;s strategic objectives to operate substantial offices in those areas as well as the North East and reflects its commitment to consistent support across all areas of the business.</p>

<p>&nbsp;</p>

<p>The firm&
#39;s Managing Partner, Martin Hulls, said: &quot;I am delighted with the consistent turnover growth we have achieved over the last eight years. During a time of unprecedented economic and societal turbulence, of a significantly rising cost base, of micro- and macro-economic headwinds both internationally and at home, we have demonstrated the benefit of being committed, consistent and considered. To achieve our highest ever turnover against this backdrop, assisted by the particularly strong growth in Leeds and Manchester as well as continued development in Newcastle, is testament to the clear focus maintained by our teams right across the firm.&quot;</p>

<p>&nbsp;</p>

<p>During the 2022-23 financial year, Ward Hadaway recruited 78 new people, and made 54 promotions, reflecting its commitment to the ongoing attraction, development and retention of talent. This includes retaining all of the 12 qualifying trainees and apprentices within the business when they completed their training contract in September.</p>

<p>&nbsp;</p>

<p>Martin added: &quot;Our business is our people. People who appreciate the needs of our clients by living and breathing their challenges with them. Employees who attract the region&
#39;s talent to work alongside them because they themselves feel motivated and invested in Ward Hadaway. Our people also challenge us to operate responsibly as we look to the future of our business, our communities and our planet.</p>

<p>&nbsp;</p>

<p>&ldquo;We have an exceptional group of legal experts across our offices supported by a fantastic business support team, and our clients appreciate our full-service offering, delivered with a straightforward, friendly approach, tailored to their needs. We are extremely proud of our committed teams, working for our loyal clients who continually challenge and inspire us.&nbsp; The firm&
#39;s consistently strong financial performance demonstrates yet again the results that this powerful combination can deliver.&rdquo;</p>

<p>&nbsp;</p>

<p>With over 450 people in three key locations &ndash; Leeds, Manchester and Newcastle &ndash; Ward Hadaway is one of the leading law firms in the North of England, established 35 years ago.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>For more information about the firm, visit:&nbsp;<a href="https://www.wardhadaway.com/">https://www.wardhadaway.com/</a></p>', 512, '2024-03-02 11:15:22', '2024-03-04 00:05:04', 0, 'https://www.wardhadaway.com/', 'ward-hadaway-march-2024', '2024-03-04 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (477, 'NIMA', 'Fish restaurant in York set to make waves following brand agency appointment', '<p>Hospitality brand consultancy, NIMA, has secured an ongoing contract with Fish&amp;Forest, one of York&rsquo;s increasingly popular restaurants that is garnering national acclaim from respected food critics, ahead of the restaurant&rsquo;s relocation to bigger premises. The move is set to take place in May 2024 and will increase the number of covers by fifty percent.</p>

<p>NIMA has been brought on board to devise and implement a growth strategy that will include a customer journey analysis, creative direction for a series of photoshoots, web development and a nationwide media relations campaign to support the imminent opening of the new restaurant.</p>

<p>Fish&amp;Forest opened its doors at Spark York in 2020 and is now based on Micklegate, York. It&rsquo;s already attracting attention within the city&rsquo;s food scene, being recognised in the Michelin Guide for its Commitment to Sustainable Gastronomy, was recently listed on Harden&rsquo;s and has received an AA Rosette.</p>

<p>The restaurant, which serves seasonal and sustainable fish, game and forest foods, is the brainchild of Yorkshireman Stephen Andrews, a self-taught chef who has worked and trained in Australia and Canada.</p>

<p>Stephen explained: &ldquo;I&rsquo;m delighted to be working with NIMA, ahead of our relocation to new premises. Moving into a bigger space is a really exciting step forward for us, but it&rsquo;s really important that we can keep the heart of Fish&amp;Forest alive. The team has quickly gained a detailed understanding of the brand and it&rsquo;s incredibly reassuring that their enthusiastic approach is supporting our exciting plans for growth. Together, we&rsquo;ve already implemented some positive changes and I&rsquo;m really looking forward to our continued work with NIMA.&rdquo;</p>

<p>Nicky Hayer, Managing Director at NIMA, added: &ldquo;When I first met with Stephen, I knew we&rsquo;d be a great fit so I&rsquo;m thrilled that Fish&amp;Forest has chosen NIMA ahead of its expansion. We&rsquo;re passionate about working with independent brands that resonate with our own ethos which centres around an exceptional customer journey, and hearing Stephen&rsquo;s focus on creating the perfect welcome spoke volumes to me. We&rsquo;re looking forward to working closely with Stephen and his team to bring the identity of Fish&amp;Forest to life, and cement their role</p>

<p>within the York foodie scene. I&rsquo;m particularly keen to see us celebrate this incredibly talented chef who has engineered a concept that is warm, inviting and impressive.&rdquo;</p>

<p>&nbsp;</p>

<p>NIMA is a full service brand management agency which operates in the hospitality and luxury sectors to support new and established brands through consultancy and project management. An example of their work can be found on their website, www.nima.co.uk</p>', 513, '
2024-03-02 11:18:51
', '2024-03-04 00:05:06
', 0, 'https://nima.co.uk/', 'nima', '2024-03-04 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (478, 'Eleven Arches', 'Plans submitted to tackle housing shortage in Tadcaster and boost local economy', '<p>PLANS to deliver a mix of new homes and a riverside public park a short walk from Tadcaster’s town centre have been submitted to North Yorkshire Council.</p>

<p></p>

<p>Eleven Arches  the proposed development for land off Wetherby Road  is aimed at addressing a chronic local housing shortage which has stunted the town
’s growth. Just 27 homes (including only two affordable homes) having been built in Tadcaster since 2011, meaning people have had to leave the town to find a place to live.</p>

<p></p>

<p>An outline application has now been submitted to the council following extensive consultation with the community. It seeks permission for:</p>

<ul>
	<li>
· Up to 410 high-quality new homes, with a range of types to meet local demand</li>
	<li>
· Up to 40% to be affordable homes</li>
	<li>
· Up to 5,100 sqm of specialist older persons
’ accommodation to address local demand</li>
	<li>
· A mobility hub including electric vehicle charging points, car club spaces and a school drop-off zone to support the neighbouring primary school</li>
	<li>
· Extensive planting, landscaping, public open space and children
’s play areas, creating a new riverside park</li>
	<li>
· Measures to manage surface water and increase flood resilience</li>
</ul>

<p>Every aspect of the plan is aimed at addressing issues faced by the town  from providing new affordable homes and addressing an identified need for new children
’s play spaces; to delivering traffic-calming measures on Wetherby Road. It
’s also predicted that the project will boost the town
’s economy. As well as generating an estimated
£1.82 million in Community Infrastructure Levy payments to be spent in Tadcaster, it would also deliver more than
£8 million in council tax payments over 10 years and 169 full time equivalent (FTE) direct jobs during construction.</p>

<p></p>

<p>But one of the biggest changes would be the effect on the vibrancy of Tadcaster
’s future, with more people and families helping increase footfall for local businesses and services. Figures from the last decade show the number of children, young people, those of working age and those employed in Tadcaster all declining (compared to increases elsewhere in the region), threatening the long-term viability of local services.</p>

<p></p>

<p>Such is pent up demand that people are already expressing an interest in new affordable homes, which can be done online at: www.elevenarchestadcaster.com</p>

<p></p>

<p>The plans are being brought forward by Gladman Developments Ltd, who is working with the Grimston Park Estate who own the land.</p>

<p></p>

<p>Katherine Putnam, Planning Director at Gladman, said:</p>

<p>
“Tadcaster is a really good example of what happens when virtually no new homes are built. You see an aging population as young people and families leave, house prices rocketing and the hyper local economy taking a hit. We
’re bringing forward a sustainable and considered plan aimed squarely at reversing those trends.</p>

<p></p>

<p>
“Every aspect of the plans we
’ve put forward responds to Tadcaster
’s needs, from the 40% affordable homes to address a chronic local shortage to greatly increasing accessibility to open green spaces as part of the public park.</p>

<p></p>

<p>
“Eleven Arches is not only aimed at helping people find a home and build a life in Tadcaster  something that
’s currently really hard to do  but to inject more vitality into the town centre. Creating opportunities for more people and families to live in the town, spending time and money here, will ultimately shore up the future of local services and support local independent businesses to thrive.
”</p>

<p></p>

<p>The plans are available on North Yorkshire Council
’s website, with the application reference: ZG2023/1299/EIA. People can now submit comments on the application on the council
’s website here: https://public.selby.gov.uk/online-applications/caseDetails.do?caseType=Application&amp;keyVal=S56RTUNX0EX00</p>

<p>Further information can be found at www.elevenarchestadcaster.com, by contacting info@elevenarchestadcaster.com or by calling the community information line 0800 689 1095. You can also follow the project on social media:</p>

<p>
· Instagram: @11ArchesTad</p>

<p>
· Facebook: facbebook.com/elevenarchestadcaster</p>

<p></p>

<p>Tadcaster context:</p>

<p>Eleven Arches is being brought forward against a unique backdrop in Tadcaster. Bringing more people to live in Tadcaster is essential to regenerating the town, which has experienced changes in social and economic demographics out of step with local, regional and national trends. Key points include:</p>

<p>
· 9% drop in the number of children living in Tadcaster since 2001: Compared to a 5.5% increase in Selby District</p>

<p>
· 10% drop in the number of young people (16-24) living in Tadcaster since 2001: Compared to an increase of between 10 and 11% locally</p>

<p>
· 3% drop in the number of working age people living in Tadcaster in 2001: Compared to an increase of 11% nationally and up to 13% in Selby District</p>

<p>
· 56% rise in house prices since 2011: Compared to 20% in England and 42% in Selby District</p>

<p>
· 16% drop in employees based in Tadcaster since 2010: Compared to an increase of up to 44% locally (Sherburn in Elmet)</p>', 514, '
2024-03-02 11:24:22
', '2024-03-06 00:05:04
', 0, 'https://elevenarchestadcaster.com/', 'eleven-arches', '2024-03-06 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (479, 'CO2Sustain®', 'CO2Sustain® starts 2024 with bold growth plans', '<p>CO2Sustain will be hitting the ground running in 2024 as three key new hires come onboard to play their part in delivering the company&rsquo;s ambitious growth plans.</p>

<p>&nbsp;</p>

<p>Carlos Leon and Dinesde Barros with be responsible for key strategic geographical areas that have been identified for growth. Whilst Terri Peters joins to manage our laboratory and work alongside technical manager Sue Wood.</p>

<p>&nbsp;</p>

<p>Carlos Leon joins as part of CO2Sustain&rsquo;s strategic move to elevate its market presence in Latin America and Dinesde Barros will be managing Africa and the Middle East.</p>

<p>&nbsp;</p>

<p>CO2Sustain welcomes Carlos to the team as sales manager for the LATAM region. Carlos will bring over 15 years of hands-on expertise in B2B sales management and international trade, particularly within the chemical industry, with a primary focus on food and beverage manufacturers.</p>

<p>&nbsp;</p>

<p>In his role as Sales Manager in LATAM, Carlos is set to steer CO2Sustain towards this interesting market, championing the benefits&nbsp;that carbonated soft-drinks manufacturers can derive from CO2Sustain&
#39;s innovative product with an exclusive patented formulation.&nbsp;His mission is to share the unparalleled advantages that this product brings to the industry, contributing to sustainability and&nbsp;fostering mutually beneficial partnerships with clients.</p>

<p>&nbsp;</p>

<p>Dinesde Barros, in his role as sales account manager, is a newcomer to the world of carbonated soft drinks but brings with him a strong background in sales and a wealth of ambition to succeed. Dinesde will work with local and global drinks manufacturers in another key strategic area, Africa and the Middle East. This is a region with many large drinks manufacturers who produce a wide range of local beverages. CO2Sustain works well in drinks manufactured and consumed in hot climates.</p>

<p>&nbsp;</p>

<p>Jonathan Stott, general manager at CO2Sustain said &ldquo;We are delighted to welcome Carlos, Dinesde and Terri to our team at this exciting point in the company&rsquo;s growth. The team and I look forward to supporting them to succeed and wish them well.</p>

<p>&nbsp;</p>

<p>Carlos Leon commented &ldquo;I truly believe that CO2Sustain should become a must for every carbonated soft-drinks manufacturer in&nbsp;LATAM. Improves drinking experience, helps the environment and reduces cost, I can see only benefits to generate profits for CSD&nbsp;manufacturers&rdquo;.</p>

<p>&nbsp;</p>

<p>Similarly, Dinesde said &ldquo;I am excited to challenge myself and develop into my new role. I am confident that carbonated soft drinks manufacturers in Africa and Middle East will keen to use and enjoy the benefits that our product brings to their business but more importantly the benefits it brings to their consumers who will no doubt soon be enjoying fizzier for longer drinks.</p>', 515, '
2024-03-02 22:35:01
', '2024-03-06 00:05:06
', 0, 'https://www.co2sustain.com/', 'co2sustain-1', '2024-03-06 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (480, 'Camira', 'Flurry of awards success for Yorkshire textile manufacturer', '<p>Yorkshire-based global designer and manufacturer of contract and transport textiles, Camira is celebrating a Stylepark Selected Award for its post-consumer recycled polyester, RePlay. The business has also been named a finalist in two awards for its work in sustainability advancement.</p>

<p>&nbsp;</p>

<p>Shortlisted in the Sustainability category at Business Desk&rsquo;s Business of the Year Awards, Camira has been recognised as an&nbsp;innovator and disruptor in sustainability.&nbsp; The manufacturer&nbsp;is also in the line-up for Circular Economy Innovation of the Year at the Edie Awards for its breakthrough closed loop wool fabric, Revolution.</p>

<p>&nbsp;</p>

<p>Commenting on the award shortlists, Ian Burn, director of marketing and sustainability said: &ldquo;I&rsquo;m so proud that our talented team of designers and makers have been recognised for their hard work and dedication into launching two fantastic textiles, each with their own eco-story to tell.&nbsp;The industry has a poor reputation for irresponsible production, but Camira is leading the way in demonstrating a new, more sustainable approach to manufacturing, based on circular economy and regenerative principles.</p>

<p>&nbsp;</p>

<p>&ldquo;The next step for us is to recycle our customers&rsquo; upholstery waste, extending our impact through our investment in iinouiio, and allowing other companies to embrace circular economy principles. We are also preparing to launch our next closed loop fabric in 2024 which will include a higher percentage of recycled content.&rdquo;</p>

<p>&nbsp;</p>

<p>Camira, which will celebrate its 50th&nbsp;year in business in 2024, manufactures more than eight million metres of fabric globally every year and boasts manufacturing capabilities, extending from raw yarn to weaving, dyeing and the finishing of fabrics.</p>

<p>&nbsp;</p>

<p>To find out more visit&nbsp;<a href="http://www.camirafabrics.com/">www.camirafabrics.com</a></p>', 516, '
2024-03-02 22:55:18
', '2024-03-05 00:05:04
', 0, 'https://www.camirafabrics.com/en-uk', 'camira-2', '2024-03-05 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (481, 'Levi Solicitors', 'Gemma Horner celebrates promotion at Levi Solicitors', '<p>Levi Solicitors is delighted to confirm the promotion of Gemma Horner to partner in the firm&rsquo;s highly respected dispute resolution team. The promotion brings the firm&rsquo;s total number of partners to 12.</p>

<p>&nbsp;</p>

<p>Gemma has been with the firm since she qualified as a solicitor in 2019. Altogether, Gemma has over 11 years&rsquo; experience in dispute resolution, and in 2022 obtained her Civil Advocacy Higher Rights of Audience to become a Solicitor Advocate. Gemma has particular expertise in complex residential property disputes, professional negligence claims, commercial property disputes and commercial issues. She acts for commercial clients and individuals in West Yorkshire and further afield.</p>

<p>&nbsp;</p>

<p>Gemma said, &ldquo;Since joining Levi, I have had the benefit of excellent support and guidance from the department and the firm as a whole. I hope I can support and inspire others to continue to develop the already strong offering that Levi has across a range of specialisms.&rdquo;</p>

<p>&nbsp;</p>

<p>Ed Smith, Head of Dispute Resolution, said of Gemma&rsquo;s promotion,&nbsp;&ldquo;Having worked with Gemma for the past four years, it gives me great pleasure to see her promoted to partner. She is a true asset to our team.&rdquo;</p>

<p>&nbsp;</p>

<p>Ian Land, Levi&rsquo;s Senior Partner, added, &ldquo;We are delighted that Gemma is joining the partnership. We believe in nurturing and developing talent, and rewarding colleagues&rsquo; hard work and expertise.&rdquo;</p>

<p>&nbsp;</p>

<p>Levi Solicitors provides a range of personal and commercial legal services to clients throughout the country, and celebrates its 90th&nbsp;anniversary later in 2024.</p>', 517, '
2024-03-02 22:59:11
', '2024-03-05 00:05:06
', 0, 'https://levisolicitors.co.uk/', 'levi-solicitors', '2024-03-05 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (482, 'Incommunities', '£2,000 donation brings joy to two Bradford foodbanks', '<p>Two Bradford foodbanks have each received a £1,000 donation this Christmas thanks to support from employees at housing provider Incommunities.</p>

<p></p>

<p>Instead of sending Christmas cards this year, Incommunities asked staff to nominate and vote for charities that they would like to benefit from a
£2,000 donation.</p>

<p></p>

<p>The winning charities were Bradford foodbanks and the two foodbanks receiving the much needed cash are Bradford Metropolitan Food Bank and Bradford Central Foodbank.</p>

<p></p>

<p>Janey Carey, Executive Director of Customer and Communities at Incommunities, said:
“We have a strong relationship with the foodbanks across the Bradford district thanks to the vital work that they do supporting local people.</p>

<p></p>

<p>
“So, it
’s great that our colleagues also recognise this fantastic work and have chosen the foodbanks to benefit from this Christmas donation.</p>

<p></p>

<p>
“Our staff support the local communities across Bradford, particularly at Christmas. For example, some colleagues went to Sandale Trust to help wrap over 300 presents for Sandale
’s Secret Santa Appeal and more than 100 toys were donated by our staff to the Salvation Army Toy Appeal this year.
”</p>

<p></p>

<p>Graham Walker, from Bradford Metropolitan Food Bank, said:
“We expect to give out over 15,000 bags in 2023, and this is only possible because of the efforts of our co-ordinator, our volunteers and the generosity of individual and corporate donors.</p>

<p></p>

<p>
“We are grateful to Incommunities for their vital and continued support.
”</p>

<p></p>

<p>Other charities that were nominated for the
£2,000 donation were Andy
’s Man Club, West Yorkshire Dog Rescue, and Guide Dogs for the Blind.</p>', 518, '
2024-03-02 23:03:17
', '2024-03-07 00:05:04
', 0, 'https://www.incommunities.co.uk/', 'incommunities-3', '2024-03-07 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (483, 'Underpin Sports', 'LEEDS-BASED UNDERPIN SPORTS ENDS YEAR ON A HIGH DUE TO RECORD GROWTH', '<p>A Leeds-based golf mental performance coaching company is ending the year on a high after having more than doubled revenue, rapidly expanded its corporate network membership, trebled its player portfolio and recently launched in Scotland.</p>

<p><a href="https://www.underpinsports.com/">Underpin Sports</a>, which is dedicated to unlocking higher performance by helping elite amateur and professional golfers cope psychologically with the pressures of competing across the globe, aims to further increase revenue by 50% in the next 12 months.</p>

<p>&nbsp;</p>

<p>Its 72 Club corporate network is a major source of revenue for the business, with its membership increasing by 132% year-on-year. It provides a programme of events and strategic partnership opportunities to sectors ranging from finance to construction. The bulk of its membership is currently Yorkshire-based and includes firms such as&nbsp;<a href="https://www.lawblacks.com/">Blacks Solicitors</a>,&nbsp;<a href="https://sagars.co.uk/">Sagars</a>&nbsp;and&nbsp;<a href="https://www.proaktive.co.uk/">ProAktive Insurance</a>.</p>

<p>&nbsp;</p>

<p>In a further boost to its growth ambitions, the company has also recently expanded into Scotland and is headed up north of the border by Edinburgh-based Scott Dickson who is a well-known figure across the drinks, golf and whisky industry.</p>

<p>&nbsp;</p>

<p>Other major milestones for the business include key senior appointments such as Sean Jarvis, who came on board as non-executive director earlier in the year. Sean&rsquo;s leadership, sporting experience and expertise will help Underpin Sports drive its long-term strategic growth aims as he balances it with his full-time commitment as CEO of Leicestershire County Cricket Club (LCCC).</p>

<p>&nbsp;</p>

<p>From a player perspective, Underpin Sports has launched its academy to provide closer support to the grass roots of the game and also increased its elite-level player portfolio from six to 20 in just 12 months, comprising 15 professionals and five amateurs. It has supported some of the game&rsquo;s rising stars, including Charlotte Heath who was crowned leading amateur and winner of the Smyth Salver at this year&rsquo;s AIG Women&rsquo;s Open.</p>

<p>&nbsp;</p>

<p>Growing the player portfolio has been enhanced by various commercial deals. Securing a key partnership with the&nbsp;Clutch Pro Tour - a feeder to both the Challenge and DP World Tours &ndash; has proved pivotal in helping the business forge greater relationships with up-and-coming talent competing on the tour.</p>

<p>&nbsp;</p>

<p>Stewart Clough, managing director for Underpin Sports, said: &ldquo;The business is still largely in its infancy however it&rsquo;s hugely satisfying to achieve record growth and play such a key role in the mental performance and career development of some of the UK&rsquo;s rising golf stars.</p>

<p>&nbsp;</p>

<p>&ldquo;There&rsquo;s a natural synergy between golf and business and so we&rsquo;ve worked hard over the last year to develop a company which harnesses the widespread passion for the game but also provides an attractive proposition to corporate organisations. Our 72 Club network has made giant strides and it&rsquo;s exciting to see it drive new commercial opportunities for those firms involved and enable them to get more involved in supporting the future of the amateur and professional game.&rdquo;</p>

<p>&nbsp;</p>

<p>&ldquo;The exceptional performance, key senior-level appointments we&rsquo;ve made and our recent expansion into Scotland hopefully signals our growth ambitions. We don&rsquo;t want to stand still though and see massive potential to scale the business even further as we move into 2024.&rdquo;</p>', 519, '
2024-03-02 23:21:06
', '2024-03-07 00:05:06
', 0, 'https://underpinsports.com/', 'underpin-sports', '2024-03-07 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (484, 'AD:VENTURE', 'AD:VENTURE Gives Business Support Boost to Ambitious Start Ups', '<p>What does a successful marketing company, an architect and a living wall company have in common? They’re all West Yorkshire start-up businesses that have enjoyed successful growth thanks to start-up loans, growth grants and business support from <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXCPnxSSbpoSKCtXVTJSD-2BLiWWHVDomDpMIxveG-2FROt63H4xH_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij73cK9JzIJDZrWieBr2xAlkbf05uk-2FpGEINYYynQu5ok1viJHccjtDeC-2BWte5qj2JWrozLa6ybU-2BbHJXWhleRPstcNQl1M3d-2F1NEw9dl-2FX-2B-2BmHaSFE1NujM7w9SEnj35FoijduEXS-2ByH2MvJX73h-2Bjp8SfES9CM-2FXDp-2B1-2FMEvmF2qzqEyxn8uLCrNAz7-2BZsjTw-3D">AD:VENTURE</a>.</p>

<p></p>

<p>All three companies (<a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXMNXsFaCaVlCEQknMB9od4L9Q6iq5AblQDoNYeIFouREIOMa_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij6BU4eNB4j9XSQ50trlhlg7-2FBGCOXqjHxGwBYWyg0XVG659L-2BQwyw7URBEgq01HhkDS1AVOg4uN8XmOux-2BAtJOsFg0J-2FNab520-2BTlUcgo4l5zuLDl82-2FIW5lPLtLFf21LdEMZc8zg0jqC61pyBfZ5zwS52vIea7Zn8fxAkv7N4E-2Fv130uAFoiYi1c1VNrDYfBU-3D">Youbee Media</a>, THIS Architecture, <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXKlpNMSbx9uUkKuotpg4mHHxQcah06Bt1oWJZm76-2B5x-2BFKyV_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij70hoDg-2BJkfciosrf9GiUNwW-2FjrGV4F4PY20vyWfwhgX4cmD9c5SujpWuPl19f3QxkmBs5vyGMrP5b3dFPIPKQPWGsUaYvYnqu9ZBK7linikm6n-2FIF19U8sHvuGMo61c33Oa6kRoUpeDSxIxXUJUgXoNDdCckpzWavSKF5ir5CQyOkYCvBgcvMoTRg-2BOSdDJ4A-3D">Living Walls</a>) are about to pay-it-forward as guest speakers at the Leeds leg of the new AD:VENTURE programme launch roadshow, (<a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXJOtRQFdZT3vXJfvdeYwPQmz0UXNOFDN29oLwasNzC-2BsLQ-2Fsa-2F-2BWzcK-2BDoyQeZD-2F3hmdeopbTE-2F4p4IWs2EFJrBTxb7ISCBJYXs5bhwapz-2B3DFkgIbX2q5EzNPjqBTc8IA0Ex2M4xcrcfQjUz6YIsgpbwYA-2Bt1NzfODwNKXn81BcFOi4_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij6slgbE8meLuWIuj2A9jXnP8zrMIF07tn8hM2dnLNAk9mDZK9JuJpLRifEPdiIMnlECzvNK76Q8kbLPU2w7oc55fIgyMWiuPeL06oCaOBlYRgZ9m94xYeTlsgUICQMuSIFxBY5RXatpumK9DMrikMqQoJ1zZ6-2BINdyfu2p-2BkGFaJMKYUQ5hEhZ0Tv2lF0eCQn4-3D">Thursday 12th October, Avenue HQ, Leeds</a>). They
’ll share their personal business success stories, and explain how funding, mentoring, webinars and workshops from the business support programme have propelled their fledgling businesses to the next level.</p>

<p></p>

<p>The event is aimed at young businesses across West Yorkshire, that have been trading for less than three years, with a clear ambition for growth. AD:VENTURE is keen to welcome business support and intermediaries too, such as accountants, professional services and consultants to understand what the new look comprehensive programme has to offer new businesses and start-ups. </p>

<p></p>

<p>After seven years of delivery, AD:VENTURE has secured new funding through the West Yorkshire Combined Authority to provide ambitious early stage businesses based in West Yorkshire with fully funded, tailored support to help them flourish. </p>

<p></p>

<p>Cllr Jonathan Pryor, Deputy Leader of Leeds Council and Executive Member for Economy, Culture and Education, said:
“The AD:VENTURE programme is funded by the West Yorkshire Combined Authority and its Partners which allows us to offer these business support services at no cost to the early stage companies. </p>

<p></p>

<p>
“Over 25,000 new start-ups were created in West Yorkshire in 2022 and we recognised the value organisations such as AD:VENTURE bring to support the survival and success of new businesses that make a huge contribution to our economy. We have secured new funding for business growth, and a broader eligibility criterion which allows us to support even more clients. That, in turn, encourages business growth, creates employment opportunities which improves the economic wealth and well-being of West Yorkshire.
”</p>

<p></p>

<p>Enterprises under three years
’ old have access to funding and grants from
£1,500 -
£10,000 and knowledge as well as support from experienced Business Managers in their local area, to guide them to growth and stability. New businesses are also given a helping hand with access to a wide range of free training workshops such as mapping your customer journey, marketing, strategy, and sales. </p>

<p></p>

<p>Rebecca Hopwood will share her experience of the support received by <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXMNXsFaCaVlCEQknMB9od4L9Q6iq5AblQDoNYeIFouRExl4i_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij7qmx0hMh429-2FwyH4T5vW57hJp-2BrEklh6CDaHR9ZaVqaPRpx-2FksywJ7CQ7K2vO0Ww8qahhH4jSFqo9sPE1m8SMwJuWtNZNFYLjnofq6qsN2zbZVuE-2BNts4eIo-2B8mDeDtxA6jrGor3XOLYDu6AbfRsyha9Ei18q4O-2F75ZUEd7ANloh09Dnvct2eGirRbF21VQaA-3D">Youbee Media</a>, an award-winning marketing agency based in Morley that provides cost-effective traditional and digital marketing. About to celebrate its third birthday, Youbee Media has benefited from grants and business support from AD:VENTURE and has also delivered marketing advice to support other AD:VENTURE clients.</p>

<p></p>

<p><a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXKlpNMSbx9uUkKuotpg4mHHxQcah06Bt1oWJZm76-2B5x-2BVJfM_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij6dNW5AQnJiS99At-2BpJSyqLOl1TADfcYcJv76H-2FSavKChBBTUL4R3G2P2gVHFfj432Y3uclNE5c9KbxAjzpMJYoEzjo4WcdniBfu5T7bpu4jLXxO0IWiJYIUJZNb-2BHzlYYkP6gMD-2FsHLdzW7miYMPC4X0HYBkFTU3s-2BN7brjTQ4qAGpyrDHl4hctopDsD1HXE0-3D">Living Walls</a> installs and maintains living walls across a number of different sectors. The company is known for its premium-quality, eco-friendly living walls which sport luscious foliage, bringing event bustling city centres closer to nature. </p>

<p></p>

<p>Living Walls not only look stunning, but they can also reduce the carbon footprint of a building, improving noise installation, enhance urban biodiversity and protect exterior walls from corrosion and the weather.  Ricky Talbot will explain how AD:VENTURE has helped him to grow this successful business during the Leeds launch event.</p>

<p></p>

<p>Daniel Harvey of THIS Architecture will share the invaluable assistance provided by his business manager, Marianne Smith, who has showcased professionalism and expertise, guiding him adeptly through the application process.</p>

<p></p>

<p>The first phase of AD:VENTURE (2017 to 2023) actively worked with over <strong>4,112 </strong>businesses and <strong>1,989 </strong>individuals, delivered a programme of over <strong>1000 </strong>events, and approved 709 grants worth a total of <strong>
£3,621,774</strong>. The support also created <strong>890 </strong>additional jobs across Yorkshire.</p>

<p></p>

<p>The launch of the new-look AD:VENTURE takes place at <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXE5lyJsc8hvOIxnPmnBmbqoovi3EZOnhM3jQOMnjNj7x1p269AMVvP-2Fp6CBvfVl83w-3D-3DrBGt_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij7cW8qRWx4yMUU-2FEHDAFpYW-2F6oNAXZrlWPeiD-2FqHp8-2FzcPU-2Bqa5l0BseRTx1XRP9QQJ0zQANsh2rsXRRuelD1rshQS1At6gn229Ns3Wy2Zf-2FbNEOLWZuHSIUOQzkIMp3CNnIFyfpPjlUxCMakYkbGLmSG8aTwplMA5Sg9-2BvVXXUCP7wiT9nGF7a0q4JJa06Od4-3D">Avenue HQ</a>, on Thursday 12th October, from 9.30 am until 1pm. The presentations from the AD:VENTURE team, successful start-up businesses, Cllr Jonathan Pryor and introductions to the Leeds-based Business Managers, Marianne Smith and Balbir Sembhi, will be followed by a free networking lunch. Visit <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXJOtRQFdZT3vXJfvdeYwPQmz0UXNOFDN29oLwasNzC-2BsLQ-2Fsa-2F-2BWzcK-2BDoyQeZD-2F3hmdeopbTE-2F4p4IWs2EFJrBTxb7ISCBJYXs5bhwapz-2B3DFkgIbX2q5EzNPjqBTc8IA0Ex2M4xcrcfQjUz6YIsgpbwYA-2Bt1NzfODwNKXn81BcOf8A_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3Y48msH35iN16KNzCaAzT28piznWljIgEnoK0BEV4kT489et-2FjO9o6qT2wIPRPbxY0hklpUpj2AdjMAZho3Xg-2F3XNUelnLspD336HRLi74WhfcRq4eBGQmIiHAge8LKp5jFs-2FZwMboh3xSXJwGrbGBNmEtNUFuzbF5jX7RWcvQ-2FH2cjm-2FQvpff-2F2f7BVQT6Ij4bAQzC7dolmIhmOPbjxNikx9HzoVPn-2Fvr6dSHUNnVQWoTmZSp2quRLOipr65KJGnjafCk59zdTS9bOJVOrwNjPhd7z9CootIpTxpHsHq2HX-2BQCLBqXPGi8GbF-2Fk2207o6VvSIfS67IC8YLovjXmWg-2B87NH-2FVvD-2BXGK8jvm3urAVkXewC9z7yJtDhd2Vtz7oZs-3D">AD:VENTURE Leeds Launch</a> to reserve a free place on a first come, first served basis.</p>

<p></p>

<p>For more information of how AD:VENTURE can support new businesses across West Yorkshire contact by telephone on 0113 5351199 or email <a href="mailto:adventure@leeds.gov.uk">adventure@leeds.gov.uk</a>.</p>

<p></p>', 520, '
2024-03-02 23:24:30
', '2024-03-11 00:05:04
', 0, 'https://www.ad-venture.org.uk/', 'adventure', '2024-03-11 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (485, 'FloodSax', 'Overwhelming demand for ‘sandless sandbags’ from Yorkshire company after massive rise in number of storms', '<p></p>

<p>There have already been EIGHT named storms in the UK in less than four months compared to just TWO for the whole of the storm season 2022/2023.</p>

<p></p>

<p>And the vast number has led to a massive surge in demand for alternative sandbags made by a Huddersfield company.</p>

<p></p>

<p>So far in the autumn and winter of 2023 and 2024 the eight storms have included three in December 2023 and have been Agnes (September 27 and 28, 2023); Babet (October 18-21, 2023); Ciaran (November 1 and 2, 2023); Debi (November 13, 2023); Elin (December 9, 2023); Fergus (December 10, 2023); Gerrit (December 27, 2023); Henk (January 2, 2024).</p>

<p></p>

<p>If you
’re wondering why some of the names seem rather off-beat it
’s because the UK, Ireland and the Netherlands work together as the western Europe storm naming group and people from all three countries are keen to make suggestions for names so some are rooted in culture from Ireland or the Netherlands as well as the UK.</p>

<p></p>

<p>Spain, Portugal, France, Belgium and Luxembourg make up the south-western storm naming group with Norway, Sweden and Denmark forming the northern group.</p>

<p></p>

<p>With the season running from September 1, 2023 to August 31, 2024, there is still plenty of time for far more storms to come the UK
’s way.</p>

<p></p>

<p>In the year 2022/2023 the UK had just two named storms, Antoni and Betty, and both struck in summer - Antoni on August 4 and 5 with Betty on August 18 and 19 which shows that storms can happen at any point in the year.</p>

<p></p>

<p>In the UK a storm is named by the Met Office when it has the potential to cause disruption or damage which could result in an amber or red weather warning. </p>

<p></p>

<p>The Met Office says:
“Storms will usually be named on the basis of the impacts from strong winds, but the impacts of other weather types will also be considered, for example rain or snow if its impact could lead to flooding as advised by the Environment Agency, Scottish Environment Protection Agency or Natural Resources Wales.
”</p>

<p></p>

<p>The recent storms have caused widespread flooding throughout the UK with Nottingham and other parts of the Midlands terribly hit by the most recent storm, Henk, earlier this month.</p>

<p></p>

<p>Now Environmental Defence Systems Ltd says demand for its innovative FloodSax alternative sandbags has been overwhelming with tens of thousands sold in recent weeks.</p>

<p></p>

<p>Lucy Bailey from EDS said:
“It feels like it
’s hardly stopped raining since October but we saw a big surge in demand in early November for FloodSax and that has just kept on going. FloodSax are the original sandless sandbags and we have been selling them worldwide since 2007 but we
’ve never known demand so high for the product, especially as it
’s far more environmentally-friendly than traditional sandbags.
”</p>

<p></p>

<p>FloodSax are now widely used by homeowners, businesses, councils, NHS trusts, supermarkets and facilities management companies as they are flexible and multi-use. They come in packs of five which are vacuum-packed, making them incredibly space-saving to store yet they are very easy and quick to deploy.</p>

<p></p>

<p>In their dry state they can quickly soak up water dripping or leaking inside buildings to stop damage from spreading, but immerse them fully in water and they absorb the water and retain it, transforming them into instant sandbags but without any sand. Around three million have now been sold worldwide.</p>', 521, '
2024-03-02 23:31:21
', '2024-03-12 00:05:04
', 0, 'https://www.floodsax.co.uk/', 'floodsax', '2024-03-12 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (486, 'OXYGEN', 'OXYGEN ACTIVEPLAY IS OPENING IN YORK', '<p><strong>Clifton Moor Centre Retail Park will be home to leading indoor activity and trampoline park, Oxygen - offering big-time family fun all year round!</strong></p>

<p>&nbsp;</p>

<p><a href="https://oxygenfreejumping.co.uk/activity-parks/york/">Oxygen Activeplay</a>,&nbsp;the next generation concept in UK indoor leisure,&nbsp;is set to open a flagship indoor activity and trampoline park at Clifton Moor Retail Park on February 12, 2024.&nbsp;</p>

<p>&nbsp;</p>

<p>Today Oxygen reveals its plans for the 27,500 sq. ft indoor super park on Hurricane Way. Investing &pound;2.3million in a lavish build, Oxygen York will boast parkour bounce zones; inflatable obstacle courses; an interactive sports pitch; a multi-challenge climbing zone; open bounce trampolines; giant airbag; trapeze; strike arena, and a dedicated sensory baby and toddler park. There will also be peaceful play sessions for those who enjoy a quieter experience.&nbsp;</p>

<p>&nbsp;</p>

<p>Along with an extensive caf&eacute; plus a separate&nbsp;quiet<strong>&nbsp;</strong>lounge, Oxygen York will have&nbsp;&nbsp;two VIP party rooms on the mezzanine floor and additional party areas on the lower floor. Each party area is geared-up with the latest AV tech, birthday halo, kid&rsquo;s throne, a dedicated host and plenty of frills.</p>

<p>&nbsp;</p>

<p>Lynne Watson, manager,&nbsp;<a href="https://oxygenfreejumping.co.uk/activity-parks/york/">Oxygen York</a>&nbsp;said:&nbsp;&nbsp;&ldquo;Oxygen parties are epic. Our party offering has proved exceptionally popular at all our existing parks and Oxygen York will be taking them up a level - providing&nbsp;the ultimate high-end&nbsp;adventure-packed celebrations.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;We can&rsquo;t wait to open the doors in February and unleash big-time active fun for the people of York!&rdquo;</p>

<p>&nbsp;</p>

<p>James Nickson, head of communications, said: &ldquo;Oxygen York will be the first of its kind in this region. As pioneers of play,&nbsp;the investment in and&nbsp;opening of Oxygen York marks another important step in our mission to revolutionise the active play world on every level for all the family.&nbsp;With state-of-the-art installations, highly trained teams plus a packed timetable of activities including Oxygen&rsquo;s glo-tastic Neon Nights ( first Friday of every month), there is something for all ages - right through to the grown-ups! &ldquo;</p>

<p>&nbsp;</p>

<p>Oxygen York also brings 50 new job opportunities to the city, offering a range of career opportunities from managerial positions to Oxygen&rsquo;s famous bouncer teams as well as&nbsp;&nbsp;party hosts and caf&eacute; staff.&nbsp;</p>', 522, '
2024-03-02 23:33:51
', '2024-03-11 00:05:06
', 0, 'https://oxygenfreejumping.co.uk/activity-parks/york/', 'oxygen', '2024-03-11 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (487, 'GRIPPLE', 'GRIPPLE ACHIEVES CARBON NEUTRALITY IN THE UK', '<p>Sheffield headquartered Gripple, the manufacturer of wire joiners and tensioners and suspension systems, has released its 2022 Sustainability Progress Report which outlines how it has achieved its first sustainability goal: carbon neutral across its UK operations.</p>

<p></p>

<p>This means that Gripple, through dedicated efforts across its South Yorkshire manufacturing plants, has managed to achieve emission reduction targets, supplemented with sustainable offsetting to deliver no net impact on the environment, as certified by Planet Mark* and in accordance with PAS2060:2014. This achievement demonstrates Gripple
’s commitment to comprehensively mitigating its environmental impact.</p>

<p></p>

<p>As demonstrated in its 2022 Sustainability Progress Report, Gripple has reduced its electricity usage from the national grid in 2022, compared to the previous year by four per cent, despite adding a new manufacturing site to its UK operations in 2022.</p>

<p></p>

<p>Tasha Lyth, Sustainability Manager at Gripple, said:
“Carbon neutral for our UK operations is an important milestone on our journey to climate positivity and it also paves the way for our other manufacturing facilities across the world to do the same.  We are extremely proud to have achieved this goal, through a focus on reducing carbon emissions, energy and water consumption, business travel and waste across all areas of our business.
”</p>

<p></p>

<p>She adds:
“The release of our 2022 Sustainability Progress Report helps our customers, partners, and colleagues understand more about how we are growing our business in an inclusive, environmentally conscious way.  We are committed to integrating cutting-edge technologies, forging strategic partnerships and engaging with stakeholders to ensure we leave an indelible positive impact on the planet.
”</p>

<p></p>

<p>Gripple achieved carbon neutral for its UK operations by focusing on improving energy management, investing in staff training and education and appointing Sustainability Champions in its business. Its Sustainability Champions support Gripple
’s Operations teams with frequent energy audits and reviews, identifying new improvement projects and sharing best practice across its UK sites. </p>

<p></p>

<p>Gripple has carefully chosen its carbon credits to offset carbon emissions on a number of hydro power, wind farms, solar farms and wind projects globally. </p>

<p></p>

<p>Part of Gripple
’s journey to climate positive involves evaluating the environmental performance of its products and systems.  The company
’s pre-assembled trapeze bracket system Fast Trak is the first product to have published verified Environmental Product Declarations (EPDs) including a full set of verified type III LCAs The Life Cycle Assessment (LCA).</p>

<p></p>

<p>Gripple
’s Fast Trak EPDs provide valuable information to architects and construction professionals who want to assess the long-term sustainability of products being used in the built environment.</p>

<p></p>

<p>Kevin St Clair, Managing Director of Gripple  UK &amp; Ireland, said:
“As an employee-owned company, we are incredibly proud to be building a business that is future proofed in terms of its environmental performance.  We are determined to play our part in reducing our impact on the planet and contribute to a brighter, more sustainable future. Our activities go beyond being carbon neutral and achieving net zero carbon emissions, by designing products and solutions that deliver environmental benefits to customers and local communities in our areas of operation globally.
”</p>

<p></p>

<p>In addition to achieving carbon neutrality for its UK operations, Gripple has been awarded a Silver Medal as a recognition of its EcoVadis Sustainability Rating.  The EcoVadis rating methodology measures the quality of a company
’s sustainability management system, which allows benchmarking against industry peers and evaluation of overall sustainability performance.</p>

<p></p>

<p>Gripple is already well on the way to reaching its second goal: to achieve carbon neutrality for its global operations by reducing its total carbon emissions (tco2e) from 862.7 to 822.6, representing a total decrease of 4.6%.   Its third goal is to achieve net zero by 2030 and Gripple has invested in upgrading its data collection processes to ensure a full carbon footprint can be assessed and analysed.</p>

<p></p>

<p>Steve Malkin, CEO of Planet Mark, explains:
“It is fantastic that Gripple has achieved carbon neutrality for its UK operations. The company is very much leading by example, by making the next ambitious commitment to achieve carbon neutrality for its global operations.
”</p>

<p></p>

<p>Gripple follows the Planet Mark business certification methodology on data collection and data quality for the areas of operations that contribute to its carbon footprint, including: energy, water, waste, travel and some elements of procurement.</p>

<p></p>

<p>Gripple is a globally recognised manufacturer, delivering innovative, value-added solutions to a wide range of sectors, including construction, manufacturing, solar, renewables, civils and agricultural, amongst others.</p>

<p></p>

<p>To download
‘Our Journey to Climate Positive
’ Progress Report, visit: <a href="https://www.gripple.com/media/12392/0001_gr_sustainability_report_interactive-4.pdf">https://www.gripple.com/media/12392/0001_gr_sustainability_report_interactive-4.pdf</a></p>', 523, '2024-03-02 23:41:56', '2024-03-13 00:05:04', 0, 'https://www.gripple.com/', 'gripple-2', '2024-03-13 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (488, 'University of Sheffield', 'University of Sheffield AMRC welcomes Productive Machines as newest member', '<p><a href="https://productivemachines.co.uk/">Productive Machines</a>&nbsp;joins the University of Sheffield Advanced Manufacturing Research Centre (<a href="https://www.amrc.co.uk/">AMRC)</a>&nbsp;as a new member, with the aim of helping to increase productivity and reduce carbon emissions in manufacturing.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Productive Machines, a pioneering Software as a Service (SaaS) and AMRC spin-out company, specialises in AI-based technology designed to reduce waste and optimise machining processes through easy-to-use solutions replacing trial and error processes for big and small engineering companies.</p>

<p>&nbsp;</p>

<p>The partnership between the AMRC and Productive Machines signifies a strategic alignment of expertise in the pursuit of precision engineering. As the newest&nbsp; member of the AMRC, Productive Machines brings its wealth of knowledge and disruptive technologies to the collaborative table, contributing significantly to the AMRC&
#39;s mission of driving net zero manufacturing.</p>

<p>&nbsp;</p>

<p>Dr Erdem Ozturk, CEO of Productive Machines, expressed enthusiasm about joining the AMRC community. He said: &quot;We have a special bond as a spin-out of the AMRC and we are thrilled to join this partnership and again become a part of the AMRC.</p>

<p>&nbsp;</p>

<p>&ldquo;This collaboration provides us with unique opportunities to promote techniques such as digital tap testing and to accelerate machine tool process optimisation contributing to shaping the future of advanced manufacturing.&quot;</p>

<p>&nbsp;</p>

<p>Matt Farnsworth, commercial director for the AMRC, said: &ldquo;As a spin out from the AMRC&
#39;s machining dynamics research team, we are extremely proud of the success of Productive Machines and absolutely delighted to have them joining the AMRC&
#39;s industrial partner network.</p>

<p>&nbsp;</p>

<p>&nbsp;&ldquo;We feel they have unique technology differentiation within the field of machining dynamics optimisation and modelling that provides a huge opportunity for further collaboration within the AMRC network and perfectly compliments our other technology providing members in this space.&rdquo;</p>

<p>&nbsp;</p>

<p>Productive Machines has turned years of research into commercially available products, bringing the results of groundbreaking innovation to every machining workshop.</p>

<p>&nbsp;</p>

<p>The Sheffield-based company has swiftly gained recognition for its machining dynamics expertise and cutting-edge solutions to eliminate chatter vibrations. Productive Machines&
#39; commitment to democratising technology and achieving more sustainable processes aligns seamlessly with the AMRC&
#39;s vision of fostering breakthroughs in manufacturing processes.</p>

<p>&nbsp;</p>

<p>Productive Machines unique digital twin simulates millions of combinations of machine settings to arrive at the optimum feed rates and spindle speeds for a given process prior to manufacturing. This optimisation unleashes the full potential of the machine tool by eliminating chatter vibrations and achieving improved productivity and part quality.</p>

<p>&nbsp;</p>

<p>Critical to the smooth running of operations is the preventative maintenance of machine tools. Spindles play a crucial role in the machining process and regular checks help to identify potential issues early on, reducing the risk of unexpected breakdowns and minimising downtime.</p>

<p>&nbsp;</p>

<p>Productive Machines provides a proactive approach to ensure the optimal performance and longevity of spindles by monitoring spindle&rsquo;s health through vibration analysis. Performing regular tap tests on the spindle, any unusual variations can indicate imbalances, misalignments, or wear in the spindle bearings.</p>

<p>&nbsp;</p>

<p>Today, the company has already impacted leading manufacturers from a variety of sectors, from medical to construction. Productive Machines has also raised a cumulative &pound;3m in investment and grown an amazing team.</p>

<p>&nbsp;</p>

<p>They continue to work closely with and enjoy the support of the AMRC and have exciting growth plans for 2024 and beyond.</p>

<p>&nbsp;</p>

<p>The partnership is expected to yield advancements in machining efficiency, precision, and sustainability, with potential applications across various industries. Collaborative projects will focus on integrating Productive Machines&
#39; technologies into existing manufacturing processes, driving productivity and reducing environmental impact.</p>

<p>&nbsp;</p>

<p>As the AMRC continues to be a global leader in research and development, the addition of Productive Machines as a member underscores the importance of collaboration between academia and industry in propelling technological innovation forward.</p>

<p>&nbsp;</p>

<p>This partnership not only benefits the involved entities, but also holds promise for creating a positive impact on the broader manufacturing ecosystem.</p>

<p>&nbsp;</p>

<p>The AMRC and Productive Machines anticipate exciting breakthroughs and look forward to the shared journey of future collaboration and of technological advances in manufacturing.</p>', 524, '
2024-03-02 23:50:41
', '2024-03-18 05:49:45
', 0, 'https://www.amrc.co.uk/', 'university-of-sheffield', '2024-03-14 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (489, 'Ardent', 'Ardent, has appointed Sharon Daly as a non-executive director', '<p>Ardent, the leading Leed-based surveying, communications and consents management practice, has appointed Sharon Daly as a non-executive director to help drive the next wave of growth.</p>

<p>&nbsp;</p>

<p>Sharon is the chief operating officer of Steer, a global consultancy that combines commercial, economic, technical and planning expertise to support clients across the world.</p>

<p>&nbsp;</p>

<p>She has worked with Ardent on a range of projects over the last 20 years, most recently on the Bank Station capacity upgrade for Transport for London. As non-executive director, Sharon brings operational experience and expertise, including deep knowledge of equality, diversity and inclusion and environmental, social and corporate governance to the Board.
.</p>

<p>&nbsp;</p>

<p>Her appointment is the latest move by Ardent to execute its ambitious growth plans after it reached &pound;10 million turnover in its most recent accounts.</p>

<p>&nbsp;</p>

<p>The company, which has expertise in the transport, renewables, utilities and regeneration sectors, has offices in Birmingham, London, Leeds, Glasgow, Warrington and Dublin and expects to reach a 250 headcount by 2026.</p>

<p>&nbsp;</p>

<p>Richard Caten, chief executive of Ardent, said: &ldquo;We have made no secret that we are very ambitious when it comes to growth and that has seen us make a series of appointments and promotions over the past 18 months.</p>

<p>&nbsp;</p>

<p>&ldquo;Bringing in Sharon Daly as non-executive director is a big part of that.</p>

<p>&nbsp;</p>

<p>&ldquo;We&rsquo;ve worked with Sharon, and the team at Steer, on numerous occasions over the years so we know that her experience and expertise will help support our growth and development over the coming years.&rdquo;</p>

<p>&nbsp;</p>

<p>Sharon said: &ldquo;I am really pleased to join the Ardent team as a non-executive director. I&rsquo;ve seen Ardent grow and prosper over many years and I know that they provide great advice and support to their clients. I am looking forward to playing a small part in the next stage of their journey.&rdquo;</p>', 525, '
2024-03-02 23:55:39
', '2024-03-12 00:05:06
', 0, 'https://www.ardent-management.com/', 'ardent', '2024-03-12 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (490, 'ECON', 'ECON ANNOUNCES LANDMARK DEAL FOR WORLD’S FIRST EVER FULLY ELECTRIC GRITTER', '<p>Econ Engineering&rsquo;s fully electric-powered gritter has secured a landmark contract with UK highways specialist Ringway signing up to take delivery of the revolutionary E-QCB (Electric Quick Change Body).</p>

<p>&nbsp;</p>

<p>The first ever completely electric vehicle of its kind on the market utilises the very latest in sustainable technology without diminishing performance, helping customers cut their emissions footprint.</p>

<p>&nbsp;</p>

<p>As the UK&rsquo;s leading manufacturer of winter maintenance and highways vehicles, Yorkshire-based Econ Engineering supplies&nbsp;gritters, hot boxes and tippers to hundreds of&nbsp;local councils and contractors, spanning the length and breadth of the UK.</p>

<p>&nbsp;</p>

<p>The E-QCB enables the vehicle&rsquo;s bodywork to be switched&nbsp;between gritter, tipper or caged body modes, with easy transformation taking no longer than 15 minutes.</p>

<p>&nbsp;</p>

<p>With more than 300 local authorities having declared a climate emergency, the Government has already issued guidance on how those councils, and other UK businesses, should be looking to switch their fleets to zero emissions vehicles ahead of 2035.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Ringway, responsible for looking after more than 50,000kms of the UKs highways network, has a long-term strategic fleet partnership with Econ and is the first contractor to sign up to a deal for the E-QCB.</p>

<p>&nbsp;</p>

<p>Both sustainable and practical,&nbsp;the E-QCB 19-tonne Volvo FE Electric, comes with a range of demountable body options including gritter, tipper &amp; caged body, which can be switched within just 15 minutes.</p>

<p>&nbsp;</p>

<p>Designed with versatility in mind, the vehicle comes with alternative battery configurations to allow optimisation of range and payload, it can be fully charged in less than two hours (subject to infrastructure), runs near-silent with a range of up to 170 miles (depending on load and road conditions), and with no exhaust gases produced, can make deliveries in zero emission zones.</p>

<p>&nbsp;</p>

<p>Vigorously tested&nbsp;during a&nbsp;recent demonstration tour around the UK, the E-QCB&nbsp;has already gained praise and endorsements while gritting rural routes in snowy Yorkshire, spreading sand for a Royal procession in central London and negotiating inner city streets in Scotland while collecting refuse.</p>

<p>&nbsp;</p>

<p><strong>Dave Olley, Operations Manager at Ringway</strong><strong>&nbsp;said:</strong>&nbsp;&ldquo;Ringway Hertfordshire is committed to delivering a sustainable service. Working with Econ Engineering we trialled the fully electric E-QCB multi-purpose vehicle and are now looking forward to adding it to our fleet and putting it to use on the UK&rsquo;s road network.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>Jonathan Lupton, Managing Director at Econ Engineering, said:</strong>&nbsp;&ldquo;We have spent a long time developing the E-QCB, undertaking comprehensive redesigns and trials across the UK, to ensure that we can offer a fully electric solution for our customers that can be used 52 weeks a year.</p>

<p>&nbsp;</p>

<p>&ldquo;We are delighted to extend what is already a strong relationship with Ringway by adding the E-QCB to its fleet. We are confident the vehicle will continue to impress and that its benefits will soon result in more deals of this kind.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>Christian Coolsaet, Managing Director of Volvo Trucks UK &amp; Ireland</strong><strong>, said:</strong>&nbsp;&ldquo;Every part of the transport sector needs to have its sights set on achieving net zero, and gritting and snowploughing is no exception. The team at Econ has developed an exceptional new product with our FE Electric chassis at its heart; this is going to allow gritter fleets to reap the benefits of operating cleaner, quieter and more efficient trucks.&rdquo;</p>

<p>&nbsp;</p>

<p>Feedback from the demonstration tour included:</p>

<p>&nbsp;</p>

<p><strong>North Yorkshire Council:</strong></p>

<p>&nbsp;</p>

<p>The E-QCB was tested to the limit on a cold, snowy day at Sutton Bank which has a hill gradient of 1 in 4, and includes plenty of tight bends. The vehicle undertook two 40-mile gritting runs and an NYC spokesperson said: &ldquo;The vehicle impressed us especially coping with the hills and had excellent range for an electric vehicle.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>The City of Edinburgh Council:</strong></p>

<p>&nbsp;</p>

<p>The E-QCB was put through extensive testing in both urban and hilly locations, with a council spokesperson remarking: &ldquo;Simple effortless drive, easy vehicle to use. Excellent build quality &ndash; would love one in our fleet.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>Glasgow City Council:</strong></p>

<p>&nbsp;</p>

<p>On a 90-mile gritting route in urban Glasgow, carrying an eight-ton non-diminishing payload, it returned with an impressive 13% battery life remaining, while a 50-mile gritting route saw it return to the depot with 47% battery usage.</p>

<p>&nbsp;</p>

<p>A council spokesperson said: &ldquo;Vehicle exceeded expectations, excellent build quality, cannot fault the vehicle at all.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>City of Bradford District Council:</strong></p>

<p>&nbsp;</p>

<p>The vehicle&rsquo;s flexible attributes were tested on a couple of routes, utilising both the tipper and then the gritter body, with a council spokesman commenting: &ldquo;Great drive, was pleasantly surprised. Quiet and comfortable.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>City of Westminster:</strong></p>

<p>&nbsp;</p>

<p>The E-QCB was used to lay sand across London&rsquo;s streets in readiness for the King&rsquo;s carriage as he attended the State Opening of Parliament. It travelled 66 miles in an urban environment and finished with 51% battery charge.</p>', 527, '
2024-03-18 05:49:39
', '2024-03-18 21:17:29
', 0, 'https://econ.uk.com/', 'econ', '2024-03-18 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (491, 'Progeny', 'Progeny partners with Alpha FMC to design next generation advice journeys', '<p>Progeny is partnering with Alpha Financial Markets Consulting, a specialist financial services consultancy, to design and deliver best in class, next-generation advice journeys.</p>

<p></p>

<p>The Next-Gen Advice project includes a comprehensive review of client processes, technology and policy, to ensure Progeny is setting the standard for industry best practice by being as productive, efficient and client-centric as possible.</p>

<p></p>

<p>The project is part of Progeny
’s commitment to ongoing and continuous improvement for the benefit of colleagues and clients, in line with its status as a B Corp, Corporate Chartered firm and the expectations of Consumer Duty.</p>

<p></p>

<p>All findings will be translated into an action plan to be implemented throughout 2024 and beyond.</p>

<p></p>

<p>Progeny UK Managing Director, Caroline Hawkesley, comments:</p>

<p></p>

<p>
“As an innovative and fast-growing professional services firm, we want to ensure we have the best performing systems and processes in place, built in collaboration with our team, clients and industry experts.</p>

<p></p>

<p>
“This will ensure that we have a future-fit advice journey with our clients at its heart, allowing our team to deliver a great service. It will also play an important role in our acquisitions by providing a clear set of processes and systems for acquired firms to integrate into, an approach that sets us apart from many of our peers.
”</p>

<p></p>

<p>Alpha FMC
’s Advice Technology Lead, Joe Harris, comments:</p>

<p></p>

<p>
“Alpha is delighted to support Progeny on this initiative. By working in collaboration with Progeny and leveraging our industry expertise and consulting methods we aim to design a set of best in class advice journeys.</p>

<p></p>

<p>
“We are excited to see the firm transition from the
‘Progeny of today
’ to the
‘Progeny of tomorrow
’ and to continue our work at Alpha supporting transformation across the financial advice market.
”</p>

<p></p>

<p>Caroline Hawkesley concludes:</p>

<p></p>

<p>
“It
’s so important that we continue to seek ways to improve professional standards and the way we do this is by always striving to improve our own operations.</p>

<p></p>

<p>
“We want the
‘Progeny way
’ to be synonymous with a first class, next generation client and colleague experience and we
’re looking forward to embedding our Next-Gen Advice project in the years ahead.
”</p>', 528, '
2024-03-18 21:17:23
', '2024-03-19 00:05:04
', 0, 'https://theprogenygroup.com/', 'progeny-2-1', '2024-03-19 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (492, 'GRIPPLE - March 2024', 'GRIPPLE RAISES OVER £28K FOR LOCAL CHARITY', '<p><strong>GRIPPLE RAISES OVER &pound;28K FOR LOCAL CHARITY</strong></p>

<p>&nbsp;</p>

<p>Gripple,&nbsp;the&nbsp;Sheffield-based, employee-owned manufacturer of wire joiners and tensioners and suspension solutions for agriculture, construction, solar and infrastructure,&nbsp;is celebrating a bumper year for its charitable fundraising activities, raising over &pound;28,000 for the Create a Dream Foundation.</p>

<p>&nbsp;</p>

<p>This is in addition to employees raising a total of &pound;25k in their own individual and team fundraising efforts for a number of other local charities, plus the fundraising arm of Gripple, the Gripple Foundation, donates one per cent of its budgeted profit, which includes supporting over 100 charities and community causes. The Gripple Foundation also provides up to &pound;500 match funding per employee per year to encourage their own individual fundraising efforts too.&nbsp;</p>

<p>&nbsp;</p>

<p>Back in January 2023, the Gripple Foundation, announced that Create a Dream Foundation was its nominated charity for the year.&nbsp; This is a Sheffield charity that helps fund gifts and trips for seriously ill young people in South Yorkshire.&nbsp;</p>

<p>&nbsp;</p>

<p>The money raised by Gripple will be put towards trips to exciting places like Centre Parcs, for Safari Experiences, to Disneyland Paris, to a ride on the Polar Express, as well as contributing towards items for the home and garden creating wonderful and long-lasting memories for families by making their child&rsquo;s dreams come true.</p>

<p>&nbsp;</p>

<p>Ed Stubbs, Gripple&rsquo;s Global Managing Director, said: &ldquo;We are extremely proud of our people for all the effort they have made to raise funds for Create a Dream Foundation &ndash; what they&rsquo;ve achieved is truly inspirational!&nbsp;&nbsp; What makes this so special is that individuals and teams within the business volunteered themselves for these challenges and did them, often in their own time and with real passion.&nbsp; It is fantastic that our donation will mean the children who are facing such difficulties can enjoy extra special trips out with their family and friends.&rdquo;</p>

<p>&nbsp;</p>

<p>Ailsa Watson from Create a Dream Foundation said: &ldquo;A huge thank you to everyone at Gripple for all being such amazing people!&nbsp; We are so grateful for your outstanding efforts in the past year &ndash; it is more than we could ever have hoped for!&rdquo;</p>

<p>&nbsp;</p>

<p>Throughout the year, Gripple employees have been involved in a wide variety of different fundraising activities, both for Create a Dream and other local charities.&nbsp; These include; a charity car wash, cycle challenge, 10k, half and full marathons, the GLIDE Fest Carnival fundraiser, &lsquo;Wear it Pink&rsquo; Day, a virtual relay from John o&rsquo; Groats to Lands&rsquo; End, a dog walking challenge, charity football tournament, Peak District hiking Challenge, plus Cake sales, food sales and raffles across Gripple sites.</p>

<p>&nbsp;</p>

<p>Gripple employees have also given up their time and put forward their skills to help in a number of community-based projects, supported by the Gripple Foundation.&nbsp;&nbsp; This included helping out with decorating a house for Enable Sheffield, which supports people with physical and learning disabilities to becoming independent.&nbsp; Employees also volunteered with projects such as a River Clean up at Norfolk Bridge, painting a barn for Whirlow Hall Farm Trust, redecoration and repainting for Support Dogs charity when it relocated its premises and creating a wildlife friendly garden for Clifford Primary School as well as helping out at Ben&rsquo;s Centre, a Homeless Charity in Sheffield. These activities are both motivational, enable team building and help create a strong sense of giving back to the local community.</p>

<p>&nbsp;</p>

<p>An employee-owned business, Gripple is committed to its people and is dedicated to its unique culture &ndash; &lsquo;the Gripple spirit&rsquo; that sets it apart.&nbsp; The company aims to employ people who share its values &ndash; of fun, innovation, passion, entrepreneurship, teamwork, integrity - who are invested in the shared purpose to be a fun, rewarding place to work.&nbsp;</p>

<p>&nbsp;</p>

<p>The Gripple Foundation is the charitable arm of Gripple, which is proud to support local charities and good causes, helping to make a positive different in the community.</p>

<p>&nbsp;</p>

<p>For more details email&nbsp;<a href="mailto:gripplefoundationcharity@gripple.com">gripplefoundationcharity@gripple.com</a>&nbsp;or visit the website&nbsp;<a href="http://www.gripple.com/">www.gripple.com</a>.</p>', 529, '
2024-03-18 23:00:00
', '2024-03-20 06:57:33
', 0, 'https://www.gripple.com/', 'gripple-march-2024', '2024-03-20 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (493, 'Stratticus', 'Leeds businesswoman says neuroscience can unlock hidden workforce potential', '<p><em>Leeds-based employee advocacy programme launches with a free webinar</em></p>

<p>&nbsp;</p>

<p>A Leeds businesswoman, Karolina Szymanska, has teamed with a London branding agency,&nbsp;<a href="https://stratticusstudio.com/social-media-training/">Stratticus</a>&nbsp;to offer business leaders an innovative way to empower their workforce using neuroscience.</p>

<p>&nbsp;</p>

<p>Gallup studies show that more than&nbsp;<strong>90 per cent&nbsp;</strong>of employees are sadly&nbsp;<strong>disengaged</strong>, leading to&nbsp;<strong>37 per cent higher absenteeism</strong>, an&nbsp;<strong>18 per cent reduction in productivity</strong>&nbsp;and a&nbsp;<strong>15 per cent decrease in profitability.</strong></p>

<p>&nbsp;</p>

<p>Neuroscience uses physiology and neural signals to gain insight into people&rsquo;s motivations, preferences and decision-making.</p>

<p>&nbsp;</p>

<p>Drawing on her passion for neuroscience, Karolina, a Strategic Marketing Advisor has launched a new&nbsp;<strong>Social Media Employee Advocacy Programme (SMEAP)&nbsp;</strong>with&nbsp;<strong>Jowita Penkala,&nbsp;</strong>a Mind Mastery Specialist and London-based branding agency Stratticus designed to accelerate employee engagement and build strong company advocacy.</p>

<p>&nbsp;</p>

<p>Jowita Penkala is a Mind Mastery Specialist, who helps individuals and businesses achieve &lsquo;<em>the impossible&rsquo;</em>&nbsp;and accelerate their growth. Stratticus is a design agency specialising in creating and developing brand strategies for organisations such as The Harley Street Clinic Children&rsquo;s Hospital and ABER Instruments.</p>

<p>&nbsp;</p>

<p>Together they are inviting business leaders from across Leeds and the UK to learn more at a free webinar, available via&nbsp;<a href="http://click.agilitypr.delivery/ls/click?upn=VUryq6u56kbH6FAnudt-2Fb-2FYcuBFIBpNF89lx-2B4VGNfrhevhd6MpovzopqASwLMjPoOOV_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3bxQY3LHgPvX-2BfVFgsDf8lBsU0bKNigajJEPXBBdjHgZ2dcw4Q33v-2FG4T3FAMiMbbvY-2F3YK7DLRDYAfLAwY7dfx5sKs57SQMx-2ByBjtSi-2FtD-2FWUyBfTrD5xMEXMdfs6tOVWzmIMSCvkjjK6qe-2BxXRuyJyxbEEN5dWSBTDu4-2BpqeMKLOVxBsa3254BoWEpHC8YAwpL2WcHry62TMNiikI6yA5RRQbvCfdDcqEKvP0WAZhHfEz5b8K24fe-2B-2FHv6QqhrqpOpctXZNpF4tqDCKi0u8HPUWs35-2FuhGwKGyshH4-2BmglUeOctJyb-2FJEBAUOU4JTGzISn-2FrtXhQ-2FVjEDihtSJP4-2BcyZ7N0cqJoG99RoJWWAk4cvuC24cOHMA71yK9SNi3F8-3D">https://meet.zoho.eu/c3GojuvR7L</a>&nbsp;&nbsp;on Tuesday, February 6th&nbsp;at 3PM to discuss the benefits of unleashing employees&#39; potential and turning them into fierce brand advocates.</p>

<p>&nbsp;</p>

<p><strong>Attendees of the free SMEAP webinar can expect to learn:</strong></p>

<p>&nbsp;</p>

<ul>
	<li>The value and importance of engaging employees</li>
	<li>How to train and mobilise&nbsp;your employees to use&nbsp;social media</li>
	<li>How to create brand awareness and generate leads</li>
	<li>How to make the most of your personal brand on LinkedIn</li>
</ul>

<p>&nbsp;</p>

<p><strong>Speaking about the launch, Ms Szymanska said</strong>,</p>

<p>&nbsp;</p>

<p><em>&ldquo;What if I told you, your most powerful brand ambassadors are right under your roof? Helping employees to feel empowered to develop their own personal brand leads to greater buy-in to the company vision with clear benefits to the business and bottom line. We hope you&rsquo;ll join our free webinar&nbsp;3PM on Tuesday 6</em><em>th</em><em>&nbsp;February.&rdquo;</em></p>

<p>&nbsp;</p>

<p><em>&nbsp;</em>SMEAP will help employees understand their company&rsquo;s higher purpose and develop a deeper understanding of their own personal brand. Unlocking the hidden potential of the workforce using neuroscience is the key to customer engagement, strong brand loyalty,&nbsp;increased leads&nbsp;and increased profits according to Ms. Szymanska.</p>

<p>&nbsp;</p>

<p><strong>Szymanska continues</strong>,</p>

<p>&nbsp;</p>

<p><em>&ldquo;Digital marketing is still perceived as solely &lsquo;owned&rsquo; by the marketing department. Today, more than ever, there is a need for cross-departmental collaboration to make digital marketing work for the business and deliver tangible results.</em></p>

<p>&nbsp;</p>

<p><em>We look forward to sharing our unique approach to neuromarketing and helping businesses connect more deeply with their customers and employees.&rdquo;</em></p>

<p>&nbsp;</p>

<p>The SMEAP series of workshops are carried out (online and in-person) over 17 weeks with group and individual sessions offering expert advice, training, coaching and implementation of proven neuromarketing tools.</p>

<p>&nbsp;</p>

<p>The benefits of the advocacy programme include creating a strong sense of belonging amongst employees, leading to a decrease in absenteeism and an increase in productivity. Establishing strong relationships with the target audience and transforming them into engaged communities, loyal customers and qualified leads.</p>

<p>&nbsp;</p>

<p>For more visit,&nbsp;<a href="https://stratticusstudio.com/social-media-training/">https://stratticusstudio.com/social-media-training/</a>&nbsp;|&nbsp;webinar via&nbsp;<a href="http://click.agilitypr.delivery/ls/click?upn=VUryq6u56kbH6FAnudt-2Fb-2FYcuBFIBpNF89lx-2B4VGNfrhevhd6MpovzopqASwLMjPm3tf_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3bxQY3LHgPvX-2BfVFgsDf8lBsU0bKNigajJEPXBBdjHgZ2dcw4Q33v-2FG4T3FAMiMbbvY-2F3YK7DLRDYAfLAwY7dfx5sKs57SQMx-2ByBjtSi-2FtD-2FWUyBfTrD5xMEXMdfs6tOVWzmIMSCvkjjK6qe-2BxXRuyJyxbEEN5dWSBTDu4-2BpqeMKLOVxBsa3254BoWEpHC8YAwLp8524VLdIu8-2FjEto4hoUqRrt8A4dP-2BPoXOBk9SUDXtVe-2BEZSVTqnPMNlnLjar8IQtksAyzPPeuLQAoFXnWJ0s7AtbKQzn8tewgDio66SYLn8w4Xr5-2BugRMB-2BUdpli95sq5ugB3aZfJcdVcJqOEMUtGpj2RjFfdBK-2Brj3OIBy8ETTOZc0KVdgKBE7pbrE-2FUI-3D">https://meet.zoho.eu/c3GojuvR7L</a>&nbsp;&nbsp;&nbsp;</p>', 531, '2024-03-20 06:57:26', '2024-03-21 06:29:07', 0, 'https://stratticusstudio.com/', 'stratticus', '2024-03-21 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (494, 'Ansvar', 'Specialist insurer launches winter weather risks guide for charitable organisations', '<p>Ansvar, an expert insurance provider for the charity, faith, and not-for-profit sectors, is taking proactive steps to support organisations facing the ongoing challenges of severe winter weather. With adverse conditions showing no signs of relenting, Ansvar has consolidated its extensive winter-related expertise and is providing cold weather advice to aid Yorkshire charities and non-profits in managing winter risks.</p>

<p>&nbsp;</p>

<p>Ansvar&rsquo;s specialised guidance on critical risk mitigation areas covers flash flooding, business continuity planning, environmental concerns, storm safety, and water leak prevention. Through the Cold Weather Advice page on Ansvar&rsquo;s website, organisations can also access informative videos and downloadable guides to share with staff and volunteers.</p>

<p>&nbsp;</p>

<p>Kelly Barter, Head of Customer Service at&nbsp;<a href="https://click.agilitypr.delivery/ls/click?upn=cUaLemB2eLzSQ3uXprtiBXSRmR9Yw3F03EPNEunmvDDFUv8a2wMqsjVRNCzVHFAoELnc_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3b0IrEsyvcU7RhhG-2BL1XA78D9N9vEC4gJXWeEscIN6Z-2FEPevhgC7DOLvMvRy-2Fe3v9ZNErx9GYeV9lm5ozy9zl91d8MWN5oL-2B-2B9DQDQ2TdPvkAClNG63wLpbzoXi7jia57savCnmgt9uAVMf-2FOs67vwG4eyLkXYUnaXuMCWn-2FEBF-2BAnmADY3IPvT87bntJwJTkzPcpTgoQkWv9myy3mNrl1wzuYxAtTcwd5Wf8bGrbAvqFQ27PRr1qAT-2F549r2PRayKOJmhaR6iOLGJXTJWzHCx0vyPwMF-2BfneHPT-2BrjkzaYOFPG96dU5Ac9qEYU8Ko-2F79qrFlaTrgL5yrCsJq4tlopRZdewNsXq-2Fatg5DrWxmCcXsGu7Yp9e42Nt0YvTBxKo50-3D">Ansvar</a>, commented:</p>

<p>&nbsp;</p>

<p>&ldquo;We understand the ongoing challenges that charities and not-for-profits face, especially during the current relentless winter weather. Ansvar is more than just an insurance provider; we are a partner in risk management. We want to empower charities and not-for-profits with the knowledge they need to weather any storm, both figuratively and literally.&rdquo;</p>

<p>&nbsp;</p>

<p>&ldquo;Our cold weather advice is there to serve as a resource for managing and mitigating the risks effectively, helping organisations protect their people and premises.&rdquo;</p>

<p>&nbsp;</p>

<p>In addition to the information provided through the Cold Weather Advice page on its website, Ansvar advises charities and not-for-profits to:&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Conduct volunteer training:</strong>&nbsp;Hold training sessions for volunteers to raise awareness about the specific risks associated with their roles and winter weather. Equip them with the knowledge to identify and address potential hazards during outreach activities and community events.</p>

<p>&nbsp;</p>

<p><strong>Share resources:</strong>&nbsp;Develop a collaborative approach with other charities to share resources and best practices for winter preparedness. This can include creating a network for the exchange of equipment, emergency supplies, and expertise in handling cold-weather challenges.</p>

<p>&nbsp;</p>

<p><strong>Review insurance protection:</strong>&nbsp;Ensure comprehensive liability coverage that includes protection against winter-related incidents is in place. Review insurance policies to understand coverage limits and exclusions and make any necessary adjustments to mitigate potential financial risks.</p>

<p>&nbsp;</p>

<p><strong>Install an emergency response plan:</strong>&nbsp;Develop and implement effective emergency response plans tailored to winter weather conditions, such as establishing a thorough communication channel.</p>

<p>&nbsp;</p>

<p><strong>Manage unoccupied buildings:</strong>&nbsp;Vacant properties can be more exposed to risk whether they are empty for a long time, or just over a holiday period. When there is no one to keep an eye on buildings, the damage caused by winter weather can go unnoticed, increasing the loss.</p>

<p>&nbsp;</p>

<p>Ansvar is part of the Benefact Group, the charity-owned specialist financial services organisation. The Benefact Group stands as the UK&
#39;s third-largest corporate donor, reinforcing Ansvar&
#39;s commitment to supporting the broader charitable community.</p>

<p>&nbsp;</p>

<p>For more information and to access the Cold Weather Advice page, visit&nbsp;<a href="https://click.agilitypr.delivery/ls/click?upn=cUaLemB2eLzSQ3uXprtiBXSRmR9Yw3F03EPNEunmvDDallgYqPJ204j9-2BStaxJAkXFfux7HZAVdHXNXfrAi-2FXnj4WWJeUlWrc-2FV5QmTJKjOiRmCshloqZhhIvg8okDSkJrCK_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3b0IrEsyvcU7RhhG-2BL1XA78D9N9vEC4gJXWeEscIN6Z-2FEPevhgC7DOLvMvRy-2Fe3v9ZNErx9GYeV9lm5ozy9zl91d8MWN5oL-2B-2B9DQDQ2TdPvkAClNG63wLpbzoXi7jia57savCnmgt9uAVMf-2FOs67vwG4eyLkXYUnaXuMCWn-2FEBF-2BAnmADY3IPvT87bntJwJTkwMGMolcAXb5IQKLY54UDXvj4UYQae9peFNL8DTc2e6jACz4Uv5I-2BNklO9IVU3HnrpftQYurdPwCGeQmJWH8UNjjro0jB8KERuSqV0WgZ8-2BqgdlxPBzmRnANh9-2BZJeXtUqI9BnXx6bWbPRYfTtTZUXIoWqVDi4uJl-2Fg1gJ2U8K6Sas45wGxru03LrLhtc6ffgk-3D">https://www.ansvar.co.uk/home/risk-management-guides-cold-weather-advice/</a>.</p>

<p>&nbsp;</p>', 532, '
2024-03-20 06:59:26
', '2024-03-21 06:28:42
', 0, 'https://www.ansvar.co.uk/', 'ansvar', '2024-03-20 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (495, 'Lichfields', 'Lichfields Announces Key Promotions', '<p>In its London office Jennie Baker has been promoted to Planning Director. Jennie leads the company&rsquo;s policy and law analysis team nationally and specialises in complex development management enquiries and Community Infrastructure Levy advice and strategies.</p>

<p>&nbsp;</p>

<p>Also in London, Nancy Stuart and Nuala Wheatley have moved into Associate Director roles, following their success on Development Consent Order (DCO) and major commercial development project work. Colin Smith has been promoted to a Senior Townscape Consultant.</p>

<p>&nbsp;</p>

<p>Other promotions include Heather Overhead in the Newcastle office, who specialises in Environmental Assessments and Biodiversity Net Gain (BNG) on major projects; and Tom Willshaw who is an integral part of the development management team in the Leeds office.&nbsp;Both have been promoted to Associate Director roles.</p>

<p>&nbsp;</p>

<p>The promotions follow the consultancy&rsquo;s continued growth and commitment to nurturing its talent. Awarded &lsquo;Consultancy Practice of the Year 2023&rsquo; at the sector-leading Property Week Resi Awards, Lichfields currently employs over 250 staff across its nine UK offices.</p>

<p>&nbsp;</p>

<p>James Fennell, Lichfields&
#39; Chief Executive said: &ldquo;We traded very well last year, and our promoted employees played a big part in that success. With no let-up in the pace of planning reform and a General Election not far away, they will have plenty to get their teeth stuck into. We wish them all the very best in their new roles.&rdquo;</p>', 536, '
2024-03-21 06:29:02
', '2024-03-21 06:29:18
', 0, 'https://lichfields.uk/', 'lichfields', '2024-03-21 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (496, 'Digital Enterprise', 'Grant Funding Helps Dempsey Dyer Undertake Digital Transformation', '<p>Grant Funding Helps Dempsey Dyer Undertake Digital Transformation</p>

<ul>
	<li>Family businesses, <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXOpOSB6Qda1oBu6e8DK-2FnC3VrBYts-2F2mfKW-2BDR2qlc42MXM8_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3ZRmNuK8mAKyo3T5LYXA2VfAxKDWGW88qkmOJ-2FGtGtZSIk-2FUzYlMDDpQ7rChYhjRru3efgYUgAl6EsQT-2BPJ48lMXsqolCQvZ6pe-2FkhmDDXjNbhhe7z7peOQ-2Ftf6UnigtFIzcKOzk13WJpDN3BEOZi3Vvh3hzY1iEQ1vVsJ5C9LEpS9L4ZVK2Vx5tucMpgrogvsVUVK2GkfzR3eNZVefKCZabCiAlpPB-2FS7Y2jH387lEwG8ZmhyMWhJQH5sLkfr4SyDjxvYNtkYPwQyssxnL1IUBmxQB3QY6viZuIDA3PCak6aqktGbWqr1G7I2hDgPkXQnphb8TCG3mEUAQPvbrQDbLrS-2ByUcA9cSf7Fml72RoYdFte3SG0JfW8F2Un-2Fs714XY-3D">Dempsey Dyer</a>, uses <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXOxzOm66EOd48POYYRmR6OjE9313rnms-2F3yidekEhQ8nF_eV_BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3ZRmNuK8mAKyo3T5LYXA2VfAxKDWGW88qkmOJ-2FGtGtZSIk-2FUzYlMDDpQ7rChYhjRru3efgYUgAl6EsQT-2BPJ48lMXsqolCQvZ6pe-2FkhmDDXjNbhhe7z7peOQ-2Ftf6UnigtFIzcKOzk13WJpDN3BEOZi3Vvh3hzY1iEQ1vVsJ5C9LEpS9L4ZVK2Vx5tucMpgrogvvdVirsAng0MaklWY7pXO8WYTF7q7taE-2BX4a7tHXwq10TjK9FgtIeZcxEqcqdLqpWrQ7HMComWN-2BXMVSkiqNR85dXz5l9sSG4t7BW-2FoNFKl7m-2Fto8GrbvjRKUbSOLr-2B-2FQ6ZLCt8FnrV6P-2FiwKTZW-2FzV8P2FWlboHbyeJ5nQMKxde9DAjB1KLn3WOvUkwXrvh-2BU-3D">Digital Enterprise</a> Funding to bring operating systems in line with its award-winning manufacturing</li>
	<li>Streamlined Business Support Programme results
£50,000 costs saving</li>
	<li>Company now poised for impressive growth thanks to efficient new business model</li>
	<li>Reduced admin leads to increased employee satisfaction and improved staff retention</li>
</ul>

<p>For nearly four decades, Dempsey Dyer has been a cornerstone of the Yorkshire community, renowned for its commitment to crafting high-quality windows and doors. As technology continued to evolve, the family-run business recognised the need to adapt to the digital era to ensure its long-term sustainability.</p>

<p></p>

<p>The myriad of challenges in recent years has meant businesses of all sizes have had to reflect and adapt to survive. For Dempsey Dyer, the very foundation of its operations were tested, requiring innovation, resilience, a willingness to embrace change, and the need to invest in digital. Understanding the cost of implementing new technology, Dempsey Dyer applied to business support programme Digital Enterprise (DE3) for financial support. </p>

<p></p>

<p><strong>Digital Enterprise (DE3) has been empowering established businesses across West Yorkshire to thrive and innovate since 2016. Their expertise, impartial advice and grant funding has enabled many businesses to invest in resilient technology solutions, helping to build foundations for growth through to complete digital transformation. With support from Digital Enterprise, Dempsey Dyer embarked on a transformative journey, embracing technology, increasing productivity, and redefining its approach to business.</strong></p>

<p></p>

<p>The
£7,000 grant awarded by Digital Enterprise (DE3) allowed Dempsey Dyer to remap its internal processes, leading to significant efficiency gains. Streamlined processes not only reduced the workload for staff but also enhanced job satisfaction by eliminating time-consuming administrative tasks. Additionally, the company was able to reduce its demand for paper trails, minimising its environmental impact.</p>

<p></p>

<p>Dempsey Dyer estimates that the grant has helped them save over
£50,000 in labour costs. Systems Manager Mark Suaznabar explains, "The efficiencies we\'ve been able to develop have streamlined the processes in our stores and accounts departments. But rather than making our workforce smaller, we\'ve reallocated roles and responsibilities to our team. By removing constrictive methods, team engagement has soared."</p>

<p></p>

<p>With a streamlined and more efficient business model, the company is now poised for continued growth in 2024. "We\'ve identified an opportunity to develop key relationships with small businesses and trades across the country," says Mark Suaznabar. "As the business is now much more functional and streamlined, we can offer the same fantastic rates and service to businesses of any size, making this an area where we can see an opportunity for real growth."</p>

<p></p>

<p>As Dempsey Dyer looks to the future, they hope to access further support from Digital Enterprise, which is now on its third round of business support. Digital Enterprise 3 (DE3) is specifically for West Yorkshire SMEs, over three years old and ready to invest in digital technology to take their business to the next level.</p>

<p></p>

<p>Digital Enterprise Programme Manager, Suzanne Bradbury, said:
“Dempsey Dyer stands as a shining example of the transformative impact DE3 can have on businesses. By empowering businesses to embrace technology with confidence, we enable them to unlock their full potential. We eagerly welcome businesses looking to improve productivity and grow through investment in digital technology and solutions, as we
’re able offer support and funding for a wide range of projects.
”</p>

<p></p>

<p>Mark commented,
“The advice and support from Digital Enterprise has been invaluable. We feel that everyone we have spoken to is on our side and has a genuine interest in seeing the business succeed. It
’s given us the confidence to make decisions and take risks, but also shown how businesses large and small, old and new can benefit from new ideas and a fresh perspective.
”</p>

<p></p>

<p>By embracing technology and redefining its approach to business, the company has secured its place as a leader in the windows and doors industry, ensuring its continued success for generations to come. As such, Dempsey Dyer were named in Digital Enterprise
’s Top 100, a celebration of the most transformational businesses across the region. Dempsey Dyer
\'s story serves as a testament to the power of adaptation and innovation in a fast-paced environment.</p>

<p></p>

<p>Through its Digital Accelerator Grant, DE3 can provide up to 50% (maximum
£10,000) of costs for projects ranging in value from
£2,000 to
£25,000. Additionally, DE3 is able to offer a limited number of High Impact Grants, for projects that will have a significant and transformational impact on businesses. Eligible businesses can receive up to 50% funding (or a maximum
£35,000) for new digital initiatives costing between
£25,000 and
£100,000. As High Impact projects take longer to complete, DE3 is encouraging businesses to apply sooner rather than later.</p>

<p></p>

<p>This programme is funded by the UK Government through the UK Shared Prosperity Fund. The UKSPF is led by the Department for Levelling Up, Housing and Communities. Digital Enterprise is being delivered in partnership with the West Yorkshire Combined Authority, acting as Lead Authority for the Funds.</p>

<p></p>

<p>Ambitious growth businesses based in West Yorkshire can find out more about eligibility for Digital Enterprise, and apply for a Digital Accelerator Grant or High Impact Grant while there
’s still time by visiting: <a href="https://click.agilitypr.delivery/ls/click?upn=1ZD3PGic3a8iOuOK-2FEeWXOxzOm66EOd48POYYRmR6OjE9313rnms-2F3yidekEhQ8n5AZ__BHBYAIZgNg-2FQdFmLHn0y0mAdsXfd51k8o5eeWG7-2FdK6TecUkCnPpR1B6yhOW6OQW3lcbWV7GQYgALyezymHCbMT-2BkAoWAO-2BjLSwU8-2FXUN3ZRmNuK8mAKyo3T5LYXA2VfAxKDWGW88qkmOJ-2FGtGtZSIk-2FUzYlMDDpQ7rChYhjRru3efgYUgAl6EsQT-2BPJ48lMXsqolCQvZ6pe-2FkhmDDXjNbhhe7z7peOQ-2Ftf6UnigtFIzcKOzk13WJpDN3BEOZi3Vvh3hzY1iEQ1vVsJ5C9LEpS9L4ZVK2Vx5tucMpgrogvs-2FuKT4iji1Xykyubdtkqq6ZGXgmsMA2tbWgc33GUGI8nvXT40ZpgadiEH28-2FCP-2BhF8fR6NILjodN8u9Xrn7HJQHVaRE7rYJ6QOB7kEVaQwPLQ2NEqLt6Trc9Q8KNY-2BPdqSpi6Im4-2FYluRTUIWX7Hr79HISp6SaolAxowfBiellarPtJTztEAa9QSYGdo1OxwI-3D">https://www.digitalenterprise.co.uk/</a></p>', 537, '2024-03-21 07:17:38', '2024-03-25 00:05:04', 0, 'https://www.digitalenterprise.co.uk/', 'digital-enterprise', '2024-03-25 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (497, 'Zinc Consult', 'Firm grows with new division focussing on a ‘lost art’ that is in big demand', '<p>A LEADING construction consultancy has added another "key component" in the development of the business by launching a crucial new division.</p>

<p>Zinc Consult has created an Estimating and Cost Planning Division after integrating family-run Infrastructure Estimating LTD into the business.</p>

<p></p>

<p>The new division will be headed up by estimating manager Andy Tucker, who has over 40 years of experience in mainstream construction and rail, having held several major positions with consultants and a senior role with Network Rail.</p>

<p>"I am excited to be working with Zinc Consult. We share a passion for innovation, and I am impressed with Zincs commitment to in house training and development of new technologies," said Andy, a fellow of the Association of Cost Engineers.  </p>

<p>"Zinc has a proven track record in delivering for clients across the rail and construction industry. I really believe we can help grow and strengthen the business",</p>

<p>Zinc
’s Estimating and Cost Planning Division will be based in Filey, on the Yorkshire Coast, where Infrastructure Estimating LTD has operated for the past decade out of the Evron Centre.</p>

<p>However, there are now plans to grow by moving into a bigger site  and to develop the next generation of estimators. Andy has recently trained his son Joe which he has found extremely rewarding, with Joe now being an experienced and productive estimator.</p>

<p>For Andy, his major concern is that estimating from first principles is becoming a "lost art"  and one he hopes to help maintain.</p>

<p>
“There
’s a real shortage of experienced first principles estimators these days, so this move really shows Zinc Consult
’s commitment to offering cutting-edge solutions and helps set us apart from our competitors,
” said Andy, who has held key roles with several major construction firms and consultancies/</p>

<p>The firm
’s already receiving demand for this new division
’s skills from contractors and other businesses.</p>

<p>And interest in utilising the new division isn
’t just coming from the rail industry, with the firm also providing specialist knowledge in estimating for traditional construction, including restoration projects on classical building along with alterations and building repurposing projects.</p>

<p>Amid sweeping changes to the British high street, Andy foresees repurposing of buildings becoming more common given the environmental benefits it provides especially the reduced carbon footprint.</p>

<p>"Understanding a project\'s carbon footprint is becoming increasingly important for projects to get the green light and the required financial support," Andy explains.</p>

<p>
“We
\'re already working on the carbon impact for some big projects right from their initial cost estimates. It\'s crucial to start evaluating a project\'s environmental impact as early as possible.”</p>

<p>And with so much interest in the new division, Zinc
’s co-founder, Jonathan Blenkey, said the division will play a big role in the firm
’s planned growth.</p>

<p>"We’ve known and worked with Infrastructure Estimating for a long time and have always been impressed with the expertise and high level of service that they provide," he added.</p>

<p>"We felt they were an ideal match for Zinc and share our core values of delivering a quality service on time and to budget.</p>

<p>
“We are absolutely delighted that they have joined the team and will play a key role in our future expansion as we expand Zinc
’s service offering, whilst maintaining our quality reputation.
”</p>

<p>For more information on Zinc Consult, please visit <a href="https://www.zincconsult.co.uk/contact">https://www.zincconsult.co.uk/contact</a></p>', 538, '2024-03-21 07:19:45', '2024-03-25 00:05:06', 0, 'https://www.zincconsult.co.uk/', 'zinc-consult', '2024-03-25 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (498, 'Progeny - March 2024', 'Progeny announces acquisition of Chartered Wealth Management', '<p>Multi-disciplinary professional services firm, <a href="https://theprogenygroup.com/">Progeny</a>, has acquired Chartered financial advice firm, <a href="https://charteredwealthmanagement.co.uk/">Chartered Wealth Management</a>.</p>

<p></p>

<p>This will take Progeny
’s total assets under management to
£9bn.</p>

<p></p>

<p>With offices in Manchester and London, Chartered Wealth Management offer tailored financial planning and asset management services to high-net-worth clients.</p>

<p></p>

<p>They are a team of 21 members of staff, with nine Chartered financial planners and wealth managers, with a collective 170 years
’ experience in providing financial help and advice.</p>

<p></p>

<p>Mark Stanbury, Founding Director of Chartered Wealth Management, said:
“This is an exciting day for our team and our clients, as we join Progeny  a business rapidly growing in scale and profile.</p>

<p></p>

<p>
“We look forward to being part of this unique business as well as the mission to transform and improve financial advice for the better, for our clients and the industry at large.
”</p>

<p></p>

<p>Progeny CEO, Neil Moles, said:
“It
’s a pleasure to welcome such a prestigious and high-performing business into Progeny, one which is forward-thinking and committed to excellence in equal measure.</p>

<p></p>

<p>
“This is the culmination of a five-year search for the right firm, one that will allow us to expand into a new geography but which also crucially meets the strict criteria we set for our acquisition targets around average client size and age, as well as the age and ambition of the firm
’s team.
”</p>

<p></p>

<p>
“With Chartered Wealth Management onboard, we look forward to building our presence and extending our proposition in the north west of England. There are exciting times ahead.
”</p>

<p></p>

<p>A team from global law firm Squire Patton Boggs acted as legal adviser to Progeny during the deal and law firm TLT acted for shareholders of Chartered Wealth Management.</p>', 539, '
2024-03-22 05:25:43
', '2024-03-26 00:05:04
', 0, 'https://theprogenygroup.com/', 'progeny-march-2024', '2024-03-26 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (499, 'Microsearch Laboratories', 'Microsearch Laboratories Successfully Maintains ISO17025 Accreditation Following Rigorous UKAS Audit', '<p>Halifax, West Yorkshire  Microsearch Laboratories, a leading microbiological and allergen testing company, is pleased to announce the successful completion of a comprehensive 3-day audit by the United Kingdom Accreditation Service (UKAS). The audit resulted in the renewal of Microsearch Laboratories\' ISO17025 accreditation for another year, showcasing the company\'s unwavering commitment to maintaining the highest standards in their industry.</p>

<p></p>

<p>ISO17025 accreditation is a testament to Microsearch Laboratories
\' dedication to quality and excellence. The company takes pride in consistently upholding the strict requirements outlined by ISO17025, demonstrating its commitment to providing top-tier services to clients.</p>

<p></p>

<p>"We are thrilled with the result.” said Danny Franklin, Joint Managing Director at Microsearch Laboratories. “We want to thank and congratulate all our colleagues whose hard work and dedication played a crucial role in upholding these rigorous standards on a daily basis. Their collective efforts ensure that Microsearch Laboratories continues to provide unparalleled services to its valued clients.”</p>

<p></p>

<p>About Microsearch Laboratories:</p>

<p>Microsearch Laboratories is a microbiological and allergen testing company based in Halifax, West Yorkshire.</p>

<p></p>

<p>For more information about Microsearch Laboratories and its services, please visit: <a href="https://micro-search.co.uk">https://micro-search.co.uk</a></p>', 540, '2024-03-22 05:32:05', '2024-03-26 00:05:05', 0, 'https://micro-search.co.uk/', 'microsearch-laboratories', '2024-03-26 00:00:00');
INSERT INTO `threads` (`id`, `title`, `intro`, `body`, `meta_id`, `created_at`, `updated_at`, `hide`, `url`, `slug`, `published_at`) VALUES (500, 'Banks Homes', 'MICHAEL JOINS BANKS HOMES AS HEAD OF CONSTRUCTION', '<p>Regional housebuilder Banks Homes is gearing up for growth after appointing Michael Harty as its new head of construction.</p>

<p></p>

<p>Michael brings well over than 30 years
’ housebuilding experience to his new role, which involves overseeing the safe, efficient and high-quality delivery of the family firm
’s residential developments across Yorkshire and North East England.</p>

<p></p>

<p>Michael originally trained as an apprentice bricklayer before working his way up through site manager, construction manager and construction director roles with well-known regional housebuilders.</p>

<p></p>

<p>He has been responsible for the delivery of numerous award-wining residential developments ranging from 30 to 300 homes across an area stretching from York up to Newcastle.</p>

<p></p>

<p>Set up last year by regional employer the Banks Group, Banks Homes is looking to develop residential sites of all sizes across Yorkshire and North East England, with its first sites in Yorkshire being set to come forward this year.</p>

<p></p>

<p>Michael Harty says:
“My career path has given me a first-hand view of every aspect of residential development and what the right ways are to plan, realise and complete these complex construction projects.</p>

<p></p>

<p>
“I knew of the Banks Group
’s various different businesses and found that the Banks Homes management team shared the same commitment as me towards caring about building the best possible homes in the best possible ways for its customers.</p>

<p></p>

<p>
“This new role provides the challenge I was looking for and I
’m enjoying being part of a high-quality team that delivers on the promises that it makes.
”</p>

<p></p>

<p>The Banks Group has 48 years
’ experience of developing land for a range of uses including residential and commercial property, mineral extraction and land reclamation.</p>

<p></p>

<p>As well as managing the construction on some of the sites that sister company Banks Property brings forward, Banks Homes is also acquiring its own development sites directly from the market.</p>

<p></p>

<p>Banks Property is continuing to work independently, promoting land opportunities and bringing high quality development sites to the market for many of the UK
’s best-known housebuilders, as it has done successfully for over 35 years.</p>

<p></p>

<p>Russ Hall, managing director at Banks Homes, adds:
“Michael
’s wide-ranging experience of residential development and his absolute commitment to maintaining the highest possible construction standards made him an ideal candidate for this role and we
’re very pleased he is</p>

<p>now part of our senior team.</p>

<p></p>

<p>
“Banks Homes is focused on building new, high-quality and sustainable homes across Yorkshire and North East England, and we have a pipeline of projects at different stages of development that will be continuing to expand.</p>', 541, '
2024-03-22 05:34:42
', '2024-03-22 05:34:42
', 1, 'https://www.bankshomes.co.uk/', 'banks-homes', '2024-03-27 00:00:00');
