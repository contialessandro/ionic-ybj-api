id,title,intro,body,meta_id,created_at,updated_at,hide,url,slug,published_at
224,"BRABNERS - Press Release","BRABNERS HELPS UNLOCK YORKSHIRE DALES CONSERVATION PROJECT","<p>Leading independent law firm Brabners has invested &pound;50,000 to help kickstart a new project that will restore more than 400 hectares of internationally important peatland in the Yorkshire Dales as part of its sustainability strategy.</p>

<p>&nbsp;</p>

<p>The carbon-neutral firm, which is due to open a new office in Leeds this summer, is working with Yorkshire Peat Partnership and Kingsdale Head Farm as part of a wider four-year programme, the Great North Bog, which in turn is restoring more than 3,500 hectares of peatland and blanket bogs across Northern England. The project at Kingsdale Head is expected to prevent the loss of at least 450 tonnes of carbon &ndash; equivalent to planting more than 22,000 trees - to the atmosphere each year.</p>

<p>&nbsp;</p>

<p>Brabners&rsquo; initial investment will help to unlock &pound;550,000 in grant funding for restoration work at Kingsdale Head via the Nature for Climate Peatland Capital Grant Scheme. The work, which will undo the damage inflicted on the landscape, includes the creation of 5,000 dams and sediment traps to reverse artificial drainage across the site. The firm intends to support Kingsdale Head beyond its financial commitment and already has an employer supported volunteering day booked in to plant trees at Kingsdale Head in the autumn.</p>

<p>&nbsp;</p>

<p>Yorkshire accounts for more than a quarter of England&rsquo;s peatland &ndash; the majority of which is damaged. Yorkshire Peat Partnership, led by Yorkshire Wildlife Trust, is leading the peatland restoration efforts in North Yorkshire&rsquo;s uplands.</p>

<p>&nbsp;</p>

<p>Kingsdale Head was acquired by owners Catherine Bryan and Tim Yetman in 2020. Working with their conservation and farm manager, Jamie McEwan, and partners including the Woodland Trust, Yorkshire Dales National Park, the University of Manchester and many local experts, they intend to restore and improve the biodiversity and carbon capturing properties of the 608-hectare site near Ingleton.&nbsp;</p>

<p>&nbsp;</p>

<p>Led by its internal Sustainability, Environmental &amp; Green Group (SEGG), Brabners achieved carbon neutral status for the first time in 2020. It was the first UK law firm outside of London to join the climate change awareness-raising organisation &lsquo;Business Declares a Climate Emergency&rsquo;, where the firm was introduced to fellow members, the Kingsdale Head project.</p>

<p>&nbsp;</p>

<p>Catherine Bryan, owner of Kingsdale Head Farm, said:&nbsp;&ldquo;We are at the beginning of a long journey at Kingsdale Head to bring a greater diversity of life back into the landscape, as well as restore precious peatland, enabling it to hold more water and store more carbon. The UK is one of the most nature depleted countries in the world but, with the support of Brabners, Yorkshire Peat Partnership and many other partners, we hope to further demonstrate how upland farms can generate sustainable income whilst delivering action to improve the resilience and diversity of our natural world.&rdquo;</p>

<p>Robert White, CEO of Brabners, said:&nbsp;&ldquo;With more than 10% of the world&rsquo;s blanket bog on our doorstep, the UK has a unique role to play in natural carbon capture and the fight against climate change. High quality peatland can act as a super storer but much of its natural benefits have been lost to support the development of land for agriculture and grazing. Through SEGG, we continually assess how we as a business can make a positive difference &ndash; with the project at Kingsdale Head providing both an immediate and long-term impact. We see this very much as the start of our relationship and look forward to working with Catherine and Tim to showcase sustainable farming as a viable approach to agriculture. Investing in a bog may not be glamorous, but it is pioneering and in keeping with our focus on innovation as a leading independent law firm. It will have a huge impact, and we cannot wait to get our wellies on and get across to our Brabners&rsquo; bog in October.&rdquo;</p>

<p>&nbsp;</p>

<p>Dr. Tim Thom, Peat Programme Manager at Yorkshire Wildlife Trust, said:&nbsp;&ldquo;We have ambitious plans to renew, replant and restore all of Yorkshire&rsquo;s upland peatland by 2035. Kingsdale Head has a significant role to play in achieving that target as we look to deliver the changes in landscape and biodiversity that will support natural regeneration well into the future. Having the backing of the private sector is hugely valuable in unlocking new funding, so we&rsquo;re incredibly grateful to have Brabners&rsquo; support, which will help accelerate investment for the benefit of everyone.&rdquo;</p>",235,"2022-09-25 08:04:40","2022-10-04 00:05:54",0,https://www.brabners.com/,brabners-press-release,"2022-10-04 00:00:00"
225,"Progeny - Press Release","Progeny acquires Balmoral Asset Management to expand Scottish presence","<p>Progeny has today announced plans to acquire Edinburgh-based Chartered financial planners, Balmoral Asset Management.&nbsp;</p>

<p>This represents the next step in Progeny&rsquo;s concerted expansion into Scotland and will take their total assets under management to more than &pound;6.5bn.&nbsp;</p>

<p>Based in Edinburgh, Balmoral Asset Management were established to provide a comprehensive wealth management service to a select number of private clients.</p>

<p>Launched in 1999, Balmoral is renowned for its expertise in providing growth for clients while meeting their complex financial needs.</p>

<p><strong>Stuart MacDonald, Managing Director, Balmoral Asset Management, said:</strong>&nbsp;&ldquo;We&rsquo;re immensely proud of what we have created at Balmoral Asset Management: a prestigious advice firm that&rsquo;s built a reputation on getting results for our clients.</p>

<p>&ldquo;Joining Progeny will allow us to continue to grow, and to increase the range of services we offer our clients, many of whom have complex requirements which will benefit greatly from the multi-disciplinary professional services expertise we will now be able to provide.</p>

<p>&ldquo;We&rsquo;re diligent in maintaining the high standards we have set. There are not many firms out there who we could join with and see an improvement in our client offering, but Progeny is one such firm and we can&rsquo;t wait to get started on this next phase of our journey.&rdquo;</p>

<p><strong>Neil Moles, CEO of Progeny, said:&nbsp;</strong>&ldquo;We have been steadily expanding our presence in Scotland in the last few years and our acquisition of Balmoral Asset Management will allow us to take a significant step forward in this aim.</p>

<p>&ldquo;Balmoral Asset Management are a highly respected firm who have become a byword for impeccable standards and exceptional client service.</p>

<p>&ldquo;There are so many areas of overlap and common ground between us, in our services, our business ethos and in wanting to make a meaningful commitment to the next generation of our industry.</p>

<p>&ldquo;I look forward to working with Stuart and his excellent team, and to everything we can achieve together.&rdquo;</p>

<p>Progeny first entered the Scottish market with the acquisition of&nbsp;<a href="https://theprogenygroup.com/blog/press-release-progeny-expands-into-scotland-and-south-east/">Innovate</a>&nbsp;Financial Services in Edinburgh in February 2019. This was followed in April 2021 with the acquisition of Ayrshire-based&nbsp;<a href="https://theprogenygroup.com/blog/progeny-acquires-ayrshire-based-financial-advice-firm-to-continue-expansion-into-scotland/">Affinity</a>&nbsp;Financial Planning.</p>",236,"2022-09-25 08:06:47","2022-10-05 00:05:04",0,https://theprogenygroup.com/,progeny-press-release,"2022-10-05 00:00:00"
226,"Bright Spaces - Press Release","Proptech startup Bright Spaces partners with Bruntwood Works and expands its presence in the UK","<ul>
	<li><strong>Bright Spaces has signed a partnership with UK-based commercial property developer Bruntwood Works to launch a visualisation and leasing platform for </strong><a href="https://westvillageleeds.co.uk/"><strong>West </strong></a><a href="https://westvillageleeds.co.uk/"><strong>Village</strong></a><strong> Leeds - part of Bruntwood Works&rsquo; &pound;200m Pioneer programme to create cutting-edge workspaces of the future </strong></li>
	<li><strong>Digitalisation of real estate is accelerating, demonstrating the increasing importance of proptech solutions both for developers and tenants.</strong></li>
</ul>

<p>&nbsp;</p>

<p>Proptech startup <a href="https://brightspaces.tech">Bright Spaces</a> has partnered with Bruntwood Works to present a virtual representation of West Village Leeds and its available spaces to future customers. The 3D model will showcase virtual walk through technology and allow viewings to be requested digitally. This partnership is based on the common drive for innovation and desire to be closer to today&rsquo;s customers&rsquo; needs through an advanced, 3D-digital twin based solution. The virtual visualisation of the West Village Leeds development is aimed to offer a more immersive view of the project, meeting clients&rsquo; requirements for tech solutions and to accelerate the leasing process.</p>

<p>&nbsp;</p>

<p>Bruntwood Works is one of the UK&rsquo;s leading property providers and is firmly embedded in its cities and regions. It blends work and lifestyle to create spaces that encourage interaction and create communities. Its workspace, retail and leisure offering is design-led, people-driven and socially responsible. It creates, owns and manages over 5m sq ft of inspiring workspace environments. It offers everything from individual coworking desks and meeting rooms to fully managed offices and retail spaces, all in superb locations across Manchester, Cheshire, Leeds, Liverpool and Birmingham to help businesses thrive and grow.&nbsp;West Village Leeds will comprise innovative human-centric spaces designed to support wellbeing and productivity, that will centre around a newly-transformed courtyard and act as a place for local independent food and drink pop-ups, with restorative green spaces.</p>

<p>&nbsp;</p>

<p>Sam Wilson, Director of Digital Solutions and Technology at Bruntwood Works: &ldquo;<em>We are committed to providing the very best experience for our customers, and </em><em>our vision for West Village Leeds is to create a destination for the community to come together, that has creativity and collaboration at its heart. </em><em>We believe innovative technology has a fundamental role to play in shaping the workplaces of the future, and </em><em>our partnership with Bright Spaces is the next step in realising the landmark development for the region. With the work we&rsquo;ve done together we&rsquo;re allowing our customers to bring an empty space to life and see how great it could look when they move in. We look forward to working together to bring the vision to life.</em>&rdquo;</p>

<p>&ldquo;<em>The UK is a highly important market for us and we are grateful to be trusted by a major player in the country. Bruntwood Works&rsquo; extensive experience and know-how represent a major benefit for our development. We are growing our presence here, through collaborations with local clients and through our local office, to be opened soon. These steps are essential to our scaling at a European level and to our mission to become a digital universe for the built world.</em>&rdquo; - Bogdan Nicoara, CEO and Cofounder Bright Spaces.</p>

<p>&nbsp;</p>

<p>Bright Spaces and Bruntwood Works will soon disclose more specific details about the digital solution and their plans moving forward. Digitalisation is the future of real estate and the two partners are committed to being at the forefront of innovation in real estate.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>About Bright Spaces</strong></p>

<p>&nbsp;</p>

<p><strong>BRIGHT SPACES</strong> is a European Venture Backed PropTech startup that offers a complete digital showcasing and commercializing solution for office and residential spaces. By using 3D visualization, digital space availability, and various automation, optimization, and digitalization features in the leasing or selling process, Bright Spaces aims to increase the number of relevant requests and to foster commercial agreements in these segments.&nbsp; <a href="https://brightspaces.tech">https://brightspaces.tech</a></p>

<p>&nbsp;</p>

<p><strong>BRUNTWOOD WORKS </strong></p>

<p>Bruntwood Works, which is part of the Bruntwood Group, is one of the UK&rsquo;s leading property providers and is firmly embedded in its cities and regions. It blends work and lifestyle to create spaces that encourage interaction and create communities. Its workspace, retail and leisure offering is design-led, people-driven and socially responsible.</p>

<p>It creates, owns and manages over 5m sq ft of inspiring workspace environments. It offers everything from individual coworking desks and meeting rooms to fully managed offices and retail spaces, all in superb locations across Manchester, Cheshire, Leeds, Liverpool and Birmingham to help businesses thrive and grow.&nbsp;</p>

<p>Bruntwood Works&rsquo; &pound;200m Pioneer programme is creating the future of workspace design and innovation. The buildings of tomorrow, today - centred on wellness, sustainability, biophilia, art, amenity and technology. Bruntwood Works&rsquo; Pioneer buildings include <a href="https://bruntwood.co.uk/our-locations/manchester/bloc/">Bloc</a>, <a href="https://bruntwood.co.uk/our-locations/manchester/blackfriars-house/">Blackfriars House</a>, <a href="https://pioneerbybruntwoodworks.co.uk/pioneer-buildings/neo/">Neo </a>and <a href="https://pioneerbybruntwoodworks.co.uk/pioneer-buildings/111-piccadilly/">111 Piccadilly</a> in Manchester,&nbsp;and <a href="https://pioneerbybruntwoodworks.co.uk/pioneer-buildings/the-plaza/">Plaza</a> in Liverpool.</p>

<p>The wider portfolio includes spaces such as<a href="https://bruntwood.co.uk/our-locations/liverpool/cotton-exchange/">Cotton Exchange</a> in Liverpool,<a href="https://bruntwood.co.uk/our-locations/cheshire/booths-park/"> Booths Park</a> in Cheshire,<a href="https://bruntwood.co.uk/our-locations/leeds/west-one/">West One</a> in Leeds and<a href="https://bruntwood.co.uk/our-locations/birmingham/cornerblock/"> Cornerblock</a> in Birmingham.</p>

<p>Bruntwood Works promotes a better work-life balance with access to a range of wellness, retail and leisure facilities within its buildings. And it owns, operates and manages extensive retail and leisure space including at <a href="http://www.afflecks.com">Afflecks</a> and <a href="http://www.hatchmcr.com">Hatch</a>.</p>

<p>Its customers can access a unique network of resources and expertise, including customer support programme Spark by Bruntwood Works, which helps businesses evolve and grow through access to webinars, networking opportunities and training. Its wellness partnerships include FORM, Bode Clinic and Altius Healthcare.</p>

<p><br />
It is also committed to a sustainable future and the Bruntwood Group was the first commercial property partner to sign up to the Green Building Council&rsquo;s Net Zero Carbon Commitment pledge, which aims for new buildings to be net zero carbon by 2030 and older buildings the same by 2050.</p>",237,"2022-09-25 08:31:22","2022-10-06 00:05:04",0,https://www.insights.brightspaces.tech/,bright-spaces-press-release,"2022-10-06 00:00:00"
227,"Cleanology - Press Release","Award-winning commercial and office cleaning contractor Cleanology is bringing its sustainable and ethical approach from London to Leeds.","<p>After 20 years servicing the capital, and nearly 10 years in Manchester, Cleanology is forging ahead with new hubs across the Midlands, Scotland and the South-West. Now working in 14 cities across the UK, national clients include a well-known electric vehicle manufacturer and a number of leading national chains. Cleanology&rsquo;s new Leeds base is at Park House in Park Square West.</p>

<p>To mark the national launch, Cleanology has commissioned an HGV to be specially branded in its iconic green &amp; blue flag logo. Speaking at the launch, CEO Dominic Ponniah said: &ldquo;We are excited that this branded HGV will be travelling to every part of the country at the same time as we expand our business across the UK. While our base has always been London, we&rsquo;ve been delivering contracts in Manchester for almost 10 years and now have offices in Manchester and Leeds. The launch of our national service is a natural expansion which will make our future growth plans possible. This year, we have already grown by 50%. We aim to double our turnover in the next three years to &pound;30m; going national is part of that journey.&rdquo;</p>

<p>In just the past 12 months, Cleanology has expanded from its London and Manchester hubs, to 12 additional towns and cities, including Banbury, Bedford, Bicester, Birmingham, Bristol, Edinburgh, Exeter, Glasgow, Liverpool, Reading and Southampton &ndash; as well as Leeds. A further dozen locations will be launched over the next year.</p>

<p>Ponniah described the move as a fresh offering for commercial contract cleaning in the regions. He said: &ldquo;Cleanology is large enough to cope, but small enough to care. We are known for our award-winning sustainable projects &ndash; some of which have helped us to save 28,000 plastic bottles every year &ndash; and, in 2021, we won a Living Wage Champion Award from the Living Wage Foundation. We are looking forward to bringing innovation and green cleaning to a wider audience.&rdquo;</p>

<p>Clients have also welcomed the move. Katy Tennant, Director of UK Operations at Clockwise, said: &ldquo;We use Cleanology at a number of Clockwise locations throughout the country. Their &lsquo;clean green&rsquo; approach, using chemical free cleaning; their attention to detail, and their ethics are just a few of the many reasons why we chose them. The Cleanology team have become an extension of our own building teams and our members rave about their service.&rdquo;</p>

<p>The national launch ties in with Cleanology&rsquo;s 20th&nbsp;anniversary celebrations. Festivities were delayed in 2020 due to Covid, but in April this year, a number of Cleanology clients and staff attended a glittering black-tie dinner at Tower Bridge to celebrate.</p>",238,"2022-09-25 08:33:44","2022-10-11 00:05:04",0,https://cleanology.com/,cleanology-press-release,"2022-10-11 00:00:00"
228,"Number Crunchers - Press Release","MAKING TAX DIGITAL HOW DOES IT AFFECT SOLE TRADERS IN THE YORKSHIRE REGION?","<p>If you are a Limited Company or VAT registered, you will already be familiar with Making Tax Digital. However, if you are a self-employed Sole Trader with an annual income of over &pound;10,000 you will soon need to start preparing&nbsp;for Making Tax Digital, which will come into place in April 2024.</p>

<p>From 6th&nbsp;April 2024, all self-employed businesses and landlords with an annual business or property income above &pound;10,000 will need to follow the rules for Making Tax Digital for Income Tax. HMRC will require that a return is submitted quarterly, and that businesses will need to use Making Tax Digital compatible software to keep digital records.</p>

<p><strong>Here Isabella Callaghan, Managing Director of Number Crunchers (Sheffield) Ltd discusses how Sole Traders in the Yorkshire region can prepare for Making Tax Digital:&nbsp;</strong>&ldquo;I have been working with Sole Traders and Companies in the Yorkshire region for over 26 years, so I have a strong respect for their skills and services, as well as understanding their everyday stresses when it comes to bookkeeping.&nbsp;</p>

<p>&nbsp;</p>

<p>As an Accountant, I and my fellow accountants have now been tasked to bring these Sole Traders into the new tax system called MTD, which means Making Tax Digital, which comes into effect in April 2024.</p>

<p>&nbsp;&nbsp;</p>

<p><strong>At present a lot of Sole Traders think:</strong></p>

<ul>
	<li>That doesn&rsquo;t apply to me &ndash; some Sole Traders are ignoring the emails, software advertisements, as they genuinely believe it&rsquo;s not going to affect them.</li>
	<li>More costs &ndash; some Sole Traders are concerned that their accountancy bills or MTD compatible software packages are going to quadruple.</li>
	<li>2024 is ages away &ndash; some Sole Traders want to wait until April 2024 before thinking about Making Tax Digital.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Now let me dispel a few of those concerns:</strong></p>

<ul>
	<li>If you are a Sole Trader with an income above &pound;10k &ndash; it means you!&nbsp; HMRC will require that you submit a return every quarter, as the name suggests (Making Tax Digital) this means digitally and to do that you will need MTD compatible software to keep the digital records.</li>
	<li>It doesn&rsquo;t need to be daunting if you start preparing now. We have already transitioned many clients to MTD compatible software, and the majority now prefer the move to digital bookkeeping. I am not going to pretend that it wasn&rsquo;t different for them at first, but we have had the time to train them at no additional cost and they are now up and running and just can&rsquo;t believe how much time they are saving.</li>
	<li>There are various MTD compatible software packages available that you can use, such as Sage, Xero, FreeAgent and QuickBooks to name a few. Please undertake your research, as some are more affordable than others. Your accountant can also advise you on the most cost-effective option to suit your business needs.</li>
	<li>April 2024 seems a long way away, but it is only 18 months away and if you move now to digital bookkeeping, you have time to sort any niggles and training out. This will most definitely not be the case in 18 months&rsquo; time.</li>
</ul>

<p>&nbsp;</p>

<p>In conclusion, don&rsquo;t wait until 2024 - talk to your accountant today! Digital Software &ndash; will improve the way you do business by saving you time and making you look professional to your customers.&rdquo;</p>

<p>Number Crunchers (Sheffield) Ltd specialises in providing accountancy services to small to medium sized businesses (SME&rsquo;s) in the Yorkshire region. Founded by Isabella Callaghan in 1996 and with Chris Pheasey BA (Hons) MAAT ACMA CGMA joining in 2006, Number Crunchers (Sheffield) Ltd has been successfully supporting SME&rsquo;s including sole traders, start-ups and Limited companies for over 26 years. The company offers a range of services including accounting, cloud accounting using Sage, Xero, FreeAgent, QuickBooks etc., bookkeeping, self assessment, payroll and business advice.</p>

<p>For more information on Number Crunchers (Sheffield) Ltd visit:&nbsp;<a href="https://www.number-crunchers.co.uk/">https://www.number-crunchers.co.uk/</a></p>

<p>Follow us on Twitter:&nbsp;@NumberCrunche /&nbsp;Instagram: @numbercruncherssheff</p>

<p><strong>Number Crunchers (Sheffield) Ltd</strong></p>

<p><strong>2 Rotherside Court, Eckington Business Park, Rotherside Road, Eckington, Sheffield, S21 4HL.</strong></p>",239,"2022-09-25 08:38:20","2022-10-12 00:05:04",0,https://www.number-crunchers.co.uk/,number-crunchers-press-release,"2022-10-12 00:00:00"
229,"Grays Court Hotel - Press Release","Local Independent Hotelier Is Shortlisted for Prestigious Hotelier Award.","<p>Helen Heraty, owner of Grays Court Hotel and The Bow Room Restaurant has been nominated for the prestigious Independent Hotelier Award 2022. Helen was shortlisted after being nominated by Grays Court Co-Director Sarah Czarnecki. The nomination is brilliant news for Yorkshire&rsquo;s hospitality sector and sees Helen up against five of the country&rsquo;s top hoteliers who are responsible for hotels including the five-star Stafford in London and Michelin Award winning Atlantic Hotel in Jersey.</p>

<p>The Independent Hotelier Award is presented to an individual,&nbsp;couple or business partners&nbsp;who have&nbsp;made a significant contribution to the independent hotel sector. An independently spirited pioneer, recognised for excellence, with a reputation for pushing boundaries and creating exceptional guest experiences. Above all, the winner is someone committed to the development of their team, the success of their establishment and the industry at large.</p>

<p>The only person to be nominated in the North of England, Helen Heraty can be said to be the &lsquo;embodiment&rsquo; of independent spirit.&nbsp; After losing her partner, with whom she purchased Grays Court after 2 years of opening the hotel, she battled on as sole-proprietor and single parent of seven children in order to make the business a success. With no group or individual investor providing financial backing, Helen&rsquo;s tenacity and perseverance provide the perfect example of how entrepreneurial drive and determination to succeed can be achieved.&nbsp; A true visionary, Helen has constantly strived to make business improvements. &nbsp;Being voted the <em>&lsquo;Visit England Best Small Hotel&rsquo;</em> and achieving Michelin Guide status and 3 AA Rosettes for the restaurant all underline these efforts.&nbsp; Independent and tenacious, a core strength of Helen&rsquo;s is possession of the insight and leadership skills to ensure she has the right team in place to enable her to achieve her vision of making Grays Court one of the best hotels in England.</p>

<p>Sarah Czarnecki, Co-Director at Grays Court said &ldquo;<em>The team at Grays Court &amp; The Bow Room Restaurant are delighted that Helen has made the shortlist for the Independent Hotelier Awards.&nbsp; It is testament to her hard work, vision and resilience that the hotel and restaurant keep going from strength to strength.&nbsp; We are privileged to be able to work with, and support her, to secure the future success of the business going forward&rdquo;.</em></p>

<p>Local residents can support Yorkshire&rsquo;s bid for the award by voting for Helen and Grays Court using the following link - &nbsp;<a href="https://www.independenthotelshow.co.uk/cast-your-vote">Cast your vote - Independent Hotel Show London 2022</a>. You can find out more about Grays Court &amp; The Bow Room Restaurant at <a href="http://www.grayscourtyork.com">www.grayscourtyork.com</a></p>",240,"2022-09-25 08:44:18","2022-10-13 00:05:04",0,https://www.grayscourtyork.com/,grays-court-hotel-press-release,"2022-10-13 00:00:00"
230,"The Banks Group - Press Release","RICHARD TAKES ON MANAGER ROLE AT BANKS RENEWABLES’ YORKSHIRE WIND FARMS","<p>An experienced engineer has taken on a new Yorkshire role with the renewable energy arm of property, renewable energy and infrastructure business The Banks Group.</p>

<p><img alt="" src="https://ybj.yorkshirebusinessjournal.co.uk//img/Richard%20Warrior%20pic.jpeg" style="height:267px; width:200px" /><br />
<br />
Richard Warrior is now responsible for ensuring the safe and efficient operation of two of Banks Renewables&rsquo; four onshore wind farms in Yorkshire - the Hook Moor wind farm to the east of Leeds and the Hazlehead wind farm near Barnsley.<br />
<br />
Richard previously spent more than three decades working for The Banks Group&rsquo;s minerals and mining operations, working his way up through the firm to become a technical engineering and process manager within its plant division.<br />
<br />
But he has now moved his focus from the ground to the air to become a wind farm manager with Banks Renewables, which is one of the leading owner/operators in the UK&rsquo;s onshore wind sector.<br />
<br />
Almost 89,000 MWh of electricity was generated in total by Banks&rsquo; four Yorkshire wind farm during the company&rsquo;s last financial year, which is enough to meet the annual electricity needs of more than 29,000 homes or of a town the size of Pontefract.<br />
<br />
By doing so, they also displaced around 18,900 tonnes of carbon dioxide from the electricity supply network.<br />
<br />
Banks Renewables&rsquo; other two Yorkshire wind farms are the Penny Hill wind farm near Sheffield and the Hazlehead scheme near Barnsley, while the family-owned firm also has a number of other renewable energy projects within the county at different stages of development.<br />
<br />
Richard Warrior says: &ldquo;This is a totally different role to what I&rsquo;ve done previously, but I&rsquo;m really enjoying the challenge of learning about a new industry and getting to grips with all the different parts of my job.<br />
<br />
&ldquo;Many of the problem-solving skills that I&rsquo;ve developed over the years are just as applicable in my new role as they were previously, and while there&rsquo;s been a lot to learn to get fully up to speed with my new role, I&rsquo;m being well supported by my new colleagues and getting lots of training that&rsquo;s helping me build on my engineering knowledge every day.<br />
<br />
&ldquo;I&rsquo;ve enjoyed working for Banks for more than three decades and am really pleased to have the chance to start this new chapter of my career with the company.&rdquo;<br />
<br />
Dan Thomas, operations and grid director at Banks Renewables, adds: &ldquo;Richard brings an impressive range of skills to his new role, as well as real enthusiasm for the job and a willingness to learn about a new sector and we&rsquo;re really pleased to have him as part of our team.<br />
<br />
&ldquo;Using the widest possible range of efficient renewable energy generation technologies will allow the UK to decarbonise its power supply and achieve its Net Zero targets more quickly by increasing the amount of clean green electricity available to power our homes, schools and workplaces.&rdquo;</p>",241,"2022-09-25 08:49:27","2022-10-18 00:05:04",0,https://www.banksgroup.co.uk/,the-banks-group-press-release,"2022-10-18 00:00:00"
231,"Fenton Packaging Solutions - Press Releease","Sustainable alternative for bulk storage and dispensing","<p>The UN&nbsp;bag-in-box&nbsp;(BiB)&nbsp;containers from Leeds-based Fenton Packaging Solutions offer&nbsp;a&nbsp;sustainable flatpack alternative to traditional rigid bulk liquid packaging. BiB provides all the benefits of a rigid container coupled with flexible sustainability in respect of transportation and storage requirements.</p>

<p>&nbsp;</p>

<p>&ldquo;Swapping to bag-in-box delivers major benefits, reduces costs, and improves a carbon footprint of a business,&rdquo; says&nbsp;Chris&nbsp;Warren, Sales Development Director of Fenton Packaging Solutions, which is based at the Kinetic 45 development in Newmarket Lane. &ldquo;Prior to filling, BiB saves up to 40% in warehouse capacity and takes up 90% less transport space, when compared to rigid alternatives. In addition, BiB creates 60% less non-recyclable waste.</p>

<p>&nbsp;</p>

<p>&ldquo;The plastic reduction is dramatic, as a typical 20 litre UN jerrycan weighs in at around 1,100 grams whereas a 20 litre UN bag with closure weighs in at just 110 grams &ndash; potentially offsetting some of the costs associated with the recently introduced&nbsp;2022&nbsp;Plastic Packaging Tax.&nbsp;The advantages don&rsquo;t stop there. BiB is lighter in weight and easy to handle, plus there&rsquo;s less risk of spillage when pouring and when using a tap, it is drip-free.&rdquo;</p>

<p>&nbsp;</p>

<p>&ldquo;The pallet quantity of bags changes the requirements for a business&rsquo;s incoming logistics, moving and handling, warehousing, filling, and packing for onward distribution. Using BiB significantly&nbsp;reduces&nbsp;the time and cost associated with the handling of rigid plastic packs. BiB is available in 5 litre, 10 litre and 20 litre sizes, but as a comparison, one pallet of 10 litre UN bags contains 3,000 bags which is very different to the pallet quantity for 10 litre UN jerrycans.</p>

<p>&nbsp;</p>

<p>&ldquo;It is the cost of acquisition that is interesting to companies adopting BiB products combined with the benefits available to their end customers when the pack enters its end-of-life phase.&rdquo;</p>

<p>&nbsp;</p>

<p>The sustainability advantages of BiB over rigid containers are clear. BiB packs consist of a cardboard box which is easy to recycle once the product has been emptied and a plastic co-polymer cube-shaped bag with gland and closure which weighs in at about 10% of the weight of the equivalent capacity jerrycan.</p>

<p>&nbsp;</p>

<p>Cardboard goes into one waste stream and the bag will go into another recycling stream where it exists for the recycling of co-polymer containers. If the bag cannot be recycled it can be processed on the waste to energy ticket.</p>

<p>&nbsp;</p>

<p><strong>Exclusive in the UK</strong></p>

<p>&ldquo;Fenton Packaging Solutions is our chosen and exclusive partner in the UK market, to represent our flexible packaging and our special UN bag-in-box concept,&rdquo; says CDF Corporation/Quadpak AB Sales and Marketing Director Helena Bysell. &ldquo;Fenton Packaging Solutions has the knowledge and experience of bag-in-box packaging also for challenging products such as chemicals and other non-food applications, and with a nationwide sales team, the company provides excellent service, support and reliability to customers across the UK.&rdquo;</p>

<p>&nbsp;</p>

<p><strong>BiB &ndash; from beverages to bulk</strong></p>

<p>&ldquo;Most people are familiar with the BiB packaging format, as it has been used in the food industry for many years, for milk, wine, beer and cider,&rdquo; says Fenton Packaging Solutions Purchasing and Operations Director David Wilson. &ldquo;BiB is also used in the foodservice sector to supply bulk quantities of product on a B2B basis. But industrial companies in the detergents, oils &amp; lubricants and more recently the coatings &amp; inks sectors are now considering alternative packs to the traditional rigid plastic jerrycan, with UN BiB being viewed as an alternative to rigid plastics.</p>

<p>&nbsp;</p>

<p>&ldquo;As a consultative business, we are keen to direct our customers to more sustainable packaging solutions and we are recommending BiB as an alternative to existing, traditional rigid packaging.</p>

<p>&nbsp;</p>

<p>&ldquo;The high cost and difficulty of disposal of rigid plastic containers at our&nbsp;end-customers is driving interest, as swapping to BiB supports both the OEM and end-customers&rsquo; sustainability goals. Early adopters already include oils and lubricants companies, ad blue producers, professional detergent companies, and more recently manufacturers of coatings.</p>

<p>&nbsp;</p>

<p>&ldquo;UK-based filling machine producers exist, and production equipment is available to support both medium to large volume filling of BiB containers. We are receiving an increasing number of enquiries about BiB for industrial applications, where a UN container is required and as such are talking to companies in a variety of industry sectors.&rdquo;</p>

<p>&nbsp;</p>

<p><a href="http://www.fentonpackaging.co.uk/">http://www.fentonpackaging.co.uk/</a></p>",242,"2022-10-08 07:41:17","2022-10-19 00:05:04",0,www.fentonpackaging.co.uk,fenton-packaging-solutions-press-releease,"2022-10-19 00:00:00"
232,"The Brighouse Deal - Press Release","Key moment for Brighouse Deal as £19.1million plans submitted","<p>After an extensive consultation over the past three years, the Brighouse Deal plans that will write a new chapter in the story of the town have been submitted to the Government.</p>

<p>&nbsp;</p>

<p>The business cases for the &pound;19.1m proposals have been approved by Calderdale Council and the Brighouse Town Deal Board,&nbsp;made up of representatives from the town&rsquo;s private and voluntary sector, community and residents&rsquo; groups.</p>

<p>&nbsp;</p>

<p>It will now be for the Department for Levelling Up, Housing and Communities to review the plans and, once this review is complete, release the funding allowing work to begin on making the Deal developments a reality.</p>

<p>&nbsp;</p>

<p>The approved projects put forward are:</p>

<p>&nbsp;</p>

<ul>
	<li><strong>Brighouse Welcome &ndash;&nbsp;</strong>&pound;400,000 to develop the public realm on the edge of the town centre and enhance the welcome to everyone - pedestrians, cyclists, public transport users or car users who&rsquo;ve parked just outside the town centre and then walked in. It will encourage active lifestyles, contribute to Calderdale&#39;s climate commitments, and create a place that enhances Brighouse&#39;s profile as a destination. It aims to increase the number of commuters walking and cycling.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Canalside &amp; Thornton Square -</strong>&nbsp;&pound;6 million to improve outdoor recreation opportunities and the image of Brighouse to existing and potential residents and businesses via enhanced connectivity to the Canalside, and to harness the historic value of Thornton Square to provide a quality event and community space which enhances Brighouse&#39;s image as a quality destination for residents, businesses and visitors. It aims to increase the number of visitors to the Canalside and the number who visit cultural and heritage events.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Brighouse Retail &amp; Leisure -&nbsp;</strong>&pound;9 million to enhance the retail and leisure offer of Brighouse&#39;s town centre by prioritising people, enhancing the streetscape and providing opportunities to sit and meet friends to increase footfall, dwell times and support the 24-hour economy. The ambition is to increase by 30 per cent the time spent in Brighouse and the amount spent in shops, cafes, and restaurants.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Market Revitalisation -&nbsp;</strong>&pound;3 million to deliver a new market building on the Canalside with 40 fixed and pop-up market stalls with supporting infrastructure, including new toilets and spaces to sit, meet and rest. It is hoped this will help start-up businesses by providing a route for them to use pop-up stalls occasionally, through to fixed market units, through to a permanent commercial space in the town centre.</li>
</ul>

<p>&nbsp;</p>

<ul>
	<li><strong>Industry 4.0 &amp; Skills</strong>&nbsp;- &pound;650,000 to put high-tech manufacturing at the heart of the town&rsquo;s future by creating an Industry 4.0 Hub where small and medium sized businesses can explore how digital technology can improve their productivity and increase innovation. It will provide the capacity for 70 new apprentices by working with partners led by Calderdale College.</li>
</ul>

<p>&nbsp;</p>

<p>If projects are given the go-ahead, contractors will be invited to tender for the work and planning applications prepared during late 2022 and early 2023, with an ambition for projects to get underway and construction to begin later in 2023, to be completed by mid-2025.</p>

<p>&nbsp;</p>

<p>Cllr Sophie Whittaker, co-chair of the Brighouse Town Deal Board, said:</p>

<p>&nbsp;</p>

<p>&ldquo;This is an important moment for the future of Brighouse. After extensive discussions over the projects that would help unlock the town&rsquo;s potential, the business cases have now been approved and submitted to Government.</p>

<p>&nbsp;</p>

<p>&ldquo;As a Board made up of people from across different sectors and groups in Brighouse, we are excited by the plans &ndash; to make our town centre more attractive for shoppers and visitors, boost the economy by encouraging growth and investment, and create a safer, cleaner and greener environment that builds on our heritage and is fit for the future.</p>

<p>&nbsp;</p>

<p>&ldquo;We look forward to receiving the approval for the developments and making sure the Deal delivers for everyone in our town.&rdquo;</p>

<p>&nbsp;</p>

<p>Cllr Sarah Courtney, Calderdale Council&rsquo;s Cabinet Member for Towns, Engagement and Public Health, said:</p>

<p>&nbsp;</p>

<p>&ldquo;This major milestone means so much for Brighouse&rsquo;s future resilience and success, and to everyone in the Town Deal partnership and community who has worked extremely hard to bring forward inspiring proposals to boost the town.</p>

<p>&nbsp;</p>

<p>&ldquo;There is already much to be proud of in Brighouse. The proposals build on this to help create an even more thriving economic and social hub where residents and visitors want to spend more time and more businesses want to invest. This supports the Council&rsquo;s priority to build strong towns where people can connect, contribute to the local economy, be more active, feel safe, start well and age well.&rdquo;&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>More details about the agreed plans are available on The Brighouse Deal website at&nbsp;<a href="http://www.brighousedeal.co.uk/">www.brighousedeal.co.uk</a>.</p>",243,"2022-10-08 07:43:07","2022-10-20 00:05:04",0,www.brighousedeal.co.uk,the-brighouse-deal-press-release,"2022-10-20 00:00:00"
233,"Artisan Real Estate - Press Release","Artisan Real Estate Unveils UK-Wide Operational Restructure","<p><strong>New Leeds-based Office Launched for the North of England&nbsp;</strong></p>

<p><strong>UK Property Director Clive Wilding Set for Retirement&nbsp;</strong></p>

<p><img alt="" src="https://ybj.yorkshirebusinessjournal.co.uk//img/Team%20Artisan%20-%20North%20of%20England.jpg" style="height:200px; width:368px" /></p>

<p>Artisan Real Estate is restructuring its UK-wide operation following the announcement that UK Property Director, Clive Wilding, is stepping away from the business during the next few months into a long-awaited retirement.&nbsp;Wilding&rsquo;s departure from his operational role heralds an ambitious expansion of the company&rsquo;s UK-wide business network, in accordance with the company&rsquo;s succession planning, including the launching of a North of England operation headquartered in Leeds.&nbsp;</p>

<p>&nbsp;</p>

<p>Artisan&rsquo;s North of England business will now be headed up by James Bulmer who has been promoted to Regional Development Director and is supported by a newly recruited seven-strong in-house team combining full development function with a direct build capability &ndash; spanning project management, commercial, technical and sales.&nbsp;&nbsp;This includes the appointment of Stacey Finneran as Regional Sales Manager.&nbsp;</p>

<p>&nbsp;</p>

<p>Artisan&rsquo;s initial focus will be predominately large-scale residential brownfield development as the company looks to focus predominately on sustainable and low carbon projects in the Leeds area and further afield across Yorkshire.&nbsp;&nbsp;Artisan North launched its first major project in late 2021 - the &pound;65 million Kirkstall Place scheme for 263-new sustainable homes now currently under construction. The five-acre site, formerly home to Kirkstall District Centre, is due to be completed in 2024 and has been designed to set new UK benchmarks in sustainable and low carbon volume residential development.&nbsp;&nbsp;The developer has also recently announced the acquisition of an 11-acre site adjacent to the Salts Mill World Heritage Site in Saltaire with proposals currently being prepared to create a new residential led mixed-use community. Pre-application consultation has now been completed, with an on-site date target set for 2023.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>James Bulmer, Development Director for Artisan North said: &ldquo;We are now in a superb position to capitalise on the growth and investment in Leeds and the wider Yorkshire areas. Our Kirkstall Place development will provide an ideal springboard to take forward the Artisan model of sustainable low carbon development on brownfield sites in sensitive environmental areas. &nbsp;Our next development in Saltaire will provide the perfect example of the kind of innovative and ambitious transformational development that has defined the Artisan approach across the rest of the UK, and we look forward to moving this forward along with other projects currently in the pipeline.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>Mr Wilding, who for the last 13 years has been the driving force behind Artisan&rsquo;s evolution into one of the UK&rsquo;s most progressive and respected commercial and residential developers, is retiring after 46 years in the property industry. Having stepped down from the Board in March 2022, he will continue to support Artisan in an advisory capacity for the remainder of the year.&nbsp;</p>

<p>&nbsp;</p>

<p>Jaco Jansen, Artisan&rsquo;s Chief Executive Officer, added: &ldquo;Artisan has enjoyed spectacular UK regional growth since the company was established in 2009 as&nbsp;an independent, entrepreneurial, values-led property developer and investor. Since day one, Clive has been instrumental in driving forward this success, identifying and delivering pioneering complex mixed-use developments which have transformed cities across the UK - and made a fundamental difference to how people live their lives.&nbsp;We are now building on this remarkable legacy and significantly expanding our regional presence across the north of England, strengthening and investing more in a local, experienced team. This is a direct response to the very positive market opportunities that this important area now provides - especially in large-scale sustainable residential and mixed-use development in sensitive city centre environments.&rdquo;&nbsp;</p>

<p>&nbsp;</p>

<p>Clive Wilding&rsquo;s tenure at Artisan incorporates some of the most significant and award-winning city centre developments delivered anywhere in the UK over the last decade. This included Edinburgh&rsquo;s New Waverley, a &pound;250 million transformation of the heart of the city&rsquo;s historic Old Town into a thriving mixed-use community, the regeneration of Glasgow&rsquo;s famous Custom House building into a riverside hotel complex and the conversion of the Everards Print Works in Bristol to a vibrant hotel, residential and commercial quarter.&nbsp;&nbsp;Wilding also helped drive Artisan&rsquo;s sustainable low carbon residential development agenda, incorporating strong guiding principles which set new best-practice benchmarks for sustainable housing developments in the UK &ndash; now being delivered in Kirkstall Place and in the centre of Edinburgh.&nbsp;&nbsp;</p>

<p>Speaking of his time at Artisan, Mr Wilding said: &ldquo;I have been immensely fortunate to have been involved in some truly memorable and development projects during my time at Artisan. I am especially pleased to have shone the development spotlight firmly on high-quality city centre regional development. It is fitting that our regional offices now have the right platform to build on that legacy and continue to deliver transformational development to the benefit of our people, our cities and our wider environment.&rdquo;&nbsp;&nbsp;</p>

<p>For more information on Artisan&rsquo;s developments, visit&nbsp;<a href="http://www.artisanrealestate.co.uk/">www.artisanrealestate.co.uk</a></p>",244,"2022-10-08 07:47:11","2022-10-08 07:48:31",1,https://artisanrealestate.co.uk/,artisan-real-estate-press-release,"2022-10-25 00:00:00"
234,"ALB Group - Press Release","Nottingham developer set to boost fortunes of Sheffield city centre","<p>A landmark building in the heart of Sheffield city centre is set for a makeover after being purchased by Nottingham property company ALB Group.&nbsp;</p>

<p>&nbsp;</p>

<p>The 48,000 sq ft building, spanning numbers 2-18 Fargate, Sheffield, has been acquired for an undisclosed sum by ALB, as part of the developer&rsquo;s mission to breathe new life into struggling town and city centres across the UK.</p>

<p>&nbsp;</p>

<p>Already housing retail businesses such as Starbucks, Greggs and Hotel Chocolat on the ground floor, plans are underway to transform the four upper floors, totalling almost 30,000 sq ft of vacant office space, into apartments.</p>

<p>&nbsp;</p>

<p>The move follows a similar refurbishment model employed by ALB Group in other UK centres, including Stoke-on-Trent, Ipswich, Birkenhead and Derby, which are already experiencing a turnaround in fortunes.</p>

<p>&nbsp;</p>

<p>The initiative has been a major success in Nottingham&rsquo;s Bridlesmith Gate area, where once vacant stores have been filling up with niche businesses, helping to achieve ALB&rsquo;s aim of creating a vibe similar to London&rsquo;s Carnaby Street.&nbsp;</p>

<p>&nbsp;</p>

<p>Group managing director Arran Bailey has long been committed to finding ways to reverse the trend of decay in UK town centres, particularly by encouraging local, independent entrepreneurs to launch new high street businesses, by offering lower rents with more flexible terms.</p>

<p>&nbsp;</p>

<p>ALB is seeking to do the same with its vacant retail units in the Fargate building.</p>

<p>&nbsp;</p>

<p>Announcing his latest property deal, Arran said: &ldquo;We are delighted to have secured yet another prime city centre building, right in the middle of Sheffield&rsquo;s shopping district.</p>

<p>&nbsp;</p>

<p>&ldquo;All UK high streets have experienced troubling times for a number of years, and Sheffield is no exception. But with the right commitment from landlords like ourselves and by offering more forward-thinking terms, we can turn their fortunes around.</p>

<p>&nbsp;</p>

<p>&ldquo;Part of the secret is to ensure the huge amount of vacant upper floor space in buildings like 2-18 Fargate are brought back into use.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;Given the huge housing shortage, plus the frequently reported difficulties faced by first-time buyers, the most obvious answer is to turn this unused office space into much-needed, quality apartments or student accommodation. This not only provides homes in central locations, it adds footfall for the ground floor retailers &ndash; a genuine win-win situation.&rdquo;</p>

<p>&nbsp;</p>

<p>Rob Cassidy, Arran&#39;s business partner in this venture, said:&nbsp; &ldquo;We are absolutely over the moon to have purchased this prime building in the heart of Sheffield and we are looking forward to getting started on our plans.&rdquo;</p>

<p>&nbsp;</p>

<p>Some of ALB&rsquo;s most recent city centre acquisitions that are undergoing a similar transformation include an 80,000 sq ft building in New Street, Huddersfield, Moxon Island shopping centre in Hanley, Stoke-on-Trent, Sailmakers Shopping Centre in Ipswich, and seven key buildings in Derby city centre&rsquo;s Albion Street.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Phil Daniels, director at FHP Property Consultants, who acted as acquisition adviser for ALB on the Sheffield transaction, said: &ldquo;We&rsquo;ve worked alongside ALB on several of these deals, and I must say Arran&rsquo;s approach to our UK-wide problem of underperforming high street retail assets is always refreshing.</p>

<p>&nbsp;</p>

<p>&ldquo;He&rsquo;s putting his money where his mouth is &ndash; and unlike many other high street initiatives, which may have proven ineffective or half-hearted, ALB is providing real solutions that are already working.&nbsp;</p>

<p>&nbsp;</p>

<p>&ldquo;By offering shorter, more flexible leases and lower rents, ALB is offering a lifeline to UK high streets and beginning to attract a new generation of traders, from coffee shops and fashion retailers to leisure activities.&rdquo;</p>",245,"2022-10-08 07:51:08","2022-10-08 07:51:19",1,https://albgroup.co.uk/,alb-group-press-release,"2022-10-26 00:00:00"
235,"BioScapes - Press Release","Leading Yorkshire-based biodiversity expert BioScapes® achieves ‘Made in Britain’ accreditation","<p>The UK&rsquo;s leading producer of biodiversity habitats and associated products, BioScapes&reg;, based in Seaton Ross near York, has successfully achieved &lsquo;Made in Britain&rsquo; certification for its innovative range of wildlife planters, designed to tackle the huge ecological threat of rapidly decreasing biodiversity.</p>

<p>BioScapes&nbsp;collaborated with expert ecologists over the course of several years to&nbsp;launch the range of innovative, &lsquo;self-contained ecosystems&rsquo; that are the first of their kind, and suitable for application in commercial, educational and residential settings.</p>

<p>The &lsquo;Made in Britain&rsquo; accolade is a part of a scheme dedicated to support and promote British manufacturing and to help consumers, buyers, and specifiers identify products that are manufactured in Britain, to the very highest standards. The mark is widely recognised as the ultimate authority of British originality and perceived as a global seal of superiority, domestically and worldwide.</p>

<p>Neil Spiers, Head of Commercial at BioScapes said: &ldquo;We are very proud that our unique range of wildlife planters has received the &lsquo;Made in Britain&rsquo; accolade that reinforces our credentials as an organisation dedicated to providing the highest quality products, while at the same time, ensuring the materials are sourced from within Britain.</p>

<p>&ldquo;Our ambitions very much align with Made in Britain&rsquo;s aspirations to reach net-zero by 2050. As the UK&rsquo;s first-ever manufacturer of self-contained multiple biodiversity planters, we have established our objectives to help contribute towards biodiversity restoration.&rdquo;</p>

<p>The WildPod&reg;, NatureArk&reg;, and BioCube&reg; wildlife planters each feature multiple, integrated habitats designed to help plants, insects, invertebrates, amphibians and small mammals thrive.</p>

<p>WildPod is suited to domestic settings and gardens, and the larger NatureArk works well in schools to help teach future generations about the importance of preserving biodiversity. The largest planter - BioCube - is designed for use in larger spaces, such as by developers of commercial or public settings, including retail outlets and housing developments, to help meet zero net gain legislation.</p>

<p>BioScapes is an environmentally conscious business that sources all its product materials from within Britain and the products are entirely manufactured at its main construction hub in Seaton Ross.&nbsp;The &lsquo;Made in Britain&rsquo; mark of quality will now be proudly displayed on the full range.</p>

<p>Achieving the certification aligns with BioScapes&rsquo; ambitions to achieve its sustainability goals. The &lsquo;Made in Britain&rsquo; scheme also offers its members a one-of-a-kind Green Growth survey, which sets a benchmark for businesses to understand their progress towards sustainability.</p>

<p>The Green Growth programme has been exclusively created in partnership with the Cambridge Judge Business School and aims to help assess businesses to measure their sustainability performance and identify key areas to improve.</p>

<p>Neil continued, &ldquo;We are also joining the exciting Green Growth Programme that will underline our commitments to achieving our sustainability goals and enable us to identify ecological enhancements we can continue to make to our business.&rdquo;</p>",246,"2022-10-08 07:52:45","2022-10-08 07:53:02",1,https://bioscapes.co.uk/,bioscapes-press-release,"2022-10-27 00:00:00"
236,"Lemon Locksmith","Family run Leeds based locksmith","<div class="youtube-embed-wrapper" style="position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden">&nbsp;</div>

<p>&nbsp;</p>

<p>Hi my name is Andrew and I own Lemon Locksmith.</p>

<p>It&#39;s just me in the business however I&#39;m hoping to grow and expand. I started this business after becoming interested in lock picking as a hobby when younger. Now I offer a range of lock, door and window related services. I can upgrade your existing locks to higher security locks (anti-snap).</p>

<p>I can also repair doors which are catching, broken, jammed shut etc.</p>

<p>I am a fully member of the MLA (Master Locksmith Association) which took me 2 years to obtain. This is a body which regulates locksmiths in the UK and checks the quality of their work. Should you need any lock / door related services feel free to get in touch!</p>

<p><a href="https://lemonlocksmith.co.uk/">Lemon Locksmith</a></p>",247,"2022-10-08 08:04:15","2022-10-10 19:13:31",1,https://lemonlocksmith.co.uk/,lemon-locksmith,"2022-11-14 00:00:00"
237,"Accountancy services with a difference.","My Management Accountant","<div class="youtube-embed-wrapper" style="position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden">&nbsp;</div>

<p>&nbsp;</p>",248,"2022-10-08 09:08:16","2022-10-10 19:20:35",1,https://mymanagementaccountant.co.uk/,accountancy-services-with-a-difference,"2022-11-21 00:00:00"
238,"CitrusConnect Recruitment","CitrusConnect Recruitment","<p>- Please introduce yourself and the company.</p>

<p>My name is Leena Parmar, and in 2008 I saw a gap in the recruitment marketplace in 2009 I founded&nbsp;<a href="https://www.citrus-connect.co.uk/">Citrus Connect Recruitment</a>&nbsp;with my launch client, The Automobile Association. With my help and expertise, we grew their direct sales force from an 80-person headcount to 250 within 18 months.&nbsp;I perfected a recruitment process so simple, easy and efficient to specifically recruit &#39;self-employed&#39; direct sales personnel. &nbsp;We now work with clients in all industries and are sought after for our track and evaluate processes which give you real-time insights of the recruitment journey specific to your business.</p>

<p>I started Citrus Connect Recruitment to share the power of direct sales and how it can completely transform lives, today we recruit for all types of sales roles from canvassers, telesales to direct sales personnel. And now recruiting for sales managers and directors across all sectors.</p>

<p>&nbsp;</p>

<p>- What has been your biggest milestone so far?</p>

<p>My biggest milestone so far has been winning Best Recruitment Sales Agency in the North by the Northern enterprise Awards 2021, and being featured in the Daily Mail and Yorkshire Post as I was nominated for Business Woman of the Year 2020.&nbsp;</p>

<p><br />
- What do you wish to accomplish within the next 12-18 months?</p>

<p>My goals for Citrus Connect is to place over 500 people in a new sales role.</p>

<p>With the candidate lead market as it is I would love to further support businesses to create a robust recruitment plan using a framework that I have created with my 12 years of experience.</p>

<p>&nbsp;</p>

<p>- How do you help support those in need of finding a job?</p>

<p>I have produced a free CV guide to everyone which includes the 7 secrets to your resume success. This can be downloaded directly from our website.</p>

<p>We also understand what our clients need, and can advise them where they will be a suitable match. The most important part of talking to a candidate is the very first conversation we have with them. Here we learn about who they are, what their goals are, and only then can we match them to a role that they will strive in.&nbsp;</p>

<p>Finding a job has almost become synonymous with finding a partner - it&#39;s about the match, the reason, the bond and the ultimate success of both!</p>

<p>&nbsp;</p>

<p>- How did you support businesses during the Pandemic?</p>

<p>We supported businesses to pivot, we didn&#39;t panic, and we worked hard at finding solutions to help businesses stay afloat and evolve with the changes. As a result, 2 key clients went completely remote during this time.&nbsp;</p>

<p>We supported businesses&#39; cash flow and created&nbsp;candidate profiling services, which help businesses identify who the ideal candidate needs to be for 1. Their company and 2. The role.&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;- How does it feel to be featured in the Daily Mail and Yorkshire Post?</p>

<p>It&rsquo;s less about me but more about how increasing the profile of Citrus Connect and the value we add to both clients and candidates.&nbsp;I was thrilled for being recognised for my resilience and passion to make a difference in what is known as such a &#39;cutthroat&#39; industry. The most common question is always...as a Woman in Business, how does it feel to be nominated? My answer is simple, my gender is irrelevant, what is relevant is the value I add to my clients business every single day! It really is that simple for me! &nbsp;</p>",249,"2022-10-10 19:18:20","2022-10-17 00:05:04",0,www.citrus-connect.co.uk/,citrusconnect-recruitment,"2022-10-17 00:00:00"
239,"CO2Sustain  - Press Release","Global fizzy drinks brand calls Leeds’ CO2Sustain ‘the best thing at drinktec’","<p>Leeds-based CO2Sustain may be a small business, but it punches well above its weight when it comes to attracting the attention of some of the biggest global brands. This was clearly demonstrated this month when they exhibited at drinktec, the world&rsquo;s foremost trade show dedicated to drinks.</p>

<p>CO2Sustain makes a patented additive for carbonated soft drinks which delivers an enhanced customer taste experience by making drinks fizzier for longer.</p>

<p>This September saw the first post-COVID return of drinktec, which is always held in Munich. With 1,002 exhibitors from 55 countries this year vying to attract business from 50,000 visitors from 169 countries, it really is the global meeting place for anyone in the beverage industry.</p>

<p>According to Jonathan Stott, general manager at CO2Sustain: &ldquo;Because there&rsquo;s been quite a gap between the last drinktec show and this one, the difference for us this year really stood out. It&rsquo;s clear that fizzy drinks manufacturers around the world are now well aware of our leading position in innovative bubble technology &ndash; and it&rsquo;s resonating with even the biggest manufacturers.</p>

<p>&ldquo;While we can&rsquo;t mention who they are, over 50 representatives from one particularly well-recognised name came to talk us. They said our product was &lsquo;one of the best things they&rsquo;d seen at the show&rsquo;.&rdquo;</p>

<p>According to Jonathan Stott, two key issues dominated conversations at the trade show. &ldquo;We already know that manufacturers are deeply concerned about serious CO2&nbsp;shortages &ndash; especially in Europe and North America &ndash; which is threatening their ability to produce and market their beverages. This is one area where we can really help, because using our additive allows fizzy-drinks manufacturers to cut the amount of CO2&nbsp;they use by around 12%, with no discernible difference to the consumer experience.</p>

<p>&ldquo;Secondly, manufacturers are concerned about the shelf life of the CO2 in their drinks, which can be as little as 4 months. Shorter shelf lives can lead to returned stocks and all the associated financial and sustainability costs. Manufacturers are very interested to hear that our CO2Sustain additive can extend shelf life by an extra 4 weeks, which means fewer returned products, less dumping of stock and packaging, less transport, more saleable products per production run, and wider distribution capability.</p>

<p>&ldquo;We&rsquo;re working really hard right now to follow up on all of the interest we generated at the show. We now have quite a number of meetings arranged with some leading brands, and a couple of production trials already agreed.&rdquo;</p>",250,"2022-10-10 21:04:09","2022-10-10 21:04:47",1,https://www.co2sustain.com/,co2sustain-press-release,"2022-11-01 00:00:00"
240,"Frame - Press Release","Frame on course to hit £1m in revenues for the first time","<p>Frame &ndash; the West-Yorkshire based technology and data science business - is forecasting robust growth over the next 12 months as it aims to hit &pound;1m in revenues by the end of the next financial year.</p>

<p>&nbsp;</p>

<p>Frame, which was established in 2020 by Liam Fulton and Hannah Bratley, is a technology and data science business that is headquartered in Slaithwaite but works with clients across the UK and internationally. Its team of end-to-end data experts, who include engineers, scientists and developers, work across two core divisions: Consultancy and Solutions.</p>

<p>&nbsp;</p>

<p>In its first year, revenues stood at &pound;61,000 which grew to &pound;453,000 last year and resulted in a 641% increase in gross profits. The business has also relocated to a new office at Globe Mill in Slaithwaite and upped its headcount to 10. The move to Globe Mill was all about preparing the business for its next phase of growth. The new office provides enough space for the planned team expansion and delivers the option of additional flexible working capabilities.<br />
<br />
The growth of the business has been driven with no outside investment with all profits being retained and reinvested.<br />
<br />
Frame works with a broad range of clients in the banking and finance, retail (both bricks &amp; mortar and online), logistics and distribution sectors. Its broad client roster includes NatWest, Sorted Group,&nbsp;<em>ECO</em><em>&nbsp;</em>Surv and Smart Data Foundry.<br />
<br />
Hannah Bratley, co-founder and CEO, said:&nbsp;<em>&ldquo;The last two years has seen rapid but sustainable growth which has been as a result of our vision and people. Our primary collective focus has always been on delivering for our clients whilst constantly innovating. That approach will never change, nor will our determination to remain independent. The decision to reinvest all our profits was an easy one as it allows us to retain full control and build the business debt free.&rdquo;</em><br />
<br />
Frame&rsquo;s Consultancy team works with clients in numerous ways to help them overcome their specific data related problems including designing and implementing bespoke data engineering pipelines, building pricing optimisation algorithms and delivering custom computer vision modelling which can be used to improve sales.</p>

<p>&nbsp;</p>

<p>Whilst the Solutions team provides clients with a range of &lsquo;off the shelf&rsquo; Applied Programming Interface (API) products that are customisable to meet their needs. Examples include data resourcing, strategy and custom data science project support.<br />
<br />
The business is also currently developing a brand-new image optimisation product for the e-commerce sector which will come to market in Q1 2023.</p>",251,"2022-10-10 21:09:27","2022-10-10 21:09:37",1,yorkshirebusinessjournal.co.uk,frame-press-release,"2022-11-02 00:00:00"
241,"STRIVELIVE - Press Release","LOCAL ENTREPRENEURS INVITED TO SHOW THEY MEAN BUSINESS WITH STRIVELIVE","<p><strong>The StriveLive start-up incubator is supporting 30 fledgling businesses to help them on the road to success.</strong><br />
&nbsp;</p>

<p>Last year the incubator supported 14 local entrepreneurs, many of whom have gone on to establish successful businesses across North Yorkshire.</p>

<p><br />
StriveLive is now looking to work with a further 30 entrepreneurs in Harrogate or York who are going to or have just launched a business, through its specifically designed seven-week programme.</p>

<p><br />
Each entrepreneur on the programme will benefit from one-to-one advice sessions and a series of workshops covering over 20 topics, from finances to marketing before they compete to impress business experts in a project finale to win up to &pound;2,500 in micro-grants.&nbsp;<br />
Delivered primarily through weekly live training sessions with regular one-to-one advice and on-demand online learning, the award-winning incubator programme is designed to make it easy and exciting to start a business.</p>

<p>&nbsp;</p>

<p>Laura Mumford a spokesperson at Enterprise CUBE, the partner facilitators for StriveLive, said: &ldquo;As we recover from the Covid-19 pandemic, and face fresh challenges such as rising energy costs, inflation, and interest rates, we need businesses of all types to start up, grow, and sustain an economic recovery.<br />
&ldquo;StriveLive is designed to nurture and support local entrepreneurs as they prepare to launch their business, so they have the best chance of achieving their goals and support the local economy.</p>

<p><br />
&ldquo;We&rsquo;re looking forward to working with 30 more entrepreneurs on our second cohort of entrepreneurs through this successful programme.&rdquo;<br />
Hanna Dilley, founder of Benji&rsquo;s Bites Toddler Food, took part in StriveLive in 2022.&nbsp; She said: &ldquo;I really enjoyed the Strive course, the subjects we discussed, and meeting fellow local business owners. Starting and running a food business by yourself is really tough so it&rsquo;s great to connect with other people going through similar challenges as me and share ideas.&rdquo;<br />
Karen Allen, Kidzplay Play Box, who also took part, said: &ldquo;Strive gave me the confidence to move forward, the understanding to face the challenges, and the connections within the local area to continue the support after the course finished. Strive for me allowed me to step up, to move on with the supportive team and experienced entrepreneurs with years of experience, knowledge, and stories.&rdquo;</p>

<p><br />
Other entrepreneurs who took part in last year&rsquo;s cohort included: Oliver Brown of Wrapd Studios; Claire Parish, of Four Legs Pet Care, and Graham Dodds of GMD Home Improvements.</p>

<p><br />
Andrew Raby, Manager of the York &amp; North Yorkshire Growth Hub, said: &ldquo;The StriveLive start-up incubator provides a hugely valuable programme of support to our local entrepreneurs allowing them to scale their ideas and make a tangible impact upon our local economy. Last year&rsquo;s results were fantastic, and we are very much looking forward to supporting all the entrepreneurs as they progress through this year&rsquo;s programme.&rdquo;<br />
The Strive project is commissioned by the York &amp; North Yorkshire Growth Hub, and supported by City of York Council and Harrogate Borough Council.</p>

<p><br />
Strive projects are run by the social enterprise, Enterprise CUBE CIC, who use their profits to support disadvantaged entrepreneurs across the UK.</p>

<p><br />
To get more details and apply go to:&nbsp;<a href="https://www.ynygrowthhub.com/events/cyc-hbc-strive-live/">https://www.ynygrowthhub.com/events/cyc-hbc-strive-live/</a></p>",252,"2022-10-10 21:13:35","2022-10-10 21:13:55",1,https://strivelive.com/,strivelive-press-release,"2022-11-03 00:00:00"
242,"West Yorkshire Housing Associations","Yorkshire based housing associations confirm transfer of engagements","<p>Board members of two Yorkshire based housing associations, York Housing Association (YHA) and Leeds &amp; Yorkshire Housing Association (LYHA), have confirmed they will join together through a transfer of engagements.</p>

<p>The decision follows a thorough consultation process where both organisations sought the views of their colleagues, customers and stakeholders on the proposals. The consultation feedback, combined with a detailed due diligence process, have shaped the final business plan which has now been approved by each board.</p>

<p>The two organisations will now join together in a transfer of engagements that will see them operating as a combined 3,000-home regional subsidiary of northern based housing association, Karbon Homes.</p>

<p>The partnership aims to deliver excellent services to customers and to play a key role in shaping places and communities across Yorkshire. The new partnership will create a stronger, more effective organisation, with a continued local approach and understanding of its customer base.</p>

<p>YHA has been a subsidiary of Karbon Homes since 2017 and has already seen the significant benefits of being part of a larger, supportive group with increased investment in YHA&rsquo;s homes and services and a growth of its geography across the wider Yorkshire area. The two organisations have also developed a really strong shared culture, values and working relationship.</p>

<p>Following a successful interview process, existing LYHA Chief Executive, Mark Pearson, will take up the role of Managing Director of the new subsidiary. He brings to the role 25 years of knowledge and experience across the housing sector.</p>

<p>The combined subsidiary will also have a new name &ndash; 54North Homes. This new name, vision and identity has been co-created with colleagues and customers from both organisations through a range of focus groups. Main feedback themes centred on access to good quality, affordable housing, having a landlord that listens and puts people first, and that local knowledge and expertise is invaluable. It was also important that the new brand should reflect the new organisation&rsquo;s Yorkshire roots and character.</p>

<p>Mark Pearson, 54North Homes Managing Director, said: &ldquo;Developed using colleague and customer insight, we think this new brand is authentic as well as bold and distinctive and reflects our down to earth and authentic approach, along with our Yorkshire roots. For centuries, travellers have used latitude and longitude to find their way home and Yorkshire lies directly on the 54th parallel.</p>

<p>&ldquo;I&rsquo;m honoured and delighted to have been appointed as Managing Director of 54North Homes. I am excited about the opportunities that coming together will bring, both in terms of our service to customers and our two teams working together as one. I&rsquo;m looking forward to getting started.&rdquo;</p>

<p>Integration plans will now progress with the aim for the combined subsidiary to be in place by December 2022.</p>",253,"2022-10-10 21:19:16","2022-10-10 21:19:31",1,https://www.lyha.co.uk/,west-yorkshire-housing-associations,"2022-11-08 00:00:00"
243,"Ripon and York","Ripon and York in the Top 4 UK Cities for Customer Service","<p><em>Cities were ranked by the relative percentage of positive and negative comments left by customers from a study of over 780,000 customer service reviews by careers experts,&nbsp;<a href="https://standout-cv.com/uk-best-and-worst-customer-service">StandOut CV.</a></em></p>

<ul>
	<li>Businesses in&nbsp;Wells in Somerset&nbsp;had the highest percentage of positive customer reviews in the UK, followed by Ripon, and Chichester.&nbsp;</li>
	<li>Businesses in&nbsp;<strong>Westminster&nbsp;</strong>received the&nbsp;<strong>highest percentage of negative customer comments</strong>&nbsp;in the UK, followed by Greater London, and Birmingham.</li>
	<li>When it came to&nbsp;<strong>hospitality&nbsp;</strong>businesses,&nbsp;<strong>Ripon&nbsp;</strong>had the highest percentage of positive customer feedback, while&nbsp;<strong>Greater London</strong>&nbsp;suffered the most negative criticism.&nbsp;</li>
	<li><strong>Retail&nbsp;</strong>businesses received the most positive feedback in&nbsp;<strong>Wells</strong>, York, and Truro, but the most negative in&nbsp;<strong>Leicester</strong>, Westminster, and Southampton.&nbsp;</li>
	<li><strong>Plumbers&nbsp;</strong>in&nbsp;<strong>Bradford&nbsp;</strong>receive the highest percentage of positive reviews in the UK, while&nbsp;<strong>Leicester&#39;s&nbsp;</strong>plumbers received the highest percentage of negative comments.&nbsp;</li>
	<li><strong>Gyms&nbsp;</strong>and fitness centres in&nbsp;<strong>Bath</strong>, Winchester, and Wells were considered to provide the best customer service, while those in&nbsp;<strong>Cambridge</strong>, Chelmsford, and Westminster, received the most negative feedback from customers.&nbsp;</li>
	<li><strong>Hairdressers and barbers</strong>&nbsp;in&nbsp;<strong>Chichester</strong>&nbsp;were delivering the best customer service, while&nbsp;<strong>Londoners were let down</strong>&nbsp;by their hair care experience.&nbsp;</li>
</ul>

<p>&nbsp;</p>

<p>A new study, by careers experts,&nbsp;<a href="https://standout-cv.com/uk-best-and-worst-customer-service">StandOut CV</a>, has analysed 784,444 customer reviews from Google Maps to reveal the best and worst cities in the UK for customer service by highlighting the frequency of positive and negative comments left by customers in each city. The full list of phrases and words analysed can be found in the methodology.&nbsp;</p>

<p>The analysis reviewed a variety of industries which included B&amp;Bs, barbers, bars, caf&eacute;s, gyms, hairdressers, restaurants, plumbers, shops and supermarkets to reveal how many times positive and negative phrases were used.&nbsp;</p>

<p>The full results and findings of the study can be found here:&nbsp;<a href="https://standout-cv.com/uk-best-and-worst-customer-service">https://standout-cv.com/uk-best-and-worst-customer-service</a>&nbsp;</p>

<p>Cities providing the worst customer service in the UK</p>

<p>Relative to the number of reviews left per city, the analysis found the city of Westminster to have the highest proportion of customer service reviews where 2.42% of business reviews contained negative comments from customers, followed by London (2.41%), and Birmingham (2.3%).&nbsp;</p>

<p>Overall, the percentages for negative comments in each city was found to be far lower than the percentages of reviews containing positive comments, indicating Brits are more likely to leave positive comments online than criticise.&nbsp;</p>

<p>&nbsp;</p>

<p>Rank</p>

<p>&nbsp;</p>

<p>City</p>

<p>&nbsp;</p>

<p>County</p>

<p>&nbsp;</p>

<p>Percentage of Reviews Containing Negative Comments</p>

<p>&nbsp;</p>

<p>1</p>

<p>&nbsp;</p>

<p>Westminster</p>

<p>&nbsp;</p>

<p>London</p>

<p>&nbsp;</p>

<p>2.42%</p>

<p>&nbsp;</p>

<p>2</p>

<p>&nbsp;</p>

<p>London</p>

<p>&nbsp;</p>

<p>Greater London</p>

<p>&nbsp;</p>

<p>2.41%</p>

<p>&nbsp;</p>

<p>3</p>

<p>&nbsp;</p>

<p>Birmingham</p>

<p>&nbsp;</p>

<p>West Midlands</p>

<p>&nbsp;</p>

<p>2.30%</p>

<p>&nbsp;</p>

<p>4</p>

<p>&nbsp;</p>

<p>Leicester</p>

<p>&nbsp;</p>

<p>Leicestershire</p>

<p>&nbsp;</p>

<p>2.25%</p>

<p>&nbsp;</p>

<p>5</p>

<p>&nbsp;</p>

<p>Bradford</p>

<p>&nbsp;</p>

<p>West Yorkshire</p>

<p>&nbsp;</p>

<p>2.20%</p>

<p>&nbsp;</p>

<p>6</p>

<p>&nbsp;</p>

<p>Cambridge</p>

<p>&nbsp;</p>

<p>Cambridgeshire</p>

<p>&nbsp;</p>

<p>2.14%</p>

<p>&nbsp;</p>

<p>7</p>

<p>&nbsp;</p>

<p>Manchester</p>

<p>&nbsp;</p>

<p>Greater Manchester</p>

<p>&nbsp;</p>

<p>2.08%</p>

<p>&nbsp;</p>

<p>8</p>

<p>&nbsp;</p>

<p>Oxford</p>

<p>&nbsp;</p>

<p>Oxfordshire</p>

<p>&nbsp;</p>

<p>2.05%</p>

<p>&nbsp;</p>

<p>9</p>

<p>&nbsp;</p>

<p>Nottingham</p>

<p>&nbsp;</p>

<p>Nottinghamshire</p>

<p>&nbsp;</p>

<p>2.01%</p>

<p>&nbsp;</p>

<p>10</p>

<p>&nbsp;</p>

<p>Salford</p>

<p>&nbsp;</p>

<p>Greater Manchester</p>

<p>&nbsp;</p>

<p>2.00%</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Cities providing the best customer service in the UK</p>

<p>The study found Wells in Somerset had the highest percentage of positive customer comments, relative to the city&#39;s overall total, with 26.9% of reviews mentioning any of the positive customer service phrases analysed in the study.&nbsp;Ripon in North Yorkshire was found to be the second best city (20.7%) of their reviews having positive feedback, followed by West Sussex&#39;s historic town, Chichester (19.9%).&nbsp;</p>

<p>Cities in the UK generally had a higher percentage of positive comments than negative, however, most reviews were left with no comments, where users only left a star rating.&nbsp;</p>

<p>You can see the top ten cities ranked by the city&#39;s percentage of positive reviews below:</p>

<p>&nbsp;</p>

<p>Rank</p>

<p>&nbsp;</p>

<p>City</p>

<p>&nbsp;</p>

<p>County</p>

<p>&nbsp;</p>

<p>Percentage of Reviews Containing Positive Comments</p>

<p>&nbsp;</p>

<p>1</p>

<p>&nbsp;</p>

<p>Wells</p>

<p>&nbsp;</p>

<p>Somerset</p>

<p>&nbsp;</p>

<p>20.89%</p>

<p>&nbsp;</p>

<p>2</p>

<p>&nbsp;</p>

<p>Ripon</p>

<p>&nbsp;</p>

<p>North Yorkshire</p>

<p>&nbsp;</p>

<p>20.73%</p>

<p>&nbsp;</p>

<p>3</p>

<p>&nbsp;</p>

<p>Chichester</p>

<p>&nbsp;</p>

<p>West Sussex</p>

<p>&nbsp;</p>

<p>19.90%</p>

<p>&nbsp;</p>

<p>4</p>

<p>&nbsp;</p>

<p>York</p>

<p>&nbsp;</p>

<p>North Yorkshire</p>

<p>&nbsp;</p>

<p>19.19%</p>

<p>&nbsp;</p>

<p>5</p>

<p>&nbsp;</p>

<p>Truro</p>

<p>&nbsp;</p>

<p>Cornwall</p>

<p>&nbsp;</p>

<p>18.54%</p>

<p>&nbsp;</p>

<p>6</p>

<p>&nbsp;</p>

<p>Lichfield</p>

<p>&nbsp;</p>

<p>Staffordshire</p>

<p>&nbsp;</p>

<p>18.50%</p>

<p>&nbsp;</p>

<p>7</p>

<p>&nbsp;</p>

<p>Plymouth</p>

<p>&nbsp;</p>

<p>Devon</p>

<p>&nbsp;</p>

<p>18.24%</p>

<p>&nbsp;</p>

<p>8</p>

<p>&nbsp;</p>

<p>Salisbury</p>

<p>&nbsp;</p>

<p>Wiltshire</p>

<p>&nbsp;</p>

<p>18.24%</p>

<p>&nbsp;</p>

<p>9</p>

<p>&nbsp;</p>

<p>Norwich</p>

<p>&nbsp;</p>

<p>Norfolk</p>

<p>&nbsp;</p>

<p>17.72%</p>

<p>&nbsp;</p>

<p>10</p>

<p>&nbsp;</p>

<p>Bath</p>

<p>&nbsp;</p>

<p>Somerset</p>

<p>&nbsp;</p>

<p>17.67%</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Cities where customers used positive and negative descriptors the most:</strong></p>

<p>In addition to this data, StandOut CV&#39;s researchers were also able to discover where in the UK customers used different terms the most. Brummies used &#39;disappointed&#39; to review their local businesses the most out of anyone else, while those in York commented on how &#39;lovely&#39; their service had been.&nbsp;</p>

<p>&nbsp;</p>

<p><strong>Phrase/word</strong></p>

<p>&nbsp;</p>

<p><strong>City where customers use this phrase the most</strong></p>

<p>&nbsp;</p>

<p>Disappointed</p>

<p>&nbsp;</p>

<p>Birmingham</p>

<p>&nbsp;</p>

<p>Polite</p>

<p>&nbsp;</p>

<p>Bradford</p>

<p>&nbsp;</p>

<p>Good value</p>

<p>&nbsp;</p>

<p>Cardiff</p>

<p>&nbsp;</p>

<p>Great value</p>

<p>&nbsp;</p>

<p>Cardiff</p>

<p>&nbsp;</p>

<p>Terrible service</p>

<p>&nbsp;</p>

<p>Coventry</p>

<p>&nbsp;</p>

<p>Bad service</p>

<p>&nbsp;</p>

<p>Greater London</p>

<p>&nbsp;</p>

<p>Highly recommend</p>

<p>&nbsp;</p>

<p>Greater London</p>

<p>&nbsp;</p>

<p>Speed (Took ages)</p>

<p>&nbsp;</p>

<p>Greater London</p>

<p>&nbsp;</p>

<p>Poor value</p>

<p>&nbsp;</p>

<p>Leeds</p>

<p>&nbsp;</p>

<p>The best job</p>

<p>&nbsp;</p>

<p>Leeds</p>

<p>&nbsp;</p>

<p>Cowboy</p>

<p>&nbsp;</p>

<p>Manchester<br />
Westminster</p>

<p>&nbsp;</p>

<p>Above and beyond</p>

<p>&nbsp;</p>

<p>Newcastle</p>

<p>&nbsp;</p>

<p>Pleasantly surprised</p>

<p>&nbsp;</p>

<p>Oxford</p>

<p>&nbsp;</p>

<p>Excellent service</p>

<p>&nbsp;</p>

<p>Sheffield</p>

<p>&nbsp;</p>

<p>Great service</p>

<p>&nbsp;</p>

<p>Sheffield</p>

<p>&nbsp;</p>

<p>Speed (Only took)</p>

<p>&nbsp;</p>

<p>Southampton</p>

<p>&nbsp;</p>

<p>Disgusting</p>

<p>&nbsp;</p>

<p>Westminster</p>

<p>&nbsp;</p>

<p>Don&#39;t recommend</p>

<p>&nbsp;</p>

<p>Westminster</p>

<p>&nbsp;</p>

<p>Rude</p>

<p>&nbsp;</p>

<p>Westminster</p>

<p>&nbsp;</p>

<p>Bad value</p>

<p>&nbsp;</p>

<p>Winchester</p>

<p>&nbsp;</p>

<p>Lovely</p>

<p>&nbsp;</p>

<p>York</p>

<p>&nbsp;</p>

<p>Hospitality businesses:&nbsp;</p>

<p>StandOut CV&#39;s analysis of the Google data discovered Ripon in North Yorkshire had the highest percentage of reviews containing positive phrases within the hospitality sector (25%) followed by Wells (23.3%) and York (23.2%).&nbsp;</p>

<p><strong>Top 10 cities for positive customer feedback in hospitality:&nbsp;</strong></p>

<ol>
	<li>
	<p>Ripon (25%)</p>
	</li>
	<li>
	<p>Wells (23.3%)</p>
	</li>
	<li>
	<p>York (23.2%)</p>
	</li>
	<li>
	<p>Plymouth (22.8%)</p>
	</li>
	<li>
	<p>Salisbury (22.7%)</p>
	</li>
	<li>
	<p>Chichester (22.7%)</p>
	</li>
	<li>
	<p>Truro (22.0%)</p>
	</li>
	<li>
	<p>Lincoln (21.9%)</p>
	</li>
	<li>
	<p>Chester (21.6%)</p>
	</li>
	<li>
	<p>Bath (21.3%)</p>
	</li>
</ol>

<p>When it came to the worst hospitality reviews, the analysis found Greater London to have the highest percentage of negative feedback (3%), followed by Birmingham (2.6%), and Westminster (2.5%).</p>

<p><strong>Top 10 cities for negative customer feedback in hospitality:</strong></p>

<ol>
	<li>
	<p>Greater London (3%)</p>
	</li>
	<li>
	<p>Birmingham (2.6%)</p>
	</li>
	<li>
	<p>Westminster (2.5%)</p>
	</li>
	<li>
	<p>Coventry (2.5%)</p>
	</li>
	<li>
	<p>Leeds (2.5%)</p>
	</li>
	<li>
	<p>Bradford (2.4%)</p>
	</li>
	<li>
	<p>Salford (2.4%)</p>
	</li>
	<li>
	<p>Manchester (2.4%)</p>
	</li>
	<li>
	<p>Brighton (2.3%)</p>
	</li>
	<li>
	<p>Lichfield (2.3%)</p>
	</li>
</ol>

<p><strong>Hairdressers and barbers:</strong></p>

<p>Chichester, West Sussex, was found to have the best customer service by barbers and hairdressers with 24% of reviews containing positive comments from customers, the highest in the country.&nbsp;</p>

<p><strong>Top 10 cities for positive customer feedback in hairdressing and barbering:</strong></p>

<ol>
	<li>Chichester (24.0%)</li>
	<li>London (22.7%)</li>
	<li>Oxford (22.6%)</li>
	<li>Lichfield (22.6%)</li>
	<li>Brighton (21.7%)</li>
	<li>Truro (20.4%)</li>
	<li>Norwich (20.4%)</li>
	<li>Canterbury (20.2%)</li>
	<li>York (19.9%)</li>
	<li>Wells (19.5%)</li>
</ol>

<p><strong>Top 10 cities for negative customer feedback in hairdressing and barbering:</strong></p>

<p>Despite the Big Smoke ranking highly with many positive customer comments, the study found Greater London also had the highest percentage of negative comments (9.5%) followed by Oxford (8.2%). The high percentage of reviews for both positive and negative experiences in Greater London does indicate the city polarises clients more than any other hair destination in the country.&nbsp;</p>

<ol>
	<li>London (9.5%)</li>
	<li>Oxford (8.2%)</li>
	<li>Westminster (8.1%)</li>
	<li>Brighton (7.9%)</li>
	<li>Chichester (7.5%)</li>
	<li>Exeter (7.3%)</li>
	<li>Norwich (7.2%)</li>
	<li>Chelmsford (7.0%)</li>
	<li>Chester (6.8%)</li>
	<li>Cambridge (6.8%)</li>
</ol>

<p>Retail businesses:</p>

<p>The best-reviewed city for retail experiences was found to be Wells in Somerset, with 9.3% of reviews posting positive comments about their customer service. In addition, the study also discovered York was the second best, with 8.4% of the city&#39;s reviews containing positive customer feedback.</p>

<p><strong>Top 10 cities for positive customer feedback in retail:</strong></p>

<ol>
	<li>Wells (9.3%)</li>
	<li>York (8.4%)</li>
	<li>Truro (8.2%)</li>
	<li>Ripon (8.2%)</li>
	<li>Ely (7.3%)</li>
	<li>Chichester (7.2%)</li>
	<li>Winchester (6.9%)</li>
	<li>Salisbury (6.7%)</li>
	<li>Manchester (6.5%)</li>
	<li>Chester (6.5%)</li>
</ol>

<p><strong>Top 10 cities for negative customer feedback in retail:</strong></p>

<ol>
	<li>Leicester (3.25%)</li>
	<li>Westminster (2.91%)</li>
	<li>Southampton (2.90%)</li>
	<li>Bradford (2.86%)</li>
	<li>Birmingham (2.85%)</li>
	<li>Manchester (2.53%)</li>
	<li>Oxford (2.53%)</li>
	<li>Bristol (2.34%)</li>
	<li>Nottingham (2.30%)</li>
	<li>Cambridge (2.27%)</li>
</ol>

<p><strong>Gyms and fitness centres:&nbsp;</strong></p>

<p>StandOut CV found Bath in Somerset had the highest percentage of positive comments for its gyms (14%), followed by Winchester (13.8%) and Wells (11.9%).</p>

<p><strong>Top 10 cities for positive customer feedback in gyms and fitness centres:</strong></p>

<ol>
	<li>Bath (14.0%)</li>
	<li>Winchester (13.8%)</li>
	<li>Wells, (11.9%)</li>
	<li>Ely (11.9%)</li>
	<li>Chichester (11.4%)</li>
	<li>Ripon (11.3%)</li>
	<li>Westminster (11.2%)</li>
	<li>Truro (10.4%)</li>
	<li>London (9.9%)</li>
	<li>Cambridge (9.0%)</li>
</ol>

<p><strong>Top 10 cities for negative customer feedback in gyms and fitness centres:</strong></p>

<p><strong>The worst gym experiences in the UK were found to be in Cambridge which had the highest percentage of negative comments (3.2%) followed by Chelmsford (2.3%).&nbsp;</strong></p>

<ol>
	<li>Cambridge (3.24%)</li>
	<li>Chelmsford (2.32%)</li>
	<li>Westminster (2.12%)</li>
	<li>Portsmouth (1.86%)</li>
	<li>Norwich (1.86%)</li>
	<li>Bristol (1.73%)</li>
	<li>Oxford (1.72%)</li>
	<li>Wells (1.71%)</li>
	<li>York (1.70%)</li>
	<li>London (1.62%)</li>
</ol>

<p><strong>Plumbers:</strong></p>

<p>Bradford was found to have the highest percentage of positive comments (55.2%) for plumbers, while Leicester and London came joint first when it came to the highest percentage of negative comments per city.&nbsp;</p>

<p><strong>Top 10 cities for positive customer feedback in plumbing:</strong></p>

<ol>
	<li>Bradford (55.2%)</li>
	<li>Canterbury (49.0%)</li>
	<li>Lichfield (47.0%)</li>
	<li>Cardiff (45.8%)</li>
	<li>Plymouth (43.3%)</li>
	<li>St Albans (43.1%)</li>
	<li>York (42.7%)</li>
	<li>Wakefield (42.6%)</li>
	<li>Birmingham (42.6%)</li>
	<li>Derby (42.6%)</li>
</ol>

<p><strong>Top 10 cities for negative customer feedback in plumbing:</strong></p>

<ol>
	<li>Leicester (3.1%) +&nbsp;London (3.1%)</li>
	<li>Salford (2.5%)</li>
	<li>Swansea (2.4%)</li>
	<li>Preston (2.3%)</li>
	<li>Lincoln (2.2%)</li>
	<li>Stoke-on-Trent (2.2%)</li>
	<li>Edinburgh (1.9%)</li>
	<li>Worcester (1.8%)</li>
	<li>Wakefield (1.8%)</li>
</ol>

<p>StandOut CV also spoke to&nbsp;<a href="https://ladyjaney.co.uk/about-me/">Jane Hawkes</a>, &lsquo;Queen of Customer Service&rsquo; and Radio Presenter, about the study&#39;s findings and the UK&#39;s customer service crisis:</p>

<p>&ldquo;We have been riding on a tide of mediocrity for years with falling standards of customer service evident in an increasing number of areas yet still only around 90% of customers were complaining. However, the pandemic has proved the final straw as customers have become increasingly frustrated with the old Covid chestnut excuse for poor customer service.&rdquo;</p>

<p>&quot;Although a number of companies responded well during the pandemic by adapting their offering to suit the changing landscape, others struggled to step up to deal with the new challenges and long-standing pre-pandemic issues were exacerbated. As a result, we are seeing record levels of complaints about everything from long waits on hold, poor quality products and services, delays issuing refunds, time taken to resolve complaints and difficulty in getting through to customer services.&quot;</p>

<p>&ldquo;I don&rsquo;t think it&rsquo;s necessarily that these smaller towns and cities are more polite, just that there&rsquo;s a different social culture. In large towns and cities, customer service is delivered very much on a &lsquo;grab and go&rsquo; basis with easy, quick and convenient topping the priority list rather than the meet and greet smiles and pleasantries you may find elsewhere. Londoners aren&rsquo;t rude or unfriendly, it&rsquo;s just a different pace of life as you would expect from a city so in some instances less time for casual &rsquo;chitty chat&rsquo; in the working day.&rdquo;</p>",254,"2022-10-10 21:21:34","2022-10-10 21:21:53",1,https://standout-cv.com/,ripon-and-york,"2022-11-09 00:00:00"
