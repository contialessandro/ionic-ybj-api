SELECT JSON_OBJECT(
               '_id', CONCAT('ObjectId("', LPAD(HEX(UUID()), 24, '0'), '")'),
               'id', t.id,
               'media_id', tm.image_id,
               'thread_meta_id', tm.id,
               'image', MAX(m.file_name),
               'alt', MAX(m.name),
               'body', t.body,
               'category', c.name,
               'color', c.color,
               'filename', MAX(m.file_name),
               'intro', t.intro,
               'location', l.name,
               'slug', t.slug,
               'ticker', tm.ticker,
               'title', t.title,
               'url', t.url,
               'awsUrl',
               CONCAT('https://ybjlaravel.s3.eu-west-2.amazonaws.com/', MAX(m.model_id), '/', MAX(m.file_name)),
               'hide', t.hide,
               'published_at',
               DATE_FORMAT(STR_TO_DATE(t.published_at, '%Y-%m-%d %H:%i:%s'), '%Y-%m-%dT%H:%i:%s.000+00:00'),
               'created_at', DATE_FORMAT(STR_TO_DATE(t.created_at, '%Y-%m-%d %H:%i:%s'), '%Y-%m-%dT%H:%i:%s.000+00:00')
       ) AS ''
FROM threads t
         JOIN
     thread_metas tm ON t.meta_id = tm.id
         JOIN
     media m ON t.meta_id = m.model_id
         JOIN
     locations l ON tm.location_id = l.id
         JOIN
     categories c ON tm.category_id = c.id
WHERE
t.id > 415
GROUP BY
t.id, tm.id, tm.image_id, tm.ticker, t.slug, t.body, t.intro, t.title, t.url, t.hide, t.published_at,
         t.created_at;
