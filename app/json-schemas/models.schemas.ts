import {model, Schema} from 'mongoose';

interface ILocation {
    _id: string;
    id: number;
    name: string;
}

export interface IThreadMeta {
    _id: String,
    id: string,
    image_id: string,
    alt: string,
    location_id: string
    category_id: string
    user_id: number,
    thread_id: number
}

export interface ICategory {
    _id: string;
    id: number;
    type: string;
    name: string;
    color: string;
}

export interface IThread {
    _id: string,
    id: number,
    title: string,
    intro: string,
    body: string,
    // meta_id: { type: 'String', ref: "ThreadMeta" },
    meta_id: number,
    hide: boolean,
    url: string,
    slug: string,
    created_at:string,
    published_at:string
}

export interface IMedia {
    _id: string,
    id: string;
    model_type: string,
    model_id: number,
    collection_name: string,
    name: string,
    file_name: string,
    mime_type: string,
    disk: string,
    size: string,
    manipulations: string,
    custom_properties: string,
    responsive_images: string,
    order_column: string
}

export interface IPost{
    id: number;
    media_id: string;
    thread_meta_id: string;
    image: string;
    alt: string;
    body: string;
    category: string;
    color: string;
    created_at: string;
    filename: string;
    intro: string;
    location: string;
    slug: string;
    ticker: string;
    title: string;
    url: string;
    hide: boolean;
    awsUrl: string;
    audio_url?: string;
    audio_task_id?: string;
    published_at: string;
}

export interface IAudioTask {
    _id: Object;
    task_id: string;
    slug: string;
    status: string;
}

export const CategoryModel = model<ICategory>('Category', new Schema({
    _id: String,
    id: Number,
    type: String,
    name: String,
    color: String
}, {timestamps: true}));
export const LocationModel = model<ILocation>('Locations', new Schema({
    _id: String,
    id: Number,
    name: String
}, {timestamps: true}));

const ThreadMetaSchema = new Schema({
    _id: String,
    id: String,
    image_id: {type: 'String', ref: "medias"},
    alt: String,
    location_id: {type: 'String', ref: "Locations"},
    category_id: {type: 'String', ref: "Category"},
    user_id: Number,
    thread_id: {type: 'String', ref: "Thread"},
}, {timestamps: true});
export const ThreadMetaModel = model<IThreadMeta>('ThreadMeta', ThreadMetaSchema);

export const ThreadSchema = new Schema({
    _id: String,
    id: Number,
    title: String,
    intro: String,
    body: String,
    meta_id: Number,
    hide: Boolean,
    url: String,
    slug: String,
    published_at: {
        type: Date
    },
}, {timestamps: true});
export const ThreadModel = model<IThread>('Thread', ThreadSchema);

const MediaSchema = new Schema({
    _id: String,
    id: String,
    model_type: String,
    model_id: Number,
    collection_name: String,
    name: String,
    file_name: String,
    mime_type: String,
    disk: String,
    size: String,
    manipulations: String,
    custom_properties: String,
    responsive_images: String,
    order_column: String
}, {timestamps: true});
export const MediaModel = model<IMedia>('medias', MediaSchema);

const PostSchema = new Schema(
    {
        _id: String,
        id_uuid: Object,
        id: String,
        media_id: String,
        thread_meta_id: String,
        image: String,
        alt: String,
        body: String,
        category: String,
        color: String,
        created_at: String,
        filename: String,
        intro: String,
        location: String,
        slug: String,
        ticker: String,
        title: String,
        url: String,
        hide: Boolean,
        awsUrl: String,
        published_at: Date,
        audio_url: String,
        audio_task_id: String
    }
)
export const PostModel = model<IPost>('posts', PostSchema);

const AudioTaskSchema = new Schema({
    _id: Object,
    task_id: String,
    slug: String,
    status: String
})

export const AudioTaskModel = model<IAudioTask>('audioTask', AudioTaskSchema)
