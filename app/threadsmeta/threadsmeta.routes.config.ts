import {Router} from 'express'
import {ThreadsMetaController} from "./controllers/threadsmeta.controller";
// import {JwtMiddleware} from "../auth/middlewares/jwt.middleware";

const router: Router = Router();
const threadController = new ThreadsMetaController();
// const threadMetaMiddleware = ThreadsMetaMiddleware.getInstance();
// const jwtMiddleware = JwtMiddleware.getInstance();
router.get(`/threadmeta`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    threadController.listThreadMeta
]);
router.get(`/homepage`, [
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    threadController.getHomePageThreads
]);
router.post(`/threadmeta`, [
    threadController.createThreadMeta
]);
router.patch(`/threadmeta`, [
    threadController.patch
]);
router.get(`/threadmetaUpdate`, [
    threadController.updateThreadMetasById
]);

export const ThreadMetaRouter: Router = router;
