import {CRUD} from '../../common/interfaces/crud.interface';
import {GenericInMemoryDao} from '../daos/in.memory.dao';
import {ThreadsMetaDao} from '../daos/threadsmeta.dao';

export class ThreadsMetaServices implements CRUD {
    private static instance: ThreadsMetaServices;
    dao: GenericInMemoryDao;

    constructor() {
        this.dao = GenericInMemoryDao.getInstance();
    }

    static getInstance(): ThreadsMetaServices {
        if (!ThreadsMetaServices.instance) {
            ThreadsMetaServices.instance = new ThreadsMetaServices();
        }
        return ThreadsMetaServices.instance;
    }

    async create(resource: any) {
        return await ThreadsMetaDao.getInstance().addThreadMeta(resource);
    }

    deleteById(resourceId: any) {
        return ThreadsMetaDao.getInstance().removeThreadMetaById(resourceId);
    }

    list(limit: number, page: number) {
        return ThreadsMetaDao.getInstance().listThreadMeta(limit, page);
    }

    patchById(resource: any) {
        return ThreadsMetaDao.getInstance().patchThreadMeta(resource);
    }

    readById(resourceId: any) {
        return ThreadsMetaDao.getInstance().getThreadMetaById(resourceId);
    }

    updateById(resource: any) {
        return ThreadsMetaDao.getInstance().patchThreadMeta(resource);
    }

    async getThreadByName(email: string) {
        return ThreadsMetaDao.getInstance().getThreadMetaByName(email);
    }

    async updateThreadMetasById() {
        return ThreadsMetaDao.getInstance().UpdateThreadMetasLocationIDS();
    };

    async getHomePageThreads() {
        return ThreadsMetaDao.getInstance().getHomePageThreads();
    }

    async getHomepageThreadMetas(id: any) {
        return ThreadsMetaDao.getInstance().getHomepageThreadMetas(id);
    }

    async getThreadMetasByCategory(categoryId: string) {
        return await ThreadsMetaDao.getInstance().getThreadMetasByCategory(categoryId);
    }

    async getThreadMetasById(categoryId: string) {
        return await ThreadsMetaDao.getInstance().getThreadMetasById(categoryId);
    }
}
