import {MongooseService} from '../../common/services/mongoose.service';
import * as shortUUID from 'short-uuid';
import {
    CategoryModel,
    IMedia,
    IPost,
    IThread,
    IThreadMeta,
    LocationModel,
    MediaModel,
    ThreadMetaModel,
    ThreadModel
} from '../../json-schemas/models.schemas';
import {helperFunction} from '../../common/helper';
import * as fs from 'fs';
import {ThreadsServices} from '../../threads/services/threads.services';
import {PostsDao} from '../../posts/daos/posts.dao';
import {CategoryService} from '../../categories/services/category.services';
import {LocationsServices} from '../../locations/services/locations.services';
import {config} from '../../config/config';
import {PostsServices} from "../../posts/services/posts.services";

export class ThreadsMetaDao {
    private static instance: ThreadsMetaDao;
    mongooseService: MongooseService = MongooseService.getInstance();
    Schema = this.mongooseService.getMongoose().Schema;
    Location = LocationModel;
    Category = CategoryModel;
    Thread = ThreadModel;
    ThreadMeta = ThreadMetaModel;
    Media = MediaModel;

    public static getInstance() {
        if (!this.instance) {
            this.instance = new ThreadsMetaDao();
        }
        return this.instance;
    }

    async addThreadMeta(metaFields: any) {
        metaFields._id = shortUUID.generate();
        const meta = new this.ThreadMeta(metaFields);
        await meta.save();
        return meta;
    }

    async getThreadMetaByName(name: string) {
        return this.ThreadMeta.findOne({name: name});
    }

    async getThreadMetaBySlug(slug: string) {
        return this.ThreadMeta.find({'slug': slug}).limit(1);
    }

    async removeThreadMetaById(userId: string) {
        await this.ThreadMeta.deleteOne({_id: userId});
    }

    async getThreadMetaById(userId: string) {
        return this.ThreadMeta.findOne({_id: userId});
    }

    async listThreadMeta(limit = 25, page = 0) {
        return this.ThreadMeta.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async getHomePageThreads(limit = 9) {
        return await this.Thread.find({hide: false}).sort({published_at: -1}).limit(12)
            .exec();
    }

    async getHomepageThreadMetas(metaId: any) {
        return this.ThreadMeta.find({_id: metaId}).sort({$natural: -1}).limit(12)
            .populate('location_id').populate('category_id').populate('image_id');
    }

    async getThreadMetasByCategory(categoryId: string) {
        return this.ThreadMeta.find({category_id: categoryId}).populate('location_id').populate('category_id').populate('image_id');
    }

    async getThreadMetasById(id: string) {
        return this.ThreadMeta.find({_id: id}).populate('location_id').populate('category_id').populate('image_id').limit(1);
    }

    async patchThreadMeta(threadMetaFields: any) {
        const meta: any = await this.ThreadMeta.findById(threadMetaFields._id);
        if (meta) {
            for (const i in threadMetaFields) {
                meta[i] = threadMetaFields[i];
            }
            await meta.save();
            return meta;
        }
    }

    async UpdateThreadMetasLocationIDS() {
        try {
            const threadmetass: any=await this.ThreadMeta.find({});
            //this.Thread.find({id:172,meta_id:182});
            for (const threadmeta of threadmetass) {
                if (threadmeta.id >= 224) {
                    const Media = await this.GetMedia(threadmeta);
                    console.log(`find THREAD with id of ${threadmeta.thread_id}`);
                    const Thread = await this.GetThread(threadmeta);
                    let newCollection = {};
                    if (Thread && Media) {
                        const categoryService = CategoryService.getInstance();
                        const category = await categoryService.getCategoryByOriginalId(+threadmeta.category_id);
                        const locationService = LocationsServices.getInstance();
                        const location = await locationService.getLocationByOriginalId(+threadmeta.location_id);
                        if (!location) {
                            console.log(`${threadmeta.location_id} don't exist`);
                        }
                        if (!category) {
                            console.log(`${threadmeta.category_id} don't exist`);
                        }
                        newCollection = this.mergePostInformation(threadmeta, Thread, Media, category, location);
                        console.log(newCollection);
                        const Post = await this.GetPost(Thread.id);
                        if (!Post) {
                            await this.savePost(newCollection);
                            await helperFunction.Delay(200);
                        }
                    }
                }
            }
        } catch (e) {
            console.log({
                message: e.message,
                line: e.line
            })
            return Promise.resolve(e);
        }

    }

    async GetThread(threadMeta: IThreadMeta) {
        const threadService = ThreadsServices.getInstance();
        const Thread: any = await threadService.readById(threadMeta.thread_id);

        return Thread;
    }

    async GetPost(postId: IPost) {
        const postService = PostsServices.getInstance();
        const Post: any = await postService.readById(postId);

        return Post;
    }

    async GetMedia(threadMeta: any) {
        const Media: any = await this.Media.findOne({model_id: +threadMeta.id});
        if (Media) {
            console.log(`found media ${Media.id}`);
        } else {
            console.log(`not found media ${+threadMeta.id}`);
        }
        return Media;
    }

    async SaveFile(data: any) {
        try {
            fs.writeFileSync('/app/app/common/object.json', JSON.stringify(data));
        } catch (err) {
            console.error(err)
        }
    }

    mergePostInformation(ThreadMeta: any, Thread: IThread, Media: IMedia,category:any,location:any) :IPost
    {
        const publicImagePath= `${config.s3BucketBaseUrl}/${ThreadMeta.image_id}/${Media.file_name}`;
        let publishDate = new Date(`${Thread.published_at}`);

        return <IPost>{
            id: +Thread?.id,
            media_id: Media?.id,
            thread_meta_id: ThreadMeta?.id,
            image: Media.file_name,
            alt: Media.file_name,
            body: Thread?.body,
            category: category?.name ?? 'News',
            color: category?.color ?? 'blue',
            filename: Media.file_name,
            intro: Thread?.intro,
            location: location?.name ?? 'Leeds',
            slug: Thread?.slug,
            ticker: '...',
            title: Thread?.title,
            url: Thread?.url,
            awsUrl: publicImagePath,
            hide: Thread.hide,
            published_at: `${publishDate}`,
            created_at: `${publishDate}`
        }
    }
    async savePost(post: any){
        return await PostsDao.getInstance().addPost(post);
    }
}
