export interface IHomepage {
    "id": number;
    "link": string;
    "title": string;
    "label": string;
    "location": string;
    "image": string;
    "image1": string;
    "image2": string;
    "createdAt": string;
    "intro": string;
    "alt": string;
    "color": string;
    "body": string;
    "slug": string;
}
