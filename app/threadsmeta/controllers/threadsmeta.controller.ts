import express from 'express';
import {ThreadsMetaServices} from '../services/threadsmeta.services';
import {MediaService} from '../../media/services/media.services';


export class ThreadsMetaController {
    protected threadsService: ThreadsMetaServices;

    constructor() {
        this.threadsService=ThreadsMetaServices.getInstance();
    }

    async listThreadMeta(req: express.Request, res: express.Response) {
        const threadsService=ThreadsMetaServices.getInstance();
        const treads = await threadsService.list(1000, 0);
        res.status(200).send(treads);
    }

    async getThreadMetaById(req: express.Request, res: express.Response) {
        const threadsService = ThreadsMetaServices.getInstance();
        const user = await threadsService.readById(req.params.id);
        res.status(200).send(user);
    }

    async createThreadMeta(req: express.Request, res: express.Response) {
        const threadsService = ThreadsMetaServices.getInstance();

        //default should always hide when inserting the first time
        const threadmeta = await threadsService.create(req.body);
        return res.status(201).send(threadmeta);
    }

    async patch(req: express.Request, res: express.Response) {
        const threadsService = ThreadsMetaServices.getInstance();
        await threadsService.patchById(req.body);
        res.status(204).send(``);
    }

    async updateThreadMetasById(req: express.Request, res: express.Response) {
        const threadsService = ThreadsMetaServices.getInstance();
        await threadsService.updateThreadMetasById();
        res.status(204).send(``);
    }

    async put(req: express.Request, res: express.Response) {
        const threadsService = ThreadsMetaServices.getInstance();

        await threadsService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeThreadMeta(req: express.Request, res: express.Response) {
        const threadsService = ThreadsMetaServices.getInstance();
        await threadsService.deleteById(req.params.mediaId);
        res.status(204).send(``);
    }

    async getHomePageThreads(req: express.Request, res: express.Response) {
        try {
            const threadsService = ThreadsMetaServices.getInstance();
            const homepageThreads: any = await threadsService.getHomePageThreads();
            const homepageResources: any = [];
            for (const homepageThread of homepageThreads) {
                const metas: any = await threadsService.getHomepageThreadMetas(homepageThread.meta_id);

                if (metas[0]) {
                    const meta = metas[0];
                    const mediaService = MediaService.getInstance();
                    const imageObject = await mediaService.readById(meta.id);
                    let imagePath = '';
                    if (imageObject) {
                        imagePath = 'https://ybjlaravel.s3.eu-west-2.amazonaws.com/' + imageObject?.id + '/' + imageObject?.file_name;
                    }
                    const obj = {
                        id: homepageThread?.id,
                        label: meta?.category_id?.name ?? '',
                        link: meta?.category_id?.name ?? '',
                        location: meta?.location_id?.name ?? '',
                        title: homepageThread.title ?? '',
                        image: imagePath,
                        image1: imagePath,
                        image2: imagePath,
                        createdAt: homepageThread.published_at,
                        intro: homepageThread.intro,
                        alt: meta.category_id?.type,
                        color: meta.category_id?.color,
                        body: homepageThread.body,
                        slug: homepageThread.slug,
                    };
                    homepageResources.push(obj);
                } else {
                    console.log({homepageThread, metas});
                }
            }
            return res.status(200).send(homepageResources);
        } catch (e) {
            return res.status(500).send({error_message: (e as any).message});
        }
    }

}
