import { FileUploadController } from './controllers/FileUploadController';
import { Router } from 'express';
import { VerifyJWT } from '../config/Auth';

const fileController=new FileUploadController();
const router: Router=Router();
router.put('/admin/file/upload', [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    VerifyJWT,
    fileController.UploadFile
]);
router.post('/admin/file/signedUrl', [
    VerifyJWT,
    fileController.GetSignedUrl
]);


export const FileRouter: Router = router;
