import express from 'express';
import {PublicImagesBucket} from '../../aws/Bucket';

export class FileUploadController {
    UploadFile(req: express.Request, res: express.Response) {
        return res.status(201).send({body: req.body});
    }

    GetSignedUrl = async (
        req: express.Request,
        resp: express.Response,
        next: express.NextFunction
    ) => {
        try {
            const payload = req.body;
            const now = new Date();
            const pathPrefix = now.getMonth();
            const fullpath = [pathPrefix, payload.file_name].join("/");
            const signedUrl = await PublicImagesBucket.GetPutSignedUrl(
                fullpath,
                payload.file_type
            );
            const response = {signed_url: signedUrl, file_storage_path: fullpath};
            console.log(response)
            return resp.status(200).send(response);
        } catch (err) {
            console.log(err);
            return resp.status(503).send({message: (err as any).message});
        }
    };

    GetFilePath(payload: any) {
        const directory = payload.type;
        const now = new Date();
        let fileNameParts = payload?.file_name?.split(".");

        let fileExtension = fileNameParts?.pop();

        let fileNameWithoutExtension = [
            fileNameParts.join("."),
            ...now.toISOString().split(":"),
        ].join("_");

        let fileName = [fileNameWithoutExtension, fileExtension].join(".");
        return [directory, fileName].join("/");
    }
}
