import {MongooseService} from '../../common/services/mongoose.service';
import * as shortUUID from "short-uuid";
import {MediaModel} from "../../json-schemas/models.schemas";

export class MediaDao {
    mongooseService: MongooseService = MongooseService.getInstance();
    private static instance: MediaDao;

    Schema = this.mongooseService.getMongoose().Schema;
    Media: any = MediaModel;


    constructor() {
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance = new MediaDao();
        }
        return this.instance;
    }

    async addMedia(categoryFields: any) {
        categoryFields._id = shortUUID.generate();
        const category = new this.Media(categoryFields);
        await category.save();
        return category;
    }

    async getMediaByName(name: string) {
        return this.Media.findOne({name: name});
    }

    async removeMediaById(userId: string) {
        await this.Media.deleteOne({_id: userId});
    }

    async getMediaByMetaId(metaId: string) {
        console.log({'getMediaByMetaId':+metaId});
        return this.Media.findOne({model_id: +metaId});
    }

    async listMedia(limit: number = 25, page: number = 0) {
        return this.Media.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async patchMedia(MediaFields: any) {
        const media = await this.Media.findById(MediaFields._id);
        if (media) {
            for (const i in MediaFields) {
                media[i] = MediaFields[i];
            }
            await media.save();
        }
        return media;
    }
}
