import {CRUD} from '../../common/interfaces/crud.interface';
import {GenericInMemoryDao} from '../daos/in.memory.dao';
import {MediaDao} from '../daos/media.dao';

export class MediaService implements CRUD {
    private static instance: MediaService;
    dao: GenericInMemoryDao;

    constructor() {
        this.dao = GenericInMemoryDao.getInstance();
    }

    static getInstance(): MediaService {
        if (!MediaService.instance) {
            MediaService.instance = new MediaService();
        }
        return MediaService.instance;
    }

    async create(resource: any) {
        return await MediaDao.getInstance().addMedia(resource);
    }

    deleteById(resourceId: any) {
        return MediaDao.getInstance().removeMediaById(resourceId);
    };

    list(limit: number, page: number) {
        return MediaDao.getInstance().listMedia(limit, page);
    };

    patchById(resource: any) {
        return MediaDao.getInstance().patchMedia(resource);
    };

    readById(resourceId: any) {
        return MediaDao.getInstance().getMediaByMetaId(resourceId);
    };
    // readById(resourceId: any) {
    //     return MediaDao.getInstance().getMediaById(resourceId);
    // };
    updateById(resource: any) {
        return MediaDao.getInstance().getMediaByName(resource);
    };

    async getMediaByName(email: string) {
        return MediaDao.getInstance().getMediaByName(email);
    }
}
