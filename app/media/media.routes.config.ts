import {MediaController} from './controllers/media.controller';
// import {MediaMiddleware} from './middlewares/media.middleware';
// import {CommonPermissionMiddleware} from '../common/middlewares/common.permission.middleware';
// import {JwtMiddleware} from '../auth/middlewares/jwt.middleware';
import {Router} from 'express'

const router: Router = Router();
const mediaController = new MediaController();
// const mediaMiddleware = MediaMiddleware.getInstance();
// const jwtMiddleware = JwtMiddleware.getInstance();
// const commonPermissionMiddleware = new CommonPermissionMiddleware();
router.get(`/media`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    mediaController.listMedia
]);
router.post(`/media`, [
    mediaController.createMedia
]);
router.patch(`/media`, [
    mediaController.patch
]);
router.get(`/media/:mediaId`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    // commonPermissionMiddleware.onlySameUserOrAdminCanDoThisAction,
    // mediaMiddleware.validateMediaExists,
    // mediaMiddleware.extractUserId,
    mediaController.getMediaById
]);
export const MediaRouter: Router = router;
