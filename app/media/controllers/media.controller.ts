import express from 'express';
import {MediaService} from '../services/media.services';
import {ThreadsServices} from "../../threads/services/threads.services";


export class MediaController {
    protected mediaService: MediaService;
    constructor() {
        this.mediaService= MediaService.getInstance();
    }

    async listMedia(req: express.Request, res: express.Response) {
        const mediaService = MediaService.getInstance();
        const users = await mediaService.list(0, 0);
        res.status(200).send(users);
    }

    getMediaById(req: express.Request, res: express.Response) {
        const mediaService= MediaService.getInstance();
        const user = mediaService.readById(req.params.mediaId);
        res.status(200).send(user);
    }

    async createMedia(req: express.Request, res: express.Response) {
        const mediaService = MediaService.getInstance();
        const media = await mediaService.create(req.body);
        res.status(201).send(media);
    }

    async patch(req: express.Request, res: express.Response) {
        const mediaService= MediaService.getInstance();
        await mediaService.patchById(req.body);
        res.status(204).send(``);
    }

    async put(req: express.Request, res: express.Response) {
        const mediaService= ThreadsServices.getInstance();
        await mediaService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeMedia(req: express.Request, res: express.Response) {
        const mediaService= MediaService.getInstance();
        await mediaService.deleteById(req.params.mediaId);
        res.status(204).send(``);
    }

}
