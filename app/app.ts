import express from 'express';
import * as http from 'http';
import * as bodyparser from 'body-parser';
import multer from 'multer';
import {UsersRouter} from './users/users.routes.config';
import {AuthRouter} from './auth/auth.routes.config'
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import helmet from 'helmet';
import {CategoryRouter} from './categories/categories.routes.config';
import {MediaRouter} from './media/media.routes.config';
import {LocationsRouter} from './locations/locations.routes.config';
import {ThreadRouter} from './threads/threads.routes.config';
import {ThreadMetaRouter} from './threadsmeta/threadsmeta.routes.config';
import {FileRouter} from './file/file.router.config';
import {PostRouter} from './posts/posts.routes.config';
import {PublishArticle} from './config/Cronjob.controller';
import {FetchTask} from './config/AudioTaskCronJob.controller';
import {collectDefaultMetrics, register} from 'prom-client';
import {EC2Router} from './EC2/ec2.routes.config';
import {AdminRouter} from "./admin/admin.routes.config";

// const all_routes = require('express-list-endpoints');
const cron = require('node-cron');

export let UPLOAD_PATH = '/var/www/public/'
// Multer Settings for file upload
const bodyParser = require('body-parser');
const cors = require('cors');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, UPLOAD_PATH)
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})
export let upload = multer({storage: storage})
export const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = 3000;
// const routes: []=[];


app.use(bodyparser.json({limit: '5mb'}));

const index = expressWinston.requestWhitelist.indexOf('headers');
if (index !== -1) expressWinston.requestWhitelist.splice(index, 1);
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
    if (req.method === 'OPTIONS') {
        return res.status(200).send();
    } else {
        return next();
    }
});
collectDefaultMetrics();
app.get('/metrics', async (_req, res) => {
    try {
        res.set('Content-Type', register.contentType);
        res.end(await register.metrics());
    } catch (err) {
        res.status(500).end(err);
    }
});
app.use(helmet());
app.use(cors());
app.options('*', cors());
app.use(bodyParser.json({limit: '10mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.json()
    )
}));
app.use('', PostRouter);
app.use('', UsersRouter);
app.use('', AuthRouter);
app.use('', CategoryRouter);
app.use('', MediaRouter);
app.use('', LocationsRouter);
app.use('', ThreadRouter);
app.use('', ThreadMetaRouter);
app.use('', FileRouter);
app.use('', EC2Router);
app.use('', AdminRouter);
app.use('/welcome', (req, resp) => {
    return resp.status(200).send({
        message: 'Welcome to ybj-nodejs-api services'
    });
});
app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.json()
    )
}));
app.use('/updateIDS', () => {

})

app.post('/generate-card', (req, res) => {
    const { title, description, imageUrl, url } = req.body;

    if (!title || !description || !imageUrl || !url) {
        return res.status(400).json({ error: 'Missing required fields' });
    }

    const metaTags = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta property="og:type" content="website">
      <meta property="og:title" content="${title}">
      <meta property="og:description" content="${description}">
      <meta property="og:image" content="${imageUrl}">
      <meta property="og:url" content="${url}">
      
      <meta name="twitter:card" content="summary_large_image">
      <meta name="twitter:title" content="${title}">
      <meta name="twitter:description" content="${description}">
      <meta name="twitter:image" content="${imageUrl}">
      
      <meta property="linkedin:title" content="${title}">
      <meta property="linkedin:description" content="${description}">
      <meta property="linkedin:image" content="${imageUrl}">
      <meta property="linkedin:url" content="${url}">
      
      <title>${title}</title>
    </head>
    </html>
  `;

    res.send(metaTags);
});
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(`Server running at port ${port}`)
});
// podcast.createEpisode('test', 'armalit test', "app/arma-litigation.mp3",'arma-litigation.mp3').then(r  =>r);
// podcast.createEpisode('test', 'armalit test', "app/arma-litigation.mp3",'arma-litigation1.mp3').then(r  =>r);

// podcast.listEpisodes().then(r => {
// });
// podcast.updateFeed().then(r => {
// });

// spotify.FindEpisodeByTitle('podcast-feed/arma-litigation.mp3').then((r:any) => {
//     console.log(r);
// });

cron.schedule('* * * * *', async function () {
    console.log('cronjob FetchTask started');
    await FetchTask();
    console.log('cronjob FetchTask finished');
});

// FetchTask().then(r => r);
// const a="<p>Leeds-based Sano Physiotherapy has acquired specialist occupational health physiotherapy provider, Back in Action UK, becoming one of the largest providers of occupational health physiotherapy in the UK.&nbsp;</p><p>&nbsp;</p><p>";
// let cleanUpText=a.toString().replace(`/<\/?[^>]+(>|$)/gi`, "");
// cleanUpText.replace("&nbsp;","").trim();
// console.log(a);


server.listen(port, async () => {
    cron.schedule('01 00 * * *', async function () {
        await PublishArticle();
        console.log('cronjob finished');
    });
    console.log(`Server running at port ${port}`);
});

export default app;
