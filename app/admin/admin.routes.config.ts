import {Router} from 'express';

import {LinkedinRouter} from "./routes/linkedin.routes.config";
import {TwitterRouter} from "./routes/twitter.route";
const router: Router = Router();


router.use('/admin/linkedin/', LinkedinRouter);
router.use('/admin/twitter/', TwitterRouter);
export const AdminRouter: Router = router;
