import {VerifyJWT} from "../../config/Auth";
import {authenticate, getAccessToken, sendPost} from "../controller/linkedin.controller";
import {IPost} from "../../json-schemas/models.schemas";
import {Router} from "express";
const router: Router = Router();
router.get(`/admin/linkedin/authenticate`,[
    VerifyJWT,
    authenticate
]);
router.get('callback', async (req, res) => {
    const code = req.query.code as string;
    const accessToken = await getAccessToken(code);
    res.send(`Access Token: ${accessToken}`);
})

router.post('createPost', async (req, res) => {
    //https://www.linkedin.com/in/alessandro-conti-84557917/recent-activity/all/
    const response = await sendPost(req.body as IPost);
    return res.status(201).send(response);
})

export const LinkedinRouter: Router = router;
