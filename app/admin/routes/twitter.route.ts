import { Router } from 'express';
import { postTweetWithImageController} from "../controller/twitter.controller";

const router = Router();

router.post('/tweet', postTweetWithImageController);

export const TwitterRouter: Router = router;
