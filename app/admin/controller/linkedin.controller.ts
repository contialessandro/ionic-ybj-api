import axios from 'axios';
import {config} from "../../config/config";
import {IPost} from "../../json-schemas/models.schemas";
import {createPost, registerImageUpload, uploadImageFromS3} from "../service/linkedin.services";

export const authenticate = (req, res) => {
    const clientId = config.linkedin.client_id;
    let redirectUri = config.linkedin.callback;
    // redirectUri = 'https://oauth.pstmn.io/v1/callback';
    const state = 'DCEeFWf45A53sdfKef424'; // Random string to protect against CSRF
    const scope = 'w_member_social';

    try{
        const authUrl = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${clientId}&redirect_uri=${redirectUri}&state=${state}&scope=${scope}`;
        console.log(authUrl);
        res.redirect(authUrl);
    }catch (e) {
        console.log(e);
    }
};

export const getAccessToken = async (code: string) => {
    const clientSecret = config.linkedin.client_secret;
    const clientId = config.linkedin.client_id;
    let redirectUri = config.linkedin.callback;
    const tokenUrl = 'https://www.linkedin.com/oauth/v2/accessToken';
    try{
        const response = await axios.post(tokenUrl, null, {
            params: {
                grant_type: 'authorization_code',
                code,
                redirect_uri: redirectUri,
                client_id: clientId,
                client_secret: clientSecret,
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });

        return response.data.access_token;
    }catch(error){
        console.log(error)
    }
};

export const sendPost = async (post:IPost)=>{
    const articleUrl=`https://www.yorkshirebusinessjournal.co.uk/articles/${post.slug}`;
    try{
        const { uploadUrl, asset } = await registerImageUpload('AQUuA0R-Lf7rIqqHKZR_yAe59SOxrKFbQ3b1DzgHSkUddz4yShIJ54j54sRQ0Pycbe3VFL78h7khpZSi2rYTwp87yi31z8uU0juPzptPMnSTqru3WGipPEULk6ZuQ2kd6bKWq0Xafw7EhapV3EkX_FEjSL9_iOmhstEfIJOY3ygDSJmzdoIM3vZpAh4gwh6P7Grp8QuwL-AmaJHswOPrVX24diGfqWQkNLckmNeQ1wPHF71BmwKdZXycpwaE30h3IG7ixGB-sLpUPG0_jKf6XbtF4FDAhvh04RboGoMD8-zxLPhxLzRoZ83V3VEQqsVixgAS_3z6v1DmctlcquMVmWMZsffomg');

        // // Step 2: Upload image from S3
        await uploadImageFromS3(uploadUrl, post.awsUrl);
        const postResponse = await createPost('AQUuA0R-Lf7rIqqHKZR_yAe59SOxrKFbQ3b1DzgHSkUddz4yShIJ54j54sRQ0Pycbe3VFL78h7khpZSi2rYTwp87yi31z8uU0juPzptPMnSTqru3WGipPEULk6ZuQ2kd6bKWq0Xafw7EhapV3EkX_FEjSL9_iOmhstEfIJOY3ygDSJmzdoIM3vZpAh4gwh6P7Grp8QuwL-AmaJHswOPrVX24diGfqWQkNLckmNeQ1wPHF71BmwKdZXycpwaE30h3IG7ixGB-sLpUPG0_jKf6XbtF4FDAhvh04RboGoMD8-zxLPhxLzRoZ83V3VEQqsVixgAS_3z6v1DmctlcquMVmWMZsffomg',post.title,post.intro,asset,articleUrl);
        console.log({postResponse});
        return Promise.resolve(postResponse);
    }catch (e) {
        console.log(e);
        return Promise.reject(e);
    }
}
