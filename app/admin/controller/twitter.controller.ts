import { Request, Response } from 'express';
import {postTweet, uploadMedia} from "../service/twitter.service";

export const postTweetWithImageController = async (req: Request, res: Response) => {
    const { title, imagePath: awsUrl } = req.body;

    try {
        // Upload media and get media ID
        const mediaId = await uploadMedia(awsUrl);

        // Post tweet with text and media ID
        const response = await postTweet(title, mediaId);

        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};
