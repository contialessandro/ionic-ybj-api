import axios from 'axios';
import crypto from 'crypto';
import OAuth from 'oauth-1.0a';
import FormData from 'form-data';
import {config} from "../../config/config";

const twitterApiUrl = 'https://api.twitter.com/2/tweets';
const twitterMediaUrl = 'https://upload.twitter.com/1.1/media/upload.json';

const getAuthHeader = (url: string, method: string, data: any) => {
    const oauthClient = new OAuth({
        consumer: {
            key: config.twitter.apiKey,
            secret: config.twitter.apiSecretKey,
        },
        signature_method: 'HMAC-SHA1',
        hash_function(base_string, key) {
            return crypto.createHmac('sha1', key).update(base_string).digest('base64');
        },
    });

    const token = {
        key: config.twitter.accessToken,
        secret: config.twitter.accessTokenSecret,
    };

    return oauthClient.toHeader(oauthClient.authorize({ url, method, data }, token));
};

export const uploadMedia = async (imagePath: string) => {
    const url = twitterMediaUrl;
    const method = 'POST';

    const data = new FormData();
    data.append('media', imagePath);

    const headers = {
        ...getAuthHeader(url, method, {}),
        ...data.getHeaders(), // Add this line to include FormData headers
    };

    const response:any = await axios.post(url, data, { headers });
    return response.data.media_id_string;
};

export const postTweet = async (text: string, mediaId: string) => {
    const url = twitterApiUrl;
    const method = 'POST';
    const data = {
        text,
        media: {
            media_ids: [mediaId],
        },
    };

    const headers = {
        ...getAuthHeader(url, method, data),
        'Content-Type': 'application/json',
    };

    const response = await axios.post(url, data, { headers });
    return response.data;
};
