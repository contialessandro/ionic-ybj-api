import axios from 'axios';
import {config} from "../../config/config";

export const createPost = async (accessToken: string, title: string, intro: string, imageUrl: string,articleUrl:string) => {
    try{
        // const { uploadUrl, asset } = await registerImageUpload(accessToken);
        // Step 2: Upload image from S3
        const postUrl = 'https://api.linkedin.com/v2/ugcPosts';
        const postData = {
            author: config.linkedin.author, // replace with your LinkedIn ID
            lifecycleState: 'PUBLISHED',
            "specificContent": {
                "com.linkedin.ugc.ShareContent": {
                    "shareMediaCategory": "ARTICLE",
                    "media": [
                        {
                            "status": "READY",
                            "description": {
                                "text": `${intro}`
                            },
                            "originalUrl": `${articleUrl}`,
                            "title": {
                                "text": `${title}`
                            },
                            "thumbnails": [
                                {
                                    "resolvedUrl": `${imageUrl}`
                                }
                            ]
                        }
                    ],
                    "contentCallToAction": {
                        "action": "SEE_MORE",
                        "landingPage": `${articleUrl}`
                    },
                    "shareCommentary": {
                        "attributes": [],
                        "text": `intro - ${intro} - intro`
                    }
                }
            },
            visibility: {
                'com.linkedin.ugc.MemberNetworkVisibility': 'PUBLIC',
            },
        };
        console.log((postData.specificContent["com.linkedin.ugc.ShareContent"].media)[0].thumbnails);
        const response = await axios.post(postUrl, postData, {
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
                'X-Restli-Protocol-Version': '2.0.0',
            },
        });
/*
* "media": [
                        {
                            "media": `${imageUrl}`,
                            "status": "READY",
                            "title": {
                                "attributes": [],
                                "text": `${title}`
                            }
                        }
                    ]*/
        return response.data;
    }catch(error){
        console.log((error as any).message);
    }
};

export const registerImageUpload = async (accessToken: string) => {
    const registerUrl = 'https://api.linkedin.com/v2/assets?action=registerUpload';
    const requestData = {
        "registerUploadRequest": {
            "recipes": [
                "urn:li:digitalmediaRecipe:feedshare-image"
            ],
            "owner": config.linkedin.author, // Replace with your LinkedIn Profile ID
            "serviceRelationships": [
                {
                    "relationshipType": "OWNER",
                    "identifier": "urn:li:userGeneratedContent"
                }
            ]
        }
    };

    try {
        const response:any = await axios.post(registerUrl, requestData, {
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
                'X-Restli-Protocol-Version': '2.0.0',
            },
        });

        const {  asset } = response.data?.value;
        console.log('Asset URN:', asset);
        const { uploadUrl } = response.data.value?.uploadMechanism['com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest'];
        console.log('Upload URL:',  response.data.value?.uploadMechanism['com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest']);

        return { uploadUrl, asset };
    } catch (error) {
        if (error.response) {
            console.error('API Error:', error.response.data);
            throw new Error(`API Error: ${JSON.stringify(error.response.data)}`);
        } else {
            console.error('Error:', error.message);
            throw new Error(`Error: ${error.message}`);
        }
    }
};

// Upload the image to LinkedIn using the S3 URL
export const uploadImageFromS3 = async (uploadUrl: string, s3ImageUrl: string) => {
    try {
        // Fetch the image from S3
        const { data: imageData, headers } = await axios.get(s3ImageUrl, { responseType: 'arraybuffer' });

        // Extract Content-Type from the S3 response headers
        const contentType = headers['content-type'] || 'image/jpeg';  // Default to 'image/jpeg' if not provided

        // Upload the image to LinkedIn
        await axios.put(uploadUrl, imageData, {
            headers: {
                'Content-Type': contentType,  // Set appropriate content type
            },
        });

        console.log('Image uploaded successfully');
    } catch (error) {
        if (error.response) {
            console.error('API Error:', error.response.data);
            throw new Error(`API Error: ${JSON.stringify(error.response.data)}`);
        } else {
            console.error('Error:', error.message);
            throw new Error(`Error: ${error.message}`);
        }
    }
};
