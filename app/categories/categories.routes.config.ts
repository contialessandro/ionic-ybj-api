import {CategoriesController} from './controllers/categories.controller';
// import {CategoryMiddleware} from './middlewares/category.middleware';
// import {CommonPermissionMiddleware} from '../common/middlewares/common.permission.middleware';
// import {JwtMiddleware} from '../auth/middlewares/jwt.middleware';
import {Router} from 'express'

const router: Router = Router();
const categoriesController = new CategoriesController();
// const usersMiddleware = CategoryMiddleware.getInstance();
// const jwtMiddleware = JwtMiddleware.getInstance();
// const commonPermissionMiddleware = new CommonPermissionMiddleware();
router.get(`/categories`, [
    categoriesController.listCategory
]);
router.post(`/categories`, [
    categoriesController.createCategory
]);
router.get(`/categories/News`, [
    categoriesController.listNewsCategories
]);
router.get(`/categories/:categories`, [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
    // commonPermissionMiddleware.onlySameUserOrAdminCanDoThisAction,
    // usersMiddleware.validateCategoryExists,
    // usersMiddleware.extractUserId,
    categoriesController.getCategoryById
]);
// export class CategoryRoutes extends CommonRoutesConfig implements configureRoutes {
//     constructor(app: express.Application) {
//         super(app, 'UsersRoute');
//         this.configureRoutes();
//     }
//
//     configureRoutes() {
//
//         this.app.get(`/categories`, [
//             jwtMiddleware.validJWTNeeded,
//             commonPermissionMiddleware.onlyAdminCanDoThisAction,
//             categoriesController.listUsers
//         ]);
//
//         this.app.post(`/categories`, [
//             usersMiddleware.validateRequiredCreateUserBodyFields,
//             usersMiddleware.validateSameEmailDoesntExist,
//             categoriesController.createUser
//         ]);
//
//         this.app.put(`/categories/:categoryId`, [
//             jwtMiddleware.validJWTNeeded,
//             commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
//             commonPermissionMiddleware.onlySameUserOrAdminCanDoThisAction,
//             usersMiddleware.validateCategoryExists,
//             usersMiddleware.extractUserId,
//             categoriesController.put
//         ]);
//
//         this.app.patch(`/categories/:categories`, [
//             jwtMiddleware.validJWTNeeded,
//             commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
//             commonPermissionMiddleware.onlySameUserOrAdminCanDoThisAction,
//             usersMiddleware.validateCategoryExists,
//             usersMiddleware.extractUserId,
//             CategoriesController.patch
//         ]);
//
//         this.app.delete(`/categories/:categories`, [
//             jwtMiddleware.validJWTNeeded,
//             commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
//             commonPermissionMiddleware.onlySameUserOrAdminCanDoThisAction,
//             usersMiddleware.validateCategoryExists,
//             usersMiddleware.extractUserId,
//             categoriesController.removeUser
//         ]);
//         this.app.get(`/categories/:categories`, [
//             jwtMiddleware.validJWTNeeded,
//             commonPermissionMiddleware.minimumPermissionLevelRequired(CommonPermissionMiddleware.BASIC_PERMISSION),
//             commonPermissionMiddleware.onlySameUserOrAdminCanDoThisAction,
//             usersMiddleware.validateCategoryExists,
//             usersMiddleware.extractUserId,
//             categoriesController.getUserById
//         ]);
//     }
//
//
// }
export const CategoryRouter: Router = router;
