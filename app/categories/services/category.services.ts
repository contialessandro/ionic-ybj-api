import {CRUD} from '../../common/interfaces/crud.interface';
import {GenericInMemoryDao} from '../daos/in.memory.dao';
import {CategoryDao} from '../daos/category.dao';

export class CategoryService implements CRUD {
    private static instance: CategoryService;
    dao: GenericInMemoryDao;

    constructor() {
        this.dao = GenericInMemoryDao.getInstance();
    }

    static getInstance(): CategoryService {
        if (!CategoryService.instance) {
            CategoryService.instance = new CategoryService();
        }
        return CategoryService.instance;
    }

    create(resource: any) {
        return CategoryDao.getInstance().addCategory(resource);
    }

    deleteById(resourceId: any) {
        return CategoryDao.getInstance().removeCategoryById(resourceId);
    };

    list(limit: number, page: number) {
        return CategoryDao.getInstance().listCategories(limit, page);
    };

    listNews(limit: number, page: number) {
        return CategoryDao.getInstance().listNewsCategories(limit, page);
    };

    patchById(resource: any) {
        return CategoryDao.getInstance().patchCategory(resource);
    };

    readById(resourceId: any) {
        return CategoryDao.getInstance().getCategoryById(resourceId);
    };

    getCategoryByOriginalId(resourceId: any) {
        return CategoryDao.getInstance().getCategoryByOriginalId(resourceId);
    };

    updateById(resource: any) {
        return CategoryDao.getInstance().patchCategory(resource);
    };

    async getCategoryByName(name: string) {
        return await CategoryDao.getInstance().getCategoryByName(name);
    }
}
