import express from 'express';
import {CategoryService} from '../services/category.services';

export class CategoryMiddleware {
    private static instance: CategoryMiddleware;

    static getInstance() {
        if (!CategoryMiddleware.instance) {
            CategoryMiddleware.instance = new CategoryMiddleware();
        }
        return CategoryMiddleware.instance;
    }

    validateRequiredCreateCategoryBodyFields(req: express.Request, res: express.Response, next: express.NextFunction) {
        if (req.body && req.body.type && req.body.name && req.body.color && req.body.thread_meta_id) {
            next();
        } else {
            res.status(400).send({error: `Missing required fields name and type`});
        }
    }

    async validateSameCategoryDoesntExist(req: express.Request, res: express.Response, next: express.NextFunction) {
        // const categoryService = CategoryService.getInstance();
        // const category = await categoryService.getCategoryByName(req.body.name);
        // if (category) {
        //     res.status(400).send({error: `User email already exists`});
        // } else {
        //     next();
        // }
    }

    async validateCategoryExists(req: express.Request, res: express.Response, next: express.NextFunction) {
        const categoryService = CategoryService.getInstance();
        const category = await categoryService.readById(req.params.categoryId);
        if (category) {
            next();
        } else {
            res.status(404).send({error: `User ${req.params.userId} not found`});
        }
    }

    async extractUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        req.body._id = req.params.userId;
        next();
    }
}
