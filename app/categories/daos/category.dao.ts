import {MongooseService} from '../../common/services/mongoose.service';
import * as shortUUID from "short-uuid";
import {
    CategoryModel,
    LocationModel,
    MediaModel,
    ThreadMetaModel,
    ThreadModel
} from "../../json-schemas/models.schemas";

export class CategoryDao {
    mongooseService: MongooseService = MongooseService.getInstance();
    private static instance: CategoryDao;

    Schema = this.mongooseService.getMongoose().Schema;

    categorySchema = new this.Schema({
            _id: String,
            id: Number,
            type: String,
            name: String,
            color: String
        }, {timestamps: true}
    );

    Location = LocationModel;
    Category = CategoryModel;
    Thread = ThreadModel;
    ThreadMeta = ThreadMetaModel;
    Media = MediaModel;


    constructor() {
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance = new CategoryDao();
        }
        return this.instance;
    }

    async addCategory(categoryFields: any) {
        categoryFields._id = shortUUID.generate();
        const category = new this.Category(categoryFields);
        await category.save();
        return categoryFields._id;
    }

    async getCategoryByName(name: string) {
        return this.Category.findOne({name: name});
    }

    async removeCategoryById(userId: string) {
        await this.Category.deleteOne({_id: userId});
    }

    async getCategoryById(categoryId: string) {
        return this.Category.findOne({_id: categoryId});
    }

    async listCategories(limit: number = 25, page: number = 0) {
        return this.Category.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }
    async getCategoryByOriginalId(id:any)  {
        return this.Category.findOne({id: +id});
    }
    async listNewsCategories(limit: number = 25, page: number = 0) {
        return this.Category.find({'type': 'News'})
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async patchCategory(CategoryFields: any) {
        // const category: CategoryDao = await this.Category.findById(CategoryFields._id);
        // if(category){
        //     for (const i in CategoryFields) {
        //         category[i] = CategoryFields[i];
        //     }
        //     return await category.save();
        // }
    }
}

