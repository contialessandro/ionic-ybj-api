import express from 'express';
import {CategoryService} from '../services/category.services';

export class CategoriesController {
    constructor() {
    }

    async listCategory(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        const users = await categoryService.list(100, 0);
        res.status(200).send(users);
    }

    async listNewsCategories(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        const users = await categoryService.listNews(100, 0);
        res.status(200).send(users);
    }

    async getCategoryById(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        const category = await categoryService.readById(req.params.categoryId);
        res.status(200).send(category);
    }

    async createCategory(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        const ids = [];
        for(const cat of req.body){
            const userId = await categoryService.create(cat);
            ids.push({_id: userId});
        }
        req.body.permissionLevel = 1 + 2 + 4 + 8;
        res.status(201).send(ids);
    }

    async patch(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        await categoryService.patchById(req.body);
        res.status(204).send(``);
    }

    async put(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        await categoryService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeCategory(req: express.Request, res: express.Response) {
        const categoryService = CategoryService.getInstance();
        await categoryService.deleteById(req.params.userId);
        res.status(204).send(``);
    }

}
