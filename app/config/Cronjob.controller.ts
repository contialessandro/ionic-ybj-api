import {PostsServices} from '../posts/services/posts.services';
import {FetchTask} from "./AudioTaskCronJob.controller";

export const PublishArticle = async () => {
    console.log('cronjob started');
    try {
        const date = new Date();
        const controller = PostsServices.getInstance();
        const postToPublish = await controller.getPostTobePublishedToday(date);
        console.log(postToPublish);
        if (!postToPublish) {
            console.log('nothing to be published');
            return Promise.resolve('nothing to be published');
        }
        await FetchTask();
        postToPublish.hide = false;
        // if(postToPublish?.audio_url){
        //     const podcastTitle = `podcast-feed/${postToPublish.slug}.mp3`;
        //     const episode = await spotify.FindEpisodeByTitle(podcastTitle).then(r => r);
        //     postToPublish.audio_url= episode.external_urls.spotify;
        // }
        await controller.updateById(postToPublish);
        return Promise.resolve(postToPublish);
    } catch (e) {
        const err: any = e;
        console.log(err.message);
        return Promise.reject(err);
    }
}

