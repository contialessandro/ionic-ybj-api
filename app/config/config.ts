interface InstagramConfig {
    client_secret: string;
    app_id: string;
    client_id: string;
    redirect_uri: string;
    user_id: string;
}
export const config={
    aws_profile: {
        region: 'eu-west-2',
        signature_version: 'v4',
        public_images_bucket: "ybjasset",
        audio_recording_bucket: "audio-recording",
        profile: `wsl-cli`
    },
    aws_profile_vpn: {
        region: 'eu-south-1',
        signature_version: 'v4',
        profile: `default`,
    },
    "environment": process.env.NODE_ENV,
    s3BucketBaseUrl: 'https://ybjlaravel.s3.eu-west-2.amazonaws.com',
    JWT_SECRET: '9BRNHSZDcNfkDdGxdLqs7JlP4BvH8BWbsrtqwrPP9BT2K6kT8THG2lR57gkpxZTTzzqFcQBl24Ptr3VSBStvcr3skz5C868Ql6JnrQjJdRBXrjwk3ZgQr6L9SpDFwDlDPtMmR27Zw4GBWRvcbRTk3sDf8PRbPdxhKl8qd7h6F4s9wtGNg3ztcxPGz3HPDhxf9x4Qj6K9mj4DH4plxT6SMB7TCqkc4rpDvVxvvv2x6Kts8gvs4RjwhwMqH7WvmQkWQzMcvhsS3jZtFJd5wnLGszdnNS25d4dx3v8Z2MgnWv5FGJCNnGMCDczBbhlNB3dQxNmcjX5hTg7s3HsLtPCTQp9MMHkthGSsFm3DVtvlmkp8V59w5GTM4WbnhKSwr2KnrL2qLdznwmvWXS5pmNRWnjdsXVGglJCtTg7sJZFWP7LVj89M6vvWCkFRqgLhF8jZ7cj5k2FpKZP2MNdCzMzHZl9vBs2FfCl2p3WXXgxWn39wBNlfFbb95NTZGmHX2N53gxpTvnXV5jk4qZPrg8SxVQggMlS7KSGXgkLvJWBQXCpdM5qmWNlmTcF98H9XrCdfgQ28pr5NSl3pMDNQjMqNnVrqXdDhFSZGks6H4jxJfBp9qZZMDKv6SHPt9C727fsJPDkl2kn6MDkmQmBC2dcB5zHjrz2tXrjFDsmlngcCPNmXXTZrV2x5RqvbrVHxwCKT4bzWWV9NlcZlcXJ3j8hvQr4xp8shGZ9flqkdjjQfhMfMjLw9MRxgm6nrnsqPpDRXcDHzv9H4D3sF4lqsKpCLXJwrFr6Krzq52HR9RwDzfvmMNgjlLznXznH6NvNsf8XmSm3mwmMRtBgfc5ddPVDTD49G7pnjVsrPxzVMsdgMWbNWNhHmJ6FxF6gRLnQDRWMWDSDCkbnWMnmCvM8mn3JmP6xwgKCHBrtJvBPvb8kHpqvnjj4DnKDRBLMJmtQV8RDJnHT7QtDXblM2cM4WHvp4hPC68XpzppCJpL3TpKLq3lbsVGP8WqqGN98hSc2ZKbDJnQKzLxn7P7M2D5MdgTwML2t9km9kvKVVzn7tbdg5cG5x4khcqj9CFmWpZwcf6ffjQScwGDxv8rX3ll9zZR3hdT3RNLMLWr38MMlkcPM2N3wmhgWz68D9zCW5HD9MLTQ5fkWF7xSbQ23zNWJfTc7fKn9Km6nHJfbrRccVMjXDXp26VMr3zX2knQZnMC5MGql4mxXlVb5LLgZcTsDRzVCcHRHFLJtxDqq77Bbb4FP248HgDlzpGWRLXnmPGTcPFCHvF63X2mJK5qRld6QXPkt8t8TpbcXCRczttn6vthHJSJtPbJM8dqJg7gJPLMQd7HpFvF8pRQHfPPMmptTsx26ZrZtPFQqhzqCp9RH8LkJDB4GL5Gv4cnTsCGzXnVsWbBvDl3DjgDnDX6kMMq6lFCvkgZCv9fHjfhBJF4SLGLGPvWsXVrSsXCPPxspCLlNSQQG6Fw7Ddczvh9PCdtGQX9457wWZ372RpBldtzqdqSP5Nspnw3VbTCtSDGhpQZWWmMqtnhxMMg2c6JtCmL9QmMHqvrZR8pMslRqPLLVPcvJL3PpS2Jx5GHPkFVTWzN9TxmqR6tMc6MN7GbLL6RjRw2XZbxQrZ5Jv2NpWdsrF5Xvtk35TNG4RqcBXP7ljJzrpswXVqjgvhtGBR54JdvN98QQXMTPwvzkvHjWQDZF2kMRJzK5WgT4mXzk5RZS8mwqK63PBbv27Zj97LmWqzJDWs3lMK9hst8sfSBDlwNP6KFXZ8tpsHwgqwzGCCtgpVcc7ZM3xL34756xjH5DbhKvVtW82xfBsXT2LWX25mhkwBW9tHX2RZgLdqjhpZPHZwC4zkSmQXKxWRJ4HM6GWwHZzXkVddC9qfPvX6WMGSPNq8jcBpHDxC6qZNdPmtTsQ24dLWDBG74rDVRVkwz6KkQRSvkh6wdVLh8KxC7dMzRMXTNcnmDQ6clqzZkBKWNJKJHlN3L6Wf628SJ8CbRGHqtGt',
    SALT: 15,
    spotify: {
        client_id: 'dae16843ec484963b49c170b71cf45c8',
        client_secret: '2c11e0ad3d034713b7a9171172e9851d'
    },
    linkedin:{
        client_id: '781cubj3crk65f',
        client_secret: 'd5fkDdUdcevBpHqU',
        callback: 'http://localhost:3000/admin/linkedin/callback',
        author: `urn:li:person:E4MyntRqgi`
    },
    twitter: {
        apiKey: 'R99mnmoV4Cn6QxXyhrTe7XJHH',
        apiSecretKey:'CxPU5SqhG90rLN18bqavx7iQcMbDxG9TUNR6fUnL0kP469TYqz',
        accessToken: process.env.TWITTER_ACCESS_TOKEN || '',
        accessTokenSecret: process.env.TWITTER_ACCESS_TOKEN_SECRET || '',
        client_id: 'ZHJIa3V0ZUd3MWlsZ0p6cVJTVTY6MTpjaQ',
        client_secret: '-7WVDx9j_vWVKBj6nkre_pngYjQedujZS0yIpViyWEmYz5095D'
    }
}

//{"token_type":"bearer","access_token":"AAAAAAAAAAAAAAAAAAAAAClKuwEAAAAAVVyJqTtEy1Jl32ozhG5LYBOjXOE%3Dh4PG8kUiMEZvEwrBw6rXCQJM1Tn7QHrjHwb9o7Uy2vEMvAJNPK"}

/*
*
* {"access_token":"EAAHHfwFxz0gBO3hZBEFw7IVPkLivZAiRA4XHCeVSYNSNQGxyY7MYToOyZAZCHQhUJaKazB8fu3Pl5SPgBnVZBpEEHkQm2ylfAOfR6THPgl2iJb02wgxgpXIYWwbQOltrIJZCZCupWhhujBWytp7SHIT699ITWGpQOJCr70qaZB6ZAnFRHadctERkaFwZDZD","token_type":"bearer","expires_in":5184000}
*
* {
  "name": "Alessandro Conti",
  "id": "10236196031109738"
}
*
* media id 18060507808981531
*
* */
