import {NextFunction, Request, Response} from 'express';
import jwt from 'jsonwebtoken';
import {config} from './config';


export const VerifyPayload=async (credential: string, req: Request) => {
    const user=jwt.verify(credential, config.JWT_SECRET);
    if (user) {
        req.body.auth=user;
        return true;
    }
}


export const VerifyJWT=async (req: Request, resp: Response, next: NextFunction) => {
    try {
        const credential=CheckAuthorizationHeader(req);
        if (await VerifyPayload(credential, req)) return next();

        throw new Error('Access denied. Authorization token invalid.');
    } catch (err) {
        console.log(err);
        return resp.status(401).send({
            message: `Access denied. ${(err as any
            ).message}`
        });
    }
};

export const GenerateJWT=(payload: any, expiresIn: string='3600000m') => {
    try {
        return Promise.resolve(jwt.sign(payload, config.JWT_SECRET, {expiresIn: expiresIn}));
    } catch (err) {
        return Promise.reject(err);
    }
};

export const CheckAuthorizationHeader=(req: Request) => {
    const credential = req.headers.authorization;
    if (!credential) throw new Error('Access denied. No token provided.');
    return credential;
}
