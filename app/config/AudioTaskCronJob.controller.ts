import {PostsServices} from "../posts/services/posts.services";
import {PollySynth} from "../aws/PollyToS3";

export const FetchTask = async () => {
    try {
        const postService = PostsServices.getInstance();
        const tasks = await postService.getAllTasks();
        if (tasks) {
            for (const task of tasks) {
                if (task?.status !== 'completed') {
                    const object: any = await PollySynth.asyncAudioTaskProcessor(task);
                    task.status = object.taskStatus;
                    await postService.patchAudioTask(task);
                    const post: any = await postService.getPostByAudioTaskId(task.task_id);
                    if (post) {
                        post.audio_url = object?.audio_url;
                        if (object.taskStatus === 'completed') {
                            await postService.deleteAudioTaskById(task.task_id);
                            console.log(`task: ${task.task_id} is completed and deleted`);
                            await postService.patchById(post);
                        }
                    }
                }
            }
        }
    } catch (e) {
        const err: any = e;
        console.log(err.message);
        return Promise.reject(err);
    }
}
