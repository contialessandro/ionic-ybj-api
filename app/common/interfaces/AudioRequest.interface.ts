export interface AudioRequestInterface {
    body: string,
    slug: string,
    id: number
}
