import {config} from "../../config/config";
import SpotifyWebApi from "spotify-web-api-node";

export class SpotifyService {
    clientId: string = '';
    secretId: string = ''
    accessToken: string = ''
    episodes: any;
    spotifyApi: SpotifyWebApi;

    constructor() {
        this.clientId = config.spotify.client_id;
        this.secretId = config.spotify.client_secret;
        this.spotifyApi = new SpotifyWebApi({
            clientId: config.spotify.client_id, clientSecret: config.spotify.client_secret,
        });
    }

    GetAccessToken() {

    }

    async GetEpisodesList() {
        this.episodes = await this.spotifyApi
            .clientCredentialsGrant()
            .then(async (data) => {
                this.spotifyApi.setAccessToken(data.body.access_token);
                this.accessToken = data.body.access_token;
                return await this.spotifyApi.getShow('6VDiEGTiJXBahslcPbbD4h', {market: 'GB'}).then((r: any) => {
                    return r.body?.episodes?.items ?? []
                });
            });
    }

    async FindEpisodeByTitle(title: string) {
        this.GetAccessToken();
        await this.GetEpisodesList();
        return this.episodes.find((e: any) => e.name.toString().toLowerCase() === title.toString().toLowerCase()) ?? null;
    }

    axiosApiCall() {
        const axios = require('axios');

        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://api.spotify.com/v1/search?q=Yorkshire Business Journal&type=show&market=GB&limit=20',
            headers: {
                'Authorization': 'Bearer ' + this.spotifyApi.getAccessToken()
            }
        };

        axios(config)
            .then((response: { data: any; }) => {
                // const episodeId = response.data.shows.items[2]['id'];
                // // console.log(episodeId);
                let config = {
                    method: 'get',
                    maxBodyLength: Infinity,
                    url: 'https://api.spotify.com/v1/shows/6VDiEGTiJXBahslcPbbD4h',
                    headers: {
                        'Authorization': 'Bearer BQAGc5DaCuE-uhDD0kaQsg_sf17FqDeiBb2H1W0FYBscq4kUo0Iaw5bbEvjuE1zObr98znPAD0Hk6ErUmPphh6-ZbSxTxhOdyjN-Q-3aP1ZDOeuM6DnM'
                    }
                };

                axios(config)
                    .then(function (response: { data: any; }) {
                    })
                    .catch(function (error: any) {
                        console.log({m: error.message});
                    });

            })
            .catch(function (error: any) {
                console.log(error);
            });
    }
}

export const spotify: SpotifyService = new SpotifyService()
