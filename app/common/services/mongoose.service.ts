import mongoose from 'mongoose';

export class MongooseService {
    private static instance: MongooseService;

    options = {
        autoIndex: false,
        poolSize: 10,
        bufferMaxEntries: 0,
        useNewUrlParser: true,
        useUnifiedTopology: true
    };
    count = 0;

    constructor() {
        this.connectWithRetry();
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance = new MongooseService();
        }
        return this.instance;
    }

    getMongoose(){
        return mongoose;
    }


    connectWithRetry() {
        console.log('MongoDB connection with retry');
        mongoose.set('useFindAndModify', false);
        mongoose.connect("mongodb+srv://dev:zWx9bkX7Jorh3pUq@cluster0.xd9kn.mongodb.net/ybj?authSource=admin&replicaSet=atlas-b8iql8-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true",
            this.options).then(() => {
            console.log('MongoDB is connected @cluster0.xd9kn.mongodb.net/ybj')
        }).catch(err => {
            console.log(`MongoDB connection unsuccessful + ${err}, retry after 5 seconds. `, ++this.count);
            setTimeout(this.connectWithRetry, 5000)
        })
    };

}
