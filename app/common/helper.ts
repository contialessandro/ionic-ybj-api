import {Router} from 'express';
import Hashids from 'hashids';
import bcrypt from 'bcrypt';
import {config} from '../config/config';

const router: Router=Router();
// const btoa=(str: string) => Buffer.from(str, 'binary').toString('base64');
// const atob=(str: string) => Buffer.from(str, 'base64').toString('binary');
const hashid=new Hashids('authenticationMiddleware', 25, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890:=@.');

class Helper {
    GetRouterList=() => {
        return router.stack.forEach(function (r: any) {
            if (r.route && r.route.path) {
                // console.log(r.route.method)
                // console.log(r.route.path)
            }
        });
    }
    Encode=(plaintext: string) => {
        const hex=Buffer.from(plaintext, 'utf8').toString('hex');
        return this.EncodeHashId(hex);
    }
    Decode=(base64string: string) => {
        const hex=this.DecodeHashId(base64string);
        return Buffer.from(hex, 'hex').toString('utf8');
    }
    EncodeHashId=(stringToEncode: string) => {
        return hashid.encodeHex(stringToEncode);
    }
    DecodeHashId=(stringToDecode: string) => {
        return hashid.decodeHex(stringToDecode);
    }
    Delay=(ms: number) => new Promise(res => setTimeout(res, ms));
    GetPassword=async (password: string) => {
        const salt=await bcrypt.genSalt(config.SALT);

        // Replace the password with the hash
        return bcrypt.hashSync(password, salt);
    }
}

export const helperFunction=new Helper();
