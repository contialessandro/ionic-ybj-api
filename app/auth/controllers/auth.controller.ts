import express from 'express';

const jwt = require('jsonwebtoken');
const crypto = require('crypto');


// todo: move to a secure place
const jwtSecret = 'My!@!Se3cr8tH4sh';
const tokenExpirationInSeconds = 36;

export class AuthController {
    constructor() {
    }

    async createJWT(req: express.Request, res: express.Response) {
        try {
            const refreshId = req.body.userId + jwtSecret;
            const salt = crypto.randomBytes(16).toString('base64');
            const hash = crypto.createHmac('sha512', salt).update(refreshId).digest("base64");
            req.body.refreshKey = salt;
            const token = jwt.sign(req.body, jwtSecret, {expiresIn: tokenExpirationInSeconds});
            const b = Buffer.from(hash);
            const refreshToken = b.toString('base64');
            return res.status(201).send({accessToken: token, refreshToken: refreshToken});
        } catch (err) {
            return res.status(500).send(err);
        }
    }
}
