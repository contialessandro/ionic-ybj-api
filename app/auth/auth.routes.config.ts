import {Router} from 'express';
import {JwtMiddleware} from "./middlewares/jwt.middleware";
import {AuthMiddleware} from "./middlewares/auth.middleware";
import {AuthController} from "./controllers/auth.controller";
const authController = new AuthController();
const authMiddleware = AuthMiddleware.getInstance();
const jwtMiddleware = JwtMiddleware.getInstance();
const router: Router = Router();

router.post(`/auth/refresh-token`, [
    jwtMiddleware.validJWTNeeded,
    jwtMiddleware.verifyRefreshBodyField,
    jwtMiddleware.validRefreshNeeded,
    authController.createJWT
]);
router.post(`/auth`, [
    authMiddleware.validateBodyRequest,
    // authMiddleware.verifyUserPassword,
    authController.createJWT
]);
export const AuthRouter: Router = router;
