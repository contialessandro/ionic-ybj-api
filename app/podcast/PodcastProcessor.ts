import AWS, {S3} from 'aws-sdk';
import {createInterface} from 'readline';
import {PublicImagesBucket} from "../aws/Bucket";
import Parser from "rss-parser";
import {config} from "../config/config";
import {Feed, FeedOptions} from 'feed';
import * as xmlbuilder from 'xmlbuilder';
import shortUUID from "short-uuid";
import {DateTime} from "luxon";


interface PodcastServiceOptions {
    bucketName: string;
    feedBucketName: string;
    feedUrlPath: string;
    feedTitle: string;
    feedDescription: string;
    feedLanguage: string;
    feedPubDate: string;
    feedTtl: number;
}

interface Episode {
    title: string;
    description: string;
    url: string;
    date: any;
    objectProperties: {
        size: string;
        Key: string;
        LastModified: any;
    }
}

const parser = new Parser({
    customFields: {
        item: ['enclosure'],
    },
});

export class PodcastProcessor {
    private readonly bucketName: string;
    private readonly feedBucketName: string;
    private readonly feedUrlPath: string;
    private readonly feedTitle: string;
    private readonly feedDescription: string;
    private readonly feedLanguage: string;
    private readonly feedPubDate: string;
    private readonly feedTtl: number;
    private readonly s3: S3;

    constructor(options: PodcastServiceOptions) {
        // Assign options to class properties
        this.s3 = new AWS.S3({
            signatureVersion: config.aws_profile.signature_version,
            region: config.aws_profile.region,
            params: {Bucket: PublicImagesBucket}
        });
        this.bucketName = options.bucketName;
        this.feedBucketName = options.feedBucketName;
        this.feedUrlPath = options.feedUrlPath;
        this.feedTitle = options.feedTitle;
        this.feedDescription = options.feedDescription;
        this.feedLanguage = options.feedLanguage;
        this.feedPubDate = options.feedPubDate;
        this.feedTtl = options.feedTtl;
    }

    public async listEpisodes(): Promise<Episode[]> {
        const params = {
            Bucket: this.bucketName,
            Prefix: 'podcast-feed'
        };
        const s3 = new AWS.S3({
            signatureVersion: config.aws_profile.signature_version,
            region: config.aws_profile.region,
            params
        });
        const data: any = await s3.listObjectsV2(params).promise();
        const episodes: Episode[] = [];
        for (const object of data.Contents) {
            if (object.Key.includes('.mp3')) {
                const {Key, LastModified, Size} = object;
                const date = DateTime.fromJSDate(object.LastModified);//Thu, 1 Sept 2022 15:00:40 GMT
                const humanReadable = date.setLocale('en-GB').toLocaleString({
                    weekday: 'short',
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: '2-digit',
                    second: 'numeric',
                    timeZoneName: 'short'
                });
                if (Key === this.feedUrlPath) {
                    continue;
                }
                const url = `https://${this.bucketName}.s3.eu-west-2.amazonaws.com/${Key}`;
                const stream = this.s3.getObject({Bucket: this.bucketName, Key}).createReadStream();
                const rl = createInterface({
                    input: stream,
                });
                let title = '';
                let description = '';
                for await (const line of rl) {
                    if (title && description) {
                        break;
                    }

                    if (line.startsWith('Title: ')) {
                        title = line.substr('Title: '.length);
                    } else if (line.startsWith('Description: ')) {
                        description = line.substr('Description: '.length);
                    }
                }
                episodes.push({
                    title,
                    description,
                    url,
                    date: "Sat, 11 Mar 2023 14:03:52 GMT",
                    objectProperties: {
                        size: Size,
                        Key,
                        LastModified: "Sat, 11 Mar 2023 14:03:52 GMT"
                    }
                });
            }
        }
        return episodes;
    }

    public async createEpisode(title: string, description: string, filePath: string, filename: string, slug: string): Promise<void> {
        const Key = `${filename}`;
//        const stream = createReadStream(filePath);
        const params = {
            Bucket: this.bucketName,
            Key: `${slug}/audio/${filename}`
        };
        const stream = this.s3.getObject(params).createReadStream();
        console.log(stream);
        const metadata = {
            'x-amz-meta-title': title,
            'x-amz-meta-description': description,
        };
        try {
            this.s3.upload({
                Bucket: this.feedBucketName,
                Key,
                Body: stream,
                Metadata: metadata,
            })
                .promise()
                .then(r => r);
        } catch (e) {
            return Promise.reject(e);
        }
    }

//            extras:'https://promocards.byspotify.com/share/4b31ad5bbd938c8cd5d6272487c86ea96b716870',
    public async updateFeed(): Promise<void> {
        const s3Object: Episode[] = await this.listEpisodes();
        const options: FeedOptions = {
            title: 'Yorkshire Business Journal',
            description: 'A weekly news website',
            id: 'https://www.yorkshirebusinessjournal.co.uk/',
            link: 'https://www.yorkshirebusinessjournal.co.uk/',
            language: 'en',
            image: 'https://img.rss.com/yorkshire-business-journal/528/20230205_120240_b05c17ca2a9f9bcef5ae9d7299c3b407.jpg',
            favicon: 'https://img.rss.com/yorkshire-business-journal/528/20230205_120240_b05c17ca2a9f9bcef5ae9d7299c3b407.jpg',
            updated: new Date(),
            generator: 'YBJ',
            author: {
                name: "YBJ Podcast",
                email: "info@yorkshirebusinessjournal.co.uk",
                link: 'https://yorkshirebusinessjournal.co.uk',
            },
            feedLinks: {
                rss: 'https://ybjlaravel-dev.s3.eu-west-2.amazonaws.com/podcast',
            },
            copyright: 'YBJ',
        };
        let feed: Feed = new Feed(options);

        const episodeUrl = 'https://ybjlaravel-dev.s3.eu-west-2.amazonaws.com/arma-litigation/audio/arma-litigation.mp3';
        // Get all audio files from S3 bucket
        feed = await this.pushItems(episodeUrl, feed);
        // Serialize feed to XML and save to S3 bucket
        const feedXml = this.feedToXml(feed);
        await this.saveFeedXml(feedXml);
    }

    feedToXml(feed: Feed): string {
        /*feed.options.author.email="admin@yorkshirebusinessjournal.co.uk";
        feed.options.author.name="YBJ Podcast";
        feed.options.author.link='https://yorkshirebusinessjournal.co.uk';
        */
        const feedXml = xmlbuilder.create('rss', {version: '1.0', encoding: 'UTF-8'})
            .att('xmlns:dc', "http://purl.org/dc/elements/1.1/")
            .att('xmlns:content', "http://purl.org/rss/1.0/modules/content/")
            .att('xmlns:atom', 'http://www.w3.org/2005/Atom')
            .att('xmlns:itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd')
            .att('xmlns:anchor', 'https://anchor.fm/xmlns')
            .att('version', '2.0');
        const channel = feedXml.ele('channel');
        channel.ele('title', feed.options.title);
        channel.ele('description', feed.options.description);
        channel.ele('link', feed.options.feedLinks.rss);

        const image = channel.ele('image');
        image.ele('url', feed.options.image);
        image.ele('title', feed.options.title);
        image.ele('link', 'https://rss.com/podcasts/yorkshire-business-journal');


        channel.ele('generator', feed.options.generator);
        channel.ele('lastBuildDate', feed.options.updated);
        channel.ele('atom:link').attribute('href', feed.options.feedLinks.rss + '.xml').att('rel', 'self').att('type', "application/rss+xml");
        channel.ele('copyright', feed.options.copyright);
        channel.ele('language', feed.options.language);
        channel.ele('itunes:author', 'Yorkshire Business Journal');
        channel.ele('itunes:summary', feed.options.title);
        const cat = channel.ele('itunes:category');
        cat.attribute('text', 'Business');
        channel.ele('itunes:type', 'episodic');
        const itunesOwner = channel.ele('itunes:owner')
        itunesOwner.ele('itunes:name', feed?.options?.author?.name)
        itunesOwner.ele('itunes:email', feed?.options?.author?.email);
        channel.ele('itunes:image').att('href', feed?.options?.image);
        // Inject author element
        const author = channel.ele('author');
        author.ele('name', feed?.options?.author?.name);
        author.ele('email', feed?.options?.author?.email);
        author.ele('link', feed?.options?.author?.link);

        const feedLink = channel.ele('feedLinks');
        feedLink.ele('rss', feed.options.feedLinks.rss);


        for (const item of feed.items) {
            //                    url: episodeUrl,
            //                     size: episodeFileSize,
            //                     type: episodeMimeType,
            const itemXml = channel.ele('item');
            itemXml.ele('enclosure').att('url', item.enclosure?.url).att('type', 'audio/mpeg');
            itemXml.ele('title', item.title);
            itemXml.ele('description', item.description);
            itemXml.ele('link', item.link);
            itemXml.ele('pubDate', item.published);
            itemXml.ele('guid', shortUUID.uuid());
        }

        return feedXml.end({pretty: true, indent: ' '});
    }

    private async pushItems(episodeUrl: string, feed: Feed) {
        const s3Objects = await this.listEpisodes();
        // Sort S3 objects by most recent modified date first
        const sortedS3Objects = s3Objects.sort((a, b) => b.objectProperties.LastModified - a.objectProperties.LastModified);
        // Add S3 objects to feed as episodes
        sortedS3Objects.forEach((s3Object, index) => {
            const episodeFileSize = s3Object.objectProperties.size;
            const episodeMimeType = 'audio/mpeg';
            console.log(s3Object);
            let episodeKey = `${s3Object.objectProperties.Key}`;
            const episodeUrl = `https://${this.bucketName}.s3.eu-west-2.amazonaws.com/${episodeKey}`;
            //Key: `${slug}/audio/${filename}`
            episodeKey.replace('episode-feed', '').replace('.mp3', '');
            const episode = {
                title: episodeKey,
                id: episodeFileSize,
                url: episodeUrl,
                published: s3Object.objectProperties.LastModified,
                date: s3Object.objectProperties.LastModified,
                description: '',
                content: 'string',
                enclosure: {
                    url: episodeUrl,
                    size: episodeFileSize,
                    type: episodeMimeType,
                },
                guid: '',
                link: '',
                copyright: 'YBJ'
            };

            feed.items.push(episode);
        });
        return feed;
    }

    private async saveFeedXml(xml: string): Promise<void> {
        await this.s3
            .upload({
                Bucket: this.bucketName,
                Key: this.feedUrlPath + '.xml',
                Body: xml,
                ContentType: 'application/rss+xml',
            })
            .promise();
        const uploadResponse = await this.s3
            .upload({
                Bucket: this.bucketName,
                Key: this.feedUrlPath,
                Body: xml,
                ContentType: 'application/rss+xml',
            })
            .promise();

        console.log(`Feed XML saved to S3 bucket: ${uploadResponse.Location}`);
    }
}

export const podcast: PodcastProcessor = new PodcastProcessor({
    bucketName: 'ybjlaravel-dev',
    feedBucketName: 'ybjlaravel-dev/podcast-feed',
    feedUrlPath: 'podcast',
    feedTitle: 'title',
    feedDescription: 'desc',
    feedLanguage: 'xml',
    feedPubDate: Date.now().toLocaleString(),
    feedTtl: 0
});
