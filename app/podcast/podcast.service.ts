//
// // Instantiate the PodcastService class with your own settings
// const podcastService = new PodcastProcessor({
//     s3: {
//         accessKeyId: '<your_access_key_id>',
//         secretAccessKey: '<your_secret_access_key>',
//         region: '<your_region>',
//     },
//     bucketName: '<your_bucket_name>',
//     feedUrlPath: '<your_feed_url_path>',
//     feedTitle: '<your_feed_title>',
//     feedDescription: '<your_feed_description>',
//     feedLanguage: '<your_feed_language>',
//     feedPubDate: new Date().toISOString(),
//     feedTtl: 60,
// });
//
// // Call the updateFeed() method periodically (e.g. every hour)
// setInterval(() => {
//     podcastService.updateFeed();
// }, 60 * 60 * 1000);
