// import {CommonPermissionMiddleware} from '../common/middlewares/common.permission.middleware';
// import {JwtMiddleware} from '../auth/middlewares/jwt.middleware';

import {Router} from 'express';

import {LocationsController} from "./controllers/locations.controller";
import {LocationsMiddleware} from "./middlewares/locations.middleware";

const router: Router = Router();
const locationController = new LocationsController();
const locationMiddleware = LocationsMiddleware.getInstance();
// const jwtMiddleware = JwtMiddleware.getInstance();
// const commonPermissionMiddleware = new CommonPermissionMiddleware();
router.get('/locations', [
    // jwtMiddleware.validJWTNeeded,
    // commonPermissionMiddleware.onlyAdminCanDoThisAction,
    locationController.listLocation
]);
router.post(`/locations`, [
    locationMiddleware.validateSameLocationDoesntExist,
    locationController.createLocation
]);
router.get(`/location/:locationId`, [
    locationController.getLocationById
]);

router.get(`/location/:locationName`, [
    locationController.getLocationByName
]);

export const LocationsRouter: Router = router;
