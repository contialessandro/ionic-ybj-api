import express from 'express';
import {LocationsServices} from "../services/locations.services";

export class LocationsMiddleware {
    private static instance: LocationsMiddleware;

    static getInstance() {
        if (!LocationsMiddleware.instance) {
            LocationsMiddleware.instance = new LocationsMiddleware();
        }
        return LocationsMiddleware.instance;
    }

    validateRequiredCreateLocationBodyFields(req: express.Request, res: express.Response, next: express.NextFunction) {
        if (req.body && req.body.id && req.body.name) {
            next();
        } else {
            res.status(400).send({error: `Missing required fields name and type`});
        }
    }

    async validateSameLocationDoesntExist(req: express.Request, res: express.Response, next: express.NextFunction) {
        const locationService = LocationsServices.getInstance();
        const media = await locationService.getMediaByName(req.body.name);
        if (media) {
            res.status(400).send({error: `media name already exists`});
        } else {
            next();
        }
    }

    async validateMediaExists(req: express.Request, res: express.Response, next: express.NextFunction) {
        const locationService = LocationsServices.getInstance();
        const media = await locationService.readById(req.params.categoryId);
        if (media) {
            next();
        } else {
            res.status(404).send({error: `Media ${req.params.userId} not found`});
        }
    }

    async extractUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        req.body._id = req.params.userId;
        next();
    }
}
