import * as shortUUID from 'short-uuid';

export class GenericInMemoryDao {
    private static instance: GenericInMemoryDao;
    categories: any = [];

    constructor() {
    }

    static getInstance(): GenericInMemoryDao {
        if (!GenericInMemoryDao.instance) {
            GenericInMemoryDao.instance = new GenericInMemoryDao();
        }
        return GenericInMemoryDao.instance;
    }

    addCategory(category: any) {
        return new Promise((resolve) => {
            category.id = shortUUID.generate();
            this.categories.push(category)
            resolve(category.id);
        });
    }

    getCategory() {
        return new Promise((resolve) => {
            resolve(this.categories);
        });
    }

    getCategoryById(categoryId: string) {
        return new Promise((resolve) => {
            resolve(this.categories.find((category: { id: string; }) => category.id === categoryId));
        });
    }

    putCategoryById(category: any) {
        const objIndex = this.categories.findIndex((obj: { id: any; }) => obj.id === category.id);
        this.categories = [
            ...this.categories.slice(0, objIndex),
            category,
            ...this.categories.slice(objIndex + 1),
        ];
        return new Promise((resolve) => {
            resolve(`${category.id} updated via put`);
        });
    }

    patchCategoryById(category: any) {
        const objIndex = this.categories.findIndex((obj: { id: any; }) => obj.id === category.id);
        let currencategory = this.categories[objIndex];
        for (let i in category) {
            if (i !== 'id') {
                currencategory[i] = category[i];
            }
        }
        this.categories = [
            ...this.categories.slice(0, objIndex),
            currencategory,
            ...this.categories.slice(objIndex + 1),
        ];
        return new Promise((resolve) => {
            resolve(`${category.id} patched`);
        });
    }

    getByEmail(email: string) {
        //     return new Promise((resolve) => {
        //         const objIndex = this.users.findIndex((obj: { email: any; }) => obj.email === email);
        //         let currentUser = this.users[objIndex];
        //         if (currentUser) {
        //             resolve(currentUser);
        //         } else {
        //             resolve(null);
        //         }
        //     });
    }
}
