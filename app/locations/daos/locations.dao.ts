import {MongooseService} from '../../common/services/mongoose.service';
import * as shortUUID from "short-uuid";
import {LocationModel} from "../../json-schemas/models.schemas";

export class LocationsDao {
    mongooseService: MongooseService = MongooseService.getInstance();
    private static instance: LocationsDao;

    Schema = this.mongooseService.getMongoose().Schema;

    locationSchema = new this.Schema({
            _id: String,
            id: Number,
            name: String
        }, {timestamps: true}
    );

    Location = LocationModel;


    constructor() {
    }

    public static getInstance() {
        if (!this.instance) {
            this.instance = new LocationsDao();
        }
        return this.instance;
    }

    async addCategory(categoryFields: any) {
        categoryFields._id = shortUUID.generate();
        const category = new this.Location(categoryFields);
        await category.save();
        return categoryFields._id;
    }

    async getLocationByName(name: string) {
        return this.Location.find({name});
    }

    async removeLocationById(userId: string) {
        await this.Location.deleteOne({_id: userId});
    }

    async getLocationById(userId: string) {
        return this.Location.findOne({_id: userId});
    }
    async getLocationByOriginalId(id:any)  {
        return this.Location.findOne({id: +id});
    }
    async listLocations(limit: number = 25, page: number = 0) {
        return this.Location.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async patchLocation(CategoryFields: any) {
        // const category: CategoryDao = await this.Category.findById(CategoryFields._id);
        // if(category){
        //     for (const i in CategoryFields) {
        //         category[i] = CategoryFields[i];
        //     }
        //     return await category.save();
        // }
    }
}
