import {CRUD} from '../../common/interfaces/crud.interface';
import {GenericInMemoryDao} from '../daos/in.memory.dao';
import {LocationsDao} from '../daos/locations.dao';

export class LocationsServices implements CRUD {
    private static instance: LocationsServices;
    dao: GenericInMemoryDao;

    constructor() {
        this.dao = GenericInMemoryDao.getInstance();
    }

    static getInstance(): LocationsServices {
        if (!LocationsServices.instance) {
            LocationsServices.instance = new LocationsServices();
        }
        return LocationsServices.instance;
    }

    create(resource: any) {
        return LocationsDao.getInstance().addCategory(resource);
    }

    deleteById(resourceId: any) {
        return LocationsDao.getInstance().removeLocationById(resourceId);
    };

    list(limit: number, page: number) {
        return LocationsDao.getInstance().listLocations(limit, page);
    };

    patchById(resource: any) {
        return LocationsDao.getInstance().patchLocation(resource);
    };

    readById(resourceId: any) {
        return LocationsDao.getInstance().getLocationById(resourceId);
    };

    getLocationByOriginalId(resourceId: any) {
        return LocationsDao.getInstance().getLocationByOriginalId(resourceId);
    };

    getLocationByName(name: any) {
        return LocationsDao.getInstance().getLocationByName(name);
    };

    updateById(resource: any) {
        return LocationsDao.getInstance().patchLocation(resource);
    };

    async getMediaByName(email: string) {
        return LocationsDao.getInstance().getLocationByName(email);
    }
}
