import express from 'express';
import {LocationsServices} from '../services/locations.services';


export class LocationsController {
    protected locationService: LocationsServices;

    constructor() {
        this.locationService = LocationsServices.getInstance();
    }

    async listLocation(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        const users = await locationService.list(100, 0);
        res.status(200).send(users);
    }

    async getLocationById(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        const user = await locationService.readById(req.params.mediaId);
        res.status(200).send(user);
    }

    async getLocationByOriginalId(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        const user = await locationService.getLocationByOriginalId(req.params.locationId);
        res.status(200).send(user);
    }

    async getLocationByName(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        const user = await locationService.getLocationByName(req.params.name);
        res.status(200).send(user);
    }

    async createLocation(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        const ids = [];
        for (const cat of req.body) {
            if (!(cat && cat.id && cat.name)) {
                res.status(400).send({error: `Missing required fields name and type`});
            }
            const userId = await locationService.create(cat);
            ids.push({_id: userId});
        }
        req.body.permissionLevel = 1 + 2 + 4 + 8;
        res.status(201).send(ids);
    }

    async patch(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        await locationService.patchById(req.body);
        res.status(204).send(``);
    }

    async put(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        await locationService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeLocation(req: express.Request, res: express.Response) {
        const locationService = LocationsServices.getInstance();
        await locationService.deleteById(req.params.mediaId);
        res.status(204).send(``);
    }

}
