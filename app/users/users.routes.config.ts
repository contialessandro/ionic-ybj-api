import {UsersController} from './controllers/users.controller';
import {UsersMiddleware} from './middlewares/users.middleware';
import {CommonPermissionMiddleware} from '../common/middlewares/common.permission.middleware';
import {JwtMiddleware} from '../auth/middlewares/jwt.middleware';
import {Router} from 'express';
import {helperFunction} from "../common/helper";

const router: Router = Router();
const usersController = new UsersController();
const usersMiddleware = UsersMiddleware.getInstance();
const jwtMiddleware = JwtMiddleware.getInstance();
const commonPermissionMiddleware = new CommonPermissionMiddleware();
router.get('/users', [
    jwtMiddleware.validJWTNeeded,
    commonPermissionMiddleware.onlyAdminCanDoThisAction,
    usersController.listUsers
]);
router.post(`/users`, [
    usersMiddleware.validateRequiredCreateUserBodyFields,
    usersMiddleware.validateSameEmailDoesntExist,
    usersController.createUser
]);
router.get(`/users/email/:email`, [
    usersController.getUserByEmail
]);
router.post(`/users/validateRequest`,[
    usersController.validateRequest
])
router.post(`/users/signin`,[
    usersController.login
]);
helperFunction.GetRouterList();

export const UsersRouter: Router = router;
