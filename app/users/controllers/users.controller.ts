import express from 'express';
import { UsersService } from '../services/user.services';
import { helperFunction } from '../../common/helper';
import bcrypt from 'bcrypt';
import { GenerateJWT } from '../../config/Auth';

export class UsersController {
    constructor() {
    }

    async listUsers(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        const users=await usersService.list(100, 0);
        res.status(200).send(users);
    }

    async getUserById(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        const user=await usersService.readById(req.params.userId);
        res.status(200).send(user);
    }

    async getUserByEmail(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        const user=await usersService.getByEmail(req.params.email);
        res.status(200).send(user);
    }

    async createUser(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        req.body.permissionLevel=1 + 2 + 4 + 8;
        const userId=await usersService.create(req.body);
        res.status(201).send({_id: userId});
    }

    async patch(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        await usersService.patchById(req.body);
        res.status(204).send(``);
    }

    async put(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        await usersService.updateById(req.body);
        res.status(204).send(``);
    }

    async removeUser(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        await usersService.deleteById(req.params.userId);
        res.status(204).send(``);
    }

    async validateRequest(req: express.Request, res: express.Response) {

        const base64Authentication=req.body.param;
        if (!base64Authentication) {
            return res.status(400).send({error: 'the request is invalid'});
        }
        const auth=helperFunction.Decode(base64Authentication).split(':');
        const usersService=UsersService.getInstance();
        const userModel=await usersService.getByName(auth[0]);
        if (userModel.password === auth[1]) {
            return res.status(200).send({message: 'ok'});
        }
        return res.status(403).send({
            error: true,
            message: 'not authenticated',
            status: 403
        });
    }

    async login(req: express.Request, res: express.Response) {
        const usersService=UsersService.getInstance();
        const userEmail=req.body.email;
        const userPassword=req.body.password;
        const userModel=await usersService.getByEmail(userEmail);
        const validPassword=bcrypt.compare(userPassword, userModel.password).catch((e) => false);
        if (!userModel || !validPassword) return res.status(401).json({
            message: 'Authentication failed'
        });
        const signRequestTimestamp=new Date().getTime();
        const jwt=await GenerateJWT({
            loggedInAs: userModel.name,
            loggedInAsEmail: userModel.email,
            permission: userModel.permissionLevel,
            signRequestTimestamp
        });
        return res.status(200).send({token: jwt});
    }
}
