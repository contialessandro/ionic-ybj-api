import AWS from 'aws-sdk';
import express, {Router} from 'express'
import {DescribeInstancesResult} from "aws-sdk/clients/ec2";

AWS.config.update({region: 'eu-south-1'});

const router: Router = Router();

AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: 'default'});
const ec2 = new AWS.EC2();
const params = {
    InstanceIds: ['i-02644015a9cfca24d'],
};
router.get(`/ec2/stop`, async (req: express.Request, res: express.Response) => {
    try {
        await ec2.stopInstances(params).promise();
        res.json({message: 'ECS instance is stopping'});
    } catch (error) {
        console.error('Error stopping ECS instance:', error);
        res.status(500).json({error: 'Failed to stop ECS instance'});
    }
});
router.get('/ec2/start', async (req: express.Request, res: express.Response) => {
    try {
        await ec2.startInstances(params).promise();

        res.json({message: 'EC2 instance is starting'});
    } catch (error) {
        console.error('Error starting EC2 instance:', error);
        res.status(500).json({error: 'Failed to start EC2 instance'});
    }
});
router.get('/ec2/status', async (req: express.Request, res: express.Response) => {
    try {
        const data: DescribeInstancesResult = await ec2.describeInstances(params).promise();
        if (data?.Reservations && data.Reservations?.length > 0) {
            // @ts-ignore
            const instanceState = data?.Reservations[0]?.Instances[0]?.State.Name;
            // @ts-ignore
            const publicIpAddress = data?.Reservations[0]?.Instances[0]?.PublicIpAddress;
            res.json({status: instanceState, publicIpAddress});
        } else {
            res.status(404).json({error: 'EC2 instance not found'});
        }
    } catch (error) {
        console.error('Error checking EC2 instance status:', error);
        res.status(500).json({error, message: 'Failed to check EC2 instance status'});
    }
});
export const EC2Router: Router = router;
